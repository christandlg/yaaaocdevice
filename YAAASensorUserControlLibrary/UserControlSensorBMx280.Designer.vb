﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class UserControlSensorBMx280
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.LabelChipSelect = New System.Windows.Forms.Label()
        Me.ComboBoxOSRS_T = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.ComboBoxInterface = New System.Windows.Forms.ComboBox()
        Me.ComboBoxOSRS_P = New System.Windows.Forms.ComboBox()
        Me.ComboBoxOSRS_H = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.ComboBoxChipSelect = New System.Windows.Forms.ComboBox()
        Me.ComboBoxMode = New System.Windows.Forms.ComboBox()
        Me.ComboBoxFilter = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.ComboBoxStandbyTime = New System.Windows.Forms.ComboBox()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.SuspendLayout()
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(167, 33)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(106, 13)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Oversampling T/P/H"
        '
        'LabelChipSelect
        '
        Me.LabelChipSelect.AutoSize = True
        Me.LabelChipSelect.Location = New System.Drawing.Point(3, 33)
        Me.LabelChipSelect.Name = "LabelChipSelect"
        Me.LabelChipSelect.Size = New System.Drawing.Size(51, 13)
        Me.LabelChipSelect.TabIndex = 5
        Me.LabelChipSelect.Text = "I2C Addr."
        '
        'ComboBoxOSRS_T
        '
        Me.ComboBoxOSRS_T.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxOSRS_T.FormattingEnabled = True
        Me.ComboBoxOSRS_T.Location = New System.Drawing.Point(279, 30)
        Me.ComboBoxOSRS_T.Name = "ComboBoxOSRS_T"
        Me.ComboBoxOSRS_T.Size = New System.Drawing.Size(45, 21)
        Me.ComboBoxOSRS_T.TabIndex = 4
        Me.ToolTip1.SetToolTip(Me.ComboBoxOSRS_T, "Temperature oversampling setting. 'x00' disables temperature measurements. ")
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(3, 6)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(49, 13)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Interface"
        '
        'ComboBoxInterface
        '
        Me.ComboBoxInterface.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxInterface.FormattingEnabled = True
        Me.ComboBoxInterface.Location = New System.Drawing.Point(58, 3)
        Me.ComboBoxInterface.Name = "ComboBoxInterface"
        Me.ComboBoxInterface.Size = New System.Drawing.Size(54, 21)
        Me.ComboBoxInterface.TabIndex = 9
        Me.ToolTip1.SetToolTip(Me.ComboBoxInterface, "Interface used to connect the BMx280 sensor to the Arduino.")
        '
        'ComboBoxOSRS_P
        '
        Me.ComboBoxOSRS_P.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxOSRS_P.FormattingEnabled = True
        Me.ComboBoxOSRS_P.Location = New System.Drawing.Point(348, 30)
        Me.ComboBoxOSRS_P.Name = "ComboBoxOSRS_P"
        Me.ComboBoxOSRS_P.Size = New System.Drawing.Size(45, 21)
        Me.ComboBoxOSRS_P.TabIndex = 10
        Me.ToolTip1.SetToolTip(Me.ComboBoxOSRS_P, "Pressure oversampling setting. 'x00' disables pressure measurements. ")
        '
        'ComboBoxOSRS_H
        '
        Me.ComboBoxOSRS_H.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxOSRS_H.FormattingEnabled = True
        Me.ComboBoxOSRS_H.Location = New System.Drawing.Point(417, 30)
        Me.ComboBoxOSRS_H.Name = "ComboBoxOSRS_H"
        Me.ComboBoxOSRS_H.Size = New System.Drawing.Size(45, 21)
        Me.ComboBoxOSRS_H.TabIndex = 11
        Me.ToolTip1.SetToolTip(Me.ComboBoxOSRS_H, "Humidity oversampling setting. 'x00' disables humidity measurements. ")
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(399, 33)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(12, 13)
        Me.Label1.TabIndex = 12
        Me.Label1.Text = "/"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(330, 33)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(12, 13)
        Me.Label4.TabIndex = 13
        Me.Label4.Text = "/"
        '
        'ComboBoxChipSelect
        '
        Me.ComboBoxChipSelect.FormattingEnabled = True
        Me.ComboBoxChipSelect.Location = New System.Drawing.Point(58, 30)
        Me.ComboBoxChipSelect.Name = "ComboBoxChipSelect"
        Me.ComboBoxChipSelect.Size = New System.Drawing.Size(54, 21)
        Me.ComboBoxChipSelect.TabIndex = 14
        Me.ToolTip1.SetToolTip(Me.ComboBoxChipSelect, "Interface = I2C:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "The I2C address of the BMx280 sensor (0x76 or 0x77)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Interface " &
        "= SPI:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "The chip select pin connected to the BMx280.")
        '
        'ComboBoxMode
        '
        Me.ComboBoxMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxMode.FormattingEnabled = True
        Me.ComboBoxMode.Location = New System.Drawing.Point(118, 3)
        Me.ComboBoxMode.Name = "ComboBoxMode"
        Me.ComboBoxMode.Size = New System.Drawing.Size(97, 21)
        Me.ComboBoxMode.TabIndex = 16
        Me.ToolTip1.SetToolTip(Me.ComboBoxMode, "Measurement mode:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "FORCED: measurements are performed on demand. " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "NORMAL: measur" &
        "ements are performed automatically, with a configurable standby time after each " &
        "measurement (recommended).")
        '
        'ComboBoxFilter
        '
        Me.ComboBoxFilter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxFilter.FormattingEnabled = True
        Me.ComboBoxFilter.Location = New System.Drawing.Point(365, 3)
        Me.ComboBoxFilter.Name = "ComboBoxFilter"
        Me.ComboBoxFilter.Size = New System.Drawing.Size(97, 21)
        Me.ComboBoxFilter.TabIndex = 17
        Me.ToolTip1.SetToolTip(Me.ComboBoxFilter, "Setting for the BMx280's integrated IIR filter. Higher filter coefficients improv" &
        "e resistance against short term changes, but increase reaction time. Should not " &
        "be used in forced mode. ")
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(221, 6)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(72, 13)
        Me.Label5.TabIndex = 19
        Me.Label5.Text = "Standby Time"
        '
        'ComboBoxStandbyTime
        '
        Me.ComboBoxStandbyTime.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxStandbyTime.FormattingEnabled = True
        Me.ComboBoxStandbyTime.Location = New System.Drawing.Point(299, 3)
        Me.ComboBoxStandbyTime.Name = "ComboBoxStandbyTime"
        Me.ComboBoxStandbyTime.Size = New System.Drawing.Size(60, 21)
        Me.ComboBoxStandbyTime.TabIndex = 18
        Me.ToolTip1.SetToolTip(Me.ComboBoxStandbyTime, "Standby time between measurements in normal mode. Has no influence if forced mode" &
        " is enabled. ")
        '
        'UserControlSensorBMx280
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.ComboBoxStandbyTime)
        Me.Controls.Add(Me.ComboBoxFilter)
        Me.Controls.Add(Me.ComboBoxMode)
        Me.Controls.Add(Me.ComboBoxChipSelect)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ComboBoxOSRS_H)
        Me.Controls.Add(Me.ComboBoxOSRS_P)
        Me.Controls.Add(Me.ComboBoxInterface)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.LabelChipSelect)
        Me.Controls.Add(Me.ComboBoxOSRS_T)
        Me.Name = "UserControlSensorBMx280"
        Me.Size = New System.Drawing.Size(465, 60)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label2 As Label
    Friend WithEvents LabelChipSelect As Label
    Friend WithEvents ComboBoxOSRS_T As ComboBox
    Friend WithEvents Label3 As Label
    Friend WithEvents ComboBoxInterface As ComboBox
    Friend WithEvents ComboBoxOSRS_P As ComboBox
    Friend WithEvents ComboBoxOSRS_H As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents ComboBoxChipSelect As ComboBox
    Friend WithEvents ComboBoxMode As ComboBox
    Friend WithEvents ComboBoxFilter As ComboBox
    Friend WithEvents Label5 As Label
    Friend WithEvents ComboBoxStandbyTime As ComboBox
    Friend WithEvents ToolTip1 As ToolTip
End Class
