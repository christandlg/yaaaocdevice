﻿'Imports ASCOM.YAAADevice

Public Class UserControlSensorBrightnessBH1750
    Implements IUserControlSensor

    Enum i2c_address_t
        Ox5C = &H5C        'ADDR pin = HIGH
        Ox23 = &H23        'ADDR pin = LOW
    End Enum

    Private sensor_ As YAAASharedClassLibrary.YAAASensor

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
    End Sub

    Public Sub setSensor(sensor As YAAASharedClassLibrary.YAAASensor) Implements IUserControlSensor.setSensor
        sensor_ = sensor.Clone()

        ComboBoxI2CAddress.Items.AddRange([Enum].GetNames(GetType(UserControlSensorBrightnessBH1750.i2c_address_t)))

        If Not [Enum].IsDefined(GetType(UserControlSensorBMx280.i2c_address_t), CInt(sensor_.sensor_settings_.sensor_parameters_(1))) Then
            ComboBoxI2CAddress.SelectedItem = [Enum].Parse(GetType(UserControlSensorBrightnessBH1750.i2c_address_t), CInt(UserControlSensorBrightnessBH1750.i2c_address_t.Ox5C)).ToString
        Else
            ComboBoxI2CAddress.SelectedItem = [Enum].Parse(GetType(UserControlSensorBrightnessBH1750.i2c_address_t), CInt(sensor_.sensor_settings_.sensor_parameters_(0))).ToString
        End If

        Dim owtr As Byte() = {&HFF, &HFF}

        Array.Copy(sensor_.sensor_settings_.sensor_parameters_, 2, owtr, 0, 2)

        'reverse byte order if we are on a little-endian system (Arduino is big-endian)
        If BitConverter.IsLittleEndian Then
            Array.Reverse(owtr)
        End If

        TextBoxOWTR.Text = (CDbl(BitConverter.ToUInt16(owtr, 0)) / 65535.0).ToString("N2")
    End Sub

    Public Function getSensor(ByRef sensor As YAAASharedClassLibrary.YAAASensor) As Boolean Implements IUserControlSensor.getSensor
        Dim valid As Boolean = True
        Dim owtr As Double = 0.0

        sensor_.sensor_settings_.sensor_parameters_(0) = CByte([Enum].Parse(GetType(UserControlSensorBrightnessBH1750.i2c_address_t), ComboBoxI2CAddress.SelectedItem))

        valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxOWTR, owtr)

        'check if the given optical window transmission rate is a valid value.
        If owtr < 0.0 Or owtr > 1.0 Then
            owtr = 1.0
            valid = False
            TextBoxOWTR.BackColor = Drawing.Color.Orange
        Else
            TextBoxOWTR.BackColor = SystemColors.Window
        End If

        Dim buffer As Byte() = BitConverter.GetBytes(CUShort(owtr * 65535.0))

        'reverse byte order if we are on a little-endian system (Arduino is big-endian)
        If BitConverter.IsLittleEndian Then
            Array.Reverse(buffer)
        End If

        Array.Copy(buffer, 0, sensor_.sensor_settings_.sensor_parameters_, 2, 2)

        sensor = sensor_.Clone()

        Return valid
    End Function
End Class
