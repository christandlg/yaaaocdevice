﻿'Imports ASCOM.YAAADevice

Public Class UserControlSensorTemperatureAnalogIC
    Implements IUserControlSensor

    Private sensor_ As YAAASharedClassLibrary.YAAASensor

    Public Sub New()
        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Public Sub setSensor(sensor As YAAASharedClassLibrary.YAAASensor) Implements IUserControlSensor.setSensor
        sensor_ = sensor.Clone()

        Dim buffer As Byte() = {&HFF, &HFF}

        With sensor_.sensor_settings_
            TextBoxInputPin.Text = YAAASharedClassLibrary.printPin(.sensor_parameters_(0))

            'copy 0° voltage to buffer 
            Array.Copy(.sensor_parameters_, 2, buffer, 0, 2)
            'Arduino (and data stored in the ASCOM profile store) is big endian
            If BitConverter.IsLittleEndian Then
                Array.Reverse(buffer)
            End If
            TextBox0Voltage.Text = CShort(BitConverter.ToInt16(buffer, 0)) / 1000

            'copy Arduino reference voltage to buffer
            Array.Copy(.sensor_parameters_, 6, buffer, 0, 2)
            'Arduino (and data stored in the ASCOM profile store) is big endian
            If BitConverter.IsLittleEndian Then
                Array.Reverse(buffer)
            End If
            TextBoxRefVoltage.Text = CShort(BitConverter.ToInt16(buffer, 0)) / 1000

            'copy temperature coefficient to buffer
            Array.Copy(.sensor_parameters_, 8, buffer, 0, 2)
            'Arduino (and data stored in the ASCOM profile store) is big endian
            If BitConverter.IsLittleEndian Then
                Array.Reverse(buffer)
            End If
            TextBoxCoefficient.Text = BitConverter.ToInt16(buffer, 0)
        End With
    End Sub

    Public Function getSensor(ByRef sensor As YAAASharedClassLibrary.YAAASensor) As Boolean Implements IUserControlSensor.getSensor
        Dim valid As Boolean = True
        Dim buffer As Byte() = {&HFF, &HFF}

        With sensor_.sensor_settings_

            If YAAASharedClassLibrary.tryParsePin(TextBoxInputPin.Text, .sensor_parameters_(0)) Then
                TextBox0Voltage.BackColor = SystemColors.Window
            Else
                valid = False
                TextBox0Voltage.BackColor = Drawing.Color.Orange
            End If

            Try
                buffer = BitConverter.GetBytes(Convert.ToInt16(CDbl(TextBox0Voltage.Text) * 1000))
                TextBox0Voltage.BackColor = SystemColors.Window
            Catch ex As Exception
                valid = False
                TextBox0Voltage.BackColor = Drawing.Color.Orange
            End Try
            'Arduino (and data stored in the ASCOM profile store) is big endian
            If BitConverter.IsLittleEndian Then
                Array.Reverse(buffer)
            End If
            Array.Copy(buffer, 0, .sensor_parameters_, 2, 2)


            buffer = {&HFF, &HFF}
            Try
                buffer = BitConverter.GetBytes(Convert.ToInt16(CDbl(TextBoxRefVoltage.Text) * 1000))
                TextBoxRefVoltage.BackColor = SystemColors.Window
            Catch ex As Exception
                valid = False
                TextBoxRefVoltage.BackColor = Drawing.Color.Orange
            End Try
            'Arduino (and data stored in the ASCOM profile store) is big endian
            If BitConverter.IsLittleEndian Then
                Array.Reverse(buffer)
            End If
            Array.Copy(buffer, 0, .sensor_parameters_, 6, 2)


            buffer = {&HFF, &HFF}
            Try
                buffer = BitConverter.GetBytes(Convert.ToInt16(TextBoxCoefficient.Text))
                TextBoxCoefficient.BackColor = SystemColors.Window
            Catch ex As Exception
                valid = False
                TextBoxCoefficient.BackColor = Drawing.Color.Orange
            End Try
            'Arduino (and data stored in the ASCOM profile store) is big endian
            If BitConverter.IsLittleEndian Then
                Array.Reverse(buffer)
            End If
            Array.Copy(buffer, 0, .sensor_parameters_, 8, 2)
        End With

        sensor = sensor_.Clone()

        Return valid
    End Function
End Class
