﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UserControlSensorLightningAS3935
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(UserControlSensorLightningAS3935))
        Me.ComboBoxInterface = New System.Windows.Forms.ComboBox()
        Me.ComboBoxAFE = New System.Windows.Forms.ComboBox()
        Me.LabelChipSelect = New System.Windows.Forms.Label()
        Me.ComboBoxMNL = New System.Windows.Forms.ComboBox()
        Me.CheckBoxMaskDist = New System.Windows.Forms.CheckBox()
        Me.ComboBoxChipSelect = New System.Windows.Forms.ComboBox()
        Me.TextBoxIRQ = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.ComboBoxNFLEV = New System.Windows.Forms.ComboBox()
        Me.ComboBoxSREJ = New System.Windows.Forms.ComboBox()
        Me.ComboBoxWDTH = New System.Windows.Forms.ComboBox()
        Me.TextBoxDetectionLifetime = New System.Windows.Forms.TextBox()
        Me.TextBoxAutoAdjustInterval = New System.Windows.Forms.TextBox()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.SuspendLayout()
        '
        'ComboBoxInterface
        '
        Me.ComboBoxInterface.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxInterface.FormattingEnabled = True
        Me.ComboBoxInterface.Location = New System.Drawing.Point(3, 3)
        Me.ComboBoxInterface.Name = "ComboBoxInterface"
        Me.ComboBoxInterface.Size = New System.Drawing.Size(105, 21)
        Me.ComboBoxInterface.TabIndex = 0
        Me.ToolTip1.SetToolTip(Me.ComboBoxInterface, "Interface used to connect the sensor. ")
        '
        'ComboBoxAFE
        '
        Me.ComboBoxAFE.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxAFE.FormattingEnabled = True
        Me.ComboBoxAFE.Location = New System.Drawing.Point(3, 30)
        Me.ComboBoxAFE.Name = "ComboBoxAFE"
        Me.ComboBoxAFE.Size = New System.Drawing.Size(105, 21)
        Me.ComboBoxAFE.TabIndex = 5
        Me.ToolTip1.SetToolTip(Me.ComboBoxAFE, "Analog Frontend setting. Set to either 'INDOOR' or 'OUTDOOR' depending on sensor " &
        "location. Incorrect setting of the Analog Frontend will cause decreased performa" &
        "nce. ")
        '
        'LabelChipSelect
        '
        Me.LabelChipSelect.AutoSize = True
        Me.LabelChipSelect.Location = New System.Drawing.Point(135, 6)
        Me.LabelChipSelect.Name = "LabelChipSelect"
        Me.LabelChipSelect.Size = New System.Drawing.Size(39, 13)
        Me.LabelChipSelect.TabIndex = 5
        Me.LabelChipSelect.Text = "CS Pin"
        Me.LabelChipSelect.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ComboBoxMNL
        '
        Me.ComboBoxMNL.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxMNL.FormattingEnabled = True
        Me.ComboBoxMNL.Location = New System.Drawing.Point(312, 30)
        Me.ComboBoxMNL.Name = "ComboBoxMNL"
        Me.ComboBoxMNL.Size = New System.Drawing.Size(60, 21)
        Me.ComboBoxMNL.TabIndex = 9
        Me.ToolTip1.SetToolTip(Me.ComboBoxMNL, "Minimum Number of Lightnings to be registered within a 15 minute window before th" &
        "e sensor reports lightning events. Higher numbers increase resistance against di" &
        "sturbers. ")
        '
        'CheckBoxMaskDist
        '
        Me.CheckBoxMaskDist.AutoSize = True
        Me.CheckBoxMaskDist.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxMaskDist.Location = New System.Drawing.Point(422, 5)
        Me.CheckBoxMaskDist.Name = "CheckBoxMaskDist"
        Me.CheckBoxMaskDist.Size = New System.Drawing.Size(43, 17)
        Me.CheckBoxMaskDist.TabIndex = 4
        Me.CheckBoxMaskDist.Text = "MD"
        Me.ToolTip1.SetToolTip(Me.CheckBoxMaskDist, "Mask Disturbers. If enabled, disturbers will not be indicated by the sensors and " &
        "thus will not appear in logs and cannot be used for automatic sensitivity adjust" &
        "ments. ")
        Me.CheckBoxMaskDist.UseVisualStyleBackColor = True
        '
        'ComboBoxChipSelect
        '
        Me.ComboBoxChipSelect.FormattingEnabled = True
        Me.ComboBoxChipSelect.Location = New System.Drawing.Point(180, 3)
        Me.ComboBoxChipSelect.Name = "ComboBoxChipSelect"
        Me.ComboBoxChipSelect.Size = New System.Drawing.Size(60, 21)
        Me.ComboBoxChipSelect.TabIndex = 1
        Me.ToolTip1.SetToolTip(Me.ComboBoxChipSelect, resources.GetString("ComboBoxChipSelect.ToolTip"))
        '
        'TextBoxIRQ
        '
        Me.TextBoxIRQ.Location = New System.Drawing.Point(312, 3)
        Me.TextBoxIRQ.Name = "TextBoxIRQ"
        Me.TextBoxIRQ.Size = New System.Drawing.Size(60, 20)
        Me.TextBoxIRQ.TabIndex = 2
        Me.ToolTip1.SetToolTip(Me.TextBoxIRQ, "Input pin the sensors IRQ pin is connected to. Must be a pin that supports extern" &
        "al interrupts. ")
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(276, 8)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(26, 13)
        Me.Label7.TabIndex = 18
        Me.Label7.Text = "IRQ"
        '
        'ComboBoxNFLEV
        '
        Me.ComboBoxNFLEV.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxNFLEV.FormattingEnabled = True
        Me.ComboBoxNFLEV.Location = New System.Drawing.Point(114, 30)
        Me.ComboBoxNFLEV.Name = "ComboBoxNFLEV"
        Me.ComboBoxNFLEV.Size = New System.Drawing.Size(60, 21)
        Me.ComboBoxNFLEV.TabIndex = 6
        Me.ToolTip1.SetToolTip(Me.ComboBoxNFLEV, "Noise Floor Level Threshold. The sensor will report a noise floor event if the no" &
        "ise floor level exceeds the set threshold.")
        '
        'ComboBoxSREJ
        '
        Me.ComboBoxSREJ.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxSREJ.FormattingEnabled = True
        Me.ComboBoxSREJ.Location = New System.Drawing.Point(180, 30)
        Me.ComboBoxSREJ.Name = "ComboBoxSREJ"
        Me.ComboBoxSREJ.Size = New System.Drawing.Size(60, 21)
        Me.ComboBoxSREJ.TabIndex = 7
        Me.ToolTip1.SetToolTip(Me.ComboBoxSREJ, "Spike Rejection level. Higher values increase resistance against disturbers but c" &
        "ause decreased lightning detection efficiency. ")
        '
        'ComboBoxWDTH
        '
        Me.ComboBoxWDTH.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxWDTH.FormattingEnabled = True
        Me.ComboBoxWDTH.Location = New System.Drawing.Point(246, 30)
        Me.ComboBoxWDTH.Name = "ComboBoxWDTH"
        Me.ComboBoxWDTH.Size = New System.Drawing.Size(60, 21)
        Me.ComboBoxWDTH.TabIndex = 8
        Me.ToolTip1.SetToolTip(Me.ComboBoxWDTH, "Watchdog Threshold level. Higher values increase resistance against disturbers bu" &
        "t cause decreased lightning detection efficiency. ")
        '
        'TextBoxDetectionLifetime
        '
        Me.TextBoxDetectionLifetime.Location = New System.Drawing.Point(378, 3)
        Me.TextBoxDetectionLifetime.Name = "TextBoxDetectionLifetime"
        Me.TextBoxDetectionLifetime.Size = New System.Drawing.Size(40, 20)
        Me.TextBoxDetectionLifetime.TabIndex = 3
        Me.ToolTip1.SetToolTip(Me.TextBoxDetectionLifetime, resources.GetString("TextBoxDetectionLifetime.ToolTip"))
        '
        'TextBoxAutoAdjustInterval
        '
        Me.TextBoxAutoAdjustInterval.Location = New System.Drawing.Point(378, 30)
        Me.TextBoxAutoAdjustInterval.Name = "TextBoxAutoAdjustInterval"
        Me.TextBoxAutoAdjustInterval.Size = New System.Drawing.Size(40, 20)
        Me.TextBoxAutoAdjustInterval.TabIndex = 10
        Me.ToolTip1.SetToolTip(Me.TextBoxAutoAdjustInterval, resources.GetString("TextBoxAutoAdjustInterval.ToolTip"))
        '
        'ToolTip1
        '
        Me.ToolTip1.AutomaticDelay = 0
        Me.ToolTip1.AutoPopDelay = 50000
        Me.ToolTip1.InitialDelay = 500
        Me.ToolTip1.ReshowDelay = 100
        '
        'UserControlSensorLightningAS3935
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.TextBoxAutoAdjustInterval)
        Me.Controls.Add(Me.TextBoxDetectionLifetime)
        Me.Controls.Add(Me.ComboBoxWDTH)
        Me.Controls.Add(Me.ComboBoxSREJ)
        Me.Controls.Add(Me.ComboBoxNFLEV)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.TextBoxIRQ)
        Me.Controls.Add(Me.ComboBoxChipSelect)
        Me.Controls.Add(Me.CheckBoxMaskDist)
        Me.Controls.Add(Me.ComboBoxMNL)
        Me.Controls.Add(Me.LabelChipSelect)
        Me.Controls.Add(Me.ComboBoxAFE)
        Me.Controls.Add(Me.ComboBoxInterface)
        Me.Name = "UserControlSensorLightningAS3935"
        Me.Size = New System.Drawing.Size(465, 60)
        Me.ToolTip1.SetToolTip(Me, "Spike Rejection level. Higher values increase resistance against disturbers but c" &
        "ause decreased lightning detection efficiency. ")
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ComboBoxInterface As ComboBox
    Friend WithEvents ComboBoxAFE As ComboBox
    Friend WithEvents LabelChipSelect As Label
    Friend WithEvents ComboBoxMNL As ComboBox
    Friend WithEvents CheckBoxMaskDist As CheckBox
    Friend WithEvents ComboBoxChipSelect As ComboBox
    Friend WithEvents TextBoxIRQ As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents ComboBoxNFLEV As ComboBox
    Friend WithEvents ComboBoxSREJ As ComboBox
    Friend WithEvents ComboBoxWDTH As ComboBox
    Friend WithEvents TextBoxDetectionLifetime As TextBox
    Friend WithEvents TextBoxAutoAdjustInterval As TextBox
    Friend WithEvents ToolTip1 As ToolTip
End Class
