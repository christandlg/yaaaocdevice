﻿Imports YAAASensorUserControlLibrary
Imports YAAASharedClassLibrary

Public Class UserControlSensorLightningAS3935
    Implements IUserControlSensor

    Enum afe_t
        AFE_INDOORS = &H12
        AFE_OUTDOORS = &HE
    End Enum

    Enum i2c_address_t
        A01 = 1
        A10 = 2
        A11 = 3
    End Enum

    Enum interface_t
        I2C = 0
        SPI = 1
    End Enum

    Enum min_num_ligh_t
        MNL_1 = 0
        MNL_5 = 1
        MNL_9 = 2
        MNL_16 = 3
    End Enum

    Enum nf_lev_t
        NF_LEV_0 = 0
        NF_LEV_1 = 1
        NF_LEV_2 = 2
        NF_LEV_3 = 3
        NF_LEV_4 = 4
        NF_LEV_5 = 5
        NF_LEV_6 = 6
        NF_LEV_7 = 7
    End Enum

    Enum srej_t
        SREJ_0 = 0
        SREJ_1 = 1
        SREJ_2 = 2
        SREJ_3 = 3
        SREJ_4 = 4
        SREJ_5 = 5
        SREJ_6 = 6
        SREJ_7 = 7
        SREJ_8 = 8
        SREJ_9 = 9
        SREJ_10 = 10
    End Enum

    Enum wdth_t
        WDTH_0 = 0
        WDTH_1 = 1
        WDTH_2 = 2
        WDTH_3 = 3
        WDTH_4 = 4
        WDTH_5 = 5
        WDTH_6 = 6
        WDTH_7 = 7
        WDTH_8 = 8
        WDTH_9 = 9
        WDTH_10 = 10
    End Enum

    Private sensor_ As YAAASharedClassLibrary.YAAASensor

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        ComboBoxAFE.Items.AddRange([Enum].GetNames(GetType(UserControlSensorLightningAS3935.afe_t)))

        ComboBoxInterface.Items.AddRange([Enum].GetNames(GetType(UserControlSensorLightningAS3935.interface_t)))

        ComboBoxMNL.Items.AddRange([Enum].GetNames(GetType(UserControlSensorLightningAS3935.min_num_ligh_t)))

        ComboBoxNFLEV.Items.AddRange([Enum].GetNames(GetType(UserControlSensorLightningAS3935.nf_lev_t)))

        ComboBoxSREJ.Items.AddRange([Enum].GetNames(GetType(UserControlSensorLightningAS3935.srej_t)))

        ComboBoxWDTH.Items.AddRange([Enum].GetNames(GetType(UserControlSensorLightningAS3935.wdth_t)))

        ToolTip1.AutomaticDelay = 0
        ToolTip1.AutoPopDelay = 60000
        ToolTip1.InitialDelay = 500
        ToolTip1.ReshowDelay = 100
    End Sub

    Public Sub setSensor(sensor As YAAASensor) Implements IUserControlSensor.setSensor
        sensor_ = sensor.Clone()

        With sensor_.sensor_settings_
            'set combo boxes
            If Not [Enum].IsDefined(GetType(UserControlSensorLightningAS3935.afe_t), CInt(.sensor_parameters_(3) And &H1F)) Then
                ComboBoxAFE.SelectedItem = [Enum].Parse(GetType(UserControlSensorLightningAS3935.afe_t), CInt(UserControlSensorLightningAS3935.afe_t.AFE_INDOORS)).ToString
            Else
                ComboBoxAFE.SelectedItem = [Enum].Parse(GetType(UserControlSensorLightningAS3935.afe_t), CInt(.sensor_parameters_(3) And &H1F)).ToString
            End If

            If Not [Enum].IsDefined(GetType(UserControlSensorLightningAS3935.interface_t), CInt(.sensor_parameters_(0))) Then
                ComboBoxInterface.SelectedItem = [Enum].Parse(GetType(UserControlSensorLightningAS3935.interface_t), CInt(UserControlSensorLightningAS3935.interface_t.I2C)).ToString
            Else
                ComboBoxInterface.SelectedItem = [Enum].Parse(GetType(UserControlSensorLightningAS3935.interface_t), CInt(.sensor_parameters_(0))).ToString
            End If

            If Not [Enum].IsDefined(GetType(UserControlSensorLightningAS3935.min_num_ligh_t), CInt(.sensor_parameters_(5) And &H30) >> 4) Then
                ComboBoxMNL.SelectedItem = [Enum].Parse(GetType(UserControlSensorLightningAS3935.min_num_ligh_t), CInt(UserControlSensorLightningAS3935.min_num_ligh_t.MNL_1)).ToString
            Else
                ComboBoxMNL.SelectedItem = [Enum].Parse(GetType(UserControlSensorLightningAS3935.min_num_ligh_t), CInt(.sensor_parameters_(5) And &H30) >> 4).ToString
            End If

            If Not [Enum].IsDefined(GetType(UserControlSensorLightningAS3935.nf_lev_t), CInt((.sensor_parameters_(3) And &HE0) >> 5)) Then
                ComboBoxNFLEV.SelectedItem = [Enum].Parse(GetType(UserControlSensorLightningAS3935.nf_lev_t), CInt(UserControlSensorLightningAS3935.nf_lev_t.NF_LEV_2)).ToString
            Else
                ComboBoxNFLEV.SelectedItem = [Enum].Parse(GetType(UserControlSensorLightningAS3935.nf_lev_t), CInt(.sensor_parameters_(3) And &HE0) >> 5).ToString
            End If

            If Not [Enum].IsDefined(GetType(UserControlSensorLightningAS3935.srej_t), CInt(.sensor_parameters_(5) And &HF)) Then
                ComboBoxSREJ.SelectedItem = [Enum].Parse(GetType(UserControlSensorLightningAS3935.srej_t), CInt(UserControlSensorLightningAS3935.srej_t.SREJ_2)).ToString
            Else
                ComboBoxSREJ.SelectedItem = [Enum].Parse(GetType(UserControlSensorLightningAS3935.srej_t), CInt(.sensor_parameters_(5) And &HF)).ToString
            End If

            If Not [Enum].IsDefined(GetType(UserControlSensorLightningAS3935.wdth_t), CInt(.sensor_parameters_(4) And &HF)) Then
                ComboBoxWDTH.SelectedItem = [Enum].Parse(GetType(UserControlSensorLightningAS3935.wdth_t), CInt(UserControlSensorLightningAS3935.wdth_t.WDTH_2)).ToString
            Else
                ComboBoxWDTH.SelectedItem = [Enum].Parse(GetType(UserControlSensorLightningAS3935.wdth_t), CInt(.sensor_parameters_(4) And &HF)).ToString
            End If

            'chip select value is changed by function ComboBoxInterface_SelectedIndexChanged 

            'set text fields
            TextBoxIRQ.Text = .sensor_parameters_(2).ToString

            'copy detection lifetime and auto_adjust_interval
            Dim buffer As Byte() = {&HFF, &HFF}

            'count period
            Array.Copy(.sensor_parameters_, 6, buffer, 0, 2)

            'Arduino is big endian
            If BitConverter.IsLittleEndian Then
                Array.Reverse(buffer)
            End If

            TextBoxDetectionLifetime.Text = BitConverter.ToUInt16(buffer, 0)

            'count period
            Array.Copy(.sensor_parameters_, 8, buffer, 0, 2)

            'Arduino is big endian
            If BitConverter.IsLittleEndian Then
                Array.Reverse(buffer)
            End If

            TextBoxAutoAdjustInterval.Text = BitConverter.ToUInt16(buffer, 0)

            CheckBoxMaskDist.Checked = CBool(.sensor_parameters_(5) And &H40)
        End With
    End Sub

    Public Function getSensor(ByRef sensor As YAAASensor) As Boolean Implements IUserControlSensor.getSensor
        Dim valid As Boolean = True

        With sensor.sensor_settings_
            .sensor_parameters_(0) = [Enum].Parse(GetType(UserControlSensorLightningAS3935.interface_t), ComboBoxInterface.SelectedItem.ToString)

            If [Enum].Parse(GetType(UserControlSensorLightningAS3935.interface_t), ComboBoxInterface.SelectedItem.ToString) = UserControlSensorLightningAS3935.interface_t.I2C Then
                .sensor_parameters_(1) = CByte([Enum].Parse(GetType(UserControlSensorLightningAS3935.i2c_address_t), ComboBoxChipSelect.SelectedItem))
            Else
                .sensor_parameters_(1) = CByte(ComboBoxChipSelect.Text)
            End If

            Dim pin_irq As Byte = &HFF
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxIRQ, pin_irq)
            .sensor_parameters_(2) = pin_irq

            Dim buffer As Byte = &H0
            buffer = buffer Or CByte([Enum].Parse(GetType(UserControlSensorLightningAS3935.afe_t), ComboBoxAFE.SelectedItem.ToString))
            buffer = buffer Or (CByte([Enum].Parse(GetType(UserControlSensorLightningAS3935.nf_lev_t), ComboBoxNFLEV.SelectedItem.ToString)) << 5)
            .sensor_parameters_(3) = buffer

            buffer = &H0
            buffer = buffer Or CByte([Enum].Parse(GetType(UserControlSensorLightningAS3935.wdth_t), ComboBoxWDTH.SelectedItem.ToString))
            'buffer = buffer Or 'TunCapValue << 4
            .sensor_parameters_(4) = buffer

            buffer = &H0
            buffer = buffer Or CByte([Enum].Parse(GetType(UserControlSensorLightningAS3935.srej_t), ComboBoxSREJ.SelectedItem.ToString))
            buffer = buffer Or (CByte([Enum].Parse(GetType(UserControlSensorLightningAS3935.min_num_ligh_t), ComboBoxMNL.SelectedItem.ToString)) << 4)
            buffer = buffer Or ((CByte(CheckBoxMaskDist.Checked) And &H1) << 6)
            .sensor_parameters_(5) = buffer
        End With

        Dim time As UInt16 = 0
        Dim buffer_arr As Byte() = {&HFF, &HFF}

        'read detection lifetime
        valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxDetectionLifetime, time)

        'convert count period from seconds to ms
        buffer_arr = BitConverter.GetBytes(time)

        'Arduino is big endian
        If BitConverter.IsLittleEndian Then
            Array.Reverse(buffer_arr)
        End If

        Array.Copy(buffer_arr, 0, sensor.sensor_settings_.sensor_parameters_, 6, 2)

        'read auto adjust interval
        valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxAutoAdjustInterval, time)

        'convert count period from seconds to ms
        buffer_arr = BitConverter.GetBytes(time)

        'Arduino is big endian
        If BitConverter.IsLittleEndian Then
            Array.Reverse(buffer_arr)
        End If

        Array.Copy(buffer_arr, 0, sensor.sensor_settings_.sensor_parameters_, 8, 2)

        Return valid
    End Function

    Private Sub ComboBoxInterface_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBoxInterface.SelectedIndexChanged
        Select Case ComboBoxInterface.SelectedIndex
            Case UserControlSensorLightningAS3935.interface_t.I2C
                LabelChipSelect.Text = "I2C Addr."
                ComboBoxChipSelect.Items.Clear()
                ComboBoxChipSelect.Items.AddRange([Enum].GetNames(GetType(UserControlSensorLightningAS3935.i2c_address_t)))
                ComboBoxChipSelect.DropDownStyle = ComboBoxStyle.DropDownList

                If Not [Enum].IsDefined(GetType(UserControlSensorLightningAS3935.i2c_address_t), CInt(sensor_.sensor_settings_.sensor_parameters_(1))) Then
                    ComboBoxChipSelect.SelectedItem = [Enum].Parse(GetType(UserControlSensorLightningAS3935.i2c_address_t), CInt(UserControlSensorLightningAS3935.i2c_address_t.A01)).ToString
                Else
                    ComboBoxChipSelect.SelectedItem = [Enum].Parse(GetType(UserControlSensorLightningAS3935.i2c_address_t), CInt(sensor_.sensor_settings_.sensor_parameters_(1))).ToString
                End If

            Case UserControlSensorLightningAS3935.interface_t.SPI
                LabelChipSelect.Text = "CS Pin"
                ComboBoxChipSelect.Items.Clear()
                ComboBoxChipSelect.DropDownStyle = ComboBoxStyle.DropDown

                ComboBoxChipSelect.SelectedText = sensor_.sensor_settings_.sensor_parameters_(1).ToString
        End Select
    End Sub
End Class
