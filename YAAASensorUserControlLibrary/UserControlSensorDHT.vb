﻿'Imports ASCOM.YAAADevice

Public Class UserControlSensorDHT
    Implements IUserControlSensor

    Enum dht_type_t
        DHT11 = 11
        DHT22 = 22
    End Enum

    Private sensor_ As YAAASharedClassLibrary.YAAASensor

    Public Sub New()
        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        ComboBoxDHTType.Items.AddRange([Enum].GetNames(GetType(UserControlSensorDHT.dht_type_t)))
    End Sub

    Public Sub setSensor(sensor As YAAASharedClassLibrary.YAAASensor) Implements IUserControlSensor.setSensor
        sensor_ = sensor.Clone

        With sensor_.sensor_settings_
            TextBoxPinNr.Text = YAAASharedClassLibrary.printPin(.sensor_parameters_(0))

            If String.IsNullOrEmpty([Enum].GetName(GetType(UserControlSensorDHT.dht_type_t), .sensor_parameters_(1))) Then
                ComboBoxDHTType.SelectedItem = UserControlSensorDHT.dht_type_t.DHT11.ToString
            Else
                ComboBoxDHTType.SelectedItem = [Enum].GetName(GetType(UserControlSensorDHT.dht_type_t), .sensor_parameters_(1))
            End If
        End With
    End Sub

    Public Function getSensor(ByRef sensor As YAAASharedClassLibrary.YAAASensor) As Boolean Implements IUserControlSensor.getSensor
        Dim valid As Boolean = True

        With sensor_.sensor_settings_
            If YAAASharedClassLibrary.tryParsePin(TextBoxPinNr.Text, .sensor_parameters_(0)) Then
                TextBoxPinNr.BackColor = SystemColors.Window
            Else
                valid = False
                TextBoxPinNr.BackColor = Drawing.Color.Orange
            End If

            .sensor_parameters_(1) = [Enum].Parse(GetType(UserControlSensorDHT.dht_type_t), ComboBoxDHTType.SelectedItem.ToString)
        End With

        sensor = sensor_.Clone

        Return valid
    End Function
End Class
