﻿'Imports ASCOM.YAAADevice

Public Class UserControlSensorWindDirectionSwitchResistor
    Implements IUserControlSensor

    Private sensor_ As YAAASharedClassLibrary.YAAASensor

    Public Sub setSensor(sensor As YAAASharedClassLibrary.YAAASensor) Implements IUserControlSensor.setSensor
        sensor_ = sensor.Clone()

        With sensor_.sensor_settings_
            TextBoxPin.Text = YAAASharedClassLibrary.printPin(.sensor_parameters_(0))

            Dim angle_offset As Double = 0.0
            Dim buffer As Byte() = {&HFF, &HFF}

            Array.Copy(sensor_.sensor_settings_.sensor_parameters_, 6, buffer, 0, 2)

            'reverse byte order if we are on a little-endian system (Arduino is big-endian)
            If BitConverter.IsLittleEndian Then
                Array.Reverse(buffer)
            End If

            TextBoxAngleOffset.Text = (CDbl(BitConverter.ToInt16(buffer, 0)) / 32767.0 * 180.0).ToString("N2")
        End With
    End Sub

    Public Function getSensor(ByRef sensor As YAAASharedClassLibrary.YAAASensor) As Boolean Implements IUserControlSensor.getSensor
        Dim valid As Boolean = True
        Dim angle_offset As Double = 0.0

        valid = valid And YAAASharedClassLibrary.tryParsePin(TextBoxPin.Text, sensor_.sensor_settings_.sensor_parameters_(0))

        valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxAngleOffset, angle_offset)

        If angle_offset < -180.0 Or angle_offset > 180.0 Then
            angle_offset = 0.0
            valid = False
            TextBoxAngleOffset.BackColor = Drawing.Color.Orange
        Else
            TextBoxAngleOffset.BackColor = SystemColors.Window
        End If

        'save offset angle as signed 16bit integer, mapping the range -180.0 to 180.0 degrees to the range -32767 to 32767
        Dim buffer As Byte() = BitConverter.GetBytes(CShort(angle_offset / 180.0 * 32767.0))

        'reverse byte order if we are on a little-endian system (Arduino is big-endian)
        If BitConverter.IsLittleEndian Then
            Array.Reverse(buffer)
        End If

        Array.Copy(buffer, 0, sensor_.sensor_settings_.sensor_parameters_, 6, 2)

        sensor = sensor_.Clone

        Return valid
    End Function
End Class
