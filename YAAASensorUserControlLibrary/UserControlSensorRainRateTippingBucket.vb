﻿'Imports ASCOM.YAAADevice

Public Class UserControlSensorRainRateTippingBucket
    Implements IUserControlSensor

    Private sensor_ As YAAASharedClassLibrary.YAAASensor

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
    End Sub
    Public Sub setSensor(sensor As YAAASharedClassLibrary.YAAASensor) Implements IUserControlSensor.setSensor
        sensor_ = sensor.Clone

        With sensor_.sensor_settings_
            TextBoxInputPin.Text = YAAASharedClassLibrary.printPin(.sensor_parameters_(0))

            Dim buffer As Byte() = {&HFF, &HFF, &HFF, &HFF}

            'count period
            Array.Copy(.sensor_parameters_, 4, buffer, 0, 4)

            'Arduino is big endian
            If BitConverter.IsLittleEndian Then
                Array.Reverse(buffer)
            End If

            TextBoxCountPeriod.Text = CDbl(BitConverter.ToUInt32(buffer, 0)) / 1000.0

            ReDim buffer(1)
            buffer = {&HFF, &HFF}

            'mm per count
            Array.Copy(.sensor_parameters_, 2, buffer, 0, 2)

            'Arduino is big endian
            If BitConverter.IsLittleEndian Then
                Array.Reverse(buffer)
            End If

            TextBoxMmPerCount.Text = CDbl(BitConverter.ToUInt16(buffer, 0)) / 1000.0

            'debounce delay
            Array.Copy(sensor_.sensor_settings_.sensor_parameters_, 8, buffer, 0, 2)

            'reverse byte order if we are on a little-endian system (Arduino is big-endian)
            If BitConverter.IsLittleEndian Then
                Array.Reverse(buffer)
            End If

            TextBoxDebounceDelay.Text = (CDbl(BitConverter.ToUInt16(buffer, 0)) / 1000.0).ToString("N2")
        End With
    End Sub

    Public Function getSensor(ByRef sensor As YAAASharedClassLibrary.YAAASensor) As Boolean Implements IUserControlSensor.getSensor
        Dim valid As Boolean = True
        Dim debounce_delay As Double = 10.0
        Dim count_period As Int32 = 10
        Dim mm_per_count As Double = 1.0

        If YAAASharedClassLibrary.tryParsePin(TextBoxInputPin.Text, sensor_.sensor_settings_.sensor_parameters_(0)) Then
            TextBoxInputPin.BackColor = SystemColors.Window
        Else
            valid = False
            TextBoxInputPin.BackColor = Drawing.Color.Orange
        End If

        Dim buffer As Byte() = {&HFF, &HFF, &HFF, &HFF}


        'count period
        valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxCountPeriod, count_period)

        ReDim buffer(3)
        buffer = {&HFF, &HFF, &HFF, &HFF}

        'convert count period from seconds to ms
        buffer = BitConverter.GetBytes(count_period * 1000)

        'Arduino is big endian
        If BitConverter.IsLittleEndian Then
            Array.Reverse(buffer)
        End If

        Array.Copy(buffer, 0, sensor_.sensor_settings_.sensor_parameters_, 4, 4)


        ReDim buffer(1)
        buffer = {&HFF, &HFF}

        'mm per count
        valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxMmPerCount, mm_per_count)

        'speed must be positive and < 65.535 meters / second
        If mm_per_count < 0.0 Or mm_per_count > 65.535 Then
            mm_per_count = 1.0
            valid = False
            TextBoxMmPerCount.BackColor = Drawing.Color.Orange
        Else
            TextBoxMmPerCount.BackColor = SystemColors.Window
        End If

        'save speed as unsigned short (16bit unsigned int)
        buffer = BitConverter.GetBytes(CUShort(mm_per_count * 1000.0))

        'reverse byte order if we are on a little-endian system (Arduino is big-endian)
        If BitConverter.IsLittleEndian Then
            Array.Reverse(buffer)
        End If

        Array.Copy(buffer, 0, sensor_.sensor_settings_.sensor_parameters_, 2, 2)


        'debounce delay
        valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxDebounceDelay, debounce_delay)

        'debounce delay must be between 0.0 and 65.535 ms
        If debounce_delay < 0.0 Or debounce_delay > 65.535 Then
            debounce_delay = 10.0
            valid = False
            TextBoxDebounceDelay.BackColor = Drawing.Color.Orange
        Else
            TextBoxDebounceDelay.BackColor = SystemColors.Window
        End If

        'save speed as unsigned short (16bit unsigned int)
        buffer = BitConverter.GetBytes(CUShort(debounce_delay * 1000.0))

        'reverse byte order if we are on a little-endian system (Arduino is big-endian)
        If BitConverter.IsLittleEndian Then
            Array.Reverse(buffer)
        End If

        Array.Copy(buffer, 0, sensor_.sensor_settings_.sensor_parameters_, 8, 2)

        sensor = sensor_.Clone

        Return valid
    End Function
End Class
