﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UserControlSensorWindSpeedAnalogVoltage
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.TextBoxAnalogBaseline = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TextBoxAnalogReference = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TextBoxSlope = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TextBoxPin = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.SuspendLayout()
        '
        'TextBoxAnalogBaseline
        '
        Me.TextBoxAnalogBaseline.Location = New System.Drawing.Point(362, 29)
        Me.TextBoxAnalogBaseline.Name = "TextBoxAnalogBaseline"
        Me.TextBoxAnalogBaseline.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxAnalogBaseline.TabIndex = 15
        Me.ToolTip1.SetToolTip(Me.TextBoxAnalogBaseline, "Sensor's output voltage at 0m/s.")
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(274, 32)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(82, 13)
        Me.Label4.TabIndex = 14
        Me.Label4.Text = "Analog baseline"
        '
        'TextBoxAnalogReference
        '
        Me.TextBoxAnalogReference.Location = New System.Drawing.Point(362, 3)
        Me.TextBoxAnalogReference.Name = "TextBoxAnalogReference"
        Me.TextBoxAnalogReference.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxAnalogReference.TabIndex = 13
        Me.ToolTip1.SetToolTip(Me.TextBoxAnalogReference, "Analog reference of the Arduino's ADC.")
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(274, 6)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(88, 13)
        Me.Label3.TabIndex = 12
        Me.Label3.Text = "Analog reference"
        '
        'TextBoxSlope
        '
        Me.TextBoxSlope.Location = New System.Drawing.Point(109, 29)
        Me.TextBoxSlope.Name = "TextBoxSlope"
        Me.TextBoxSlope.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxSlope.TabIndex = 11
        Me.ToolTip1.SetToolTip(Me.TextBoxSlope, "Slope of the sensor's output value in meters / second / Volt")
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(20, 32)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(34, 13)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Slope"
        '
        'TextBoxPin
        '
        Me.TextBoxPin.Location = New System.Drawing.Point(109, 3)
        Me.TextBoxPin.Name = "TextBoxPin"
        Me.TextBoxPin.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxPin.TabIndex = 9
        Me.ToolTip1.SetToolTip(Me.TextBoxPin, "Input pin, must be an Analog Input pin.")
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(20, 6)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(22, 13)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Pin"
        '
        'UserControlSensorWindSpeedAnalogVoltage
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.TextBoxAnalogBaseline)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.TextBoxAnalogReference)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.TextBoxSlope)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.TextBoxPin)
        Me.Controls.Add(Me.Label1)
        Me.Name = "UserControlSensorWindSpeedAnalogVoltage"
        Me.Size = New System.Drawing.Size(465, 60)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents TextBoxAnalogBaseline As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents TextBoxAnalogReference As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents TextBoxSlope As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents TextBoxPin As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents ToolTip1 As ToolTip
End Class
