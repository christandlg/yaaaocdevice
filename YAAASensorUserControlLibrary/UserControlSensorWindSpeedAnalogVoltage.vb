﻿'Imports ASCOM.YAAADevice

Public Class UserControlSensorWindSpeedAnalogVoltage
    Implements IUserControlSensor

    Private sensor_ As YAAASharedClassLibrary.YAAASensor

    Public Sub setSensor(sensor As YAAASharedClassLibrary.YAAASensor) Implements IUserControlSensor.setSensor
        sensor_ = sensor.Clone()

        With sensor_.sensor_settings_
            TextBoxPin.Text = YAAASharedClassLibrary.printPin(.sensor_parameters_(0))

            Dim buffer As Byte() = {&HFF, &HFF}

            'convert analog baseline to float
            Array.Copy(sensor_.sensor_settings_.sensor_parameters_, 2, buffer, 0, 2)        '2 bytes, analog baseline in mV

            'reverse byte order if we are on a little-endian system (Arduino is big-endian)
            If BitConverter.IsLittleEndian Then
                Array.Reverse(buffer)
            End If

            TextBoxAnalogBaseline.Text = (CDbl(BitConverter.ToUInt16(buffer, 0)) / 1000.0).ToString("N2")

            buffer = {&HFF, &HFF}

            'convert analog reference to float
            Array.Copy(sensor_.sensor_settings_.sensor_parameters_, 4, buffer, 0, 2)        '2 bytes, analog reference in mV

            'reverse byte order if we are on a little-endian system (Arduino is big-endian)
            If BitConverter.IsLittleEndian Then
                Array.Reverse(buffer)
            End If

            TextBoxAnalogReference.Text = (CDbl(BitConverter.ToUInt16(buffer, 0)) / 1000.0).ToString("N2")

            buffer = {&HFF, &HFF}

            'convert slope to float
            Array.Copy(sensor_.sensor_settings_.sensor_parameters_, 6, buffer, 0, 2)        '2 bytes, slope in mm/s/V

            'reverse byte order if we are on a little-endian system (Arduino is big-endian)
            If BitConverter.IsLittleEndian Then
                Array.Reverse(buffer)
            End If

            TextBoxSlope.Text = (CDbl(BitConverter.ToUInt16(buffer, 0)) / 1000.0).ToString("N2")
        End With
    End Sub

    Public Function getSensor(ByRef sensor As YAAASharedClassLibrary.YAAASensor) As Boolean Implements IUserControlSensor.getSensor
        Dim valid As Boolean = True
        Dim baseline As Double = 0.0
        Dim reference As Double = 5.0
        Dim slope As Double = 20.25

        valid = valid And YAAASharedClassLibrary.tryParsePin(TextBoxPin.Text, sensor_.sensor_settings_.sensor_parameters_(0))

        valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxAnalogBaseline, baseline)

        'save baseline in mV, as unsigned 16bit integer
        If baseline < 0.0 Or baseline > 65.535 Then
            baseline = 0
            valid = False
            TextBoxAnalogBaseline.BackColor = Drawing.Color.Orange
        Else
            TextBoxAnalogBaseline.BackColor = SystemColors.Window
        End If

        Dim buffer As Byte() = BitConverter.GetBytes(CUShort(baseline * 1000.0))

        'reverse byte order if we are on a little-endian system (Arduino is big-endian)
        If BitConverter.IsLittleEndian Then
            Array.Reverse(buffer)
        End If

        Array.Copy(buffer, 0, sensor_.sensor_settings_.sensor_parameters_, 2, 2)


        'save analog reference voltage in mV, as unsigned 16 bit integer
        valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxAnalogReference, reference)

        If reference < 0.0 Or reference > 65.535 Then
            reference = 5.0
            valid = False
            TextBoxAnalogReference.BackColor = Drawing.Color.Orange
        Else
            TextBoxAnalogReference.BackColor = SystemColors.Window
        End If

        buffer = BitConverter.GetBytes(CUShort(reference * 1000.0))

        'reverse byte order if we are on a little-endian system (Arduino is big-endian)
        If BitConverter.IsLittleEndian Then
            Array.Reverse(buffer)
        End If

        Array.Copy(buffer, 0, sensor_.sensor_settings_.sensor_parameters_, 4, 2)


        'save slope in mm/s/V as unsigned 16bit integer
        valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxSlope, slope)

        If slope < 0.0 Or slope > 65.535 Then
            slope = 20.25
            valid = False
            TextBoxSlope.BackColor = Drawing.Color.Orange
        Else
            TextBoxSlope.BackColor = SystemColors.Window
        End If

        buffer = BitConverter.GetBytes(CUShort(slope * 1000.0))

        'reverse byte order if we are on a little-endian system (Arduino is big-endian)
        If BitConverter.IsLittleEndian Then
            Array.Reverse(buffer)
        End If

        Array.Copy(buffer, 0, sensor_.sensor_settings_.sensor_parameters_, 6, 2)

        sensor = sensor_.Clone

        Return valid
    End Function
End Class
