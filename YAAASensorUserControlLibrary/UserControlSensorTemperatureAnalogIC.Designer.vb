﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UserControlSensorTemperatureAnalogIC
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.TextBoxInputPin = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TextBox0Voltage = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TextBoxCoefficient = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TextBoxRefVoltage = New System.Windows.Forms.TextBox()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.SuspendLayout()
        '
        'TextBoxInputPin
        '
        Me.TextBoxInputPin.Location = New System.Drawing.Point(80, 3)
        Me.TextBoxInputPin.Name = "TextBoxInputPin"
        Me.TextBoxInputPin.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxInputPin.TabIndex = 0
        Me.ToolTip1.SetToolTip(Me.TextBoxInputPin, "Input pin number. must be an analog input pin.")
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(3, 6)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(49, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Input Pin"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(3, 32)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(56, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "0° Voltage"
        '
        'TextBox0Voltage
        '
        Me.TextBox0Voltage.Location = New System.Drawing.Point(80, 29)
        Me.TextBox0Voltage.Name = "TextBox0Voltage"
        Me.TextBox0Voltage.Size = New System.Drawing.Size(100, 20)
        Me.TextBox0Voltage.TabIndex = 2
        Me.ToolTip1.SetToolTip(Me.TextBox0Voltage, "Sensor's output voltage at 0°C." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10))
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(234, 6)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(103, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Coefficient [mV / °K]"
        '
        'TextBoxCoefficient
        '
        Me.TextBoxCoefficient.Location = New System.Drawing.Point(362, 3)
        Me.TextBoxCoefficient.Name = "TextBoxCoefficient"
        Me.TextBoxCoefficient.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxCoefficient.TabIndex = 4
        Me.ToolTip1.SetToolTip(Me.TextBoxCoefficient, "Sensor's temperature coefficient / Sensor gain")
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(234, 31)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(112, 13)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Reference Voltage [V]"
        '
        'TextBoxRefVoltage
        '
        Me.TextBoxRefVoltage.Location = New System.Drawing.Point(362, 28)
        Me.TextBoxRefVoltage.Name = "TextBoxRefVoltage"
        Me.TextBoxRefVoltage.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxRefVoltage.TabIndex = 6
        Me.ToolTip1.SetToolTip(Me.TextBoxRefVoltage, "Arduino's ADC reference voltage")
        '
        'ToolTip1
        '
        Me.ToolTip1.AutoPopDelay = 50000
        Me.ToolTip1.InitialDelay = 500
        Me.ToolTip1.ReshowDelay = 100
        '
        'UserControlSensorTemperatureAnalogIC
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.TextBoxRefVoltage)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.TextBoxCoefficient)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.TextBox0Voltage)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TextBoxInputPin)
        Me.Name = "UserControlSensorTemperatureAnalogIC"
        Me.Size = New System.Drawing.Size(465, 60)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents TextBoxInputPin As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents TextBox0Voltage As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents TextBoxCoefficient As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents TextBoxRefVoltage As TextBox
    Friend WithEvents ToolTip1 As ToolTip
End Class
