﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UserControlSensorDHT
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ComboBoxDHTType = New System.Windows.Forms.ComboBox()
        Me.TextBoxPinNr = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.SuspendLayout()
        '
        'ComboBoxDHTType
        '
        Me.ComboBoxDHTType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxDHTType.FormattingEnabled = True
        Me.ComboBoxDHTType.Location = New System.Drawing.Point(99, 29)
        Me.ComboBoxDHTType.Name = "ComboBoxDHTType"
        Me.ComboBoxDHTType.Size = New System.Drawing.Size(100, 21)
        Me.ComboBoxDHTType.TabIndex = 0
        Me.ToolTip1.SetToolTip(Me.ComboBoxDHTType, "Type of DHT sensor")
        '
        'TextBoxPinNr
        '
        Me.TextBoxPinNr.Location = New System.Drawing.Point(99, 3)
        Me.TextBoxPinNr.Name = "TextBoxPinNr"
        Me.TextBoxPinNr.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxPinNr.TabIndex = 1
        Me.ToolTip1.SetToolTip(Me.TextBoxPinNr, "Input pin number for DHT sensor")
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(3, 6)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(62, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Pin Number"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(3, 32)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(57, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "DHT Type"
        '
        'ToolTip1
        '
        Me.ToolTip1.AutoPopDelay = 50000
        Me.ToolTip1.InitialDelay = 500
        Me.ToolTip1.ReshowDelay = 100
        Me.ToolTip1.ToolTipTitle = "Input pin for "
        '
        'UserControlSensorHumidityDHT
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TextBoxPinNr)
        Me.Controls.Add(Me.ComboBoxDHTType)
        Me.Name = "UserControlSensorHumidityDHT"
        Me.Size = New System.Drawing.Size(465, 60)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ComboBoxDHTType As ComboBox
    Friend WithEvents TextBoxPinNr As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents ToolTip1 As ToolTip
End Class
