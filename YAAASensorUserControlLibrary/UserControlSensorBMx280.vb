﻿Public Class UserControlSensorBMx280
    Implements IUserControlSensor

    Enum filter_t
        FILTER_OFF = 0
        FILTER_x02 = 1
        FILTER_x04 = 2
        FILTER_x08 = 3
        FILTER_x16 = 4
    End Enum

    Enum i2c_address_t
        Ox76 = &H76
        Ox77 = &H77
    End Enum

    Enum interface_t
        I2C = 0
        SPI = 1
    End Enum

    Enum mode_t
        'MODE_SLEEP = 0       'device In sleep mode. 
        MODE_FORCED = 1      'measurements are performed on demand. 
        'MODE_FORCED_ALT = 2  'alternative value For BMx280_MODE_FORCED. 
        MODE_NORMAL = 3      'measurements are performed periodically. 
    End Enum

    Enum oversampling_setting_t
        x00 = 0
        x01 = 1
        x02 = 2
        x04 = 3
        x08 = 4
        x16 = 16
    End Enum

    Enum standby_time_t
        ts1ms = 0
        ts62ms = 1
        ts125ms = 2
        ts250ms = 3
        ts500ms = 4
        ts1000ms = 5
        ts10ms = 6
        ts20ms = 7
    End Enum

    Private sensor_ As YAAASharedClassLibrary.YAAASensor

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
    End Sub
    Public Sub setSensor(sensor As YAAASharedClassLibrary.YAAASensor) Implements IUserControlSensor.setSensor
        sensor_ = sensor.Clone

        ComboBoxInterface.Items.AddRange([Enum].GetNames(GetType(UserControlSensorBMx280.interface_t)))

        ComboBoxOSRS_T.Items.AddRange([Enum].GetNames(GetType(UserControlSensorBMx280.oversampling_setting_t)))
        ComboBoxOSRS_P.Items.AddRange([Enum].GetNames(GetType(UserControlSensorBMx280.oversampling_setting_t)))
        ComboBoxOSRS_H.Items.AddRange([Enum].GetNames(GetType(UserControlSensorBMx280.oversampling_setting_t)))

        ComboBoxFilter.Items.AddRange([Enum].GetNames(GetType(UserControlSensorBMx280.filter_t)))
        ComboBoxMode.Items.AddRange([Enum].GetNames(GetType(UserControlSensorBMx280.mode_t)))
        ComboBoxStandbyTime.Items.AddRange([Enum].GetNames(GetType(UserControlSensorBMx280.standby_time_t)))

        With sensor_.sensor_settings_
            'interface (I2C / SPI)
            If Not [Enum].IsDefined(GetType(UserControlSensorBMx280.interface_t), CInt(.sensor_parameters_(0))) Then
                ComboBoxInterface.SelectedItem = [Enum].Parse(GetType(UserControlSensorBMx280.interface_t), CInt(UserControlSensorBMx280.interface_t.I2C)).ToString
            Else
                ComboBoxInterface.SelectedItem = [Enum].Parse(GetType(UserControlSensorBMx280.interface_t), CInt(.sensor_parameters_(0))).ToString
            End If

            'chip select value is changed by function ComboBoxInterface_SelectedIndexChanged 

            'temperature oversampling settings
            If String.IsNullOrEmpty([Enum].GetName(GetType(UserControlSensorBMx280.oversampling_setting_t), sensor_.sensor_settings_.sensor_parameters_(2))) Then
                ComboBoxOSRS_T.SelectedItem = UserControlSensorBMx280.oversampling_setting_t.x16.ToString
            Else
                ComboBoxOSRS_T.SelectedItem = [Enum].GetName(GetType(UserControlSensorBMx280.oversampling_setting_t), sensor_.sensor_settings_.sensor_parameters_(2))
            End If

            'pressure oversampling settings
            If String.IsNullOrEmpty([Enum].GetName(GetType(UserControlSensorBMx280.oversampling_setting_t), sensor_.sensor_settings_.sensor_parameters_(3))) Then
                ComboBoxOSRS_P.SelectedItem = UserControlSensorBMx280.oversampling_setting_t.x16.ToString
            Else
                ComboBoxOSRS_P.SelectedItem = [Enum].GetName(GetType(UserControlSensorBMx280.oversampling_setting_t), sensor_.sensor_settings_.sensor_parameters_(3))
            End If

            'humidity oversampling settings
            If String.IsNullOrEmpty([Enum].GetName(GetType(UserControlSensorBMx280.oversampling_setting_t), sensor_.sensor_settings_.sensor_parameters_(4))) Then
                ComboBoxOSRS_H.SelectedItem = UserControlSensorBMx280.oversampling_setting_t.x16.ToString
            Else
                ComboBoxOSRS_H.SelectedItem = [Enum].GetName(GetType(UserControlSensorBMx280.oversampling_setting_t), sensor_.sensor_settings_.sensor_parameters_(4))
            End If

            'filter settings
            If String.IsNullOrEmpty([Enum].GetName(GetType(UserControlSensorBMx280.filter_t), sensor_.sensor_settings_.sensor_parameters_(5))) Then
                ComboBoxFilter.SelectedItem = UserControlSensorBMx280.filter_t.FILTER_OFF.ToString
            Else
                ComboBoxFilter.SelectedItem = [Enum].GetName(GetType(UserControlSensorBMx280.filter_t), sensor_.sensor_settings_.sensor_parameters_(5))
            End If

            'measurement mode
            If String.IsNullOrEmpty([Enum].GetName(GetType(UserControlSensorBMx280.mode_t), sensor_.sensor_settings_.sensor_parameters_(6))) Then
                ComboBoxMode.SelectedItem = UserControlSensorBMx280.mode_t.MODE_FORCED.ToString
            Else
                ComboBoxMode.SelectedItem = [Enum].GetName(GetType(UserControlSensorBMx280.mode_t), sensor_.sensor_settings_.sensor_parameters_(6))
            End If

            'standby time (normal mode only)
            If String.IsNullOrEmpty([Enum].GetName(GetType(UserControlSensorBMx280.standby_time_t), sensor_.sensor_settings_.sensor_parameters_(7))) Then
                ComboBoxStandbyTime.SelectedItem = UserControlSensorBMx280.standby_time_t.ts1000ms.ToString
            Else
                ComboBoxStandbyTime.SelectedItem = [Enum].GetName(GetType(UserControlSensorBMx280.standby_time_t), sensor_.sensor_settings_.sensor_parameters_(7))
            End If
        End With
    End Sub

    Public Function getSensor(ByRef sensor As YAAASharedClassLibrary.YAAASensor) As Boolean Implements IUserControlSensor.getSensor
        Dim valid As Boolean = True

        sensor_.sensor_settings_.sensor_parameters_(0) = [Enum].Parse(GetType(UserControlSensorBMx280.interface_t), ComboBoxInterface.SelectedItem.ToString)

        If [Enum].Parse(GetType(UserControlSensorBMx280.interface_t), ComboBoxInterface.SelectedItem.ToString) = UserControlSensorBMx280.interface_t.I2C Then
            sensor_.sensor_settings_.sensor_parameters_(1) = CByte([Enum].Parse(GetType(UserControlSensorBMx280.i2c_address_t), ComboBoxChipSelect.SelectedItem))
        Else
            sensor_.sensor_settings_.sensor_parameters_(1) = CByte(ComboBoxChipSelect.Text)
        End If

        sensor_.sensor_settings_.sensor_parameters_(2) = [Enum].Parse(GetType(UserControlSensorBMx280.oversampling_setting_t), ComboBoxOSRS_T.SelectedItem.ToString)
        sensor_.sensor_settings_.sensor_parameters_(3) = [Enum].Parse(GetType(UserControlSensorBMx280.oversampling_setting_t), ComboBoxOSRS_P.SelectedItem.ToString)
        sensor_.sensor_settings_.sensor_parameters_(4) = [Enum].Parse(GetType(UserControlSensorBMx280.oversampling_setting_t), ComboBoxOSRS_H.SelectedItem.ToString)

        sensor_.sensor_settings_.sensor_parameters_(5) = [Enum].Parse(GetType(UserControlSensorBMx280.filter_t), ComboBoxFilter.SelectedItem.ToString)
        sensor_.sensor_settings_.sensor_parameters_(6) = [Enum].Parse(GetType(UserControlSensorBMx280.mode_t), ComboBoxMode.SelectedItem.ToString)
        sensor_.sensor_settings_.sensor_parameters_(7) = [Enum].Parse(GetType(UserControlSensorBMx280.standby_time_t), ComboBoxStandbyTime.SelectedItem.ToString)

        sensor = sensor_.Clone()

        Return valid
    End Function

    Private Sub ComboBoxInterface_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBoxInterface.SelectedIndexChanged
        Select Case ComboBoxInterface.SelectedIndex
            Case UserControlSensorBMx280.interface_t.I2C
                LabelChipSelect.Text = "I2C Addr."
                ComboBoxChipSelect.Items.Clear()
                ComboBoxChipSelect.Items.AddRange([Enum].GetNames(GetType(UserControlSensorBMx280.i2c_address_t)))
                ComboBoxChipSelect.DropDownStyle = ComboBoxStyle.DropDownList

                If Not [Enum].IsDefined(GetType(UserControlSensorBMx280.i2c_address_t), CInt(sensor_.sensor_settings_.sensor_parameters_(1))) Then
                    ComboBoxChipSelect.SelectedItem = [Enum].Parse(GetType(UserControlSensorBMx280.i2c_address_t), CInt(UserControlSensorBMx280.i2c_address_t.Ox76)).ToString
                Else
                    ComboBoxChipSelect.SelectedItem = [Enum].Parse(GetType(UserControlSensorBMx280.i2c_address_t), CInt(sensor_.sensor_settings_.sensor_parameters_(1))).ToString
                End If

            Case UserControlSensorBMx280.interface_t.SPI
                LabelChipSelect.Text = "CS Pin"
                ComboBoxChipSelect.Items.Clear()
                ComboBoxChipSelect.DropDownStyle = ComboBoxStyle.DropDown

                ComboBoxChipSelect.SelectedText = sensor_.sensor_settings_.sensor_parameters_(1).ToString
        End Select
    End Sub

    Private Sub ComboBoxMode_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBoxMode.SelectedIndexChanged
        Select Case ComboBoxMode.SelectedItem.ToString
            Case UserControlSensorBMx280.mode_t.MODE_NORMAL.ToString
                ComboBoxStandbyTime.Enabled = True
            Case UserControlSensorBMx280.mode_t.MODE_FORCED.ToString
                ComboBoxStandbyTime.Enabled = False
        End Select
    End Sub
End Class
