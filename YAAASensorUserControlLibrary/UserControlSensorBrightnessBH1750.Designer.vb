﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UserControlSensorBrightnessBH1750
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TextBoxOWTR = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.ComboBoxI2CAddress = New System.Windows.Forms.ComboBox()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(14, 6)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(64, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "I2C Address"
        '
        'TextBoxOWTR
        '
        Me.TextBoxOWTR.Location = New System.Drawing.Point(88, 29)
        Me.TextBoxOWTR.Name = "TextBoxOWTR"
        Me.TextBoxOWTR.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxOWTR.TabIndex = 2
        Me.ToolTip1.SetToolTip(Me.TextBoxOWTR, "Optical Window Transmission Rate. must be beween 0.0 and 1.0.")
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(14, 32)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(41, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "OWTR"
        '
        'ToolTip1
        '
        Me.ToolTip1.AutoPopDelay = 50000
        Me.ToolTip1.InitialDelay = 500
        Me.ToolTip1.ReshowDelay = 100
        '
        'ComboBoxI2CAddress
        '
        Me.ComboBoxI2CAddress.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxI2CAddress.FormattingEnabled = True
        Me.ComboBoxI2CAddress.Location = New System.Drawing.Point(88, 3)
        Me.ComboBoxI2CAddress.Name = "ComboBoxI2CAddress"
        Me.ComboBoxI2CAddress.Size = New System.Drawing.Size(100, 21)
        Me.ComboBoxI2CAddress.TabIndex = 4
        '
        'UserControlSensorBrightnessBH1750
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.ComboBoxI2CAddress)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.TextBoxOWTR)
        Me.Controls.Add(Me.Label1)
        Me.Name = "UserControlSensorBrightnessBH1750"
        Me.Size = New System.Drawing.Size(465, 60)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents TextBoxOWTR As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents ComboBoxI2CAddress As ComboBox
End Class
