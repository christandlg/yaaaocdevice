﻿'Imports ASCOM.YAAADevice

Public Class UserControlSensorTemperatureThermistor
    Implements IUserControlSensor

    Private sensor_ As YAAASharedClassLibrary.YAAASensor

    Public Sub setSensor(sensor As YAAASharedClassLibrary.YAAASensor) Implements IUserControlSensor.setSensor
        sensor_ = sensor.Clone()

        With sensor_.sensor_settings_
            'set input pin chooser to saved input pin
            TextBoxInputPin.Text = YAAASharedClassLibrary.printPin(.sensor_parameters_(0))

            'buffer array for 0° and Arduino reference voltage
            Dim buffer() As Byte = Array.CreateInstance(GetType(System.Byte), 2)

            buffer = {&HFF, &HFF}
            'copy pullup resistor value to buffer 
            Array.Copy(.sensor_parameters_, 2, buffer, 0, 2)
            If BitConverter.IsLittleEndian Then
                Array.Reverse(buffer)       'Arduino (and data stored in the ASCOM profile store) is big endian
            End If
            TextBoxPullupResistor.Text = BitConverter.ToUInt16(buffer, 0) * 100

            buffer = {&HFF, &HFF}
            'copy reference resistance value to buffer 
            Array.Copy(.sensor_parameters_, 4, buffer, 0, 2)
            If BitConverter.IsLittleEndian Then
                Array.Reverse(buffer)       'Arduino (and data stored in the ASCOM profile store) is big endian
            End If
            TextBoxRefResistance.Text = BitConverter.ToUInt16(buffer, 0) * 100

            buffer = {&HFF, &HFF}
            'copy reference temperature to buffer
            Array.Copy(.sensor_parameters_, 6, buffer, 0, 2)
            If BitConverter.IsLittleEndian Then
                Array.Reverse(buffer)       'Arduino (and data stored in the ASCOM profile store) is big endian
            End If
            TextBoxRefTemperature.Text = BitConverter.ToUInt16(buffer, 0) / 10

            buffer = {&HFF, &HFF}
            'copy coefficient B to buffer
            Array.Copy(.sensor_parameters_, 8, buffer, 0, 2)
            If BitConverter.IsLittleEndian Then
                Array.Reverse(buffer)       'Arduino (and data stored in the ASCOM profile store) is big endian
            End If
            TextBoxCoefficient.Text = BitConverter.ToInt16(buffer, 0)
        End With
    End Sub

    Public Function getSensor(ByRef sensor As YAAASharedClassLibrary.YAAASensor) As Boolean Implements IUserControlSensor.getSensor
        Dim valid As Boolean = True

        With sensor_.sensor_settings_
            'save input pin for temperature sensor
            If YAAASharedClassLibrary.tryParsePin(TextBoxInputPin.Text, .sensor_parameters_(0)) Then
                TextBoxInputPin.BackColor = SystemColors.Window
            Else
                valid = False
                TextBoxInputPin.BackColor = Drawing.Color.Orange
            End If

            'buffer array for 0° and Arduino reference voltage
            Dim buffer As Byte() = {&HFF, &HFF}

            'copy pullup resistor value to buffer 
            Try
                buffer = BitConverter.GetBytes(Convert.ToUInt16(CUShort(TextBoxPullupResistor.Text) / 100))
                TextBoxPullupResistor.BackColor = SystemColors.Window
            Catch ex As Exception
                valid = False
                TextBoxPullupResistor.BackColor = Drawing.Color.Orange
            End Try

            If BitConverter.IsLittleEndian Then
                Array.Reverse(buffer)       'Arduino (and data stored in the ASCOM profile store) is big endian
            End If
            Array.Copy(buffer, 0, .sensor_parameters_, 2, 2)

            buffer = {&HFF, &HFF}
            'copy reference resistance value to buffer
            Try
                buffer = BitConverter.GetBytes(Convert.ToUInt16(CUShort(TextBoxRefResistance.Text) / 100))
                TextBoxRefResistance.BackColor = SystemColors.Window
            Catch ex As Exception
                valid = False
                TextBoxRefResistance.BackColor = Drawing.Color.Orange
            End Try

            If BitConverter.IsLittleEndian Then
                Array.Reverse(buffer)       'Arduino (and data stored in the ASCOM profile store) is big endian
            End If
            Array.Copy(buffer, 0, .sensor_parameters_, 4, 2)

            'copy reference temperature to buffer
            Try
                buffer = BitConverter.GetBytes(Convert.ToUInt16(CSng(TextBoxRefTemperature.Text) * 10))
                TextBoxRefTemperature.BackColor = SystemColors.Window
            Catch ex As Exception
                valid = False
                TextBoxRefTemperature.BackColor = Drawing.Color.Orange
            End Try
            If BitConverter.IsLittleEndian Then
                Array.Reverse(buffer)       'Arduino (and data stored in the ASCOM profile store) is big endian
            End If
            Array.Copy(buffer, 0, .sensor_parameters_, 6, 2)

            'copy coefficient B to buffer
            Try
                buffer = BitConverter.GetBytes(Convert.ToUInt16(TextBoxCoefficient.Text))
                TextBoxCoefficient.BackColor = SystemColors.Window
            Catch ex As Exception
                valid = False
                TextBoxCoefficient.BackColor = Drawing.Color.Orange
            End Try
            If BitConverter.IsLittleEndian Then
                Array.Reverse(buffer)       'Arduino (and data stored in the ASCOM profile store) is big endian
            End If
            Array.Copy(buffer, 0, .sensor_parameters_, 8, 2)

        End With


        sensor = sensor_.Clone()

        Return valid
    End Function
End Class
