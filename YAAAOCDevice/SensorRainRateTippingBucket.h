//Tipping Bucket type rain rate sensor class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef SENSOR_RAIN_RATE_TIPPING_BUCKET_
#define SENSOR_RAIN_RATE_TIPPING_BUCKET_

#include <Arduino.h>

#include "SensorRainRate.h"

class SensorRainRateTippingBucket :
	public SensorRainRate
{
public:
	SensorRainRateTippingBucket(YAAASensor::SensorSettings sensor_settings);

	~SensorRainRateTippingBucket();

	/*
	initializes the temperature sensor with the info given in in the constructor call.
	@return true on success (sensor input is analog and input pin is an analog input pin), false otherwise. */
	bool begin();

	/*
	starts a measurement.
	@return true on success, false otherwise.*/
	bool measure();

	/*
	checks if a measurement has been completed. may perform work necessary to complete a mesurement. user is
	expected to call this function after calling measure() and call it until it returns true.
	@return true if a measurement has been completed. */
	bool hasValue();

	/*
	returns the last measured value.
	user is expected to call this function only after hasValue() has returned true.
	@return the barometric pressure in the given unit. */
	float getValue();

	/*
	interrupt service routine (rotate) for tipping bucket rain rate sensors, calls function 
	rotate() if an instance has already been created. */
	static void rotateISR();

private:
	/*
	counts a tipping event. */
	void impulse();

	/*
	copies value of counts_current_ to counts_prev_ and resets the count rotate timer. */
	void rotate();

	/*
	interrupt service routine (count) for the tipping bucket rain rate sensor. */
	static void isr();

	uint8_t pin_;									//input pin. must be able to attach an interrupt to.

	uint16_t software_debounce_delay_;				//minimum time between successive impulses, in us.

	uint32_t count_period_;							//count rotate interval (measurement period) in ms.

	float mm_per_count_;							//mm of rain per count.

	volatile uint32_t impulse_last_;				//time (in us) of last impulse. necessary for debouncing.

	volatile uint32_t counts_current_;				//counter for the current interval
	volatile uint32_t counts_last_;					//counter for the previous interval

	volatile uint32_t count_start_last_;			//time since last count rotate, in ms.

	volatile uint32_t count_period_last_;			//true value for last measurement interval, in ms.

	static SensorRainRateTippingBucket *instance_;	//pointer to SensorRainRateTippingBucket instance. only one instance can be created.
};

#endif /* SENSOR_RAIN_RATE_TIPPING_BUCKET_ */