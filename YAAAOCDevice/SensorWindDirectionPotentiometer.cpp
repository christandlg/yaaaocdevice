//Wind Direction sensor (potentiometer based) class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "SensorWindDirectionPotentiometer.h"

SensorWindDirectionPotentiometer::SensorWindDirectionPotentiometer(YAAASensor::SensorSettings sensor_settings) :
	SensorWindDirection(sensor_settings),
	pin_(255),
	analog_min_(DEF_SENSOR_WIND_DIRECTION_POTENTIOMETER_ANALOG_MIN),
	analog_max_(DEF_SENSOR_WIND_DIRECTION_POTENTIOMETER_ANALOG_MAX),
	angle_offset_(DEF_SENSOR_WIND_DIRECTION_ANGLE_OFFSET),
	analog_value_(0)
{
	pin_ = sensor_settings.parameters_[0];

	analog_min_ = (sensor_settings.parameters_[2] << 8 | sensor_settings.parameters_[3]);
	analog_max_ = (sensor_settings.parameters_[4] << 8 | sensor_settings.parameters_[5]);

	int16_t angle_offset = (sensor_settings.parameters_[6] << 8 | sensor_settings.parameters_[7]);
	angle_offset_ = static_cast<float>(angle_offset) / 32767.0f * 180.0f;
}


SensorWindDirectionPotentiometer::~SensorWindDirectionPotentiometer()
{
}

bool SensorWindDirectionPotentiometer::begin()
{
	//check if the input pin is connected to an ADC.
#ifdef ARDUINO_SAM_DUE
	if ((pin_ < 54) || pin_ > 65)	//Arduino Due: pins 54 to 65 are analog input pins
#elif defined ARDUINO_AVR_MEGA2560		//__AVR_ATmega2560__
	if ((pin_ < 54) || pin_ > 69)	//Arduino Mega: pins 54 to 69 are analog input pins
#elif defined ESP8266	//__ESP8266_ESP8266__
	if (pin_ != A0)					//ESP8266: only 1 analog input pin is available.
#elif defined ESP32	
	if (pin_ != A0 &&
		pin_ != A4 &&
		pin_ != A5 &&
		pin_ != A6 &&
		pin_ != A7 &&
		pin_ != A10 &&
		pin_ != A11 &&
		pin_ != A12 &&
		pin_ != A13 &&
		pin_ != A14 &&
		pin_ != A15 &&
		pin_ != A16 &&
		pin_ != A17 &&
		pin_ != A18 &&
		pin_ != A19)
#else
	if (true)
#endif
		return false;

	pinMode(pin_, INPUT);

	return true;
}

bool SensorWindDirectionPotentiometer::measure()
{
	analog_value_ = analogRead(pin_);

	updateMeasurementTime();

	//it's not possible to check if the value is meaningful beyond the pin number check that is 
	//already done in begin(), so lets assume the value is correct.
	return true;
}

bool SensorWindDirectionPotentiometer::hasValue()
{
	//if the minimum analog value is not 0 we can check if any measurement has been done by checking
	//the analog value against 0. 
	return (analog_min_ != 0 ? analog_value_ != 0 : true);
}

float SensorWindDirectionPotentiometer::getValue()
{
	//map analog read value to 0.0 ... 360.0 degree angle
	float angle = static_cast<float>(map(analog_value_, analog_min_, analog_max_, 0, 36000)) / 100.0f;	//doing this way to avoid losing resolution

	//apply offset
	angle += angle_offset_;

	//wrap angle back around into 0.0f ... 360.0f range
	angle = angle - 360.0f * floor(angle / 360.0f);

	return angle;
}
