//Humidity sensor class.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef SENSOR_HUMIDITY_H_
#define SENSOR_HUMIDITY_H_

#include <Arduino.h>

#include "YAAASensor.h"


class SensorHumidity:
	public YAAASensor
{
public:
	enum sensor_device_t : uint8_t
	{
		//DEVICE_ANALOG = 0,		//generic device that outputs an analog voltage that corresponds to the barometric pressure.
		DEVICE_DHT = 1,			//measurement device is a DHT11 or DHT22
		DEVICE_HTU21 = 2,		//measurement device is a HTU21
		DEVICE_BMx280 = 3,		//measurement device is a BME280
		DEVICE_UNKNOWN = 255
	};

	enum result_unit_t : uint8_t
	{
		UNIT_RELATIVE = 0,		//current absolute relative to maximum absolute for this temperature
		UNIT_ABSOLUTE = 1,		//g / m^3
		UNIT_SPECIFIC = 2,		//ratio mass of water vapor to total mass of measurement volume
	};

	SensorHumidity(YAAASensor::SensorSettings sensor_settings);

	~SensorHumidity();

	/*
	initializes the temperature sensor with the info given in in the constructor call.
	@return true on success, false otherwise. */
	virtual bool begin() = 0;

	/*
	starts a measurement.
	@return true on success, false otherwise.*/
	virtual bool measure() = 0;

	/*
	checks if a measurement has been completed. may perform work necessary to complete a mesurement. user is
	expected to call this function after calling measure() and call it until it returns true.
	@return true if a measurement has been completed. */
	virtual bool hasValue() = 0;

	/*
	returns the last measured value.
	user is expected to call this function only after hasValue() has returned true.
	@return the barometric pressure in the given unit. */
	virtual float getValue() = 0;

	uint8_t getSensorType();

	/*
	@return pressure unit as result_unit_t. */
	uint8_t getUnit();

	/*
	@param unit desired unit as result_unit_t.
	@return true on success, false otherwise. */
	bool setUnit(uint8_t unit);

protected:
	///*
	//converts a value from one unit to another.
	//@param pressure value to convert
	//@param unit_src unit of value
	//@param unit_dst desired unit
	//@return converted pressure in unit unit_dst */
	//float convertUnit(float humidity, uint8_t unit_src, uint8_t unit_dst);

	uint8_t unit_;

private:
};

#endif /* SENSOR_HUMIDITY_H_ */
