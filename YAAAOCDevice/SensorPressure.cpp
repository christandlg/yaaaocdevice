//Air Pressure sensor class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "SensorPressure.h"

SensorPressure::SensorPressure(YAAASensor::SensorSettings sensor_settings) :
	YAAASensor(sensor_settings),
	pressure_unit_(DEF_SENSOR_PRESSURE_UNIT)
{
	setUnit(sensor_settings.unit_);
}

SensorPressure::~SensorPressure()
{
	//nothing to do here...
}

uint8_t SensorPressure::getSensorType()
{
	return YAAASensor::SENSOR_PRESSURE;
}

uint8_t SensorPressure::getUnit()
{
	return pressure_unit_;
}

bool SensorPressure::setUnit(uint8_t unit)
{
	//check if the given unit is one of the valid units:
	switch (unit)
	{
		case SensorPressure::UNIT_PASCAL:
		case SensorPressure::UNIT_HECTOPASCAL:
		case SensorPressure::UNIT_BAR:
		case SensorPressure::UNIT_PSI:
		case SensorPressure::UNIT_ATM:
		case SensorPressure::UNIT_TORR:
		case SensorPressure::UNIT_INHG:
			pressure_unit_ = unit;
			return true;
		default:	//if an invalid unit is given, break the switch statement and return false
			break;
	}

	return false;
}

float SensorPressure::convertPressure(float pressure, uint8_t unit_src, uint8_t unit_dst)
{
	if (unit_src == unit_dst)
		return pressure;

	//convert pressure to Pascal
	switch (unit_src)
	{
		case SensorPressure::UNIT_HECTOPASCAL:
			pressure *= 100.0f;
			break;
		case SensorPressure::UNIT_BAR:
			pressure *= 100000.0f;
			break;
		case SensorPressure::UNIT_PSI:
			pressure *= 6894.757f;
			break;
		case SensorPressure::UNIT_ATM:
			pressure *= 101325.0f;
			break;
		case SensorPressure::UNIT_TORR:
			pressure *= 133.3f;
			break;
		case SensorPressure::UNIT_INHG:
			pressure *= 3386.389f;
			break;
		default:
			break;
	}

	//convert pressure to requested unit
	switch (unit_dst)
	{
		case SensorPressure::UNIT_HECTOPASCAL:
			pressure /= 100.0f;
			break;
		case SensorPressure::UNIT_BAR:
			pressure /= 100000.0f;
			break;
		case SensorPressure::UNIT_PSI:
			pressure /= 6894.757f;
			break;
		case SensorPressure::UNIT_ATM:
			pressure /= 101325.0f;
			break;
		case SensorPressure::UNIT_TORR:
			pressure /= 133.3f;
			break;
		case SensorPressure::UNIT_INHG:
			pressure /= 3386.389f;
			break;
		default:
			break;
	}

	return pressure;
}
