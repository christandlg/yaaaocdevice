//Brightness sensor (BH1750) class.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef SENSOR_AMBIENT_LIGHT_BH1750_H_
#define SENSOR_AMBIENT_LIGHT_BH1750_H_

#include "SensorBrightness.h"

#include <Arduino.h>

#include <Wire.h>

#include "defaults.h"

class SensorBrightnessBH1750 :
	public SensorBrightness
{
public:
	SensorBrightnessBH1750(YAAASensor::SensorSettings sensor_settings);

	~SensorBrightnessBH1750();

	bool begin();

	/*
	starts a measurement.
	@return true on success, false otherwise. */
	bool measure();

	/*
	checks if a measurement has been completed. may perform work necessary to complete a mesurement. user is
	expected to call this function after calling measure() and call it until it returns true.
	@return true if a measurement has been completed. */
	bool hasValue();

	/*
	returns the last measured value.
	user is expected to call this function only after hasValue() has returned true.
	@return brightness. */
	float getValue();

private:
	enum measurement_mode_t : uint8_t
	{
		MODE_CONTINUOUS_H = 0x10,			//continuous, 1lx resolution
		MODE_CONTINUOUS_H2 = 0x11,			//continuous, 0.5lx resolution
		MODE_CONTINUOUS_L = 0x13,			//continuous, 4lx resolution
		MODE_SINGLE_H = 0x20,			//one time, 1lx resolution
		MODE_SINGLE_H2 = 0x21,			//one time, 0.5lx resolution
		MODE_SINGLE_L = 0x23,			//one time, 4lx resolution
	};

	//typical measurement times for different resolution modes, in ms.
	enum measurement_time_t : uint32_t
	{
		MEASUREMENT_TIME_H = 120,
		MEASUREMENT_TIME_H2 = 120,
		MEASUREMENT_TIME_L = 16
	};

	enum mt_reg_t : uint8_t
	{
		MT_REG_MIN = 0x1F,			//minimum value for measurement time register
		MT_REG_DEF = 0x45,			//default value for measurement time register
		MT_REG_MAX = 0xFE,			//maximum value for measurement time register
	};

	enum measurement_state_t : uint8_t
	{
		STATE_IDLE,				//not currently measuring
		STATE_LRES,				//using L-res mode to decide which H-res mode and measurement time to use
		STATE_HRES,				//using H-res mode for accurate reading
		STATE_DONE				//measurement done, can fetch value
	};

	/*
	@return maximum estimated measurement time (in ms) at current mesaurement mode and mesurement time
	settings. value is calculated based on typical measurement times and further adjusted by a factor
	of 1.5f to get estimated maximum measurement time. */
	void updateMeasurementWaitTime();

	/*
	reads the measurement result from the sensor. 
	@return raw value (counts). */
	uint16_t readMeasurementResult();

	/*
	sends a measurement command to the sensor.
	@return true on success, false otherwise */
	bool sendMeasurementCommand();

	/*
	sets one of measurement_mode_t measurement modes.
	@param measurement mode as measurement_mode_t
	@return true on success, false otherwise */
	bool setMeasurementMode(uint8_t mode);

	/*
	sets the sensor's measurement time. higher measurement time leads to higher sensitivity of sensor. 
	@param time time setting. must be larger or equal to MT_REG_MIN and smaller or equal to MT_REG_MAX. 
		defaults to MT_REG_DEF. needs 2 I2C transmissions to execute.
	@return true on success, false otherwise*/
	bool setMeasuremenTime(uint8_t time);

	uint8_t i2c_address_;					//sensor I2C address (0x5C or 0x23)

	uint8_t measurement_mode_;				//sensor resolution setting (as resolution_mode_t) 

	uint8_t mt_reg_;						//measurement time register value

	float owtr_;							//optical window transmission rate [ 0...1 ]

	uint8_t measurement_state_;

	uint32_t time_measurement_last_;		//time of last measurement in ms.
	
	uint32_t time_measurement_wait_;		//time to wait between measurements.

	float result_;							//result of last successful measurement
};

#endif /* SENSOR_AMBIENT_LIGHT_BH1750_H_ */
