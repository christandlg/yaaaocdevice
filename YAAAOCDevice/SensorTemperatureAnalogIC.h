//Analog IC (LM35, TMP36, ...) Temperature sensor class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef SENSOR_TEMPERATURE_ANALOGIC_H_
#define SENSOR_TEMPERATURE_ANALOGIC_H_

#include "SensorTemperature.h"

class SensorTemperatureAnalogIC :
	public SensorTemperature
{
public:
	//SensorSettings::parameters layout for SensorTemperatureAnalogIC class:
	// - (1 byte) pin number 
	// - (1 byte) RFU 
	// - (2 byte) minimum temperature 
	// - (2 byte) RFU
	// - (2 byte) reference voltage in mV 
	// - (2 byte) temperature coefficient in mV/°C
	SensorTemperatureAnalogIC(YAAASensor::SensorSettings sensor_settings);

	~SensorTemperatureAnalogIC();

	/*
	initializes the temperature sensor with the info given in in the constructor call.
	@return true on success (input pin is an analog input pin), false otherwise. */
	bool begin();

	//reads the temperature sensor.
	//@return: true if measuring the temperature was successful, false otherwise.
	bool measure();

	/*
	@return true. */
	bool hasValue();

	//@return: the last measured temperature in the requested unit.
	float getValue();

private:
	uint8_t pin_;					//analog input pin the sensor is connected to

	int16_t zero_voltage_;			//output voltage at 0°C in mV.
	int16_t reference_voltage_;		//ADC reference voltage in mV

	int16_t coefficient_;			//the sensors temperature coefficient (i.e. 10mV/°C)

	int16_t analog_voltage_;		//analog voltage in mV.
};

#endif /* SENSOR_TEMPERATURE_ANALOG_H_ */

