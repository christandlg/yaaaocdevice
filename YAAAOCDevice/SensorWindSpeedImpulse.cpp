//Wind Speed sensor (impulse based) class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "SensorWindSpeedImpulse.h"

SensorWindSpeedImpulse *SensorWindSpeedImpulse::instance_ = NULL;

SensorWindSpeedImpulse::SensorWindSpeedImpulse(YAAASensor::SensorSettings sensor_settings) :
	SensorWindSpeed(sensor_settings),
	pin_(255),
	count_period_(DEF_SENSOR_WIND_SPEED_IMPULSE_COUNT_PERIOD),
	wind_speed_at_1Hz_(DEF_SENSOR_WIND_SPEED_IMPULSE_SPEED_1HZ),
	software_debounce_delay_(DEF_SENSOR_WIND_SPEED_IMPULSE_DEBOUNCE_DELAY),
	impulse_last_(0L),
	counts_current_(0),
	counts_last_(0),
	count_start_last_(0L),
	count_period_last_(1L)
{
	pin_ = sensor_settings.parameters_[0];

	uint16_t wind_speed_at_1Hz = (sensor_settings.parameters_[2] << 8 | sensor_settings.parameters_[3]);	//given here in mm / s
	wind_speed_at_1Hz_ = static_cast<float>(wind_speed_at_1Hz) / 1000.0f;

	count_period_ =
		(static_cast<uint32_t>(sensor_settings.parameters_[4]) << 24 |
			static_cast<uint32_t>(sensor_settings.parameters_[5]) << 16 |
			static_cast<uint32_t>(sensor_settings.parameters_[6]) << 8 |
			static_cast<uint32_t>(sensor_settings.parameters_[7]));

	software_debounce_delay_ = (sensor_settings.parameters_[8] << 8 | sensor_settings.parameters_[9]);
}

SensorWindSpeedImpulse::~SensorWindSpeedImpulse()
{
}

bool SensorWindSpeedImpulse::begin()
{
	//check if an ISR can be attached to the input pin.
#ifdef ARDUINO_SAM_DUE
	if (pin_ > 53)								//Arduino Due: all digital pins can be used as interrupt sources
#elif defined(ARDUINO_AVR_MEGA2560) //__AVR_ATmega2560__
	if ((pin_ != 2) && (pin_ != 3) && (pin_ != 18) && (pin_ != 19) && (pin_ != 20) && (pin_ != 21))		//Arduino Mega: pins 2, 3, 18, 19, 20, 21 can be used as interrupt sources
#elif defined(ESP8266) //__ESP8266_ESP8266__
	if ((pin_ == D0) || (pin_ > 16))		//ESP8266: all pins except D0 (GPIO16) can be used as interrupt surouces.
#else
	if (true)
#endif
		return false;

	//do nothing if an instance has already been created.
	if (instance_)
		return false;

	instance_ = this;

	pinMode(pin_, INPUT);

	//attach reed switch interrupt
	attachInterrupt(digitalPinToInterrupt(pin_), SensorWindSpeedImpulse::isr, FALLING);

	return true;
}

bool SensorWindSpeedImpulse::measure()
{
	return true;		//measurement is handled by interrupts
}

bool SensorWindSpeedImpulse::hasValue()
{
	return true;		//measurement is handled by interrupts
}

float SensorWindSpeedImpulse::getValue()
{
	//calculate average number of impulses per second based number of impulses counted during last count interval
	float avg_impulses = static_cast<float>(counts_last_) / static_cast<float>(count_period_last_) * 1000.0f;

	//calculate wind speed and return value
	return convertSpeed(
		wind_speed_at_1Hz_ * avg_impulses,
		SensorWindSpeed::SPEED_METERS_PER_SECOND,
		speed_unit_);
}

void SensorWindSpeedImpulse::rotateISR()
{
	if (!instance_)
		return;

	noInterrupts();

	instance_->rotate();

	instance_->updateMeasurementTime();

	interrupts();
}

void SensorWindSpeedImpulse::impulse()
{
	//count a pulse only if the software debounce delay has passed.
	if (micros() - impulse_last_ > software_debounce_delay_)
	{
		counts_current_++;
		impulse_last_ = micros();
	}
}

void SensorWindSpeedImpulse::rotate()
{
	if (millis() - count_start_last_ > count_period_)
	{
		count_period_last_ = millis() - count_start_last_;

		counts_last_ = counts_current_;
		counts_current_ = 0;

		count_start_last_ = millis();
	}
}

void SensorWindSpeedImpulse::isr()
{
	noInterrupts();

	if (instance_)
		instance_->impulse();

	interrupts();
}
