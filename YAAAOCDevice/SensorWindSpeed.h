//Wind Speed sensor class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef SENSOR_WIND_SPEED_H_
#define SENSOR_WIND_SPEED_H_

#include <Arduino.h>

#include "YAAASensor.h"

class SensorWindSpeed :
	public YAAASensor
{
public:
	enum sensor_device_t : uint8_t
	{
		DEVICE_ANALOG_VOLTAGE = 0,		//device generates a voltage that corresponds to wind speed
		DEVICE_IMPULSE = 1,				//device generates impulses whose interval corresponds to wind speed
		DEVICE_UNKOWN = 255
	};

	enum speed_unit_t : uint8_t
	{
		SPEED_METERS_PER_SECOND = 0,
		SPEED_KM_PER_HOUR = 1,
		SPEED_FEET_PER_SECOND = 2,
		SPEED_MILES_PER_HOUR = 3,
		SPEED_KNOTS = 4
	};

	SensorWindSpeed(YAAASensor::SensorSettings sensor_settings);

	~SensorWindSpeed();

	virtual bool begin() = 0;

	/*
	starts a measurement.
	@return true on success, false otherwise.*/
	virtual bool measure() = 0;

	/*
	checks if a measurement has been completed. may perform work necessary to complete a mesurement. user is
	expected to call this function after calling measure() and call it until it returns true.
	@return true if a measurement has been completed. */
	virtual bool hasValue() = 0;

	/*
	returns the last measured value.
	user is expected to call this function only after hasValue() has returned true.
	@return the barometric pressure in the given unit. */
	virtual float getValue() = 0;

	uint8_t getSensorType();

	uint8_t getUnit();

	bool setUnit(uint8_t unit);

protected:
	static float convertSpeed(float speed, uint8_t unit_src, uint8_t unit_dst);

	uint8_t speed_unit_;		//speed unit as speed_unit_t.

private:
};

#endif /* SENSOR_WIND_SPEED_H_ */