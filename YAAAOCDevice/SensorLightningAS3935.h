//AS3935 lightning sensor class.

#ifndef SENSOR_LIGHTNING_AS3935_H_
#define SENSOR_LIGHTNING_AS3935_H_

#include <Arduino.h>
#include <SPI.h>
#include <Wire.h>

#include "SensorLightning.h"

#include "AS3935MI.h"

#define NUM_EVENTS 10

class SensorLightningAS3935 : public SensorLightning
{
public:
	enum event_t : uint8_t
	{
		EVENT_NH = AS3935MI::AS3935_INT_NH,   //noise floor too high
		EVENT_D = AS3935MI::AS3935_INT_D,		//disturber 
		EVENT_L = AS3935MI::AS3935_INT_L,		//lightning
		EVENT_NA = 255						//event not available
	};

	enum interface_t : uint8_t
	{
		INTERFACE_I2C = 0,
		INTERFACE_SPI = 1
	};

	struct Event
	{
		uint8_t event_ : 2;       //event as event_t.
		uint8_t distance_ : 6;    //distance estimation. set to 0b111111 if event_ != EVENT_L.
		uint32_t time_;           //system time (in ms) of event.
	};

	SensorLightningAS3935(YAAASensor::SensorSettings sensor_settings);

	virtual ~SensorLightningAS3935();

	//initializes the sensor.
	bool begin();

	bool measure();

	bool hasValue();

	/*
	@return distance to storm front in km, or NAN if no lightning has been detected yet or the last detection lifetime has expired. */
	float getValue();

	/*
	@return true if the AS3935 signalled an interrupt happened by pulling the interrupt pin HIGH. */
	bool hasEvent();

	/*
	@return event as string in the format "interrupt_name data" */
	String handleEvent();

private:
	bool decreaseNoiseFloorThreshold();

	bool increaseNoiseFloorThreshold();

	/*
	decreases the lightning sensors sensitivity by adjusting either the watchdog threshold or the spike rejection ratio.
	@return true if the sensitivity was successfully adjusted, false otherwise (if the lowest possible sensitivity has already been reached) */
	bool decreaseSensitivity();

	/*
	increases the lighning sensors sensitivity by adjusting either the watchdog threshold or the spike rejection ratio.
	@return true if the sensitivity was successfully adjusted, false otherwise (if the highest possible sensitivity has already been reached) */
	bool increaseSensitivity();

	uint8_t interface_;											//interface as AS3935::AS3935::interface_t

	uint8_t chip_select_;										//chip select pin or I2C Address

	uint8_t pin_irq_;											//irq pin

	uint8_t afe_gb_;											//analog frontend gain 
	uint8_t tun_cap_;											//tuning capacitors settings
	uint8_t nf_lev_;											//noise floor level setting
	uint8_t wdth_;												//watchdog threshold
	uint8_t min_num_ligh_;										//minimum number of lightnings before a lightning event is registered
	uint8_t srej_;												//spike rejection setting
	bool mask_dist_;											//mask disturbers

	float distance_;							                //last recorded lightning distance
	uint32_t detection_last_;									//time of last lightning detection
	uint32_t detection_lifetime_;								//after a detection lifetime has exprired, getValue() will return NAN again

	uint32_t auto_adjust_last_;									//last auto adjust time
	uint32_t auto_adjust_interval_;								//last auto adjust interval

	static SPISettings spi_settings_;							//spi settings object. is the same for all AS3935 sensors

	volatile static bool interrupt_;							//true if the AS3935 pulled the IRQ pin high.
	volatile static uint32_t interrupt_time_;					//time of last interrupt. MCU must wait 2ms before querying the AS3935 after an interrupt

	static AS3935MI *as3935_;						          //only one sensor can be created. 

	static void as3935ISR();
};

#endif /* SENSOR_LIGHTNING_AS3935_H_ */

