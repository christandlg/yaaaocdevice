#ifndef YAAAHTU21D_H_
#define YAAAHTU21D_H_

#include <Arduino.h>
#include <Wire.h>

class YAAAHTU21D
{
public:
	enum measurement_resolution_t : uint8_t
	{
		RESOLUTION_12BIT_14BIT = 0b00000000,
		RESOLUTION_08BIT_12BIT = 0b00000001,
		RESOLUTION_10BIT_13BIT = 0b10000000,
		RESOLUTION_11BIT_11BIT = 0b10000001
	};

	const uint8_t BIT_HEATER = 0x04;

	YAAAHTU21D(uint8_t i2c_address);
	~YAAAHTU21D();

	/*
	@param (optional) soft_reset true to perform a soft reset of the sensor. defaults to false.
	@return true on success, false otherwise. 
	*/
	bool begin(bool soft_reset = false);

	/*
	starts a humidity measurement. 
	@return true on success, false otherwise. */
	bool measureHumidity();

	/*
	starts a temperature measurement.
	@return true on success, false otherwise. */
	bool measureTemperature();

	/*
	@return true if a humidity measurement has been completed, false otherwise. */
	bool hasHumidity();

	/*
	@return true if a temperature measurement has been completed, false otherwise. */
	bool hasTemperature();

	/*
	@return last measured humidity, in %RH. */
	float getHumidity();

	/*
	@return last measured temperature, in �C. */
	float getTemperature();

	/*
	@return measurement resolution as measurement_resolution_t. */
	uint8_t getMeasurementResolution();

	/*
	@param measurement resolution as measurement_resolution_t.
	@return true on success, false otherwise. */
	bool setMeasurementResolution(uint8_t resolution);

	/*
	@return true if the heater is enabled, false otherwise or if the check failed. */
	bool getHeater();

	/*
	The heater is intended to be used for functionality diagnosis: relative humidity drops upon rising temperature. 
	The heater consumes about 5.5mW and provides a temperature increase of about 0.5-1.5�C.
	@param true to enable, false to disable the heater
	@return true on success, false otherwise. */
	bool setHeater(bool enable);

	/*
	soft reset the sensor. the reset takes less than 15ms. 
	@param wait true: wait 15ms before return to allow sensor to reset. false: function returns immediately. */
	void softReset();

private:
	enum command_code_t : uint8_t
	{
		//MEASURE_TEMPERATURE_HOLD = 0xE3,		//start a temperature measurement, holding master. not supported.
		//MEASURE_HUMIDITY_HOLD = 0xE5,		//start a humidity measurement, holding master. not supported.
		MEASURE_TEMPERATURE = 0xF3,		//start a temperature measurement. 
		MEASURE_HUMIDITY = 0xF5,		//start a humidity measurement.
		REGISTER_WRITE = 0xE6,
		REGISTER_READ = 0xE7,
		RESET_SOFT = 0xFE
	};

	/*
	reads the user register. 
	@param (by reference) user register. 
	@return true on success, false otherwise. */
	bool readUserRegister(uint8_t &reg);

	/*
	writes the user register. 
	@param user register.
	@return true on success, false otherwise. */
	bool writeUserRegister(uint8_t reg);

	uint8_t i2c_address_;

	uint16_t value_humidity_;		//last measured humidity value
	uint16_t value_temperature_;	//last measured temperature value
};

#endif /* YAAAHTU21D_H_ */