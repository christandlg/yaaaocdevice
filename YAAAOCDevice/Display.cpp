//Display Class for YAAADevice. 
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "Display.h"

Display::Display() :
features_(0)
{
}

Display::~Display() 
{
}

void Display::write(uint16_t pos, uint16_t line, float message, uint8_t dec)
{
	printMessage(pos, line, String(message, dec));
}

bool Display::checkFeature(uint32_t feature)
{
	//check if the given feature bit is set for this device.
	if ((feature && features_) != 0)
		return true;

	else return false;
}

bool Display::addCustomChar(uint8_t index, uint8_t* character)
{
	return false;
}

bool Display::printCustomChar(uint16_t pos, uint16_t line, byte index)
{
	return false;
}

uint8_t Display::getBrightness()
{
	return 0;
}

uint8_t Display::getContrast()
{
	return 0;
}

bool Display::setBrightness(uint8_t value)
{
	return false;
}

bool Display::setContrast(uint8_t value)
{
	return false;
}

bool Display::getResolution(uint16_t &x, uint16_t &y)
{
	x = 0;
	y = 0;
	return false;
}

void Display::setFeature(uint32_t feature)
{
	features_ |= feature;
}
