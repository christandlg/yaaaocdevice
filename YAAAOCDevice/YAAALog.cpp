//Log class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "YAAALog.h"

uint8_t YAAALog::sd_state_ = YAAALog::SD_NOT_INITIALIZED;

YAAALog::YAAALog(String file_name) :
	log_state_(YAAALog::LOG_NOT_INITIALIZED),
	file_name_(file_name)
{
	//nothing to do here...
}

YAAALog::~YAAALog()
{
}

bool YAAALog::initSD(uint8_t chip_select_pin)
{
	if (sd_state_ == YAAALog::SD_INITIALIZED)
		return true;

	if (sd_state_ == YAAALog::SD_ERROR)
		return false;

	if (SD.begin(chip_select_pin))
		sd_state_ = YAAALog::SD_INITIALIZED;
	else
	{
		sd_state_ = YAAALog::SD_ERROR;
		return false;
	}

	return true;
}

bool YAAALog::begin()
{
	if (sd_state_ == YAAALog::SD_NOT_INITIALIZED)
	{
		log_state_ = YAAALog::LOG_NOT_INITIALIZED;
		return false;
	}

	//return if the SD card could not be initialized 
	if (sd_state_ == YAAALog::SD_ERROR)
	{
		log_state_ = YAAALog::LOG_ERROR_SD;
		return false;
	}

	//do nothing if this instance has been initialized before. 
	if ((log_state_ > YAAALog::LOG_DISABLED) ||
		(log_state_ == YAAALog::LOG_ENABLED))
		return true;

	file_ = File(SD.open(file_name_, FILE_WRITE));

	if (!file_)
	{
		log_state_ = YAAALog::LOG_ERROR_FILE;
		return false;
	}

	log_state_ = YAAALog::LOG_DISABLED;

	return true;
}

bool YAAALog::setLogMode(bool enable)
{
	if (sd_state_ != YAAALog::SD_INITIALIZED)
		return false;

	//do not do anything as long as this intance has not been initialized
	if (log_state_ <= YAAALog::LOG_NOT_INITIALIZED)
		return false;

	//when logging is to be enalbed, attempt to open and write a timestamp
	//to the log file. if it fails, leave logging disabled and return 
	//false.
	if (enable) {
		//return true if logging is already enabled.
		if (log_state_ == YAAALog::LOG_ENABLED)
			return true;

		log_state_ = YAAALog::LOG_ENABLED;

		log("Log Start");
	}

	//when logging is to be disabled, attempt to open and write a timestamp 
	//to the log file. regardless of success, disable logging.
	else {
		//return true if logging is already disabled.
		if (log_state_ == YAAALog::LOG_DISABLED)
			return true;

		log("Log End");

		log_state_ = YAAALog::LOG_DISABLED;
	}

	return true;
}

bool YAAALog::getLogMode()
{
	return (log_state_ == YAAALog::LOG_ENABLED);
}

uint8_t YAAALog::getLogState()
{
	return log_state_;
}

bool YAAALog::hasLine()
{
	if (log_state_ <= YAAALog::LOG_NOT_INITIALIZED)
		return false;

	if (!file_)
		return false;

	return file_.available();
}

String YAAALog::getLine()
{
	String line = "";

	if (file_)
		for (uint8_t i = 0; i < LINE_LENGTH; i++)
		{
			char character = file_.read();

			line += character;

			if (character == '\n')
				break;
		}

	return line;
}

void YAAALog::startDump()
{
	file_.seek(0L);
}

void YAAALog::stopDump()
{
	file_.seek(file_.size());
}

bool YAAALog::clearLog()
{
	if (!file_)
		return false;

	file_.close();

	if (!SD.remove(file_name_))
		return false;

	file_ = File(SD.open(file_name_, FILE_WRITE));

	if (!file_)
	{
		log_state_ = YAAALog::LOG_ERROR_FILE;
		return false;
	}

	return true;
}
