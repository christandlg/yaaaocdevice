//Wind Speed sensor (analog voltage based) class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef SENSOR_WIND_SPEED_ANALOG_VOLTAGE_
#define SENSOR_WIND_SPEED_ANALOG_VOLTAGE_

#include <Arduino.h>

#include "SensorWindSpeed.h"

class SensorWindSpeedAnalogVoltage :
	public SensorWindSpeed
{
public:
	SensorWindSpeedAnalogVoltage(YAAASensor::SensorSettings sensor_settings);

	~SensorWindSpeedAnalogVoltage();

	/*
	@return true if the given input pin is an analog input pin, false otherwise. */
	bool begin();

	/*
	performs a measurement. 
	@return true */
	bool measure();

	/*
	@return true. */
	bool hasValue();

	/*
	@return wind speed in m/s. */
	float getValue();

private:
	uint8_t pin_;

	float analog_baseline_;		//analog voltage at 0 m/s in V.

	float analog_reference_;	//analog reference voltage in V.

	float slope_;				//slope of sensor's analog voltage output in m/s/V

	uint16_t analog_value_;		//mesured analog value.
};

#endif /* SENSOR_WIND_SPEED_ANALOG_VOLTAGE_ */