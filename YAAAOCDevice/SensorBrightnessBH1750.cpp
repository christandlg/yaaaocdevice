//Brightness sensor (BH1750) class.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "SensorBrightnessBH1750.h"

SensorBrightnessBH1750::SensorBrightnessBH1750(YAAASensor::SensorSettings sensor_settings) :
	SensorBrightness(sensor_settings),
	i2c_address_(DEF_SENSOR_BRIGHTNESS_BH1750_I2C_ADDRESS),		//0x5C or 0x23
	measurement_mode_(DEF_SENSOR_BRIGHTNESS_BH1750_MEASUREMENT_MODE),
	mt_reg_(DEF_SENSOR_BRIGHTNESS_BH1750_MTREG),
	owtr_(DEF_SENSOR_BRIGHTNESS_BH1750_OWTR),
	measurement_state_(SensorBrightnessBH1750::STATE_IDLE),
	time_measurement_last_(0L),
	time_measurement_wait_(0L)
{
	i2c_address_ = sensor_settings.parameters_[0];

	owtr_ = static_cast<float>(sensor_settings.parameters_[2] << 8 | sensor_settings.parameters_[3]) / 65535.0f;
}

SensorBrightnessBH1750::~SensorBrightnessBH1750()
{
}

bool SensorBrightnessBH1750::begin()
{
	Wire.beginTransmission(i2c_address_);

	//send power on command. not necessary for operation, only used as a check
	Wire.write(0x01);

	if (Wire.endTransmission() != 0)
		return false;

	if (!setMeasurementMode(SensorBrightnessBH1750::MODE_SINGLE_L))
		return false;

	if (!setMeasuremenTime(MT_REG_DEF))
		return false;

	updateMeasurementWaitTime();
	
	return true;
}

bool SensorBrightnessBH1750::measure()
{
	//set measurement mode to default value
	if (!setMeasurementMode(SensorBrightnessBH1750::MODE_SINGLE_L))
		return false;

	//reset measurement time to default value
	if (!setMeasuremenTime(SensorBrightnessBH1750::MT_REG_DEF))
		return false;

	//start measurement
	if (!sendMeasurementCommand())
		return false;

	measurement_state_ = SensorBrightnessBH1750::STATE_LRES;

	updateMeasurementTime();

	return true;
}

bool SensorBrightnessBH1750::hasValue()
{
	//return true if teh measurement wait time has not yet elapsed.
	if (millis() - time_measurement_last_ < time_measurement_wait_)
		return false;

	switch (measurement_state_)
	{
		//use low resolution mode to decide which resolution mode and measurement time setting to use 
		//for measurement
		case STATE_LRES:
		{
			uint16_t value_lres = readMeasurementResult();

			if (value_lres > 0xC000)		//approx. 40k lx
			{
				//use H-res with minimum measurement time
				setMeasuremenTime(SensorBrightnessBH1750::MT_REG_MIN);
				setMeasurementMode(SensorBrightnessBH1750::MODE_SINGLE_H);
			}
			else if (value_lres < 0x400)	//approx. 850lx
			{
				//use L-res with maximum measurement time
				setMeasuremenTime(SensorBrightnessBH1750::MT_REG_MAX);
				setMeasurementMode(SensorBrightnessBH1750::MODE_SINGLE_H2);
			}
			else
			{
				//use H-res with default integration time
				setMeasuremenTime(SensorBrightnessBH1750::MT_REG_DEF);
				setMeasurementMode(SensorBrightnessBH1750::MODE_SINGLE_H);
			}

			if (sendMeasurementCommand())
				measurement_state_ = SensorBrightnessBH1750::STATE_HRES;

			//go back to IDLE state if starting a new measurement failed.
			else
				measurement_state_ = SensorBrightnessBH1750::STATE_IDLE;
		}
		break;
		case STATE_HRES:
		{
			result_ = static_cast<float>(readMeasurementResult());

			result_ /= 1.2f;

			//when using H2 mode, LSB is numerical value 2^-1 (not 2^0 as in H and L modes)
			if (measurement_mode_ == MODE_CONTINUOUS_H2 ||
				measurement_mode_ == MODE_SINGLE_H2)
				result_ /= 2.0f;

			//adjust for measurement time 
			result_ *= static_cast<float>(MT_REG_DEF) / static_cast<float>(mt_reg_);

			//adjustment for optical window transmission rate
			result_ /= owtr_;

			measurement_state_ = SensorBrightnessBH1750::STATE_DONE;
			return true;
		}
		case STATE_DONE:
			return true;
		default:
			break;
	}

	return false;
}

float SensorBrightnessBH1750::getValue()
{
	return SensorBrightness::convertIlluminance(
		result_,
		SensorBrightness::UNIT_LUX, 
		unit_);
}

void SensorBrightnessBH1750::updateMeasurementWaitTime()
{
	float measurement_time = 0.0f;

	//get typical measurement time for the selected resolution mode
	switch (measurement_mode_)
	{
		case MODE_CONTINUOUS_L:
		case MODE_SINGLE_L:
			measurement_time = static_cast<float>(MEASUREMENT_TIME_L);
			break;
		case MODE_CONTINUOUS_H:
		case MODE_SINGLE_H:
			measurement_time = static_cast<float>(MEASUREMENT_TIME_H);
			break;
		case MODE_CONTINUOUS_H2:
		case MODE_SINGLE_H2:
			measurement_time = static_cast<float>(MEASUREMENT_TIME_H2);
			break;
	}

	//adjust typical measurement time by factor inferred from data sheet
	//H-resolution mode2 typical: 120ms, H-resolution mode2 maximum: 180ms)
	measurement_time *= 1.5f;

	//adjust measurement time by value of measurement time register
	measurement_time *= (static_cast<float>(mt_reg_) / static_cast<float>(MT_REG_DEF));

	measurement_time = ceil(measurement_time);

	time_measurement_wait_ = static_cast<uint32_t>(measurement_time);
}

uint16_t SensorBrightnessBH1750::readMeasurementResult()
{
	uint16_t return_value = 0;

	Wire.requestFrom(i2c_address_, static_cast<uint8_t>(2));

	return_value = Wire.read() << 8;
	return_value |= Wire.read();

	return return_value;
}

bool SensorBrightnessBH1750::sendMeasurementCommand()
{
	Wire.beginTransmission(i2c_address_);

	Wire.write(measurement_mode_);

	if (Wire.endTransmission() != 0)
		return false;

	time_measurement_last_ = millis();

	updateMeasurementWaitTime();

	return true;
}

bool SensorBrightnessBH1750::setMeasurementMode(uint8_t mode)
{
	switch (mode)
	{
		case SensorBrightnessBH1750::MODE_CONTINUOUS_H:
		case SensorBrightnessBH1750::MODE_CONTINUOUS_H2:
		case SensorBrightnessBH1750::MODE_CONTINUOUS_L:
		case SensorBrightnessBH1750::MODE_SINGLE_H:
		case SensorBrightnessBH1750::MODE_SINGLE_H2:
		case SensorBrightnessBH1750::MODE_SINGLE_L:
			measurement_mode_ = mode;
			return true;
		default:
			break;
	}

	return false;
}

bool SensorBrightnessBH1750::setMeasuremenTime(uint8_t time)
{
	if ((time < MT_REG_MIN) || (time > MT_REG_MAX))
		return false;

	//if (time == mt_reg_)
	//	return true;

	//change measurement time (high bit)
	Wire.beginTransmission(i2c_address_);

	Wire.write(0x40 | (0xE0 & time) >> 5);		//write 01000_MT[7,6,5]

	if (Wire.endTransmission() != 0)
		return false;

	//change measurement time (low bit)
	Wire.beginTransmission(i2c_address_);

	Wire.write(0x60 | (time & 0x1F));	//write 011_MT[4,3,2,1,0]

	if (Wire.endTransmission() != 0)
		return false;

	mt_reg_ = time;

	return true;
}
