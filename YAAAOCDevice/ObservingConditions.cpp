//ObservingConditions class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "ObservingConditions.h"

ObservingConditions::ObservingConditions() :
	sensor_counter_(0),
	heater_counter_(0),
	average_period_(DEF_SENSOR_AVERAGE_PERIOD),

	log_sensors_(NULL),
	log_delimiter_(DEF_SENSOR_LOG_DELIMITER),
	log_interval_(DEF_SENSOR_LOG_INTERVAL),
	log_last_(0L)
{
	for (uint8_t i = 0; i < NUM_CONDITIONS; i++) {
		sensors_[i] = NULL;
		automatic_measurement_configuration_[i] = NULL;
	}

	for (uint8_t i = 0; i < NUM_HEATERS; i++)
		heaters_[i] = NULL;
}

ObservingConditions::~ObservingConditions()
{
	for (uint8_t i = 0; i < NUM_CONDITIONS; i++)
	{
		if (sensors_[i]) {
			delete sensors_[i];
			sensors_[i] = NULL;
		}

		if (automatic_measurement_configuration_[i])
		{
			if (automatic_measurement_configuration_[i]->average_array_) {
				delete[] automatic_measurement_configuration_[i]->average_array_;
				automatic_measurement_configuration_[i]->average_array_ = NULL;
			}

			delete automatic_measurement_configuration_[i];
			automatic_measurement_configuration_[i] = NULL;
		}
	}

	for (uint8_t i = 0; i < NUM_HEATERS; i++)
	{
		if (heaters_[i])
		{
			delete heaters_[i];
			heaters_[i] = NULL;
		}
	}
}

bool ObservingConditions::begin()
{
	bool return_value = true;

	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	//read sensor settings from EEPROM
	ObservingConditions::SensorConfiguration sensor_configuration;

	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::OBSERVING_CONDITIONS,
		EEPROMHandler::observing_conditions_variable_::SENSOR_CONFIGURATION,
		ObservingConditions::CONDITION_CLOUD_COVER,
		sensor_configuration);

	//both sensor type and sensor device must be valid 
	if (sensor_configuration.sensor_settings_.sensor_type_ == YAAASensor::sensor_type_t::SENSOR_CLOUD_COVERAGE)
	{
		switch (sensor_configuration.sensor_settings_.sensor_type_)
		{
		default:
			break;
		}

		if (!sensors_[CONDITION_CLOUD_COVER])
			return_value = false;

		else if (!sensors_[CONDITION_CLOUD_COVER]->begin())
		{
			delete sensors_[CONDITION_CLOUD_COVER];
			sensors_[CONDITION_CLOUD_COVER] = NULL;
			return_value = false;
		}

		else if (!beginAutomaticMeasurements(CONDITION_CLOUD_COVER, sensor_configuration))
			return_value = false;
	}

	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::OBSERVING_CONDITIONS,
		EEPROMHandler::observing_conditions_variable_::SENSOR_CONFIGURATION,
		ObservingConditions::CONDITION_DEW_POINT,
		sensor_configuration);

	//both sensor type and sensor device must be valid 
	if (sensor_configuration.sensor_settings_.sensor_type_ == YAAASensor::sensor_type_t::SENSOR_DEW_POINT)
	{
		switch (sensor_configuration.sensor_settings_.sensor_type_)
		{
		default:
			break;
		}

		if (!sensors_[CONDITION_DEW_POINT])
			return_value = false;

		else if (!sensors_[CONDITION_DEW_POINT]->begin())
		{
			delete sensors_[CONDITION_DEW_POINT];
			sensors_[CONDITION_DEW_POINT] = NULL;
			return_value = false;
		}

		else if (!beginAutomaticMeasurements(CONDITION_DEW_POINT, sensor_configuration))
			return_value = false;
	}

	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::OBSERVING_CONDITIONS,
		EEPROMHandler::observing_conditions_variable_::SENSOR_CONFIGURATION,
		ObservingConditions::CONDITION_HUMIDITY,
		sensor_configuration);

	//both sensor type and sensor device must be valid 
	if (sensor_configuration.sensor_settings_.sensor_type_ == YAAASensor::sensor_type_t::SENSOR_HUMIDITY)
	{
		switch (sensor_configuration.sensor_settings_.sensor_device_)
		{
		case SensorHumidity::DEVICE_DHT:
			sensors_[CONDITION_HUMIDITY] = new SensorHumidityDHT(sensor_configuration.sensor_settings_);
			break;
		case SensorHumidity::DEVICE_HTU21:
			sensors_[CONDITION_HUMIDITY] = new SensorHumidityHTU21(sensor_configuration.sensor_settings_);
			break;
		case SensorHumidity::DEVICE_BMx280:
			sensors_[CONDITION_HUMIDITY] = new SensorHumidityBME280(sensor_configuration.sensor_settings_);
			break;
		default:
			break;
		}

		if (!sensors_[CONDITION_HUMIDITY])
			return_value = false;

		else if (!sensors_[CONDITION_HUMIDITY]->begin())
		{
			delete sensors_[CONDITION_HUMIDITY];
			sensors_[CONDITION_HUMIDITY] = NULL;
			return_value = false;
		}

		else if (!beginAutomaticMeasurements(CONDITION_HUMIDITY, sensor_configuration))
			return_value = false;
	}

	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::OBSERVING_CONDITIONS,
		EEPROMHandler::observing_conditions_variable_::SENSOR_CONFIGURATION,
		ObservingConditions::CONDITION_PRESSURE,
		sensor_configuration);

	//skip initialization if the stored configuration does not indicate a pressure sensor
	if (sensor_configuration.sensor_settings_.sensor_type_ == YAAASensor::sensor_type_t::SENSOR_PRESSURE)
	{
		switch (sensor_configuration.sensor_settings_.sensor_device_)
		{
		case SensorPressure::DEVICE_BMP180:
			sensors_[CONDITION_PRESSURE] = new SensorPressureBMP180(sensor_configuration.sensor_settings_);
			break;
		case SensorPressure::DEVICE_BMx280:
			sensors_[CONDITION_PRESSURE] = new SensorPressureBMx280(sensor_configuration.sensor_settings_);
			break;
		default:
			break;
		}

		if (!sensors_[CONDITION_PRESSURE])
		{
			return_value = false;
		}

		else if (!sensors_[CONDITION_PRESSURE]->begin())
		{
			delete sensors_[CONDITION_PRESSURE];
			sensors_[CONDITION_PRESSURE] = NULL;
			return_value = false;
		}

		else if (!beginAutomaticMeasurements(CONDITION_PRESSURE, sensor_configuration))
			return_value = false;
	}

	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::OBSERVING_CONDITIONS,
		EEPROMHandler::observing_conditions_variable_::SENSOR_CONFIGURATION,
		ObservingConditions::CONDITION_RAIN,
		sensor_configuration);

	//both sensor type and sensor device must be valid 
	if (sensor_configuration.sensor_settings_.sensor_type_ == YAAASensor::sensor_type_t::SENSOR_RAIN)
	{
		switch (sensor_configuration.sensor_settings_.sensor_device_)
		{
		case SensorRain::DEVICE_FC37:
			sensors_[CONDITION_RAIN] = new SensorRainFC37(sensor_configuration.sensor_settings_);
			break;
		default:
			break;
		}

		if (!sensors_[CONDITION_RAIN])
			return_value = false;

		else if (!sensors_[CONDITION_RAIN]->begin())
		{
			delete sensors_[CONDITION_RAIN];
			sensors_[CONDITION_RAIN] = NULL;
			return_value = false;
		}

		else if (!beginAutomaticMeasurements(CONDITION_RAIN, sensor_configuration))
			return_value = false;
	}

	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::OBSERVING_CONDITIONS,
		EEPROMHandler::observing_conditions_variable_::SENSOR_CONFIGURATION,
		ObservingConditions::CONDITION_RAIN_RATE,
		sensor_configuration);

	//both sensor type and sensor device must be valid 
	if (sensor_configuration.sensor_settings_.sensor_type_ == YAAASensor::sensor_type_t::SENSOR_RAIN_RATE)
	{
		switch (sensor_configuration.sensor_settings_.sensor_device_)
		{
		case SensorRainRate::DEVICE_TIPPING_BUCKET:
			sensors_[CONDITION_RAIN_RATE] = new SensorRainRateTippingBucket(sensor_configuration.sensor_settings_);
			break;
		default:
			break;
		}

		if (!sensors_[CONDITION_RAIN_RATE])
			return_value = false;

		else if (!sensors_[CONDITION_RAIN_RATE]->begin())
		{
			delete sensors_[CONDITION_RAIN_RATE];
			sensors_[CONDITION_RAIN_RATE] = NULL;
			return_value = false;
		}

		else if (!beginAutomaticMeasurements(CONDITION_RAIN_RATE, sensor_configuration))
			return_value = false;
	}

	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::OBSERVING_CONDITIONS,
		EEPROMHandler::observing_conditions_variable_::SENSOR_CONFIGURATION,
		ObservingConditions::CONDITION_SKY_BRIGHTNESS,
		sensor_configuration);

	//both sensor type and sensor device must be valid 
	if (sensor_configuration.sensor_settings_.sensor_type_ == YAAASensor::sensor_type_t::SENSOR_BRIGHTNESS)
	{
		switch (sensor_configuration.sensor_settings_.sensor_device_)
		{
		case SensorBrightness::DEVICE_BH1750:
			sensors_[CONDITION_SKY_BRIGHTNESS] = new SensorBrightnessBH1750(sensor_configuration.sensor_settings_);
			break;
		default:
			break;
		}

		if (!sensors_[CONDITION_SKY_BRIGHTNESS])
			return_value = false;

		else if (!sensors_[CONDITION_SKY_BRIGHTNESS]->begin())
		{
			delete sensors_[CONDITION_SKY_BRIGHTNESS];
			sensors_[CONDITION_SKY_BRIGHTNESS] = NULL;
			return_value = false;
		}

		else if (!beginAutomaticMeasurements(CONDITION_SKY_BRIGHTNESS, sensor_configuration))
			return_value = false;
	}

	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::OBSERVING_CONDITIONS,
		EEPROMHandler::observing_conditions_variable_::SENSOR_CONFIGURATION,
		ObservingConditions::CONDITION_SKY_QUALITY,
		sensor_configuration);

	//both sensor type and sensor device must be valid 
	if (sensor_configuration.sensor_settings_.sensor_type_ == YAAASensor::sensor_type_t::SENSOR_SKY_QUALITY)
	{
		switch (sensor_configuration.sensor_settings_.sensor_device_)
		{
		default:
			break;
		}

		if (!sensors_[CONDITION_SKY_QUALITY])
			return_value = false;

		else if (!sensors_[CONDITION_SKY_QUALITY]->begin())
		{
			delete sensors_[CONDITION_SKY_QUALITY];
			sensors_[CONDITION_SKY_QUALITY] = NULL;
			return_value = false;
		}

		else if (!beginAutomaticMeasurements(CONDITION_SKY_QUALITY, sensor_configuration))
			return_value = false;
	}

	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::OBSERVING_CONDITIONS,
		EEPROMHandler::observing_conditions_variable_::SENSOR_CONFIGURATION,
		ObservingConditions::CONDITION_SKY_TEMPERATURE,
		sensor_configuration);

	//both sensor type and sensor device must be valid 
	if (sensor_configuration.sensor_settings_.sensor_type_ == YAAASensor::sensor_type_t::SENSOR_TEMPERATURE)
	{
		switch (sensor_configuration.sensor_settings_.sensor_device_)
		{
		case SensorTemperature::DEVICE_ANALOG_IC:
			sensors_[CONDITION_SKY_TEMPERATURE] = new SensorTemperatureAnalogIC(sensor_configuration.sensor_settings_);
			break;
		case SensorTemperature::DEVICE_THERMISTOR:
			sensors_[CONDITION_SKY_TEMPERATURE] = new SensorTemperatureThermistor(sensor_configuration.sensor_settings_);
			break;
		case SensorTemperature::DEVICE_ONEWIRE_DS18:
			sensors_[CONDITION_SKY_TEMPERATURE] = new SensorTemperatureOneWire(sensor_configuration.sensor_settings_);
			break;
		case SensorTemperature::DEVICE_MLX90614:
			sensors_[CONDITION_SKY_TEMPERATURE] = new SensorTemperatureMLX90614(sensor_configuration.sensor_settings_);
			break;
		case SensorTemperature::DEVICE_HTU21:
			sensors_[CONDITION_SKY_TEMPERATURE] = new SensorTemperatureHTU21(sensor_configuration.sensor_settings_);
			break;
		case SensorTemperature::DEVICE_BMP180:
			sensors_[CONDITION_SKY_TEMPERATURE] = new SensorTemperatureBMP180(sensor_configuration.sensor_settings_);
			break;
		case SensorTemperature::DEVICE_BMx280:
			sensors_[CONDITION_SKY_TEMPERATURE] = new SensorTemperatureBMx280(sensor_configuration.sensor_settings_);
			break;
		default:
			break;
		}

		if (!sensors_[CONDITION_SKY_TEMPERATURE])
			return_value = false;

		else if (!sensors_[CONDITION_SKY_TEMPERATURE]->begin())
		{
			delete sensors_[CONDITION_SKY_TEMPERATURE];
			sensors_[CONDITION_SKY_TEMPERATURE] = NULL;
			return_value = false;
		}

		else if (!beginAutomaticMeasurements(CONDITION_SKY_TEMPERATURE, sensor_configuration))
			return_value = false;
	}

	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::OBSERVING_CONDITIONS,
		EEPROMHandler::observing_conditions_variable_::SENSOR_CONFIGURATION,
		ObservingConditions::CONDITION_STAR_FWHM,
		sensor_configuration);

	//both sensor type and sensor device must be valid 
	if (sensor_configuration.sensor_settings_.sensor_type_ == YAAASensor::sensor_type_t::SENSOR_STAR_FWHM)
	{
		switch (sensor_configuration.sensor_settings_.sensor_device_)
		{
		default:
			break;
		}

		if (!sensors_[CONDITION_STAR_FWHM])
			return_value = false;

		else if (!sensors_[CONDITION_STAR_FWHM]->begin())
		{
			delete sensors_[CONDITION_STAR_FWHM];
			sensors_[CONDITION_STAR_FWHM] = NULL;
			return_value = false;
		}

		else if (!beginAutomaticMeasurements(CONDITION_STAR_FWHM, sensor_configuration))
			return_value = false;
	}

	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::OBSERVING_CONDITIONS,
		EEPROMHandler::observing_conditions_variable_::SENSOR_CONFIGURATION,
		ObservingConditions::CONDITION_TEMPERATURE,
		sensor_configuration);

	//both sensor type and sensor device must be valid 
	if (sensor_configuration.sensor_settings_.sensor_type_ == YAAASensor::sensor_type_t::SENSOR_TEMPERATURE)
	{
		switch (sensor_configuration.sensor_settings_.sensor_device_)
		{
		case SensorTemperature::DEVICE_ANALOG_IC:
			sensors_[CONDITION_TEMPERATURE] = new SensorTemperatureAnalogIC(sensor_configuration.sensor_settings_);
			break;
		case SensorTemperature::DEVICE_THERMISTOR:
			sensors_[CONDITION_TEMPERATURE] = new SensorTemperatureThermistor(sensor_configuration.sensor_settings_);
			break;
		case SensorTemperature::DEVICE_ONEWIRE_DS18:
			sensors_[CONDITION_TEMPERATURE] = new SensorTemperatureOneWire(sensor_configuration.sensor_settings_);
			break;
		case SensorTemperature::DEVICE_MLX90614:
			sensors_[CONDITION_TEMPERATURE] = new SensorTemperatureMLX90614(sensor_configuration.sensor_settings_);
			break;
		case SensorTemperature::DEVICE_HTU21:
			sensors_[CONDITION_TEMPERATURE] = new SensorTemperatureHTU21(sensor_configuration.sensor_settings_);
			break;
		case SensorTemperature::DEVICE_BMP180:
			sensors_[CONDITION_TEMPERATURE] = new SensorTemperatureBMP180(sensor_configuration.sensor_settings_);
			break;
		case SensorTemperature::DEVICE_BMx280:
			sensors_[CONDITION_TEMPERATURE] = new SensorTemperatureBMx280(sensor_configuration.sensor_settings_);
			break;
		default:
			break;
		}

		if (!sensors_[CONDITION_TEMPERATURE])
			return_value = false;

		else if (!sensors_[CONDITION_TEMPERATURE]->begin())
		{
			delete sensors_[CONDITION_TEMPERATURE];
			sensors_[CONDITION_TEMPERATURE] = NULL;
			return_value = false;
		}

		else if (!beginAutomaticMeasurements(CONDITION_TEMPERATURE, sensor_configuration))
			return_value = false;
	}

	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::OBSERVING_CONDITIONS,
		EEPROMHandler::observing_conditions_variable_::SENSOR_CONFIGURATION,
		ObservingConditions::CONDITION_WIND_DIRECTION,
		sensor_configuration);

	//both sensor type and sensor device must be valid 
	if (sensor_configuration.sensor_settings_.sensor_type_ == YAAASensor::sensor_type_t::SENSOR_WIND_DIRECTION)
	{
		switch (sensor_configuration.sensor_settings_.sensor_device_)
		{
		case SensorWindDirection::DEVICE_POTENTIOMETER:
			sensors_[CONDITION_WIND_DIRECTION] = new SensorWindDirectionPotentiometer(sensor_configuration.sensor_settings_);
			break;
		case SensorWindDirection::DEVICE_SWITCH_RESISTOR:
			sensors_[CONDITION_WIND_DIRECTION] = new SensorWindDirectionSwitchResistor(sensor_configuration.sensor_settings_);
			break;
		default:
			break;
		}

		if (!sensors_[CONDITION_WIND_DIRECTION])
			return_value = false;

		else if (!sensors_[CONDITION_WIND_DIRECTION]->begin())
		{
			delete sensors_[CONDITION_WIND_DIRECTION];
			sensors_[CONDITION_WIND_DIRECTION] = NULL;
			return_value = false;
		}

		else if (!beginAutomaticMeasurements(CONDITION_WIND_DIRECTION, sensor_configuration))
			return_value = false;
	}

	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::OBSERVING_CONDITIONS,
		EEPROMHandler::observing_conditions_variable_::SENSOR_CONFIGURATION,
		ObservingConditions::CONDITION_WIND_GUST,
		sensor_configuration);

	//both sensor type and sensor device must be valid 
	if (sensor_configuration.sensor_settings_.sensor_type_ == YAAASensor::sensor_type_t::SENSOR_WIND_GUST)
	{
		switch (sensor_configuration.sensor_settings_.sensor_device_)
		{
		default:
			break;
		}

		if (!sensors_[CONDITION_WIND_GUST])
			return_value = false;

		else if (!sensors_[CONDITION_WIND_GUST]->begin())
		{
			delete sensors_[CONDITION_WIND_GUST];
			sensors_[CONDITION_WIND_GUST] = NULL;
			return_value = false;
		}

		else if (!beginAutomaticMeasurements(CONDITION_WIND_GUST, sensor_configuration))
			return_value = false;
	}

	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::OBSERVING_CONDITIONS,
		EEPROMHandler::observing_conditions_variable_::SENSOR_CONFIGURATION,
		ObservingConditions::CONDITION_WIND_SPEED,
		sensor_configuration);

	//both sensor type and sensor device must be valid 
	if (sensor_configuration.sensor_settings_.sensor_type_ == YAAASensor::sensor_type_t::SENSOR_WIND_SPEED)
	{
		switch (sensor_configuration.sensor_settings_.sensor_device_)
		{
		case SensorWindSpeed::DEVICE_ANALOG_VOLTAGE:
			sensors_[CONDITION_WIND_SPEED] = new SensorWindSpeedAnalogVoltage(sensor_configuration.sensor_settings_);
			break;
		case SensorWindSpeed::DEVICE_IMPULSE:
			sensors_[CONDITION_WIND_SPEED] = new SensorWindSpeedImpulse(sensor_configuration.sensor_settings_);
			break;
		default:
			break;
		}

		if (!sensors_[CONDITION_WIND_SPEED])
			return_value = false;

		else if (!sensors_[CONDITION_WIND_SPEED]->begin())
		{
			delete sensors_[CONDITION_WIND_SPEED];
			sensors_[CONDITION_WIND_SPEED] = NULL;
			return_value = false;
		}

		else if (!beginAutomaticMeasurements(CONDITION_WIND_SPEED, sensor_configuration))
			return_value = false;
	}

	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::OBSERVING_CONDITIONS,
		EEPROMHandler::observing_conditions_variable_::SENSOR_CONFIGURATION,
		ObservingConditions::CONDITION_LIGHTNING,
		sensor_configuration);

	//both sensor type and sensor device must be valid 
	if (sensor_configuration.sensor_settings_.sensor_type_ == YAAASensor::sensor_type_t::SENSOR_LIGHTNING)
	{
		switch (sensor_configuration.sensor_settings_.sensor_device_)
		{
		case SensorLightning::DEVICE_ANALOG:
			break;
		case SensorLightning::DEVICE_AS3935:
			sensors_[CONDITION_LIGHTNING] = new SensorLightningAS3935(sensor_configuration.sensor_settings_);
			break;
		default:
			break;
		}

		if (!sensors_[CONDITION_LIGHTNING])
			return_value = false;

		else if (!sensors_[CONDITION_LIGHTNING]->begin())
		{
			delete sensors_[CONDITION_LIGHTNING];
			sensors_[CONDITION_LIGHTNING] = NULL;
			return_value = false;
		}

		else if (!beginAutomaticMeasurements(CONDITION_LIGHTNING, sensor_configuration))
			return_value = false;
	}

	if (!forceRefresh())
		return_value = false;

	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	for (uint8_t i = 0; i < NUM_HEATERS; i++)
	{
		heaters_[i] = NULL;

		EEPROMHandler::readValue(
			EEPROMHandler::device_type_::OBSERVING_CONDITIONS,
			EEPROMHandler::observing_conditions_variable_::HEATER_CONFIGURATION,
			i,
			heaters_config_[i]);

		switch (heaters_config_[i].heater_settings_.device_)
		{
		case YAAAHeater::DEVICE_ARDUINO_PIN:
			heaters_[i] = new HeaterArduinoPin(heaters_config_[i].heater_settings_);
			break;
		}

		if (!heaters_[i])
		{
			return_value = false;
		}

		else if (!heaters_[i]->begin())
		{
			delete heaters_[i];
			heaters_[i] = NULL;
			return_value = false;
		}
	}

	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	bool enabled = false;
	//read if sensor data should be logged to a .csv file. 
	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::OBSERVING_CONDITIONS,
		EEPROMHandler::observing_conditions_variable_::LOG_CSV_FILE,
		0,
		enabled);

	if (enabled)
	{
		log_sensors_ = new YAAALog("YAAA.csv");

		if (log_sensors_)
		{
			if (!log_sensors_->begin())
				return_value = false;

			//read if logging should be enabled by default.
			EEPROMHandler::readValue(
				EEPROMHandler::device_type_::OBSERVING_CONDITIONS,
				EEPROMHandler::observing_conditions_variable_::LOG_CSV_ENABLED,
				0,
				enabled);

			if (!log_sensors_->setLogMode(enabled))
				return_value = false;

			//read log delimiter and log interval. 
			EEPROMHandler::readValue(
				EEPROMHandler::device_type_::OBSERVING_CONDITIONS,
				EEPROMHandler::observing_conditions_variable_::LOG_CSV_DELIMITER,
				0,
				log_delimiter_);

			EEPROMHandler::readValue(
				EEPROMHandler::device_type_::OBSERVING_CONDITIONS,
				EEPROMHandler::observing_conditions_variable_::LOG_CSV_INTERVAL,
				0,
				log_interval_);

			//add csv header
			String log_line = ",";

			if (sensors_[CONDITION_CLOUD_COVER])
				log_line += "cloud cover" + String(log_delimiter_);

			if (sensors_[CONDITION_DEW_POINT])
				log_line += "dew point" + String(log_delimiter_);

			if (sensors_[CONDITION_HUMIDITY])
				log_line += "humidity" + String(log_delimiter_);

			if (sensors_[CONDITION_PRESSURE])
				log_line += "pressure" + String(log_delimiter_);

			if (sensors_[CONDITION_RAIN])
				log_line += "rain" + String(log_delimiter_);

			if (sensors_[CONDITION_RAIN_RATE])
				log_line += "rain rate" + String(log_delimiter_);

			if (sensors_[CONDITION_SKY_BRIGHTNESS])
				log_line += "sky brightness" + String(log_delimiter_);

			if (sensors_[CONDITION_SKY_QUALITY])
				log_line += "sky quality" + String(log_delimiter_);

			if (sensors_[CONDITION_SKY_TEMPERATURE])
				log_line += "sky temperature" + String(log_delimiter_);

			if (sensors_[CONDITION_STAR_FWHM])
				log_line += "star FWHM" + String(log_delimiter_);

			if (sensors_[CONDITION_TEMPERATURE])
				log_line += "temperature" + String(log_delimiter_);

			if (sensors_[CONDITION_WIND_DIRECTION])
				log_line += "wind direction" + String(log_delimiter_);

			//if (sensors_[CONDITION_WIND_GUST])
			//	log_line += "wind gust,";

			if (sensors_[CONDITION_WIND_SPEED])
				log_line += "wind speed" + String(log_delimiter_);

			if (sensors_[CONDITION_LIGHTNING])
				log_line += "lightning distance" + String(log_delimiter_);

			log_sensors_->log(log_line);
		}
		else
			return_value = false;
	}

	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::OBSERVING_CONDITIONS,
		EEPROMHandler::observing_conditions_variable_::AVERAGE_PERIOD,
		0,
		average_period_);

	setAveragePeriod(average_period_);

	return return_value;		//TODO add error conditions
}

void ObservingConditions::run()
{
	automaticMeasurements();
	controlHeaters();
	logSensorData();
}

bool ObservingConditions::forceRefresh()
{
	bool return_value = true;

	for (uint8_t i = 0; i < NUM_CONDITIONS; i++)
	{
		if (sensors_[i] && performsAutomaticMeasurements(i))
		{
			if (sensors_[i]->measure())
			{
				automatic_measurement_configuration_[i]->am_start_last_ = millis();
				automatic_measurement_configuration_[i]->am_state_ = ObservingConditions::STATE_MEASUREMENT_RUNNING;
			}
			else
				return_value = false;
		}
	}

	return return_value;
}

bool ObservingConditions::performsAutomaticMeasurements(uint8_t sensor)
{
	//return false if an invalid sensor has been given.
	if (sensor >= NUM_CONDITIONS)
		return false;

	//return false if the given sensor is not enabled.
	if (!sensors_[sensor])
		return false;

	//return false if automatic measuremens are not enabled for this sensor.
	if (!automatic_measurement_configuration_[sensor])
		return false;

	return true;
}

bool ObservingConditions::performsAveraging(uint8_t sensor)
{
	//return false if an invalid sensor has been given.
	if (sensor >= NUM_CONDITIONS)
		return false;

	//return false if the given sensor is not enabled.
	if (!sensors_[sensor])
		return false;

	//return false if automatic measurements are not enabled for this sensor.
	if (!automatic_measurement_configuration_[sensor])
		return false;

	//return if averaging is enabled for this sensor.
	return automatic_measurement_configuration_[sensor]->average_samples_ != 0;
}

uint32_t ObservingConditions::getAveragePeriod()
{
	return average_period_;
}

void ObservingConditions::setAveragePeriod(uint32_t period)
{
	average_period_ = period;
}

bool ObservingConditions::measure(uint8_t sensor)
{
	if (sensor < NUM_CONDITIONS && sensors_[sensor])
		return sensors_[sensor]->measure();

	//if an invalid sensor was given, return false.
	return false;
}

bool ObservingConditions::hasValue(uint8_t sensor)
{
	if (sensor < NUM_CONDITIONS && sensors_[sensor])
		return sensors_[sensor]->hasValue();

	//if an invalid sensor was given, return false.
	return false;
}

float ObservingConditions::getValue(uint8_t sensor)
{
	if (sensor < NUM_CONDITIONS && sensors_[sensor])
		return sensors_[sensor]->getValue();

	//if an invalid sensor was given, return false.
	return NAN;
}

float ObservingConditions::getAverage(uint8_t sensor)
{
	//return false if an invalid sensor has been given.
	if (sensor >= NUM_CONDITIONS)
		return NAN;

	//return false if the given sensor is not enabled.
	if (!sensors_[sensor])
		return NAN;

	//return false if automatic measuremens are not enabled for this sensor.
	if (!performsAveraging(sensor))
		return NAN;

	//avoid unnecessary calculations
	if (automatic_measurement_configuration_[sensor]->average_samples_ == 0)
		return automatic_measurement_configuration_[sensor]->average_array_[0];

	float average = 0.0f;
	uint8_t average_cnt = 0;			//number of values used for averaging

	for (uint8_t i = 0; i < automatic_measurement_configuration_[sensor]->average_samples_; i++)
		if (!isnan(automatic_measurement_configuration_[sensor]->average_array_[i])) {				//array is initialized to NAN, skip initial values that have not yet been overwritten
			average += automatic_measurement_configuration_[sensor]->average_array_[i];
			average_cnt++;
		}

	if (average_cnt == 0)
		return NAN;

	return (average / static_cast<float>(average_cnt));
}

uint32_t ObservingConditions::getMeasurementAge(uint8_t sensor)
{
	uint32_t return_value = 0xFFFFFFFF;

	//if no sensor has been specified, get the time since the most recent update of any sensor. 
	if (sensor == ObservingConditions::CONDITION_UNKNOWN)
	{
		for (uint8_t i = 0; i < NUM_CONDITIONS; i++)
			if (sensors_[i])
				//workaround for issues with function min(a,b) on ESP8266 / ESP32.
#if defined (ESP8266) || defined(ESP32)
				return_value = _min(return_value, sensors_[i]->getMeasurementAge());
#else
				return_value = min(return_value, sensors_[i]->getMeasurementAge());
#endif
	}
	else
	{
		if (sensor < NUM_CONDITIONS && sensors_[sensor])
			return_value = sensors_[sensor]->getMeasurementAge();
	}

	return return_value;
}

bool ObservingConditions::isSensorEnabled(uint8_t sensor)
{
	if (sensor < NUM_CONDITIONS)
		return sensors_[sensor];

	return false;
}

bool ObservingConditions::isHeaterEnabled(uint8_t heater)
{
	if (heater < NUM_HEATERS)
		return heaters_[heater];

	return false;
}

uint8_t ObservingConditions::getHeaterValue(uint8_t heater)
{
	if (heater < NUM_HEATERS && heaters_[heater])
		return heaters_[heater]->getValue();

	return 0;
}

bool ObservingConditions::setHeaterValue(uint8_t heater, uint8_t setting)
{
	if (heater < NUM_HEATERS && heaters_[heater])
		return heaters_[heater]->setValue(setting);

	return false;
}

bool ObservingConditions::getLogMode()
{
	if (log_sensors_)
		return log_sensors_->getLogMode();

	return false;
}

bool ObservingConditions::setLogMode(bool enabled)
{
	if (log_sensors_)
		return log_sensors_->setLogMode(enabled);

	return false;
}

bool ObservingConditions::beginAutomaticMeasurements(uint8_t sensor, SensorConfiguration sensor_configuration)
{
	if (sensor >= NUM_CONDITIONS)
		return false;

	if (!sensors_[sensor])
		return false;

	if (automatic_measurement_configuration_[sensor])
		return false;

	if (sensor_configuration.automatic_measurement_interval_ == 0)
		return true;

	automatic_measurement_configuration_[sensor] = new AutomaticMeasurementConfiguration();

	automatic_measurement_configuration_[sensor]->am_start_interval_ = static_cast<uint32_t>(sensor_configuration.automatic_measurement_interval_) * 1000;
	automatic_measurement_configuration_[sensor]->am_start_last_ = 0L;
	automatic_measurement_configuration_[sensor]->am_check_interval_ = automatic_measurement_configuration_[sensor]->am_start_interval_ / QUERIES_PER_AVERAGE_INTERVAL;
	automatic_measurement_configuration_[sensor]->am_check_last_ = 0L;
	automatic_measurement_configuration_[sensor]->am_state_ = ObservingConditions::STATE_IDLE;
	automatic_measurement_configuration_[sensor]->average_counter_ = 0;
	automatic_measurement_configuration_[sensor]->average_samples_ = sensor_configuration.average_samples_;

	if (sensor_configuration.average_samples_ == 0)
		automatic_measurement_configuration_[sensor]->average_array_ = NULL;
	else
	{
		automatic_measurement_configuration_[sensor]->average_array_ = new float[sensor_configuration.average_samples_];

		//if array was successfully created, initialize it to NAN.
		if (automatic_measurement_configuration_[sensor]->average_array_)
		{
			for (uint8_t i = 0; i < sensor_configuration.average_samples_; i++)
				automatic_measurement_configuration_[sensor]->average_array_[i] = NAN;
		}
		else
			return false;
	}

	return true;
}

void ObservingConditions::automaticMeasurements()
{
	if (sensors_[sensor_counter_])
	{
		if (sensors_[sensor_counter_]->hasEvent())
		{
			String event = sensors_[sensor_counter_]->handleEvent();

			//log event only if the returned string is not empty
			if (event != "")
			{
				String log_line = PSTR("Sensor ");
				log_line.concat(String(sensor_counter_));
				log_line.concat(PSTR(" Event: "));
				log_line.concat(event);

				Serial.println(log_line);

				log_.log(log_line);
			}
		}
		else if (performsAutomaticMeasurements(sensor_counter_))
		{
			switch (automatic_measurement_configuration_[sensor_counter_]->am_state_)
			{
			case ObservingConditions::STATE_IDLE:
				//TODO use measurement age instead?
				//if (sensors_[sensor_counter_]->getMeasurementAge() > automatic_measurement_configuration_[sensor_counter_]->am_start_interval_)
				if (millis() - automatic_measurement_configuration_[sensor_counter_]->am_start_last_ > automatic_measurement_configuration_[sensor_counter_]->am_start_interval_)
				{
					automatic_measurement_configuration_[sensor_counter_]->am_start_last_ = millis();

					sensors_[sensor_counter_]->measure();

					automatic_measurement_configuration_[sensor_counter_]->am_state_ = ObservingConditions::STATE_MEASUREMENT_RUNNING;
				}

				break;
			case ObservingConditions::STATE_MEASUREMENT_RUNNING:
				if (millis() - automatic_measurement_configuration_[sensor_counter_]->am_check_last_ > automatic_measurement_configuration_[sensor_counter_]->am_check_interval_)
				{
					automatic_measurement_configuration_[sensor_counter_]->am_check_last_ = millis();

					if (sensors_[sensor_counter_]->hasValue())
						automatic_measurement_configuration_[sensor_counter_]->am_state_ = ObservingConditions::STATE_MEASUREMENT_COMPLETE;
				}

				break;
			case ObservingConditions::STATE_MEASUREMENT_COMPLETE:
				automatic_measurement_configuration_[sensor_counter_]->am_state_ = ObservingConditions::STATE_IDLE;
				break;
			default:
				automatic_measurement_configuration_[sensor_counter_]->am_state_ = ObservingConditions::STATE_IDLE;
				break;
			}

			//read a value for averaging if averaging is due and a value can be read from the sensor.
			if (performsAveraging(sensor_counter_) && isDueForAverage(sensor_counter_) && sensors_[sensor_counter_]->hasValue())
			{
				automatic_measurement_configuration_[sensor_counter_]->average_collect_last_ = millis();

				automatic_measurement_configuration_[sensor_counter_]->average_counter_++;
				automatic_measurement_configuration_[sensor_counter_]->average_counter_ %= automatic_measurement_configuration_[sensor_counter_]->average_samples_;
				automatic_measurement_configuration_[sensor_counter_]->average_array_[automatic_measurement_configuration_[sensor_counter_]->average_counter_] =
					sensors_[sensor_counter_]->getValue();
			}
		}
	}

	//next sensor
	sensor_counter_ = (sensor_counter_ + 1) % NUM_CONDITIONS;
}

bool ObservingConditions::isDueForAverage(uint8_t sensor)
{
	return ((millis() - automatic_measurement_configuration_[sensor_counter_]->average_collect_last_) > (average_period_ / QUERIES_PER_AVERAGE_INTERVAL));
}

float ObservingConditions::calculateDewPoint(float humidity, float temperature)
{
	float dew_point = NAN;

	if (isnan(humidity))
	{
		if (performsAveraging(ObservingConditions::CONDITION_HUMIDITY))
			humidity = getAverage(ObservingConditions::CONDITION_HUMIDITY);
		else if (hasValue(ObservingConditions::CONDITION_HUMIDITY))
			humidity = getValue(ObservingConditions::CONDITION_HUMIDITY);
	}

	if (isnan(temperature))
	{
		if (performsAveraging(ObservingConditions::CONDITION_TEMPERATURE))
			temperature = getAverage(ObservingConditions::CONDITION_TEMPERATURE);
		else if (hasValue(ObservingConditions::CONDITION_TEMPERATURE))
			temperature = getValue(ObservingConditions::CONDITION_TEMPERATURE);
	}

	//calculate dew point.
	if (!isnan(temperature) && !isnan(humidity))
	{
		//TODO make algorithm user selectable.
		if (true)
		{
			dew_point = temperature - (100.0f - humidity) / 5.0f;
		}
		else
		{
			float gamma = log(humidity / 100.0f) + (B_ * temperature) / (C_ + temperature);

			dew_point = (C_ * gamma) / (B_ - gamma);
		}
	}

	return dew_point;
}

void ObservingConditions::controlHeaters()
{
	if ((heaters_[heater_counter_] && heaters_config_[heater_counter_].automatic_control_source_ != ObservingConditions::AUTOMATIC_DISABLED) &&
		(millis() - heaters_last_update_[heater_counter_] > heaters_config_[heater_counter_].automatic_update_interval_))
	{
		heaters_last_update_[heater_counter_] = millis();

		bool enable = false;

		//check if heater should be enabled based on rain detector reading.
		if ((heaters_config_[heater_counter_].automatic_control_source_ & ObservingConditions::AUTOMATIC_RAIN) &&
			performsAutomaticMeasurements(ObservingConditions::CONDITION_RAIN) &&
			hasValue(ObservingConditions::CONDITION_RAIN))
		{
			if (getValue(ObservingConditions::CONDITION_RAIN) != 0.0f)
				enable = true;
		}

		//check if heater shold be enabled based on temperature sensor reading.
		if ((heaters_config_[heater_counter_].automatic_control_source_ & ObservingConditions::AUTOMATIC_TEMPERATURE) &&
			performsAutomaticMeasurements(ObservingConditions::CONDITION_TEMPERATURE) &&
			hasValue(ObservingConditions::CONDITION_TEMPERATURE))
		{
			if (getValue(ObservingConditions::CONDITION_TEMPERATURE) <= heaters_config_[heater_counter_].temperature_threshold_)
				enable = true;
		}

		//check if heater should be enabled based on dew point reading / calculation
		if ((heaters_config_[heater_counter_].automatic_control_source_ & ObservingConditions::AUTOMATIC_DEW_POINT) &&
			//performsAutomaticMeasurements(ObservingConditions::CONDITION_DEW_POINT) &&
			//hasValue(ObservingConditions::CONDITION_DEW_POINT) &&
			performsAutomaticMeasurements(ObservingConditions::CONDITION_TEMPERATURE) &&
			hasValue(ObservingConditions::CONDITION_TEMPERATURE))
		{
			if (getValue(ObservingConditions::CONDITION_DEW_POINT) <= getValue(ObservingConditions::CONDITION_TEMPERATURE) + heaters_config_[heater_counter_].dew_point_threshold_)
				enable = true;
		}

		//enable or disable heaters.
		if (enable)
			heaters_[heater_counter_]->setValue(heaters_config_[heater_counter_].automatic_control_value_);
		else
			heaters_[heater_counter_]->setValue(0);
	}

	heater_counter_ = (heater_counter_ + 1) % NUM_HEATERS;
}

void ObservingConditions::logSensorData()
{
	if (!log_sensors_)
		return;

	if (millis() - log_last_ < log_interval_)
		return;

	log_last_ = millis();

	String log_line = String(log_delimiter_);

	for (uint8_t i = 0; i < NUM_CONDITIONS; i++)
		if (sensors_[i])
		{
			if (performsAveraging(i))
			{
				log_line += String(getAverage(i)) + String(log_delimiter_);
			}
			else if (performsAutomaticMeasurements(i))
			{
				log_line += String(getValue(i)) + String(log_delimiter_);
			}
		}

	log_sensors_->log(log_line);
}

String ObservingConditions::getConditionName(uint8_t condition, bool short_name)
{
	switch (condition)
	{
	case CONDITION_CLOUD_COVER:
		return  (short_name ?  PSTR("CC") : PSTR("CloudCover"));
	case CONDITION_DEW_POINT:
		return  (short_name ? PSTR("DP") : PSTR("DewPoint"));
	case CONDITION_HUMIDITY:
		return  (short_name ? PSTR("HU") : PSTR("Humidity"));
	case CONDITION_PRESSURE:
		return  (short_name ? PSTR("PR") : PSTR("Pressure"));
	case CONDITION_RAIN:
		return  (short_name ? PSTR("RN") : PSTR("Rain"));
	case CONDITION_RAIN_RATE:
		return  (short_name ? PSTR("RR") : PSTR("RainRate"));
	case CONDITION_SKY_BRIGHTNESS:
		return  (short_name ? PSTR("BR") : PSTR("SkyBrightness"));
	case CONDITION_SKY_QUALITY:
		return  (short_name ? PSTR("SQ") : PSTR("SkyQuality"));
	case CONDITION_SKY_TEMPERATURE:
		return  (short_name ? PSTR("ST") : PSTR("SkyTemperature"));
	case CONDITION_STAR_FWHM:
		return  (short_name ? PSTR("SF") : PSTR("StarFWHM"));
	case CONDITION_TEMPERATURE:
		return  (short_name ? PSTR("TP") : PSTR("Temperature"));
	case CONDITION_WIND_DIRECTION:
		return  (short_name ? PSTR("WD") : PSTR("WindDirection"));
	case CONDITION_WIND_GUST:
		return  (short_name ? PSTR("WG") : PSTR("WindGust"));
	case CONDITION_WIND_SPEED:
		return  (short_name ? PSTR("WS") : PSTR("WindSpeed"));
	case CONDITION_LIGHTNING:
		return  (short_name ? PSTR("LN") : PSTR("Lightning"));
	}

	return  (short_name ? PSTR("UN") : PSTR("Lightning"));
}

void ObservingConditions::writeDefaults()
{
	EEPROMHandler::writeValue(EEPROMHandler::device_type_::OBSERVING_CONDITIONS, EEPROMHandler::observing_conditions_variable_::LOG_CSV_FILE, 0, static_cast<uint8_t>(false));
	EEPROMHandler::writeValue(EEPROMHandler::device_type_::OBSERVING_CONDITIONS, EEPROMHandler::observing_conditions_variable_::LOG_CSV_ENABLED, 0, static_cast<uint8_t>(false));
	EEPROMHandler::writeValue(EEPROMHandler::device_type_::OBSERVING_CONDITIONS, EEPROMHandler::observing_conditions_variable_::LOG_CSV_DELIMITER, 0, DEF_SENSOR_LOG_DELIMITER);
	EEPROMHandler::writeValue(EEPROMHandler::device_type_::OBSERVING_CONDITIONS, EEPROMHandler::observing_conditions_variable_::LOG_CSV_INTERVAL, 0, static_cast<uint32_t>(DEF_SENSOR_LOG_INTERVAL));

	EEPROMHandler::writeValue(EEPROMHandler::device_type_::OBSERVING_CONDITIONS, EEPROMHandler::observing_conditions_variable_::AVERAGE_PERIOD, 0, static_cast<uint32_t>(DEF_SENSOR_AVERAGE_PERIOD));

	ObservingConditions::SensorConfiguration sensor_configuration_def =
	{
		{
			DEF_SENSOR_TYPE,
			DEF_SENSOR_DEVICE,
			DEF_SENSOR_UNIT,
			DEF_SENSOR_PARAMETERS
		},
		DEF_SENSOR_AUTOMATIC_MEAUREMENTS,
		DEF_SENSOR_AVERAGE_SAMPLES
	};

	for (uint8_t i = 0; i < NUM_CONDITIONS; i++)
		EEPROMHandler::writeValue(EEPROMHandler::device_type_::OBSERVING_CONDITIONS, EEPROMHandler::observing_conditions_variable_::SENSOR_CONFIGURATION, i, sensor_configuration_def);

	ObservingConditions::HeaterConfiguration settings =
	{
		{
			DEF_HEATER_DEVICE,
			DEF_HEATER_PARAMETERS
		},
		DEF_HEATER_AUTOMATIC_CONTROL_SOURCE,
		DEF_HEATER_AUTOMATIC_CONTROL_VALUE,
		DEF_HEATER_AUTOMATIC_CONTROL_INTERVAL,
		DEF_HEATER_DEW_POINT_THRESHOLD,
		DEF_HEATER_TEMPERATURE_THRESHOLD
	};

	for (uint8_t i = 0; i < NUM_HEATERS; i++)
		EEPROMHandler::writeValue(EEPROMHandler::device_type_::OBSERVING_CONDITIONS, EEPROMHandler::observing_conditions_variable_::HEATER_CONFIGURATION, i, settings);
}
