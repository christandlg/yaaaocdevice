//Rain Rate sensor class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "SensorRainRate.h"

SensorRainRate::SensorRainRate(YAAASensor::SensorSettings sensor_settings) :
	YAAASensor(sensor_settings), 
	rain_rate_unit_(DEF_SENSOR_RAIN_RATE_UNIT)
{
	setUnit(sensor_settings.unit_);
}

SensorRainRate::~SensorRainRate()
{
}

uint8_t SensorRainRate::getSensorType()
{
	return YAAASensor::SENSOR_RAIN_RATE;
}

uint8_t SensorRainRate::getUnit()
{
	return rain_rate_unit_;
}

bool SensorRainRate::setUnit(uint8_t unit)
{
	switch (unit)
	{
		case SensorRainRate::UNIT_MM_PER_HOUR:
		case SensorRainRate::UNIT_IN_PER_HOUR:
			rain_rate_unit_ = unit;
			return true;
		default:
			break;
	}

	return false;
}

float SensorRainRate::convertRainRate(float rain_rate, uint8_t unit_src, uint8_t unit_dst)
{
	if (unit_src == unit_dst)
		return rain_rate;

	//convert to mm / hr
	switch (unit_src)
	{
		case SensorRainRate::UNIT_IN_PER_HOUR:
			rain_rate *= 2.54f;
		default:
			break;		
	}

	//convert to destination unit
	switch (unit_dst)
	{
		case SensorRainRate::UNIT_IN_PER_HOUR:
			rain_rate /= 2.54f;
		default:
			break;
	}

	return rain_rate;
}
