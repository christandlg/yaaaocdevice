//DHT11/22 Temperature sensor class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "SensorTemperatureDHT.h"

SensorTemperatureDHT::SensorTemperatureDHT(YAAASensor::SensorSettings sensor_settings) :
	SensorTemperature(sensor_settings),
	sensor_(NULL)
{
	sensor_ = new YAAADHT(sensor_settings.parameters_[0], sensor_settings.parameters_[1]);
}


SensorTemperatureDHT::~SensorTemperatureDHT()
{
	//no destructors are implemented for SimpleDHT and derived classes
	//if (sensor_)
	//	delete sensor_;

	//sensor_ = NULL;
}

bool SensorTemperatureDHT::begin()
{
	return sensor_ && sensor_->begin();
}

bool SensorTemperatureDHT::measure()
{
	return sensor_ && sensor_->measureTemperature();
}

bool SensorTemperatureDHT::hasValue()
{
	return sensor_ && sensor_->hasTemperature();
}

float SensorTemperatureDHT::getValue()
{
	if (!sensor_)
		return NAN;

	return(convertTemperature(
		sensor_->getTemperature(),
		SensorTemperature::UNIT_CELSIUS,
		temperature_unit_));
}
