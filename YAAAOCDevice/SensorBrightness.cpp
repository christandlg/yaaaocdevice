//Brightness sensor class.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "SensorBrightness.h"

SensorBrightness::SensorBrightness(YAAASensor::SensorSettings sensor_settings) :
	YAAASensor(sensor_settings), 
	unit_(DEF_SENSOR_BRIGHTNESS_UNIT)
{
	setUnit(sensor_settings.unit_);
}


SensorBrightness::~SensorBrightness()
{
}

uint8_t SensorBrightness::getSensorType()
{
	return YAAASensor::SENSOR_BRIGHTNESS;
}

uint8_t SensorBrightness::getUnit()
{
	return unit_;
}

bool SensorBrightness::setUnit(uint8_t unit)
{
	switch (unit)
	{
		case SensorBrightness::UNIT_LUX:
		case SensorBrightness::UNIT_NOX:
		case SensorBrightness::UNIT_PHOT:
		case SensorBrightness::UNIT_FC:
			unit_ = unit;
			return true;
		default:
			break;
	}

	return false;
}

float SensorBrightness::convertIlluminance(float illum, uint8_t unit_src, uint8_t unit_dst)
{
	if (unit_src == unit_dst)
		return illum;

	switch (unit_src)
	{
		case UNIT_NOX:
			illum /= 1000.0f;		//1 nx = 1/1000lx
			break;
		case UNIT_PHOT:				//1 phot = 10000lx
			illum *= 10000.0f;
			break;					//1 foot-candle = 10.764lx
		case UNIT_FC:
			illum *= 10.764;
			break;
		default:
			break;
	}

	switch (unit_dst)
	{
		case UNIT_NOX:
			illum *= 1000.0f; //1 nx = 1/1000lx
			break;
		case UNIT_PHOT:		//1 phot = 10000lx
			illum /= 10000.0f;
			break;
		case UNIT_FC:
			illum /= 10.764;
			break;
		default:
			break;
	}

	//convert illuminance to Lux.
	return illum;
}
