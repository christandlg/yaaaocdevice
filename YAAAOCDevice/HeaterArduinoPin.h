//PWM-controlled heater class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef HEATER_ARDUINO_PIN_H_
#define HEATER_ARDUINO_PIN_H_

#include "YAAAHeater.h"

class HeaterArduinoPin :
	public YAAAHeater
{
public:
	HeaterArduinoPin(HeaterSettings heater_settings);
	~HeaterArduinoPin();

	bool begin();

	/*
	@return:
	0 if the heater is disabled, 1-254 if the heater is enabled (PWM), 255 if the heater is enabled (digital). */
	uint8_t getValue();

	/*
	enables or disables the heater. fails if automatic control is enabled or PWM control requested, but unsupported.
	@param value: 0 to disable, 1-254 to enable (PWM), 255 to enable (digital). */
	bool setValue(uint8_t value);

private:
	uint8_t pin_;

	uint8_t inverted_;

	uint8_t control_value_;
};

#endif /* HEATER_ARDUINO_PIN_H_ */
