//FC37/YL83/... rain sensor class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef RAIN_SENSOR_FC37_H_
#define RAIN_SENSOR_FC37_H_

#include <Arduino.h>

#include "SensorRain.h"

class SensorRainFC37 :
	public SensorRain
{
public:
	SensorRainFC37(YAAASensor::SensorSettings sensor_settings);

	~SensorRainFC37();

	/*
	initializes the temperature sensor with the info given in in the constructor call.
	@return true on success (sensor input is analog and input pin is an analog input pin), false otherwise. */
	bool begin();

	/*
	starts a measurement.
	@return true on success, false otherwise.*/
	bool measure();

	/*
	checks if a measurement has been completed. may perform work necessary to complete a mesurement. user is
	expected to call this function after calling measure() and call it until it returns true.
	@return true if a measurement has been completed. */
	bool hasValue();

	/*
	returns the last measured value.
	user is expected to call this function only after hasValue() has returned true.
	@return the barometric pressure in the given unit. */
	float getValue();

private:
	uint8_t pin_;					//I/O pin to use.

	bool input_analog_;				//true if using an analog input value, false if using a digital input value

	bool input_inverted_;			//rain is detected if analog input value is above(false) or below(true) analog threshold or digital input is HIGH(false) or LOW(true).

	uint16_t analog_threshold_;		//analog threshold value

	bool value_;					//true if rain detected, false otherwise
};

#endif /* RAIN_SENSOR_FC37_H_*/