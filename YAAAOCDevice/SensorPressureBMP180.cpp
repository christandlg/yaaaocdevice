//BMP180 Air Pressure sensor class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "SensorPressureBMP180.h"

SensorPressureBMP180::SensorPressureBMP180(YAAASensor::SensorSettings sensor_settings, void *sensor) :
	SensorPressure(sensor_settings),
	sensor_(NULL), 
	measurement_state_(SensorPressureBMP180::STATE_IDLE)
{
	if (sensor)
		sensor_ = static_cast<BMP180MI*>(sensor);
	else
		sensor_ = new BMP180I2C(sensor_settings.parameters_[0]);
}

SensorPressureBMP180::~SensorPressureBMP180()
{
	if (sensor_)
		delete sensor_;

	sensor_ = NULL;
}

bool SensorPressureBMP180::begin()
{
	if (!sensor_)
		return false;
		
	return sensor_->begin();
}

bool SensorPressureBMP180::measure()
{
	if (!sensor_)
		return false;

	//return false if a measurement is currently running
	if (measurement_state_ != SensorPressureBMP180::STATE_IDLE)
		return false;

	if (!sensor_->measureTemperature())
		return false;

	measurement_state_ = SensorPressureBMP180::STATE_MEASURE_TEMP;
}

bool SensorPressureBMP180::hasValue()
{
	if (!sensor_)
		return false;

	//return false if the sensor is currently measuring. 
	if (!sensor_->hasValue())
		return false;
		
	//if a temperature measurement has been completed, start a pressure measurement. 
	if (measurement_state_ == SensorPressureBMP180::STATE_MEASURE_TEMP)
	{
		sensor_->measurePressure();
		measurement_state_ = SensorPressureBMP180::STATE_MEASURE_PRESS;
		return false;
	}

	//if a pressure measurement has been completed, change satate to idle
	else if (measurement_state_ == SensorPressureBMP180::STATE_MEASURE_PRESS)
		measurement_state_ = SensorPressureBMP180::STATE_IDLE;

	return true;
}

float SensorPressureBMP180::getValue()
{
	if (!sensor_)
		return NAN;

	return SensorPressure::convertPressure(
		sensor_->getPressure(), 
		SensorPressure::UNIT_PASCAL, 
		pressure_unit_);
}
