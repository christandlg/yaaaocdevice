//Wind Speed sensor (analog voltage based) class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "SensorWindSpeedAnalogVoltage.h"

SensorWindSpeedAnalogVoltage::SensorWindSpeedAnalogVoltage(YAAASensor::SensorSettings sensor_settings) : 
	SensorWindSpeed(sensor_settings),
	pin_(255),
	analog_baseline_(DEF_SENSOR_WIND_SPEED_ANALOG_BASELINE),
	analog_reference_(DEF_SENSOR_WIND_SPEED_ANALOG_REFERENCE),
	slope_(DEF_SENSOR_WIND_SPEED_ANALOG_SLOPE),
	analog_value_(0)
{
	pin_ = sensor_settings.parameters_[0];

	uint16_t analog_baseline = (sensor_settings.parameters_[2] << 8 | sensor_settings.parameters_[3]);		//here given in mV
	analog_baseline_ = static_cast<float>(analog_baseline) / 1000.0f;

	uint16_t analog_reference = (sensor_settings.parameters_[4] << 8 | sensor_settings.parameters_[5]);		//here given in mV
	analog_reference_ = static_cast<float>(analog_reference) / 1000.0f;

	uint16_t slope = (sensor_settings.parameters_[6] << 8 | sensor_settings.parameters_[7]);				//here given in mm/s/V
	slope_ = static_cast<float>(slope) / 1000.0f;
}


SensorWindSpeedAnalogVoltage::~SensorWindSpeedAnalogVoltage()
{
	//nothing to do here...
}

bool SensorWindSpeedAnalogVoltage::begin()
{
	//check if the input pin is connected to an ADC.
#ifdef ARDUINO_SAM_DUE
	if ((pin_ < 54) || pin_ > 65)	//Arduino Due: pins 54 to 65 are analog input pins
#elif defined ARDUINO_AVR_MEGA2560		//__AVR_ATmega2560__
	if ((pin_ < 54) || pin_ > 69)	//Arduino Mega: pins 54 to 69 are analog input pins
#elif defined ESP8266	//__ESP8266_ESP8266__
	if (pin_ != A0)					//ESP8266: only 1 analog input pin is available.
#elif defined ESP32	
	if (pin_ != A0 &&
		pin_ != A4 &&
		pin_ != A5 &&
		pin_ != A6 &&
		pin_ != A7 &&
		pin_ != A10 &&
		pin_ != A11 &&
		pin_ != A12 &&
		pin_ != A13 &&
		pin_ != A14 &&
		pin_ != A15 &&
		pin_ != A16 &&
		pin_ != A17 &&
		pin_ != A18 &&
		pin_ != A19)
#else
	if (true)
#endif
		return false;

	pinMode(pin_, INPUT);

	return true;
}

bool SensorWindSpeedAnalogVoltage::measure()
{
	analog_value_ = analogRead(pin_);

	updateMeasurementTime();

	return true;
}

bool SensorWindSpeedAnalogVoltage::hasValue()
{
	return true;
}

float SensorWindSpeedAnalogVoltage::getValue()
{
	//calculate sensor output voltage 
	float value = static_cast<float>(analog_value_) / 1024.0f * analog_reference_;

	//calculate sensor output voltage above baseline
	value -= analog_baseline_;

	//calculate speed
	value *= slope_;

	return convertSpeed(
		value, 
		SensorWindSpeed::SPEED_METERS_PER_SECOND, 
		speed_unit_);
}
