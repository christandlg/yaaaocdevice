//InfoDisplay class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef INFODISPLAY_H_
#define INFODISPLAY_H_

#include "defaults.h"

#include "Display.h"
#include "DisplayHD47780.h"
//#include "DisplayU8GLIB.h"
//#include "DisplayGLCD.h"

class InfoDisplay
{
public:

	InfoDisplay();
	~InfoDisplay();

	void init();

	bool available();

	//retrieves information and updates what is displayed. 
	void run();

	//writes some default values to the Arduino's EEPROM.
	static void writeDefaults();

private:
	Display *display_;

	uint32_t update_interval_;		//display update interval (in milliseconds)
	uint32_t update_time_prev_;		//last display update time (in milliseconds)

	//----------------------------------------------------------------------------------------------
	//templates for reading and writing to/from the EEPROM.
	template <class T> static void readEEPROM(EEPROMHandler::general_variable_::GeneralVariable_t_ variable, T& value)
	{
		EEPROMHandler::readValue
			(
			EEPROMHandler::device_type_::GENERAL,
			variable,
			0,
			value
			);
	};

	template <class T> static void writeEEPROM(EEPROMHandler::general_variable_::GeneralVariable_t_ variable, const T& value)
	{
		EEPROMHandler::writeValue
			(
			EEPROMHandler::device_type_::GENERAL,
			variable,
			0,
			value
			);
	};
};

extern InfoDisplay info_display_;

#endif /*INFODISPLAY_H_*/

