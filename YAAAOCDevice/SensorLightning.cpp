#include "SensorLightning.h"

SensorLightning::SensorLightning(YAAASensor::SensorSettings sensor_settings) : 
	YAAASensor(sensor_settings),
	unit_(DEF_SENSOR_LIGHTNING_UNIT)
{
	setUnit(sensor_settings.unit_);
}

SensorLightning::~SensorLightning()
{
}

uint8_t SensorLightning::getSensorType()
{
	return YAAASensor::SENSOR_LIGHTNING;
}

uint8_t SensorLightning::getUnit()
{
	return unit_;
}

bool SensorLightning::setUnit(uint8_t unit)
{
	switch(unit)
	{
		case UNIT_METERS:
		case UNIT_KILOMETERS:
		case UNIT_MILES:
			unit_ = unit;
			break;
		default:
			return false;
	}
	
	return true;
}

float SensorLightning::convertUnit(float dist, uint8_t unit_src, uint8_t unit_dst)
{
	//convert given distance to kilometers.
	switch (unit_src)
	{
		case UNIT_METERS:
			dist /= 1000.0f;
			break;
		case UNIT_MILES:
			dist *= 1.609344f;
			break;
	}
	
	//convert given distance to target unit.
	switch (unit_dst)
	{
		case UNIT_METERS:
			dist *= 1000.0f;
			break;
		case UNIT_MILES:
			dist /= 1.609344f;
			break;
	}
	
	return dist;
}
