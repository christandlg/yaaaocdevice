//Temperature sensor class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef SENSOR_TEMPERATURE_H_
#define SENSOR_TEMPERATURE_H_

#include <Arduino.h>

#include "YAAASensor.h"

//SensorTemperature interface class.
class SensorTemperature :
	public YAAASensor
{
public:
	enum sensor_device_t : uint8_t
	{
		DEVICE_ANALOG_IC = 0,			//temperature sensor is a LM35 or TMP36 temperature sensor (or similar)
		DEVICE_THERMISTOR = 1,			//temperature sensor is based on a thermistor.
		DEVICE_ONEWIRE_DS18 = 11,		//temperature sensor is a DS18x20 or DS1820 one wire temperature sensor.
		DEVICE_DHT = 20,				//temperature sensor is a DHT11 or DHT22 temperature and humidity sensor.
		DEVICE_HTU21 = 40,				//temperature sensor is the sensor integrated in a HTU21 humidity sensor.
		DEVICE_BMP180 = 50,				//temperature sensor is the sensor integrated in a BMP180 atmospheric pressure sensor.
		DEVICE_BMx280 = 51,				//temperature sensor is the sensor integrated in a BMx280.
		DEVICE_MLX90614 = 60,			//temperature sensor is a MLX90614 contactless infrared thermometer
		DEVICE_UNKOWN = 255
	};

	enum result_unit_t : uint8_t
	{
		UNIT_CELSIUS = 0,
		UNIT_KELVIN = 1,
		UNIT_FAHRENHEIT = 2
	};

	SensorTemperature(YAAASensor::SensorSettings sensor_settings);

	virtual ~SensorTemperature();

	//initializes the instance.
	//@param parameters: array containing parameters (temperature sensor specific)
	//@return: true if the initialization was successful, false otherwise.
	virtual bool begin() = 0;

	//start a temperature measurement. 
	virtual bool measure() = 0;

	//@return true if a value is available, false otherwise.
	virtual bool hasValue() = 0;

	//@return: the temperature in the requested temperature unit.
	//must be implemented by derived classes.
	virtual float getValue() = 0;

	uint8_t getSensorType();

	//@return temperature sensor unit as SensorTemperature::result_unit_t
	uint8_t getUnit();

	//sets the temperature unit to the given unit (as result_unit_t).
	//@param unit: temperature unit.
	//@return: true if the unit was successfully set, false otherwise.
	bool setUnit(uint8_t unit);

protected:
	//converts temperatures from unit_src to unit_dst.
	//@param temp: temperature to convert
	//@param unit_src: temperature unit of temp
	//@param unit_dst: temperature unit of converted value
	//@return: converted temperature.
	static float convertTemperature(float temp, uint8_t unit_src, uint8_t unit_dst);

	uint8_t temperature_unit_;				//currently set temperature unit.

private:
};

#endif /* SENSOR_TEMPERATURE_H_ */

