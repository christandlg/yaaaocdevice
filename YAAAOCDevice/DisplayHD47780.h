//HD47780 Display class for YAAADevice. 
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef DISPLAYHD47780_H_
#define DISPLAYHD47780_H_

#include <Arduino.h>

#include "LiquidCrystal.h"
#include "Display.h"

class DisplayHD47780 :
	public Display 
{
public:
	DisplayHD47780(
		uint16_t chars,					//number of chracters per line
		uint16_t lines,					//number of lines
		uint8_t rs,						//rs pin number
		uint8_t enable,					//enable pin number
		uint8_t d0,						//d0 pin number
		uint8_t d1,						//d1 pin number
		uint8_t d2,						//d2 pin number
		uint8_t d3,						//d3 pin number
		uint8_t pin_brightness = 255,	//(optional) brightness (pwm) pin number 
		uint8_t pin_contrast = 255		//(optional) contrast (pwm) pin number
		);

	enum update_state_t : uint8_t
	{
		UPDATE_RESET = 0,				//reset update sequence
		UPDATE_CHECK = 1,				//check if an update at the current position is necessary
		UPDATE_CURSOR = 2,				//set cursor position
		UPDATE_WRITE = 3,				//wirte a character to the current cursor position and calculate next position
		UPDATE_NEXT = 4,				//calculate next position
		UPDATE_DONE = 5,				//does nothing
	};

private:
	//-----------------------------------------------------------------------------
	//display interface

	LiquidCrystal *display_;

	/*Destructor. Deletes display content arrays.*/
	~DisplayHD47780();

	/*adds a custom character for the LCD.
	@param: 
	 - index
	 - character
	@return: true if the call was successful, false otherwise.*/
	bool addCustomChar(uint8_t index, uint8_t* character);

	/*calculates the next position on the display.
	@param pos: position in the current line
	@param line: current line
	@return: true: next position calculated successfully, false: position has been reset*/
	bool calculateNextPosition();

	/*clears the display.*/
	void clear();

	/*prints a custom character to the specified location on the display.
	 - line to write to
	 - line position to write to
	 - index of character to write.*/
	bool printCustomChar(uint16_t pos, uint16_t line, byte index);

	/*writes a string to the display.
	 - line to write to
	 - line position to write to
	 - message to write.*/
	void printMessage(uint16_t pos, uint16_t line, String message);

	/*updates the display on a per-character basis if new data is available.
	only 1 character is ever updated in one program loop, this ensures the display
	does not block the execution of the rest of the code.*/
	void update();

	/*@return: the currently set brightness or 0 if brightness feature is not enabled.*/
	uint8_t getBrightness();

	/*@return: the currently set contrast or 0 if contrast feature is not enabled.*/
	uint8_t getContrast();

	/*sets the brightness to the given value (0 - 255).
	@return: true if call was successful, false otherwise.*/
	bool setBrightness(uint8_t value);

	/*sets the contrast to the given value (0 - 255).
	@return: true if call was successful, false otherwise.*/	
	bool setContrast(uint8_t value);

	/*writes the resolution of the display to the variables x and y and returns true.*/
	virtual bool getResolution(uint16_t &x, uint16_t &y);

	//end of interface
	//-----------------------------------------------------------------------------

	/*adds a character to the display content array.
	@return: true if successful, false otherwise (pos > num_chars_ or 
	line > num_lines_).*/
	bool addDisplayContent(uint16_t pos, uint16_t line, byte character);

	/*calculates the index of a character in the display_content_ and 
	display_content_previous_ arrays based on x and y coordinates.*/
	uint16_t index(uint16_t x, uint16_t y);

	/*resets the update sequence.*/
	void updateReset();

	/*checks if an update is necessary at the current position.*/
	void updateCheck();

	/*sets the cursor to the current position.*/
	void updateCursor();

	/*writes a character at the current position.*/
	void updateCharacter();

	/*calculates the next position to update.*/
	void updateNext();

	byte* display_content_;				//what should be displayed
	byte* display_content_previous_;	//what was previously displayed

	uint16_t num_chars_;				//number of characters per line of the display
	uint16_t num_lines_;				//number of lines of the display

	uint16_t curr_char_;				//current character number
	uint16_t curr_line_;				//current line number

	uint8_t pin_brightness_;			//pwm pin for brighness setting.
	uint8_t pin_contrast_;				//pwm pin for contrast setting.

	uint8_t val_brightness_;			//brightness setting
	uint8_t val_contrast_;				//contrast setting 
	
	update_state_t update_state_;		//display update state 

	//byte smiley_[8] = {
	//	B00000,
	//	B10001,
	//	B00000,
	//	B00000,
	//	B10001,
	//	B01110,
	//	B00000,
	//};

	byte locked_[8] = {
		B00000,
		B01110,
		B10001,
		B10001,
		B11111,
		B11011,
		B11011,
		B11111
	};

	byte unlocked_[8] = {
		B01110,
		B10001,
		B10001,
		B00001,
		B11111,
		B11011,
		B11011,
		B11111
	};

	byte locked_local_[8] = {
		B01110,
		B10111,
		B10111,
		B10111,
		B10111,
		B10111,
		B10001,
		B01110
	};

	byte locked_remote_[8] = {
		B01110,
		B10011,
		B10101,
		B10101,
		B10011,
		B10011,
		B10101,
		B01110
	};

	byte parked_[8] = {
		B01110,
		B10011,
		B10101,
		B10101,
		B10011,
		B10111,
		B10111,
		B01110
	};

	byte thermometer_[8] =
	{
		B00000,
		B00100,
		B01010,
		B01010,
		B01010,
		B01010,
		B10001,
		B01110
	};

	//byte telescope[8] = {
	//	B00000,
	//	B00001,
	//	B01111,
	//	B10001,
	//	B11110,
	//	B10100,
	//	B01010,
	//	B10001
	//};
	//
	//byte telescope1[8] = {
	//	B00001,
	//	B01111,
	//	B10001,
	//	B10001,
	//	B11110,
	//	B10100,
	//	B01010,
	//	B10001
	//};
	//
	//byte telescope2[8] = {
	//	B00000,
	//	B00000,
	//	B11111,
	//	B10001,
	//	B11111,
	//	B00100,
	//	B01010,
	//	B10001
	//};
};

#endif /*DISPLAYHD47780_H_*/

