//Command Interface (WiFi) class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#if defined(ESP8266) || defined(ESP32)

#include "CommandInterfaceWifi.h"

CommandInterfaceWifi::CommandInterfaceWifi() : 
	server_(YAAAWifi::getWiFiServer(port_)),
	port_(DEF_G_WIFI_PORT)
{
	//nothing to do here...
}

CommandInterfaceWifi::~CommandInterfaceWifi()
{
	//nothing to do here...
}

String CommandInterfaceWifi::getConfig(uint8_t config)
{
	String return_value = "";

	switch (config)
	{
		case CONFIG_IP:
		case CONFIG_DNS:
		case CONFIG_GATEWAY:
		case CONFIG_SUBNET:
		case CONFIG_RSSI:
			return_value = YAAAWifi::getConfig(config);
		break;
		case CONFIG_PORT:
			return_value = String(port_);
			break;
		default:
			return_value = "";
			break;
	}

	return return_value;
}

bool CommandInterfaceWifi::setConfig(uint8_t config, String value)
{
	return false;
}

int CommandInterfaceWifi::available()
{
	//check if a client has disconnected
	if (client_ && !client_.connected())
		client_.stop();

	//check if new clients exist
	if (server_.hasClient())
	{
		//if a client is already connected, close the connection immediately
		if (client_)
			server_.available();
		else //if (!client_)
			client_ = server_.available();
	}

	return client_ && client_.available();
}

int CommandInterfaceWifi::read()
{
	return client_.read();
}

int CommandInterfaceWifi::peek()
{
	return client_.peek();
}

size_t CommandInterfaceWifi::write(uint8_t data)
{
	return client_.write(data);
}

size_t CommandInterfaceWifi::write(const uint8_t * buffer, size_t size)
{
	return client_.write(buffer, size);
}

void CommandInterfaceWifi::flush()
{
	client_.flush();
}

bool CommandInterfaceWifi::beginInterface()
{
	if (!YAAAWifi::getInitialized())
		return false;

	//read port setting
	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::WIFI_PORT,
		0,
		port_);

	//open server...
	server_ = YAAAWifi::getWiFiServer(port_);

	server_.begin(port_);

	server_.setNoDelay(true);

	return true;
}

void CommandInterfaceWifi::writeDefaults()
{
	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::WIFI_PORT,
		0,
		static_cast<uint16_t>(DEF_G_WIFI_PORT));
}

#endif /* defined(ESP8266) || defined(ESP32) */
