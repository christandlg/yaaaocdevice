//BMx280 Pressure sensor class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef SENSOR_PRESSURE_BMx280_H_
#define SENSOR_PRESSURE_BMx280_H_

#include <Arduino.h>

#include "SensorPressure.h"

#include "Wire.h"
#include "SPI.h"
#include "BMx280MI.h"

class SensorPressureBMx280 :
	public SensorPressure
{
public:
    enum interface_t : uint8_t
    {
        INTERFACE_I2C = 0,
        INTERFACE_SPI = 1
    };

	SensorPressureBMx280(YAAASensor::SensorSettings sensor_settings, void *sensor = NULL);

	~SensorPressureBMx280();

	/*
	initializes the temperature sensor with the info given in in the constructor call.
	@return true on success (input pin is an analog input pin), false otherwise. */
	bool begin();

	/*
	starts a measurement.
	@return true on success, false otherwise.*/
	bool measure();

	/*
	checks if a measurement has been completed. may perform work necessary to complete a mesurement. user is
	expected to call this function after calling measure() and call it until it returns true.
	@return true if a measurement has been completed. */
	bool hasValue();

	/*
	returns the last measured value.
	user is expected to call this function only after hasValue() has returned true.
	@return the barometric pressure in the given unit. */
	float getValue();

private:
	BMx280MI *sensor_;					//sensor
};

#endif /* SENSOR_PRESSURE_BMx280_H_ */