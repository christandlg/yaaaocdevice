//Command Interface (Serial) class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef COMMAND_INTERFACE_SERIAL_H_
#define COMMAND_INTERFACE_SERIAL_H_

#include <Arduino.h>

#include "CommandInterface.h"

#include "EEPROMHandler.h"

class CommandInterfaceSerial :
	public CommandInterface
{
public:
	enum baud_rates_t : uint32_t {
		SERIAL_BAUD_RATE_300 = 300,
		SERIAL_BAUD_RATE_600 = 600,
		SERIAL_BAUD_RATE_1200 = 1200,
		SERIAL_BAUD_RATE_2400 = 2400,
		SERIAL_BAUD_RATE_4800 = 4800,
		SERIAL_BAUD_RATE_9600 = 9600,
		SERIAL_BAUD_RATE_14400 = 14400,
		SERIAL_BAUD_RATE_19200 = 19200,
		SERIAL_BAUD_RATE_28800 = 28800,
		SERIAL_BAUD_RATE_38400 = 38400,
		SERIAL_BAUD_RATE_57600 = 57600,
		SERIAL_BAUD_RATE_115200 = 115200,
	};

	enum config_t : uint8_t
	{
		CONFIG_BAUD_RATE = 0
	};

	CommandInterfaceSerial();
	~CommandInterfaceSerial();

	/*
	@param config element identifier.
	@return config element as String. */
	String getConfig(uint8_t config);

	/*
	@param config element identifier.
	@param new value.
	@return true on success, false otherwise. */
	bool setConfig(uint8_t config, String value);

	int available();
	int read();
	int peek();

	size_t write(uint8_t data);
	//size_t write(const uint8_t *buffer, size_t size);

	void flush();

	/*
	closes the serial connection and reopens it with the given baud rate.
	@param baud_rate: baud rate to switch to.
	@return: true if setting the new baud rate was successful, false otherwise. */
	bool changeBaudRate(uint32_t baud_rate);

	/*
	checks if the baud rate is in the list of supported baud rates.
	@return: true if the baud rate is supported, false otherwise. */
	static bool checkBaudRate(uint32_t baud_rate);

	/*
	writes default values for the Serial Interface to EEPROM. */
	static void writeDefaults();

private:

	bool beginInterface();

	uint32_t baud_rate_;
};

#endif /* COMMAND_INTERFACE_SERIAL_H_ */
