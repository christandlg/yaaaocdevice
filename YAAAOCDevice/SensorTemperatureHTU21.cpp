//HTU21 Temperature sensor class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "SensorTemperatureHTU21.h"

SensorTemperatureHTU21::SensorTemperatureHTU21(YAAASensor::SensorSettings sensor_settings, void *sensor) :
	SensorTemperature(sensor_settings),
	sensor_(NULL)
{
	if (sensor)
		sensor_ = static_cast<YAAAHTU21D*>(sensor);
	else
		sensor_ = new YAAAHTU21D(sensor_settings.parameters_[0]);
}

SensorTemperatureHTU21::~SensorTemperatureHTU21()
{
	if (sensor_)
		delete sensor_;

	sensor_ = NULL;
}

bool SensorTemperatureHTU21::begin()
{
	return sensor_ && sensor_->begin();
}

bool SensorTemperatureHTU21::measure()
{
	return sensor_ && sensor_->measureTemperature();
}

bool SensorTemperatureHTU21::hasValue()
{
	return sensor_ && sensor_->hasTemperature();
}

float SensorTemperatureHTU21::getValue()
{
	if (!sensor_)
		return NAN;

	return convertTemperature(sensor_->getTemperature(), UNIT_CELSIUS, temperature_unit_);
}

void * SensorTemperatureHTU21::getSensor()
{
	return sensor_;
}

bool SensorTemperatureHTU21::setMeasurementResolution(uint8_t resolution)
{
	return sensor_->setMeasurementResolution(resolution);
}
