//Dew Point sensor class.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef SENSOR_DEWPOINT_H_
#define SENSOR_DEWPOINT_H_

#include <Arduino.h>

#include "YAAASensor.h"

//SensorDewPoint interface class.
class SensorDewPoint :
	public YAAASensor
{
public:
	enum sensor_device_t : uint8_t
	{
		DEVICE_ANALOG = 0,
		DEVICE_CALCULATOR = 1,		//dew point is calculated from temperature and relative humidity
		DEVICE_UNKOWN = 255
	};

	enum result_unit_t : uint8_t
	{
		UNIT_CELSIUS = 0,
		UNIT_KELVIN = 1,
		UNIT_FAHRENHEIT = 2
	};

	SensorDewPoint(YAAASensor::SensorSettings sensor_settings);

	virtual ~SensorDewPoint();

	//initializes the instance.
	//@param parameters: array containing parameters (temperature sensor specific)
	//@return: true if the initialization was successful, false otherwise.
	virtual bool begin() = 0;

	//start a temperature measurement. 
	virtual bool measure() = 0;

	//@return true if a value is available, false otherwise.
	virtual bool hasValue() = 0;

	//@return: the temperature in the requested temperature unit.
	//must be implemented by derived classes.
	virtual float getValue() = 0;

	uint8_t getSensorType();

	//@return temperature sensor unit as SensorDewPoint::result_unit_t
	uint8_t getUnit();

	//sets the temperature unit to the given unit (as result_unit_t).
	//@param unit: temperature unit.
	//@return: true if the unit was successfully set, false otherwise.
	bool setUnit(uint8_t unit);

protected:
	//converts temperatures from unit_src to unit_dst.
	//@param temp: temperature to convert
	//@param unit_src: temperature unit of temp
	//@param unit_dst: temperature unit of converted value
	//@return: converted temperature.
	static float convertTemperature(float temp, uint8_t unit_src, uint8_t unit_dst);

	uint8_t temperature_unit_;				//currently set temperature unit.

private:
};

#endif /* SENSOR_DEWPOINT_H_ */

