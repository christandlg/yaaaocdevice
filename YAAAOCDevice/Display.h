//Display Class for YAAADevice. 
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef DISPLAY_H_
#define DISPLAY_H_

#include <Arduino.h>

#include "EEPROMHandler.h"

class Display
{
public:
	enum display_features_t : uint32_t
	{
		FEATURE_UNDEFINED = 0,		//no features known.
		FEATURE_TEXT = 1,			//the display can show text.
		FEATURE_CUSTOM_CHAR = 2,	//the display can show custom characters.
		FEATURE_DRAW = 4,			//the display can show drawings.
		FEATURE_BRIGHTNESS = 64,	//the displays brightness can be adjusted.
		FEATURE_CONTRAST = 128		//the displays contrast can be adjusted.
	};

	enum display_type_t : uint8_t
	{
		DISPLAY_NONE = 0,			//display deativated.
		DISPLAY_SEGMENT = 1,		//segment display (e.g. 7 segment displays).
		DISPLAY_CHARACTER = 2,		//character display (e.g. HD47780 and compatible).
		//DISPLAY_GRAPHIC = 3,		//graphic display (e.g. 128x64px LCDs).
		DISPLAY_U8G2LIB = 4,		//displays controlled with the U8G2lib library.
		DISPLAY_OTHER = 254
	};

	enum display_interface_type_t : uint8_t
	{
		INTERFACE_NATIVE = 0,	//native interface (e.g. 4 pin parallel for HD47780 displays)
		INTERFACE_I2C = 1,		//I2C interface
		INTERFACE_SPI = 2,		//SPI interface
	};

	Display();

	virtual ~Display();

	/*clears the display. must be implemented by derived class.*/
	virtual void clear() = 0;

	/*updates the display if an update is due. must be implemented by derived class.*/
	virtual void update() = 0;

	/*@return: true if the given feature is supported, false otherwise.*/
	bool checkFeature(uint32_t feature);

	/*@return: the currently set brightness or 0 if brightness feature is not enabled.
	@default behaviour: returns 0 when not implemented by derived class.*/
	virtual uint8_t getBrightness();

	/*@return: the currently set contrast or 0 if contrast feature is not enabled.
	@default behaviour: returns 0 when not implemented by derived class.*/
	virtual uint8_t getContrast();

	/*sets the brightness to the given value (0 - 255).
	@return: true if call was successful, false otherwise.
	@default behaviour: returns false when not implemented by derived class.*/
	virtual bool setBrightness(uint8_t value);

	/*sets the contrast to the given value (0 - 255).
	@return: true if call was successful, false otherwise.
	@default behaviour: returns false when not implemented by derived class.*/
	virtual bool setContrast(uint8_t value);

	/*writes the resolution of the display to the variables x and y.
	@return: true when the call was successful.
	@ default behaviour: returns false and sets x and y to 0.*/
	virtual bool getResolution(uint16_t &x, uint16_t &y);

	/*adds a custom character for the LCD.
	@param: 
	 - index 
	 - character
	@return: true if the call was successful, false otherwise.
	@default behavior: returns false when not implemented in derived class.*/
	virtual bool addCustomChar(uint8_t index, uint8_t* character);

	/*prints a custom character to the display.
	@param: pos - 
	@param: line - 
	@param: index if char (0 - 7)
	@return: 
	 - true if printing was successful.
	 - false if printing failed (invalid index given, display cannot print custom 
		 characters, ...)
	@default behaviour: returns false when not implemented by derived class.*/
	virtual bool printCustomChar(uint16_t pos, uint16_t line, byte index);

	/*converts the given float to a string (with given number of digits after the 
	decimal point) and prints it to the specifed location on the display.*/
	void write(uint16_t pos, uint16_t line, float message, uint8_t dec = 2);

	/*converts a message of any data type (except float) to a string and prints
	it to the specified location on the display.*/
	template <class T> void write(uint16_t pos, uint16_t line, const T&  message)
	{
		printMessage(pos, line, String(message));
	}

protected:
	/*prints the given message to the display. must be implemented by derived class.*/
	virtual void printMessage(uint16_t pos, uint16_t line, String message) = 0;

	/*sets the given feature flag.*/
	void setFeature(uint32_t feature);

private:
	//feature flags.
	uint32_t features_;
};

#endif /*DISPLAY_H_*/

