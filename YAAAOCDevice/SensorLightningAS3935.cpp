#include "SensorLightningAS3935.h"

SPISettings SensorLightningAS3935::spi_settings_ = SPISettings(2000000, MSBFIRST, SPI_MODE1);
volatile bool SensorLightningAS3935::interrupt_ = false;
volatile uint32_t SensorLightningAS3935::interrupt_time_ = 0L;
AS3935MI *SensorLightningAS3935::as3935_ = NULL;


SensorLightningAS3935::SensorLightningAS3935(YAAASensor::SensorSettings sensor_settings) :
	interface_(DEF_SENSOR_LIGHTNING_AS3935_INTERFACE),
	chip_select_(DEF_SENSOR_LIGHTNING_AS3935_CS),
	pin_irq_(DEF_SENSOR_LIGHTNING_AS3935_PIN_IRQ),
	SensorLightning(sensor_settings),
	afe_gb_(DEF_SENSOR_LIGHTNING_AS3935_AFE_GB),
	wdth_(DEF_SENSOR_LIGHTNING_AS3935_WDTH),
	srej_(DEF_SENSOR_LIGHTNING_AS3935_SREJ),
	nf_lev_(DEF_SENSOR_LIGHTNING_AS3935_NF_LEV),
	min_num_ligh_(DEF_SENSOR_LIGHTNING_AS3935_MIN_NUM_LIGH),
	tun_cap_(DEF_SENSOR_LIGHTNING_AS3935_TUN_CAP),
	mask_dist_(DEF_SENSOR_LIGHTNING_AS3935_MASK_DIST),
	distance_(NAN),
	detection_last_(0L),
	detection_lifetime_(DEF_SENSOR_LIGHTNING_AS3935_DETECTION_LIFETIME),
	auto_adjust_last_(DEF_SENSOR_LIGHTNING_AS3935_AUTO_ADJUST_LAST),
	auto_adjust_interval_(DEF_SENSOR_LIGHTNING_AS3935_AUTO_ADJUST_INTERVAL)
{
	interface_ = sensor_settings.parameters_[0];
	chip_select_ = sensor_settings.parameters_[1];
	pin_irq_ = sensor_settings.parameters_[2];

	afe_gb_ = sensor_settings.parameters_[3] & 0x1F;				//5 bits
	nf_lev_ = sensor_settings.parameters_[3] & 0xE0 > 5;			//3 bit
	wdth_ = sensor_settings.parameters_[4] & 0x0F;					//4 bits
	tun_cap_ = sensor_settings.parameters_[4] & 0xF0 > 4;			//4 bits
	srej_ = sensor_settings.parameters_[5] & 0x0F;					//4 bits
	min_num_ligh_ = sensor_settings.parameters_[5] & 0x30 > 4;		//2 bits
	mask_dist_ = sensor_settings.parameters_[5] & 0x40 > 6;			//1 bit

	detection_lifetime_ = static_cast<uint32_t>(sensor_settings.parameters_[6] << 8 | sensor_settings.parameters_[7]) * 1000;
	auto_adjust_interval_ = static_cast<uint32_t>(sensor_settings.parameters_[8] << 8 | sensor_settings.parameters_[9]) * 1000;
}

SensorLightningAS3935::~SensorLightningAS3935()
{
	delete as3935_;
	as3935_ = NULL;
}

bool SensorLightningAS3935::begin()
{
	//only one instance of AS3935 can be created
	if (as3935_)
		return false;

	switch (interface_)
	{
	case INTERFACE_I2C:
		as3935_ = new AS3935I2C(chip_select_, pin_irq_);
		break;
	case INTERFACE_SPI:
		as3935_ = new AS3935SPI(chip_select_, pin_irq_);
		break;
	default:
		return false;
		break;
	}

	if (!as3935_)
		return false;

	if (!as3935_->begin())
		return false;

	if (!as3935_->calibrateResonanceFrequency())
		return false;

	if (!as3935_->calibrateRCO())
		return false;

	as3935_->writeAFE(afe_gb_);

	as3935_->writeNoiseFloorThreshold(nf_lev_);

	as3935_->writeWatchdogThreshold(wdth_);

	as3935_->writeSpikeRejection(srej_);

	as3935_->writeMaskDisturbers(mask_dist_);

	as3935_->writeMinLightnings(min_num_ligh_);

	//check if an ISR can be attached to the input pin.
#ifdef ARDUINO_SAM_DUE
	if (pin_irq_ > 53)								//Arduino Due: all digital pins can be used as interrupt sources
#elif defined(ARDUINO_AVR_MEGA2560) //__AVR_ATmega2560__
	if ((pin_irq_ != 2) && (pin_irq_ != 3) && (pin_irq_ != 18) && (pin_irq_ != 19) && (pin_irq_ != 20) && (pin_irq_ != 21))		//Arduino Mega: pins 2, 3, 18, 19, 20, 21 can be used as interrupt sources
#elif defined(ESP8266) //__ESP8266_ESP8266__
	if ((pin_irq_ == D0) || (pin_irq_ > 16))		//ESP8266: all pins except D0 (GPIO16) can be used as interrupt surouces.
#elif defined (ESP32)
	if (false)
#else
	if (true)
#endif
		return false;

	digitalWrite(pin_irq_, LOW);
	pinMode(pin_irq_, INPUT);

	//attach IRQ pin interrupt
	attachInterrupt(digitalPinToInterrupt(pin_irq_), SensorLightningAS3935::as3935ISR, RISING);

	return true;
}

bool SensorLightningAS3935::measure()
{
	//actual "measurement" is performed elsewehere
	if (as3935_ &&
		(detection_lifetime_ != 0) &&
		(millis() - detection_last_ > detection_lifetime_))
		distance_ = NAN;

	return as3935_;
}

bool SensorLightningAS3935::hasValue()
{
	return as3935_;
}

float SensorLightningAS3935::getValue()
{
	return convertUnit(distance_, SensorLightning::UNIT_KILOMETERS, unit_);
}

bool SensorLightningAS3935::hasEvent()
{
	if ((auto_adjust_interval_ != 0) && (millis() - auto_adjust_last_ > auto_adjust_interval_))
		return true;
	if (interrupt_ && (millis() - interrupt_time_ > 2))
		return true;

	return false;
}

String SensorLightningAS3935::handleEvent()
{
	if (!as3935_)
		return "";

	String data = "";

	if ((auto_adjust_interval_ != 0) && (millis() - auto_adjust_last_ > auto_adjust_interval_))
	{
		data.concat(PSTR("AUTO_ADJ "));
		increaseSensitivity();
		decreaseNoiseFloorThreshold();
		auto_adjust_last_ = millis();
	}
	else if (interrupt_ && (millis() - interrupt_time_ > 2))
	{
		uint8_t event = as3935_->readInterruptSource();

		switch (event)
		{
		case SensorLightningAS3935::EVENT_NH:
			data.concat(PSTR("INT_NH "));
			data += String(nf_lev_, BIN);

			if (auto_adjust_interval_ != 0)
			{
				increaseNoiseFloorThreshold();
				auto_adjust_last_ = millis();
			}
			break;
		case SensorLightningAS3935::EVENT_D:
			data.concat(PSTR("INT_D "));
			data += String(wdth_, BIN) + " " + String(srej_, BIN);

			if (auto_adjust_interval_ != 0)
			{
				decreaseSensitivity();
				auto_adjust_last_ = millis();
			}
			break;
		case SensorLightningAS3935::EVENT_L:
			data.concat(PSTR("INT_L "));

			distance_ = static_cast<float>(as3935_->readStormDistance());
			data += String(distance_, 2);
			break;
		default:
			break;
		}

		interrupt_ = false;
	}

	return data;
}

bool SensorLightningAS3935::decreaseNoiseFloorThreshold()
{
	nf_lev_ = as3935_->readNoiseFloorThreshold();

	//cannot decrease any further
	if (nf_lev_ == 0)
		return false;

	as3935_->writeNoiseFloorThreshold(nf_lev_--);

	return true;
}

bool SensorLightningAS3935::increaseNoiseFloorThreshold()
{
	nf_lev_ = as3935_->readNoiseFloorThreshold();

	//cannot increase any further
	if (nf_lev_ == 0x07)
		return false;

	as3935_->writeNoiseFloorThreshold(++nf_lev_);

	return true;
}

bool SensorLightningAS3935::decreaseSensitivity()
{
	srej_ = as3935_->readSprikeRejection();
	wdth_ = as3935_->readWatchdogThreshold();

	if ((srej_ >= 0x0F) && (wdth_ >= 0x0F))
		return false;

	if (srej_ < wdth_)
	{
		as3935_->writeSpikeRejection(++srej_);
	}
	else
	{
		as3935_->writeWatchdogThreshold(++wdth_);
	}

	return true;
}

bool SensorLightningAS3935::increaseSensitivity()
{
	srej_ = as3935_->readSprikeRejection();
	wdth_ = as3935_->readWatchdogThreshold();

	if ((srej_ == 0x00) && (wdth_ == 0x00))
		return false;

	if (srej_ > wdth_)
	{
		as3935_->writeSpikeRejection(--srej_);
	}
	else
	{
		as3935_->writeWatchdogThreshold(--wdth_);
	}

	return true;
}


void SensorLightningAS3935::as3935ISR()
{
	if (!as3935_)
		return;

	noInterrupts();
	interrupt_ = true;
	interrupt_time_ = millis();
	interrupts();
}
