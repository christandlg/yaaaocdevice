//FC37/YL83/... rain sensor class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "SensorRainFC37.h"

SensorRainFC37::SensorRainFC37(YAAASensor::SensorSettings sensor_settings) :
	SensorRain(sensor_settings),
	pin_(255),
	input_analog_(DEF_SENSOR_RAIN_FC37_INPUT_ANALOG),
	input_inverted_(DEF_SENSOR_RAIN_FC37_INPUT_INVERTED),
	analog_threshold_(DEF_SENSOR_RAIN_FC37_ANALOG_THRESHOLD),
	value_(false)
{
	pin_ = sensor_settings.parameters_[0];
	input_analog_ = sensor_settings.parameters_[1];
	input_inverted_ = sensor_settings.parameters_[2];
	analog_threshold_ = sensor_settings.parameters_[4] << 8 | sensor_settings.parameters_[5];
	pinMode(pin_, INPUT);
}

SensorRainFC37::~SensorRainFC37()
{
}

bool SensorRainFC37::begin()
{
	if (input_analog_)
	{
		//check if the input pin is an analog pin. return false if it is not.
#ifdef ARDUINO_SAM_DUE
		if ((pin_ < A0) || (pin_ > A11))		//Arduino Due: 12 ADC inputs
#elif defined(ARDUINO_AVR_MEGA2560) //__AVR_ATmega2560__
		if ((pin_ < A0) || (pin_ > A15))		//Arduino Mega: 16 ADC inputs
#elif defined(ESP8266) //__ESP8266_ESP8266__
		if (pin_ != A0)							//ESP8266: 1 ADC input 
#elif defined(ESP32)	
		if (pin_ != A0 && 
			pin_ != A4 && 
			pin_ != A5 && 
			pin_ != A6 && 
			pin_ != A7 && 
			pin_ != A10 && 
			pin_ != A11 && 
			pin_ != A12 && 
			pin_ != A13 && 
			pin_ != A14 && 
			pin_ != A15 && 
			pin_ != A16 && 
			pin_ != A17 && 
			pin_ != A18 && 
			pin_ != A19)
#else
		if ((pin_ < A0) || (pin_ > A5))
#endif
			return false;
	}

	return true;
}

bool SensorRainFC37::measure()
{
	if (input_analog_)
		value_ = (analogRead(pin_) >= analog_threshold_) ^ input_inverted_;
	else
		value_ = digitalRead(pin_) ^ input_inverted_;

	updateMeasurementTime();

	return true;
}

bool SensorRainFC37::hasValue()
{
	return true;
}

float SensorRainFC37::getValue()
{
	return static_cast<float>(value_);
}
