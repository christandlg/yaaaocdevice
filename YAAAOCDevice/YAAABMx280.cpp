#include "YAAABMx280.h"

YAAABMx280::YAAABMx280(uint8_t interface, uint8_t sensor_select):
	sensor_(NULL)
{
	switch (interface)
	{
	case YAAABMx280::INTERFACE_I2C:
		sensor_ = new BMx280I2C(sensor_select);
		break;
	case YAAABMx280::INTERFACE_SPI:
		break;
		sensor_ = new BMx280SPI(sensor_select);
	default:
		break;
	}
}


YAAABMx280::~YAAABMx280()
{
}

bool YAAABMx280::begin()
{
	if (!sensor_)
		return false;

	if (!sensor_->begin())
		return false;

	//more settings...

	return true;
}

bool YAAABMx280::measure()
{
	if (!sensor_)
		return false;

	return sensor_->measure();
}

bool YAAABMx280::hasValue()
{
	if (!sensor_)
		return false;

	return sensor_->hasValue();
}

float YAAABMx280::getHumidity()
{
	if (!sensor_)
		return NAN;

	return sensor_->getHumidity();
}

float YAAABMx280::getPressure()
{
	if (!sensor_)
		return NAN;

	return sensor_->getPressure();
}

float YAAABMx280::getTemperature()
{
	if (!sensor_)
		return NAN;

	return sensor_->getTemperature();
}

uint8_t YAAABMx280::getOversampling(uint8_t condition)
{
	if (!sensor_)
		return 255;

	switch (condition)
	{
	case YAAABMx280::YAAABMx280_HUMIDITY:
		return sensor_->readOversamplingHumidity();
		break;
	case  YAAABMx280::YAAABMx280_PRESSURE:
		return sensor_->readOversamplingPressure();
	case YAAABMx280::YAAABMx280_TEMPERATURE:
		return sensor_->readOversamplingTemperature();
		break;
	default:
		break;
	}

	return 255;
}

bool YAAABMx280::setOversampling(uint8_t condition, uint8_t setting)
{
	if (!sensor_)
		return false;

	switch (condition)
	{
	case YAAABMx280::YAAABMx280_HUMIDITY:
		return sensor_->writeOversamplingHumidity(setting);
			break;
	case  YAAABMx280::YAAABMx280_PRESSURE:
		return sensor_->writeOversamplingPressure(setting);
	case YAAABMx280::YAAABMx280_TEMPERATURE:
		return sensor_->writeOversamplingTemperature(setting);
		break;
	default:
		break;
	}

	return false;
}

bool YAAABMx280::getPowerMode()
{
	if (!sensor_)
		return false;

	return sensor_->readPowerMode();
}

bool YAAABMx280::setPowerMode(uint8_t mode)
{
	if (!sensor_)
		return false;

	return sensor_->writePowerMode(mode);
}

uint8_t YAAABMx280::getStandbyTime()
{
	if (!sensor_)
		return 255;

	return sensor_->readStandbyTime();
}

bool YAAABMx280::setStandbyTime(uint8_t standby_time)
{
	if (!sensor_)
		return false;

	return sensor_->writeStandbyTime(standby_time);
}
