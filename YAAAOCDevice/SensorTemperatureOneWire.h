//OneWire Temperature sensor class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef SENSOR_TEMPERATURE_ONEWIRE_H_
#define SENSOR_TEMPERATURE_ONEWIRE_H_

#include <Arduino.h>
#include <OneWire.h>

#include "SensorTemperature.h"

extern OneWire *one_wire_;

class SensorTemperatureOneWire :
	public SensorTemperature
{
public:
	SensorTemperatureOneWire(YAAASensor::SensorSettings sensor_settings);

	~SensorTemperatureOneWire();

	/*
	initializes the temperature sensor with the info given in in the constructor call.
	@return: true on success, false otherwise. */
	bool begin();

	/*
	searches the One Wire bus for any One Wire devices and returns the number of 
	temperature sensors (matching sensor family identifier)
	@return number of temperature sensors found. returns 0 if the One Wire object
	has not been previously initialized.*/
	static uint8_t getNumSensors();

	/*
	returns the serial number of the given temperature sensor.
	@param sensor: sensor number (0 - N-1)
	@return addr: 8 byte array contianing the address of the given sensor
	@return: true if the call was successful, false if the One Wire object was not 
	initialized, the sensor number was invalid. or the CRC check failed.*/
	static bool getSensorSerial(uint8_t sensor, uint8_t *addr);

	/*
	start a temperature conversion.
	@return: true if the measurement was started successfully, false otherwise
	(OneWire is not initialized)*/
	bool measure();

	/*
	@return true if a value is available, false otherwise.*/
	bool hasValue();

	/*
	@return: the temperature in the requested temperature unit.*/
	float getValue();

	/*
	@return: the resolution of the sensor in number ob bits.*/
	uint8_t getResolution();

	/*
	sets the resolution of the sensor to the given number of bits.
	specifying an invalid number of bits will set the resolution to the default
	value (9bit).
	DS1820 / DS18S20 sensors cannot be set to a specific resolution, these will always
	use 12bit.
	@param resolution: resolution in bits (9 - 12). */
	void setResolution(uint8_t resolution);

private:
	enum sensor_family_t
	{
		DS18S20 = 0x10,		//sensor is a DS1820 or DS18S20
		DS18B20 = 0x28,		//sensor is a DS18B20
		DS1822 = 0x22		//sensor is a DS1822
	};

	enum resolution_t
	{
		RESOLUTION_9_BIT = 0x1F,	//default
		RESOLUTION_10_BIT = 0x3F,
		RESOLUTION_11_BIT = 0x5F,
		RESOLUTION_12_BIT = 0x7F,
	};

	/*
	checks if the given family identifier matches the Dallas / Maxim temperature 
	sensor families.
	@param family: family identifier
	@param true if the family identifier matches the identifier of a DS1820, DS18B20,
	DS18S20 or DS1822 temperature sensor.*/
	static bool checkFamily(uint8_t family);

	/*
	reads the temperature from the DS18*.
	@return: true if the temperature was read successfully, false otherwise.*/
	bool readTemperature();

	/*
	writes the scratchpad using the values stored in th_register_, tl_register_ and 
	resolution_ (DS18B20 and DS1822 only).*/
	void writeScratchpad();

	/*
	reads  the scratchpad from the current sensor.
	@param data: array to write the scratchpad to (must be at least 9 bytes long)
	@return: true if the read was successful, false otherwise (CRC read failed)*/
	bool readScratchpad(uint8_t *data);

	byte serial_number_[8];		//8 byte serial number of the temperature sensor (1 byte family identifier, 6 bytes serial number, 1 byte CRC)

	//scratchpad information
	uint8_t resolution_;	//resolution of the sensor. only valid for DS18B20 or DS1822 family sensors.
	uint8_t th_register_;
	uint8_t tl_register_;

	int16_t raw_value_;			//last raw value returned by temperature sensor (fixed point number)
};

#endif /* SENSOR_TEMPERATURE_ONEWIRE_H_ */

