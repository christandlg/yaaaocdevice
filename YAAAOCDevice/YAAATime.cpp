//a class providing some time functions, i.e. for printing the system time.//simple button class.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "YAAATime.h"

float YAAATime::offset_UTC_ = 0.0f;

bool YAAATime::begin()
{
#if !defined(ARDUINO_SAM_DUE) && !defined (ESP8266) && !defined(ESP32)
	if ((RTC.get()) != 0 || (RTC.chipPresent()))
	{
		setSyncProvider(RTC.get);
		return true;
	}	
#endif

	return false;
}

char* YAAATime::timeToISO8601(char *string, time_t time)
{
	if (time == 0L)
		time = now();

	//assemble time string in ISO 8601 format.
	sprintf(&string[0], "%d", year(time));
	string[4] = '-';

	sprintf(&string[5], "%02d", month(time));
	string[7] = '-';

	sprintf(&string[8], "%02d", day(time));
	string[10] = 'T';

	sprintf(&string[11], "%02d", hour(time));
	string[13] = ':';

	sprintf(&string[14], "%02d", minute(time));
	string[16] = ':';

	sprintf(&string[17], "%02d", second(time));

	string[19] = '\0';

	return string;
}

char* YAAATime::timeToHMS(char *string, bool format_12_hrs, time_t time)
{
	if (time == 0L)
		time = now();

	sprintf(&string[0], "%02d", hour(time));
	string[2] = ':';

	sprintf(&string[3], "%02d", minute(time));
	string[5] = ':';

	sprintf(&string[6], "%02d", second(time));

	string[8] = '\0';

	return string;
}

char* YAAATime::timeToMDY(char *string, char delimiter, time_t time)
{
	if (time == 0L)
		time = now();

	sprintf(&string[0], "%02d", month(time));
	string[2] = delimiter;

	sprintf(&string[3], "%02d", day(time));
	string[5] = delimiter;

	sprintf(&string[6], "%02d", year(time) % 100);

	string[8] = '\0';

	return string;
}

time_t YAAATime::getTime()
{
	return now();
}

void YAAATime::setDateTime(time_t time)
{
	setTime(time);
}

void YAAATime::setDateTime(uint16_t year, uint8_t month, uint8_t day, uint8_t hour, uint8_t minute, uint8_t second)
{
	TimeElements time_elements;
	time_t new_time = 0;

	time_elements.Year = static_cast<uint8_t>(year - 1970);	//TimeElements.Year is years since 1970
	time_elements.Month = month;
	time_elements.Day = day;
	time_elements.Hour = hour;
	time_elements.Minute = minute;
	time_elements.Second = second;

	new_time = makeTime(time_elements);

#if !defined(ARDUINO_SAM_DUE) && !defined(ESP8266) && !defined(ESP32)
	RTC.set(new_time);
#endif		

	setTime(new_time);
}

void YAAATime::setNewDate(uint16_t year, uint8_t month, uint8_t day)
{
	time_t current_time = now();

	setDateTime(year, month, day, hour(current_time), minute(current_time), second(current_time));
}

void YAAATime::setNewTime(uint8_t hour, uint8_t minute, uint8_t second)
{
	time_t current_time = now();

	setDateTime(year(current_time), month(current_time), day(current_time), hour, minute, second);
}

bool YAAATime::setUTCOffset(float offset)
{
	if (abs(offset) > 24.0f)
		return false;

	offset_UTC_ = offset;

	return true;
}

float YAAATime::getUTCOffset()
{
	return offset_UTC_;
}

