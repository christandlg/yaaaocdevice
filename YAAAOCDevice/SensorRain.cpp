//Rain sensor class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "SensorRain.h"

SensorRain::SensorRain(YAAASensor::SensorSettings sensor_settings):
	YAAASensor(sensor_settings) 
{
	setUnit(sensor_settings.unit_);
}

SensorRain::~SensorRain()
{
	//nothing to do here
}

uint8_t SensorRain::getSensorType()
{
	return YAAASensor::SENSOR_RAIN;
}

uint8_t SensorRain::getUnit()
{
	return SensorRain::UNIT_BOOLEAN;
}

bool SensorRain::setUnit(uint8_t unit)
{
	switch (unit)
	{
		case SensorRain::UNIT_BOOLEAN:
			return true;
		default:
			break;
	}

	return false;
}
