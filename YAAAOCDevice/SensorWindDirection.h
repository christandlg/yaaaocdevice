//Wind Direction sensor class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef SENSOR_WIND_DIRECTION_H_
#define SENSOR_WIND_DIRECTION_H_

#include <Arduino.h>

#include "YAAASensor.h"

class SensorWindDirection :
	public YAAASensor
{
public:
	enum sensor_device_t : uint8_t
	{
		DEVICE_POTENTIOMETER = 1,			//device uses a potentiometer to generate an analog voltage that corresponds to wind direction
		DEVICE_SWITCH_RESISTOR = 2,			//device uses resistors and switches to generate an analog voltage that corresponds to wind direction
		DEVICE_UNKOWN = 255
	};

	enum result_unit_t : uint8_t
	{
		UNIT_DEGREES = 0,		//East = 90.0, South = 180.0, West = 270.0 North = 360.0
	};

	SensorWindDirection(YAAASensor::SensorSettings sensor_settings);

	~SensorWindDirection();

	virtual bool begin() = 0;

	/*
	starts a measurement.
	@return true on success, false otherwise.*/
	virtual bool measure() = 0;

	/*
	checks if a measurement has been completed. may perform work necessary to complete a mesurement. user is
	expected to call this function after calling measure() and call it until it returns true.
	@return true if a measurement has been completed. */
	virtual bool hasValue() = 0;

	/*
	returns the last measured value.
	user is expected to call this function only after hasValue() has returned true.
	@return the barometric pressure in the given unit. */
	virtual float getValue() = 0;

	uint8_t getSensorType();

	uint8_t getUnit();

	bool setUnit(uint8_t unit);

protected:

private:

};

#endif /* SENSOR_WIND_DIRECTION_H_ */