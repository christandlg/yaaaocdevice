//RemoteCommandHandler class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef REMOTECOMMANDHANDLER_H_
#define REMOTECOMMANDHANDLER_H_

#include <Arduino.h>

#include "constants.h"
#include "features.h"

#include "CommandInterface.h"
#include "CommandInterfaceSerial.h"
#include "CommandInterfaceEthernet.h"
//#include "CommandInterfaceSoftwareSerial.h"
#include "YAAAWifi.h"
#include "CommandInterfaceWifi.h"

#include "YAAATime.h"

#include "YAAALog.h"

#include "OTAHandler.h"

#define UNKNOWN 255					//return value for unknown command / axis / ...

#define charToInt(x) (x - '0')		//converts a single digit character to an unsigned integer.

#define UNKNOWN_OPTION static_cast<uint16_t>(-1)

#define NUM_COMMAND_INTERFACES 3

class ObservingConditions;
extern ObservingConditions observing_conditions_;

extern YAAALog log_;

class OTAHandler;

extern void writeAllDefaults();

class RemoteCommandHandler
{
public:
	enum command_interfaces_t : uint8_t
	{
		INTERFACE_SERIAL = 0,
		INTERFACE_ETHERNET = 1,
		INTERFACE_WIFI = 2,
		INTERFACE_LoRa = 3,
	};

	enum serial_error_codes_t : uint16_t
	{
		ERROR_NONE = 0,					//not necessary?
		ERROR_UNKNOWN_DEVICE = 1,
		ERROR_UNKNOWN_CMD = 2,
		ERROR_UNKNOWN_CMD_TYPE = 4,
		ERROR_INVALID_CMD_TYPE = 8,
		ERROR_INVALID_PARAMETER = 16,
		ERROR_MISSING_PARAMETER = 32,
		ERROR_UNKNOWN_OPTION = 64,
		ERROR_DEVICE_UNAVAILABLE = 128,
		ERROR_DEVICE_LOCKED = 256
	};

	enum device_type_t : uint8_t
	{
		GENERAL = 0,				//general variables (e.g. serial communication connection speed).
		OBSERVING_CONDITIONS = 8	//variables for the observing conditions sensors.
	};

	struct command_type_
	{
		enum command_type_t
		{
			GET = 0,
			SET = 1,
		};
	};

	enum protocol_t
	{
		YAAADEVICE = 0,
		LX200 = 1
	};

	struct general_command_
	{
		enum general_command_t
		{
			CONNECTED = 0,
			ONEWIRE_TEMP = 10,			//returns number of one wire temperature sensors or sensor addresses.

			TIME = 20,					//gets or sets the time of a RTC connected to the arduino.
			DATETIME = 21,				//gets or sets the time of a RTC in ISO 8601 format.
			UTC_OFFSET = 22,			//gets or sets the offset from local time to yield UTC.

			//SOFTARE_NAME = 30,		//static string "YAAADevice" 
			//SOFTWARE_VERSION = 31,		//YAAADevice software version
			//SOFTWARE_DATETIME = 32,		//upload date & time

			//DEVICE_NAME = 40,			//device name (31 chars)
			//DEVICE_INFO = 41,			//device information (31 chars)

			LOG_MODE = 50,				//gets or sets the currently set log mode
			LOG_STATE = 51,				//gets the current log mode
			LOG_DUMP = 52,				//dumps the contents of the log file to the computer
			LOG_CLEAR = 53,				//clears the log file

			ETHERNET_IP = 60,			//gets the current ethernet local IP
			ETHERNET_DNS = 61,			//gets the current ethernet DNS server
			ETHERNET_GATEWAY = 62,		//gets the current ethernet gateway
			ETHERNET_SUBNET = 63,		//gets the current ethernet subnet
			ETHERNET_PORT = 64,			//gets the current port

#if defined(ESP8266) || defined(ESP32)
			WIFI_CONNECTED = 70,		//gets if the device is connected to a wifi network. 
			WIFI_IP = 71,				//gets the current wifi local IP
			WIFI_DNS = 72,				//gets the current wifi DNS server
			WIFI_GATEWAY = 73,			//gets the current wifi gateway
			WIFI_SUBNET = 74,			//gets the current wifi subnet
			WIFI_PORT = 75,				//gets the current port
			WIFI_RSSI = 76,				//gets current RSSI

			WIFI_TCPRAW_PORT = 80,		//gets the set port for RAW TCP connections

			OTA_ENABLED = 90,			//gets if OTA is enabled or not.
			OTA_MODE = 91,				//gets current OTA mode. 
#endif /* defined(ESP8266) || defined(ESP32) */

			MQTT_ENABLED = 100,			//gets if MQTT is enabled. 
			MQTT_STATE = 101,			//gets MQTT state. 

			WRITE_DEFAULTS = 251,		//initializes EEPROM with default values.
			SRAM = 252,					//returns free SRAM in bytes.
			SETUP = 253,
			NONE = 254
		};
	};

	struct observing_conditions_command_
	{
		enum observing_conditions_command_t
		{
			AVERAGE_PERIOD = 0,			//gets or sets the average period.  

			REFRESH = 10,				//forces the sensors to refresh their values.

			MEASUREMENT_AGE = 20,		//gets the time since the last automatic measurement. 

			MEASURE = 30,				//starts a measurement (SET) or checks for completion (GET)
			VALUE = 31,					//gets a measurement result.

			AVERAGE = 40,				//gets an average. returns NAN if averaging is not enabled on this sensor.

			SENSOR_ENABLED = 50,		//gets sensor enabled status. 
			PERFORMS_AUTOMATIC = 51,
			PERFORMS_AVERAGING = 52,	//gets wether the senor performs averaging or not. 

			HEATER_ENABLED = 60,		//gets heater enabled status.
			HEATER = 61,				//gets or sets heater setting.

			CONNECTED = 251,			//returns 1 if the telescope is enabled, 0 otherwise.
			WRITE_DEFAULTS = 252,		//initiates writing of default values.
			SETUP = 253,				//gets or sets observing conditions settings.
			NONE = 254,					//no command (command_ == NULL)
		};
	};

	/*initializes some variables.*/
	RemoteCommandHandler();

	~RemoteCommandHandler();

	/*
	additional initialization. */
	void begin();

	/*
	queries interfaces for commands and if any have been received, executes commands. */
	void run();

	bool isInterfaceEnabled(uint8_t interface);

	static void writeDefaults();

private:

	/*
	extracts a command from an input string and stores it into input_.
	@param (by reference) input string. command will be removed from this string. 
	@return true if a command was returned, false otherwise. */
	bool extractCommand(String& in);

	/*
	fetches the input of the given command interface. 
	@param interface interface to check. */
	void fetchCommand(uint8_t interface);

	/*called once after a serial message has been successfully received.
	tries to parse the target device from the serial message and calls the appropriate
	[device]Command() function if successful. sends an error message to the pc when parsing 
	of the target device fails.*/
	void executeCommand();

	/*returns the correct array delimiter for the given setup option.
	@param setup_option
	@return: array delimiter*/
	static char getArrayDelimiter(uint16_t setup_device, uint16_t setup_option);

	/*@param the setup option
	@return the array size for the given setup option*/
	static uint8_t getArraySize(uint16_t setup_device, uint16_t setup_option);

	/*returns the device identified by the given string.*/
	static uint8_t getDeviceType(char* chr);

	/*returns the type of command specified by the given string. */
	uint8_t getCommandType();

	/*returns the command specified by the given string for the given device.
	@param:
	 - device identifier (EEPROMHandler::device_type_ member) */
	uint8_t parseCommand(uint8_t device);

	/*
	calls strotk(char* string, char* delimiter) with the defined delimiters.
	@param:
	 - string to be tokenized.
	@return:
	 - pointer to the token or NULL pointer (if none found).*/
	char* tokenize(char* chr);

	//parse general device command.
	void generalCommand();
	void generalSetup(uint8_t command_type);

	void observingConditionsCommand();
	void observingConditionsSetup(uint8_t command_type);

	//sends an error message to the computer.
	//error messages have the following format:
	//ERROR_#
	void sendError(uint16_t error_code);

	/*sends a response to the computer. 
	 - calls sendResponse(char* device, char* command, float return_value) using the
		 last set values of device_ and command_.
	@params:
	 - return value of the last executed command.*/
	void sendResponse(float return_value);

	/*sends a response to the computer. template for all data types except float.
	 - calls sendResponse(char* device, char* command,  long return_value) using the
		 last set values of device_ and command_.
	@params:
	 - return value of the last executed command.*/
	template <class T> void sendResponse(const T& return_value);

	/*sends a response to the computer. Supports Serial and TCP connection. response is 
	sent using the interface the command was received on.
	@params:
	 - device: a string / char array specifying the device (e.g. 'W' = filter wheel)
	 - command: a string / char array specifying the command that has been executed
	 - return_value: return value of the command. default to 0 when not available. when return_value
		 is a float, print to serial with 7 digits after the decimal point.*/
	void sendResponse(char* device, char* command, String return_value);

	/*sends the given message using the current interface.*/
	template <class T> void sendRaw(const T& message);

	///*
	//converts the given time to a String in ISO8601 format.
	//@param time: time in seconds since 1970-01-01T00:00:00.
	//@return: given time in ISO8601 format.*/
	//static String timeToISO8601(time_t time);
	//
	///*
	//converts the given time ti a String in HH:MM:SS format.
	//@param time: time in seconds since 1970-01-01T00:00:00.
	//@param format_12_hrs: when set to true, returned string will be in 12hr format 
	//(default: 24hr format).
	//@return: given time in HH:MM:SS format.	*/
	//static String timeToHMS(time_t time, bool format_12_hrs = false);
	//
	///*
	//converts the given time to a String in MM/DD/YY format.
	//@param time: time in seconds since 1970-01-01T00:00:00.
	//@param delimiter: delimiter to use (default: '/').
	//@return: given date in MM/DD/YY format.	*/
	//static String timeToMDY(time_t time, char delimiter = '/');
	//
	//static String timeTo12Hrs(time_t time);
	//
	//static String timeTo24Hrs(time_t time);

	/*reads a value from EEPROM and sends it to the pc.*/
	void readOption(
		uint16_t setup_device,
		uint16_t setup_option,
		uint16_t index,
		uint8_t variable_type);

	/*parses a string to a variable of the given type / size, saves it to the EEPROM and 
	sends it to the pc.*/
	void writeOption(
		uint16_t setup_device,
		uint16_t setup_option,
		uint16_t index,
		uint8_t variable_type);

	//uint8_t protocol_;				//protocol of the current command (YAAADEVICE or LX200)

	char input_[COMMAND_MAX_LENGTH + 1];			//complete input string.
	char* device_;		//pointer to part of message indicating recipient device for the command.
	char* command_;		//pointer to part of message containing a command identifier.
	char* value_;		//pointer to part of message holding a value.

	uint8_t command_interface_current_;
	CommandInterface *command_interfaces_[NUM_COMMAND_INTERFACES];			//collection of all command interface instances

	String input_buffers_[NUM_COMMAND_INTERFACES];	//holds all input buffers
	String input_string_;

	//TODO 
	//fetch received data from commandinterface* classes
	//store commandinterface* input in this class for all commandinterfaces

};

extern RemoteCommandHandler remote_command_handler_;

#endif /*REMOTECOMMANDHANDLER_H_*/

