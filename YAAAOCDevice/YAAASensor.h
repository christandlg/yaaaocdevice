//Sensor class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef YAAASENSOR_H_
#define YAAASENSOR_H_

#include <Arduino.h>

#include "defaults.h"

//#define QUERIES_PER_AVERAGE_INTERVAL 10		//queries to hasValue() per average interval 

class YAAASensor
{
public:
	enum sensor_type_t : uint8_t
	{
		SENSOR_BRIGHTNESS = 10,
		SENSOR_CLOUD_COVERAGE = 20,
		SENSOR_DEW_POINT = 40,
		SENSOR_HUMIDITY = 70,
		SENSOR_LIGHTNING = 120,
		SENSOR_PRESSURE = 150,
		SENSOR_RAIN = 170,
		SENSOR_RAIN_RATE = 171,
		SENSOR_SKY_QUALITY = 180,
		SENSOR_STAR_FWHM = 181,
		SENSOR_TEMPERATURE = 190,
		SENSOR_WIND_DIRECTION = 220,
		SENSOR_WIND_GUST = 221,
		SENSOR_WIND_SPEED = 222,
		SENSOR_UNKNOWN = 255,			//sensor type unknown or sensor disabled. 
	};

	enum automatic_measurement_state_t : uint8_t
	{
		STATE_IDLE,
		STATE_MEASUREMENT_RUNNING,
		STATE_MEASUREMENT_COMPLETE
	};

	//struct containing information about sensors.
	//parameters is sensor type specific.
	//BH1750FVI illumination sensor
	// - (1 byte) I2C address
	// - (1 byte) RFU
	// - (2 bytes) optical window transmission rate ( dec2hex(round( 65535 x e)), owtr = 0.0 ... 1.0 )
	// - (6 bytes) RFU
	//DHT11 humidity sensor
	// - (1 byte) pin number
	// - (1 byte) DHT11 or DHT22 (as SensorHumidityDHT11::dht_type_t)
	// - (8 bytes) RFU
	//HTU21 humidity / temperature sensor:
	// - (1 byte) I2C address
	// - (9 bytes) RFU
	//BMP180 barometric pressure / temperature sensor:
	// - (1 byte) I2C address
	// - (1 byte) oversampling setting
	// - (8 bytes) RFU
	//BMx280 humidity / pressure / temperature sensor:
	// - (1 byte) Interface
	// - (1 byte) I2C Address / CS pin
	// - (1 byte) humidity oversampling setting
	// - (1 byte) pressure oversampling setting
	// - (1 byte) temperature oversampling setting
	// - (1 byte) filter settings
	// - (1 byte) measurement mode (forced / normal)
	// - (1 byte) standby time (forced measurements only)
	// - (2 bytes) RFU
	//FC-37 analog / digial rain sensor:
	// - (1 byte) input pin
	// - (1 byte) input mode (true: analog, false: digital)
	// - (1 byte) input inverted (false: triggers above threshold / digital HIGH, true: triggers below threshold / digital LOW
	// - (1 byte) RFU
	// - (2 byte) analog threshold value
	// - (4 byte) RFU
	//Tipping Bucket Rain Rate sensor
	// - (1 byte) input pin, must be able to trigger an interrupt
	// - (1 byte) RFU
	// - (4 bytes) count rotate interval in ms
	// - (2 bytes) um of rain per count
	// - (2 bytes) RFU
	//MLX90614 contactless temperature sensor
	// - (1 byte) I2C Address
	// - (1 byte) zone (one of SensorTemperatureMLX90614::RAM_TEMP_OBJ1, SensorTemperatureMLX90614::RAM_TEMP_OBJ2, SensorTemperatureMLX90614::RAM_TEMP_A)
	// - (2 bytes) emissivity correction coefficient ( dec2hex(round( 65535 x e)), e = 0.1 ... 1.0 )
	// - (6 bytes) RFU
	//AS3935 Franklin Lightning Sensor
	// - (1 byte) Interface type (0 - I2C, 1 - SPI)
	// - (1 byte) chip select pin / I2C address
	// - (1 byte) interrupt pin
	// - (1 byte) analog frontend setting (bits 0-4) and noise floor setting (bits 5-7)
	// - (1 byte) watchdog threshold (bits 0-3) and antenna tuning (upper 4-7)
	// - (1 byte) spike rejection (bits 0-3) and minimum number of lightning events (bits 4-5) and  mask disturber (bit 6)
	// - (2 byte) detection lifetime in seconds
	// - (2 byte) auto adjust interval in seconds
	struct SensorSettings
	{
		uint8_t sensor_type_;				//sensor type as sensor_type_t
		uint8_t sensor_device_;				//sensor device 
		uint8_t unit_;						//measurement result unit / type 
		uint8_t parameters_[10];			//additional parameters (sensor dependent)

		bool operator==(const SensorSettings& a) const
		{
			bool equal = (
				sensor_type_ == a.sensor_type_ &&
				sensor_device_ == a.sensor_device_ &&
				unit_ == a.unit_);

			for (uint8_t i = 0; i < 10; i++)
				equal = (parameters_[i] == a.parameters_[i]);

			return equal;
		}
	};

	YAAASensor(YAAASensor::SensorSettings sensor_settings);

	virtual ~YAAASensor();

	/*
	initializes the sensor.
	@return true if initialization was successful, false if any error occurred that could result in the sensor
	not operating as expected (invalid pin numbers, communication errors, invalid default unit, ...) */
	virtual bool begin() = 0;

	/*
	starts a measurement.
	@return true on success, false otherwise.*/
	virtual bool measure() = 0;

	/*
	checks if a measurement has been completed. may perform work necessary to complete a mesurement. user is
	expected to call this function after calling measure() and call it until it returns true.
	additional calls to hasValue shall not change the result.
	@return true if a measurement has been completed, false otherwise. */
	virtual bool hasValue() = 0;

	/*
	returns the last measured value.
	user is expected to call this function only after hasValue() has returned true.
	repeated calls to getValue shall return the same value unless another measurement has been completed.
	@return the sensor value. if a measurement failed, this value may be undefined. */
	virtual float getValue() = 0;

	/*
	@return true if an event happened that reqires the owner classes attention. */
	virtual bool hasEvent();

	/*
  handles an event. shall not do anything if being called without an event to handle. 
  after handleEvent() has been called hasEvent() shall not return true 
  until another event has occurred or event handling is not yet finished.
	@return a String representing the event.  */
	virtual String handleEvent();

	/*
	@return sensor type as sensor_type_t. */
	virtual uint8_t getSensorType() = 0;

	/*
	@return unit of data returned by the sensor, as defined in sensor's base class. */
	virtual uint8_t getUnit() = 0;

	/*
	attempts to set the unit used when the sensor returns values. does nothing if an invalid unit is given.
	@param unit as defined by derived classes.
	@return true on success, false otherwise. */
	virtual bool setUnit(uint8_t unit) = 0;

	/*
	@return time since last measurement start, in milliseconds. */
	uint32_t getMeasurementAge();

	/*
	@return pointer to */
	virtual void *getSensor();

protected:
	/*
	updates the time of last measurement to the current time. only to be called
	after a successful start of measurement. */
	void updateMeasurementTime();

private:
	//TODO move to derived classes?
	volatile uint32_t measure_last_;					//last time (in ms) a measurement was started successfully. must be updated by derived classes.
};

#endif /* YAAASENSOR_H_ */
