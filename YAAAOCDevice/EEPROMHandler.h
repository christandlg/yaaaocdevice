//EEPROMHandler class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef EEPROMHANDLER_H_
#define EEPROMHANDLER_H_

#include "Arduino.h"

#include "features.h"

#ifdef ARDUINO_SAM_DUE
#include <DueFlashStorage.h>
#else
#include <EEPROM.h>
#endif

#define TEMP_SENSORS 3		//maximum number of temperature sensors per device.

class EEPROMHandler
{
public:
	struct device_type_
	{
		enum DeviceType_t_ : uint16_t
		{
			GENERAL = 0,				          //general variables (e.g. serial communication connection speed).
			OBSERVING_CONDITIONS = 2048   //variables for observing conditions class.
		};
	};

	struct general_variable_
	{
		enum GeneralVariable_t_ : uint16_t
		{
			EEPROM_CONTROL = 0,				//(4 byte) magic number to determine if EERPOM needs to be initialized.
			SERIAL_BAUD_RATE = 4,			//(4 byte) baud rate of the serial interface.

			//INTERFACE_SERIAL = 10,			//(1 byte) true if serial connection is enabled. 
			INTERFACE_ETHERNET_RAW = 10,	//(1 byte) true if ethernet raw TCP interface is enabled. 
			INTERFACE_ETHERNET_MQTT = 11,	//(1 byte) true if ethernet MQTT interface is enabled. 
			INTERFACE_WIFI_RAW = 14,		//(1 byte) true if wifi raw TCP interface is enabled. 
			INTERFACE_WIFI_MQTT = 15,		//(1 byte) true if wifi MQTT interface is enabled. 
			INTERFACE_LORA = 18,			//(1 byte) true if LORA interface is enabled. 

			DISPLAY_TYPE = 30,				//(1 byte) display type.
			DISPLAY_RES_X = 32, 			//(2 byte) display resolution (x axis).
			DISPLAY_RES_Y = 34,				//(2 byte) display resolution (y axis).
			DISPLAY_UPDATE_INTERVAL = 36,	//(4 byte) display update interval in milliseconds.
			DISPLAY_CONTRAST = 40,			//(1 byte) display contrast setting
			DISPLAY_BRIGHTNESS = 41,		//(1 byte) display brightness setting

			DISPLAY_INTERFACE_TYPE = 50,	//(1 byte) display interface type (parallel, I2C, SPI, ...)
			DISPLAY_INTERFACE_PIN0 = 51,	//(1 byte) display interface pin 0.
			DISPLAY_INTERFACE_PIN1 = 52,	//(1 byte) display interface pin 1.
			DISPLAY_INTERFACE_PIN2 = 53,	//(1 byte) display interface pin 2.
			DISPLAY_INTERFACE_PIN3 = 54,	//(1 byte) display interface pin 3.
			DISPLAY_INTERFACE_PIN4 = 55,	//(1 byte) display interface pin 4.
			DISPLAY_INTERFACE_PIN5 = 56,	//(1 byte) display interface pin 5.
			DISPLAY_INTERFACE_PIN6 = 57,	//(1 byte) display interface pin 6.
			DISPLAY_INTERFACE_PIN7 = 58,	//(1 byte) display interface pin 7.
			DISPLAY_INTERFACE_PIN8 = 59,	//(1 byte) display interface pin 8.
			DISPLAY_INTERFACE_PIN9 = 60,	//(1 byte) display interface pin 9.
			DISPLAY_INTERFACE_PIN10 = 61,	//(1 byte) display interface pin 10.
			DISPLAY_INTERFACE_PIN11 = 62,	//(1 byte) display interface pin 11.
			DISPLAY_INTERFACE_PIN12 = 63,	//(1 byte) display interface pin 12.
			DISPLAY_INTERFACE_PIN13 = 64,	//(1 byte) display interface pin 13.
			DISPLAY_INTERFACE_PIN14 = 65,	//(1 byte) display interface pin 14 (Brightness).
			DISPLAY_INTERFACE_PIN15 = 66,	//(1 byte) display interface pin 15 (Contrast). 

			ETHERNET_ENABLED = 80,			//(1 byte) true if ethernet connection is enabled.
			ETHERNET_DHCP = 81,				//(1 byte) true if DHCP should be used.
			ETHERNET_MAC = 82,				//(6 byte) MAC address of the ethernet shield
			ETHERNET_IP = 88,				//(4 byte) ip address (if not using DHCP)
			ETHERNET_GATEWAY = 92,			//(4 byte) gateway (if not using DHCP)
			ETHERNET_DNS = 96,				//(4 byte) DNS server to use
			ETHERNET_SUBNET = 100,			//(4 byte) subnet mask (if not using DHCP)
			ETHERNET_PORT = 104,			//(2 byte) port number to use.

			SD_CS_PIN = 110,				//(1 byte) SD card chip select pin.
			SD_LOG = 111,					//(1 byte) true if commands and responses should be logged to the SD card.

			ONEWIRE_PIN = 120,				//(1 byte) One Wire interface pin.

			I2C_PIN_SCL = 130,				//(1 byte) I2C SCL pin
			I2C_PIN_SDA = 131,				//(1 byte) I2C SDA pin

			TIME_UTC_OFFSET = 140,			//(4 byte) offset (in hours) from UTC.

			WIFI_ENABLED = 180,		  				//(1 byte) true if the WiFi connection is enabled.  
			WIFI_DHCP = 181,			    		//(1 byte) true if DHCP is used for WiFi connections. 
			WIFI_MAC = 182,				     		//(6 bytes) WiFi adapter MAC address (need not be set on ESP8266 / ESP32)
			WIFI_IP = 188,				  			//(4 bytes) IP address of adapter (ignored if DHCP is used)
			WIFI_GATEWAY = 192,			  			//(4 bytes) gateway address for WiFi connections (ignored if DHCP is used)
			WIFI_DNS = 196,							//(4 bytes) DNS server address for WiFi connections (ignored if DHCP is used)
			WIFI_SUBNET = 200,						//(4 bytes) WiFi subnet mask for WiFi connections (ignored if DHCP is used)
			WIFI_PORT = 204,						//(2 bytes) [deprecated] true if WiFi raw tcp command interface is enabled. 
			WIFI_CHANNEL = 206,						//(1 byte) WIFI channel. 0 to automatically select.
			WIFI_SSID = 210,						//(32 bytes max) c string containing the SSID (excluding '\0').
			WIFI_PASSWORD = 250,					//(63 bytes max) c string containing the password (excluding '\0').
			WIFI_HOSTNAME = 315,					//(63 bytes max) c string containing the host name (excluding '\0').

#if defined(ESP8266) || defined(ESP32)
			OTA_MODE = 400,							//OTA mode as OTAHandler::mode_t.
			OTA_HOSTNAME = 402,						//(63 bytes max) c string containing OTA hostname

			OTA_ARDUINOOTA_PORT = 476,				//port for OTA     

			OTA_ARDUINOOTA_PASSWORD_HASH = 478,		//(1 byte) true if password is provided as a MD5 hash.
			OTA_ARDUINOOTA_PASSWORD = 480,			//(63 bytes max) c string containing password for OTA

			OTA_WEBBROWSER_PORT = 550,				//(2 bytes) port to use for Webbrowser based OTA update.
			OTA_WEBBROWSER_PATH = 553,				//(63 bytes max) path to Webbrowser OTA update page. 
			OTA_WEBBROWSER_USERNAME = 617,			//(63 bytes max) Webbrowser OTA update page username.
			OTA_WEBBROWSER_PASSWORD = 681,			//(63 bytes max) Webbrowser OTA update page password.

			OTA_HTTPSERVER_UPDATE_INTERVAL = 750,	//(4 bytes) checks for new versions every n milliseconds. set to 0 to disable. 
			OTA_HTTPSERVER_ADDRESS = 753,
			OTA_HTTPSERVER_PATH = 817,
			OTA_HTTPSERVER_PORT = 880,
#endif /* defined(ESP8266) || defined(ESP32) */

			//#ifdef FEATURE_LoRa
			LoRa_ENABLED = 1024,					//(1 byte) true to enable LoRa 
			LoRa_SECURE = 1025,						//(1 byte) true to secure LoRa
			LoRa_CRC = 1026,						//(1 byte) true to enable CRC
			LoRa_INVERTIQ = 1027,					//(1 byte) true to enable invert IQ

			LoRa_PIN_CS = 1030,						//(1 byte) CS pin
			LoRa_PIN_RST = 1031,					//(1 byte) RST pin. set to 255 if the reset pin is not MCU controlled
			LoRa_PIN_DIO0 = 1032,					//(1 byte) DIO0 pin
			LoRa_PIN_DIO1 = 1033,					//(1 byte) DIO1 pin
			LoRa_PIN_DIO2 = 1034,					//(1 byte) DIO2 pin
			LoRa_SPI_FREQUENCY = 1035,				//(4 bytes) SPI frequency

#if defined(ESP8266) || defined(ESP32)
			//SPI pins are only used on ESP8266/ESP32.
			LoRa_SPI_SCK = 1040,					//(1 byte) SPI SCK pin
			LoRa_SPI_MISO = 1041,					//(1 byte) SPI MISO pin
			LoRa_SPI_MOSI = 1042,					//(1 byte) SPI MOSI pin
#endif /* defined(ESP8266) || defined(ESP32) */

			LoRa_PIN_OUTPUT = 1050,					//(2 bytes) RF output pin of LoRa frontend
			LoRa_TX_POWER = 1052,					//(2 bytes) tx power
			LoRa_SPREADING_FACTOR = 1054,			//(2 bytes) spreading factor
			LoRa_BAND = 1056,						//(4 bytes) LoRa band, in Hz (as float)
			LoRa_BANDWIDTH = 1060,					//(4 bytes) signal bandwidth 
			LoRa_CODING_RATE = 1064,				//(2 bytes) coding rate 
			LoRa_SYNCWORD = 1066,					//(2 bytes) sync word
			LoRa_PREAMBLE_LENGTH = 1068,			//(2 bytes) preamble length
			//#endif /* FEATURE_LoRa */

			//#ifdef FEATURE_MQTT
			MQTT_ENABLED = 1536,					//(1 byte) set to true to enable MQTT. 
			MQTT_SECURE = 1537,						//(1 byte) set to true to enable MQTT via SSL/TLS. 
			MQTT_SERVER = 1538,						//(63 bytes max) MQTT server, ip (as string) or domain name.
			MQTT_PORT = 1602,						//(2 bytes) MQTT port
			MQTT_CLIENT_ID = 1604,					//(63 bytes max) MQTT client ID
			MQTT_USER = 1668,						//(63 bytes max) MQTT user name
			MQTT_PASSWORD = 1732,					//(63 bytes max) MQTT password
			MQTT_TOPIC_PUBLISHED = 1796,			//(63 bytes max) MQTT published topic root.
			MQTT_TOPIC_SUBSCRIBED = 1860,			//(63 bytes max) MQTT subscribed topic. set to "" to disable subscription.
			//#endif /* FEATURE_MQTT */

			//DEVICE_NAME = 190,					//(32 bytes) device name
			//
			//DEVICE_INFO = 222						//(32 bytes) additional device information
		};
	};

	struct observing_conditions_variable_
	{
		enum ObservingConditionsVariable_t_ : uint16_t
		{
			LOG_CSV_FILE = 0,						//(1 byte) true if a .csv log file should be created, false otherwise. 
			LOG_CSV_ENABLED = 1,					//(1 byte) default value for log mode
			LOG_CSV_DELIMITER = 2,					//(1 byte) delimiter for .csv file.
			LOG_CSV_INTERVAL = 4,					//(4 bytes) log interval for .csv file logging.

			AVERAGE_PERIOD = 40,					//(4 bytes) average period for sensors, in ms.

			SENSOR_CONFIGURATION = 100,				//(15*20 bytes) observing condition sensor configuration.

			HEATER_CONFIGURATION = 451					//(3*20 bytes) heater settings.
		};
	};

	enum data_type_t
	{
		UINT8_T_,				//8bit unsigned integer.
		UINT16_T_,				//16bit unsigned integer.
		UINT32_T_,				//32bit unsigned integer.
		INT16_T_,				//16bit signed integer.
		INT32_T_,				//32bit signed integer.
		FLOAT_,					//32bit float.
		ARRAY_,
		SENSOR_SETTINGS_,		//YAAASensor::SensorSettings struct.
		SENSOR_CONFIGURATION_,	//ObservingConditions::SensorConfiguration struct.
		HEATER_CONFIGURATION_,	//YAAAObservingConditions HeaterConfiguration struct.
		STRING_,				//null terminated c string.
		UNKNOWN_ = 255
	};

public:
	/*
	initializes the device's EEPROM, if necessary. */
	static bool begin();

	/*returns the variable type for the given variable of the given device.
	@param:
	- device identifier (EEPROMHandler::device_type_)
	- option identifier (EEPROMHandler::[device_]variable_)
	@return:
	- variable size as variable_size_t.*/
	static uint8_t getVariableType(uint16_t device, uint16_t variable);

	/*
	parses a string to a variable.
	@param device:
	@param string: string containing variable name
	@param variable:
	@return: true if the variable was parsed correctly, false otherwise.*/
	static uint16_t stringToVariable(uint16_t device, char* string);

	/*
	gets the maximum length of a string stored in EEPROM.
	@param device
	@param variable
	@return maximum length of String in EEPROM. */
	static uint8_t getEEPROMMaxStringLength(uint16_t device, uint16_t variable);

	/*
	writes a given variable to the Arduinos EEPROM.
	@param device: device as DeviceType_t_
	@param variable: variable as [device]Variable_t
	@param index: additional index (i.e. filter number)
	@param value: value to write.*/
	template <class T> static void writeValue(uint16_t device, uint16_t variable, uint16_t index, const T& value)
	{
		uint16_t address = device + variable + calculateOffset(device, variable, index);

#ifndef EEPROM_WRITE_LOCK
#ifdef ARDUINO_SAM_DUE
		//use Due Flash Storage library
		uint16_t size = sizeof(value);
		byte content[size];

		memcpy(content, &value, size);

		//does not overwrite same value
		for (uint8_t i = 0; i < size; i++)
		{
			if (content[i] != dfs_.read(address + i))
				dfs_.write(address + i, content[i]);
		}
#elif defined(ESP8266) || defined(ESP32)
		EEPROM.put(address, value);

		EEPROM.commit();

		yield();
#else 
		EEPROM.put(address, value);
#endif
#endif /* EEPROM_WRITE_LOCK */
	}
	static void writeValue(uint16_t device, uint16_t variable, uint16_t index, String value);		//added to prevent use of writeValue to write strings. 
	static void writeValue(uint16_t device, uint16_t variable, uint16_t index, const char *value);	//added to prevent use of writeValue to write strings. 
	/*
	reads a given variable to the Arduinos EEPROM.
	@param device: device as DeviceType_t_
	@param variable: variable as [device]Variable_t
	@param index: additional index (i.e. filter number)
	@param value: value to read (by reference).*/
	template <class T> static void readValue(uint16_t device, uint16_t variable, uint16_t index, T& value)
	{
		uint16_t address = device + variable + calculateOffset(device, variable, index);

#ifdef ARDUINO_SAM_DUE
		//use Due Flash Storage library
		byte* content = dfs_.readAddress(address);
		memcpy(&value, content, sizeof(value));
#elif defined(ESP8266) || defined(ESP32)
		EEPROM.get(address, value);

		yield();
#else 
		EEPROM.get(address, value);
#endif
	}
	static void readValue(uint16_t device, uint16_t variable, uint16_t index, String value);		//added to prevent use of readValue to read strings. 
	static void readValue(uint16_t device, uint16_t variable, uint16_t index, const char *value);	//added to prevent use of readValue to read strings. 

	/*
	reads a given array from the Arduinos EEPROM.
	@param device: device as DeviceType_t_
	@param variable: variable as [device]Variable_t
	@param index: additional index (i.e. filter number)
	@param content: pointer to array to read to.
	@param size: number of bytes to read.*/
	static void readArray(uint16_t device, uint16_t variable, uint16_t index, byte *content, uint16_t size)
	{
		uint16_t address = device + variable + calculateOffset(device, variable, index);

		for (uint8_t i = 0; i < size; i++)
		{
#ifdef ARDUINO_SAM_DUE
			content[i] = dfs_.read(address + i);
#elif defined(ESP8266) || defined(ESP32)
			EEPROM.get(address + i, content[i]);

			yield();
#else
			EEPROM.get(address + i, content[i]);
#endif
		}
	}

	/*
	writes a given array to the Arduinos EEPROM.
	@param device: device as DeviceType_t_
	@param variable: variable as [device]Variable_t
	@param index: additional index (i.e. filter number)
	@param content: pointer to array to write.
	@param size: number of bytes to write.*/
	static void writeArray(uint16_t device, uint16_t variable, uint16_t index, byte *content, uint16_t size)
	{
		uint16_t address = device + variable + calculateOffset(device, variable, index);

		for (uint8_t i = 0; i < size; i++)
		{
#ifndef EEPROM_WRITE_LOCK
#ifdef ARDUINO_SAM_DUE
			if (content[i] != dfs_.read(address + i))
				dfs_.write(address + i, content[i]);
#elif defined(ESP8266) || defined(ESP32)
			EEPROM.put(address + i, content[i]);
#else
			EEPROM.put(address + i, content[i]);
#endif
		}

#if defined(ESP8266) || defined(ESP32)
		EEPROM.commit();

		yield();
#endif
#endif /* EEPROM_WRITE_LOCK */
	}

	/*
	reads a given array from the Arduinos EEPROM.
	@param device: device as DeviceType_t_
	@param variable: variable as [device]Variable_t
	@param index: additional index (i.e. filter number)
	@param content: pointer to char array to read to.
	@param size: number of chars to read.*/
	static void readArray(uint16_t device, uint16_t variable, uint16_t index, char *content, uint16_t size)
	{
		uint16_t address = device + variable + calculateOffset(device, variable, index);

		for (uint8_t i = 0; i < size; i++)
		{
#ifdef ARDUINO_SAM_DUE
			content[i] = dfs_.read(address + i);
#elif defined(ESP8266) || defined(ESP32)
			EEPROM.get(address + i, content[i]);

			yield();
#else
			EEPROM.get(address + i, content[i]);
#endif
		}
	}

	/*
	writes a given array to the Arduinos EEPROM.
	@param device: device as DeviceType_t_
	@param variable: variable as [device]Variable_t
	@param index: additional index (i.e. filter number)
	@param content: pointer to char array to write.
	@param size: number of chars to write.*/
	static void writeArray(uint16_t device, uint16_t variable, uint16_t index, char *content, uint16_t size)
	{
		uint16_t address = device + variable + calculateOffset(device, variable, index);

		for (uint8_t i = 0; i < size; i++)
		{
#ifndef EEPROM_WRITE_LOCK
#ifdef ARDUINO_SAM_DUE
			if (content[i] != dfs_.read(address + i))
				dfs_.write(address + i, content[i]);
#elif defined(ESP8266) || defined(ESP32)
			EEPROM.put(address + i, content[i]);
#else
			EEPROM.put(address + i, content[i]);
#endif
		}

#if defined(ESP8266) || defined(ESP32)
		EEPROM.commit();

		yield();
#endif
#endif /* EEPROM_WRITE_LOCK */
	}

	/*
	reads a given array from the Arduinos EEPROM.
	@param device: device as DeviceType_t_
	@param variable: variable as [device]Variable_t
	@param index: additional index (i.e. filter number)
	@param content: pointer to char array to read to.
	@param size: number of chars to read.*/
	static void readString(uint16_t device, uint16_t variable, uint16_t index, String &content)
	{
		uint16_t address = device + variable + calculateOffset(device, variable, index);

		char buffer[64];

		for (uint8_t i = 0; i < 64; i++)
		{
#ifdef ARDUINO_SAM_DUE
			buffer[i] = dfs_.read(address + i);
#elif defined(ESP8266) || defined(ESP32)
			EEPROM.get(address + i, buffer[i]);
#else
			EEPROM.get(address + i, buffer[i]);
#endif
			//stop reading if a '\0' char was read
			if (buffer[i] == '\0')
				break;
		}

		//add a '\0' character at the end, just in case
		buffer[63] == '\0';

		//create string
		content = String(buffer);

		yield();
	}

	/*
	writes a string to the Arduinos EEPROM.
	@param device: device as DeviceType_t_
	@param variable: variable as [device]Variable_t
	@param index: additional index (i.e. filter number)
	@param content: String to write. */
	static void writeString(uint16_t device, uint16_t variable, uint16_t index, String content)
	{
		//cut string to size
#if defined (ESP8266) || defined(ESP32)
		content.remove(_min(static_cast<uint8_t>(63), getEEPROMMaxStringLength(device, variable)));
#else
		content.remove(min(static_cast<uint8_t>(63), getEEPROMMaxStringLength(device, variable)));
#endif

		uint16_t address = device + variable + calculateOffset(device, variable, index);

		//make sure to save the '\0' character as well
		for (uint8_t i = 0; i <= content.length(); i++)
		{
#ifndef EEPROM_WRITE_LOCK
#ifdef ARDUINO_SAM_DUE
			if (content.c_str()[i] != dfs_.read(address + i))
				dfs_.write(address + i, content.c_str()[i]);
#elif defined(ESP8266) || defined(ESP32)
			EEPROM.put(address + i, content.c_str()[i]);
#else
			EEPROM.put(address + i, content.c_str()[i]);
#endif
		}

#if defined(ESP8266) || defined(ESP32)
		EEPROM.commit();

		yield();
#endif
#endif /* EEPROM_WRITE_LOCK */
	}

private:
	EEPROMHandler();		//prevents instantiation of class
	~EEPROMHandler();

	/*
	calculates a device and variable dependent offset for a given variable of a given
	device in the Arduinos EEPROM.
	@param device: device as DeviceType_t.
	@param variable: variable as [device]Variable_t.
	@param index: additional index (i.e. filter number).
	@return: offset from address 0 in bytes.*/
	static uint16_t calculateOffset(uint16_t device, uint16_t variable, uint16_t index);

#ifdef ARDUINO_SAM_DUE
	static DueFlashStorage dfs_;
#endif
};

#endif /* EEPROMHANDLER_H_ */

