//Wind Direction sensor class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "SensorWindDirection.h"

SensorWindDirection::SensorWindDirection(YAAASensor::SensorSettings sensor_settings) : 
	YAAASensor(sensor_settings)
{
	setUnit(sensor_settings.unit_);
}


SensorWindDirection::~SensorWindDirection()
{
}

uint8_t SensorWindDirection::getSensorType()
{
	return YAAASensor::SENSOR_WIND_DIRECTION;
}

uint8_t SensorWindDirection::getUnit()
{
	return 0;
}

bool SensorWindDirection::setUnit(uint8_t unit)
{
	switch (unit)
	{
		case SensorWindDirection::UNIT_DEGREES:
			return true;
		default:
			break;
	}

	return false;
}
