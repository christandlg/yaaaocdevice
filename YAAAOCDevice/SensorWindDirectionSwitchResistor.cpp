//Wind Direction sensor (resistor network based) class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "SensorWindDirectionSwitchResistor.h"

SensorWindDirectionSwitchResistor::SensorWindDirectionSwitchResistor(YAAASensor::SensorSettings sensor_settings) :
	SensorWindDirection(sensor_settings),
	pin_(255),
	angle_offset_(DEF_SENSOR_WIND_DIRECTION_ANGLE_OFFSET),
	analog_value_(0)
{
	//read pin and angle offset from sensor_settings
	pin_ = sensor_settings.parameters_[0];

	int16_t angle_offset = (sensor_settings.parameters_[6] << 8 | sensor_settings.parameters_[7]);

	angle_offset_ = static_cast<float>(angle_offset) / 32767.0f * 180.0f;
}


SensorWindDirectionSwitchResistor::~SensorWindDirectionSwitchResistor()
{
}

bool SensorWindDirectionSwitchResistor::begin()
{
	//check if the input pin is connected to an ADC.
#ifdef ARDUINO_SAM_DUE
	if ((pin_ < 54) || pin_ > 65)	//Arduino Due: pins 54 to 65 are analog input pins
#elif defined ARDUINO_AVR_MEGA2560		//__AVR_ATmega2560__
	if ((pin_ < 54) || pin_ > 69)	//Arduino Mega: pins 54 to 69 are analog input pins
#elif defined ESP8266	//__ESP8266_ESP8266__
	if (pin_ != A0)					//ESP8266: only 1 analog input pin is available.
#elif defined ESP32	
	if (pin_ != A0 &&
		pin_ != A4 &&
		pin_ != A5 &&
		pin_ != A6 &&
		pin_ != A7 &&
		pin_ != A10 &&
		pin_ != A11 &&
		pin_ != A12 &&
		pin_ != A13 &&
		pin_ != A14 &&
		pin_ != A15 &&
		pin_ != A16 &&
		pin_ != A17 &&
		pin_ != A18 &&
		pin_ != A19)
#else
	if (true)
#endif
		return false;

	pinMode(pin_, INPUT);

	return true;
}

bool SensorWindDirectionSwitchResistor::measure()
{
	analog_value_ = analogRead(pin_);

	updateMeasurementTime();

	//it's not possible to check if the value is meaningful beyond the pin number check that is 
	//already done in begin(), so lets assume the value is correct.
	return true;
}

bool SensorWindDirectionSwitchResistor::hasValue()
{
	//an analog value of 0 is not expected, so we can know if a measurement has been performed before by 
	//checking the analog value against 0
	return (analog_value_ != 0);
}

float SensorWindDirectionSwitchResistor::getValue()
{
	float angle = 0.0f;
	
	//mapping analog value to wind direction (0.0f - 360.0f)
	//values taken from https://cdn.sparkfun.com/assets/8/4/c/d/6/Weather_Sensor_Assembly_Updated.pdf, linked to by https://www.sparkfun.com/products/8942

	////original values
	//if (analog_value_ < 75)
	//	angle = 112.5f;
	//else if (analog_value_ < 88)
	//	angle = 67.5f;
	//else if (analog_value_ < 110)
	//	angle = 90.0f;
	//else if (analog_value_ < 156)
	//	angle = 157.5f;
	//else if (analog_value_ < 215)
	//	angle = 135.0f;
	//else if (analog_value_ < 266)
	//	angle = 202.5f;
	//else if (analog_value_ < 347)
	//	angle = 180.0f;
	//else if (analog_value_ < 434)
	//	angle = 22.5f;
	//else if (analog_value_ < 530)
	//	angle = 45.0f;
	//else if (analog_value_ < 615)
	//	angle = 247.5f;
	//else if (analog_value_ < 666)
	//	angle = 225.0f;
	//else if (analog_value_ < 744)
	//	angle = 337.5f;
	//else if (analog_value_ < 807)
	//	angle = 0.0f;
	//else if (analog_value_ < 857)
	//	angle = 292.5f;
	//else if (analog_value_ < 916)
	//	angle = 315.0f;
	//else
	//	angle = 270.0f;

	//same values as above, but divided by 1,08 to match ADC values better
	if (analog_value_ < 69)
		angle = 112.5f;
	else if (analog_value_ < 82)
		angle = 67.5f;
	else if (analog_value_ < 102)
		angle = 90.0f;
	else if (analog_value_ < 144)
		angle = 157.5f;
	else if (analog_value_ < 199)
		angle = 135.0f;
	else if (analog_value_ < 246)
		angle = 202.5f;
	else if (analog_value_ < 321)
		angle = 180.0f;
	else if (analog_value_ < 402)
		angle = 22.5f;
	else if (analog_value_ < 491)
		angle = 45.0f;
	else if (analog_value_ < 569)
		angle = 247.5f;
	else if (analog_value_ < 617)
		angle = 225.0f;
	else if (analog_value_ < 689)
		angle = 337.5f;
	else if (analog_value_ < 747)
		angle = 0.0f;
	else if (analog_value_ < 794)
		angle = 292.5f;
	else if (analog_value_ < 848)
		angle = 315.0f;
	else
		angle = 270.0f;

	//apply offset
	angle += angle_offset_;

	//wrap angle back around into 0.0f ... 360.0f range
	angle = angle - 360.0f * floor(angle / 360.0f);

	return angle;
}
