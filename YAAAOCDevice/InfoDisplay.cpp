//InfoDisplay class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "InfoDisplay.h"

InfoDisplay::InfoDisplay() :
	display_(NULL),
	update_interval_(250), update_time_prev_(0)
{
}

InfoDisplay::~InfoDisplay()
{
	delete display_;
}

void InfoDisplay::init()
{
	//read display type
	uint8_t display_type = Display::DISPLAY_NONE;
	readEEPROM(EEPROMHandler::general_variable_::DISPLAY_TYPE, display_type);

	//do nothing if the display is deactivated.
	if (display_type == Display::DISPLAY_NONE)
		return;

	//read display settings and create display object.
	switch (display_type)
	{
		case Display::DISPLAY_SEGMENT:
			break;

		case Display::DISPLAY_CHARACTER:
		{
			uint16_t num_chars, num_lines;
			uint8_t pin_D0, pin_D1, pin_D2, pin_D3;
			uint8_t pin_EN, pin_RS;
			uint8_t pin_brightness, pin_contrast;

			//read display configuration from EEPROM.
			readEEPROM(EEPROMHandler::general_variable_::DISPLAY_RES_X, num_chars);
			readEEPROM(EEPROMHandler::general_variable_::DISPLAY_RES_Y, num_lines);

			readEEPROM(EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN0, pin_RS);
			readEEPROM(EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN1, pin_EN);

			readEEPROM(EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN2, pin_D0);
			readEEPROM(EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN3, pin_D1);
			readEEPROM(EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN4, pin_D2);
			readEEPROM(EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN5, pin_D3);

			readEEPROM(EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN14, pin_brightness);
			readEEPROM(EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN15, pin_contrast);

			//initialize display object
			display_ = new DisplayHD47780(
				num_chars, num_lines,
				pin_RS, pin_EN,
				pin_D0, pin_D1, pin_D2, pin_D3,
				pin_brightness, pin_contrast
				);
		}
		break;

		//case Display::DISPLAY_U8GLIB:
		//{
		//	uint16_t num_chars, num_lines;
		//	uint8_t pin_D0, pin_D1, pin_D2, pin_D3, pin_D4, pin_D5, pin_D6, pin_D7;
		//	uint8_t pin_EN, pin_RS, pin_RW, pin_CS1, pin_CS2;
		//	uint8_t pin_brightness, pin_contrast;
		//
		//	//read display configuration from EEPROM.
		//	readEEPROM(EEPROMHandler::general_variable_::DISPLAY_RES_X, num_chars);
		//	readEEPROM(EEPROMHandler::general_variable_::DISPLAY_RES_Y, num_lines);
		//
		//	readEEPROM(EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN0, pin_RS);
		//	readEEPROM(EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN1, pin_EN);
		//
		//	readEEPROM(EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN2, pin_D0);
		//	readEEPROM(EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN3, pin_D1);
		//	readEEPROM(EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN4, pin_D2);
		//	readEEPROM(EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN5, pin_D3);
		//	readEEPROM(EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN6, pin_D4);
		//	readEEPROM(EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN7, pin_D5);
		//	readEEPROM(EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN8, pin_D6);
		//	readEEPROM(EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN9, pin_D7);
		//
		//	readEEPROM(EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN10, pin_RW);
		//	readEEPROM(EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN11, pin_CS1);
		//	readEEPROM(EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN12, pin_CS2);
		//
		//	readEEPROM(EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN14, pin_brightness);
		//	readEEPROM(EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN15, pin_contrast);
		//
		//	display_ = new DisplayU8GLIB();
		//}
		//break;

		//case Display::DISPLAY_GRAPHIC:
		//		//read display configuration from EEPROM.
		//		readEEPROM(EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN14, pin_brightness);
		//		readEEPROM(EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN15, pin_contrast);
		//
		//		display_ = new DisplayGLCD(pin_brightness, pin_contrast);
		//		//display_ = new DisplayGLCD(44);		//brightness pin 44
		//	break;

		case Display::DISPLAY_OTHER:
		default:
			break;
	}

	//do nothing if the display object was not initialized.
	if (!display_)
		return;

	//more initalization here. this is only executed if a display object has been 
	//created successfully.
	//read the update interval from EEPROM.
	readEEPROM(EEPROMHandler::general_variable_::DISPLAY_UPDATE_INTERVAL, update_interval_);

	//read brightness setting from EEPROM if the display supports setting brightness.
	if (display_->checkFeature(Display::FEATURE_BRIGHTNESS))
	{
		uint8_t brightness = 0;
		EEPROMHandler::readValue(
			EEPROMHandler::device_type_::GENERAL,
			EEPROMHandler::general_variable_::DISPLAY_BRIGHTNESS,
			0,
			brightness);

		display_->setBrightness(brightness);
	}

	//read contrast setting from EEPROM if the display supports setting contrast.
	if (display_->checkFeature(Display::FEATURE_CONTRAST))
	{
		uint8_t contrast = 0;
		EEPROMHandler::readValue(
			EEPROMHandler::device_type_::GENERAL,
			EEPROMHandler::general_variable_::DISPLAY_CONTRAST,
			0,
			contrast);

		display_->setBrightness(contrast);
	}

	if (display_->checkFeature(Display::FEATURE_TEXT))
	{
		//display_->write(0, 0, "__  _____  ___  ___ ");
		//display_->write(0, 1, "\ \/ / _ |/ _ |/ _ |");
		//display_->write(0, 2, " \  / __ / __ / __ |");
		//display_->write(0, 3, " /_/_/ |/_/ |/_/ |_|");

		//display_->write(0, 0, " _  _____  ___  ___ ");
		//display_->write(0, 1, "| |/ / _ |/ _ |/ _ |");
		//display_->write(0, 2, "|_  / __ / __ / __ |");
		//display_->write(0, 3, " /_/_/ |/_/ |/_/ |_|");

		display_->write(0, 0, "                    ");
		display_->write(0, 1, "                    ");
		display_->write(0, 2, "                    ");
		display_->write(0, 3, "                    ");
	}

	//print some custom characters (if the feature is supported)
	if (display_->checkFeature(Display::FEATURE_CUSTOM_CHAR))
	{
		//Serial.println("printing custom characters");
		//display_->addCustomChar(0, something);
		//...

		//display_->printCustomChar(8, 3, byte(0));
		//display_->printCustomChar(9, 3, byte(1));
		//display_->printCustomChar(10, 3, byte(2));
		//display_->printCustomChar(11, 3, byte(3));
		//display_->printCustomChar(12, 3, byte(4));
		//display_->printCustomChar(13, 3, byte(5));
	}

	//if (display_->checkFeature(Display::FEATURE_DRAW))
	//	display_->draw(...);
}

bool InfoDisplay::available()
{
	if (!display_)
		return false;

	return true;
}

void InfoDisplay::run()
{
	//do nothing if no display is available
	if (!display_)
	{
		//Serial.println("no display");
		return;
	}

	//do stuff...

	//send new data to the display if an update is due
	if ((millis() - update_time_prev_) > update_interval_)
	{
		update_time_prev_ = millis();

		uint8_t line = 0;

		//do things....
	}

	//update what is displayed.
	display_->update();
}

void InfoDisplay::writeDefaults()
{
	writeEEPROM(EEPROMHandler::general_variable_::DISPLAY_TYPE, static_cast<uint8_t>(DEF_G_DISPLAY_TYPE));
	writeEEPROM(EEPROMHandler::general_variable_::DISPLAY_RES_X, static_cast<uint16_t>(DEF_G_DISPLAY_RES_X));
	writeEEPROM(EEPROMHandler::general_variable_::DISPLAY_RES_Y, static_cast<uint16_t>(DEF_G_DISPLAY_RES_Y));
	writeEEPROM(EEPROMHandler::general_variable_::DISPLAY_UPDATE_INTERVAL, static_cast<uint32_t>(DEF_G_DISPLAY_UPDATE_INTERVAL));
	writeEEPROM(EEPROMHandler::general_variable_::DISPLAY_CONTRAST, static_cast<uint8_t>(DEF_G_DISPLAY_BRIGHTNESS));
	writeEEPROM(EEPROMHandler::general_variable_::DISPLAY_BRIGHTNESS, static_cast<uint8_t>(DEF_G_DISPLAY_CONTRAST));

	writeEEPROM(EEPROMHandler::general_variable_::DISPLAY_INTERFACE_TYPE, static_cast<uint8_t>(DEF_G_DISPLAY_INTERFACE_TYPE));
	writeEEPROM(EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN0, static_cast<uint8_t>(DEF_G_DISPLAY_INTERFACE_PIN0));
	writeEEPROM(EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN1, static_cast<uint8_t>(DEF_G_DISPLAY_INTERFACE_PIN1));
	writeEEPROM(EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN2, static_cast<uint8_t>(DEF_G_DISPLAY_INTERFACE_PIN2));
	writeEEPROM(EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN3, static_cast<uint8_t>(DEF_G_DISPLAY_INTERFACE_PIN3));
	writeEEPROM(EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN4, static_cast<uint8_t>(DEF_G_DISPLAY_INTERFACE_PIN4));
	writeEEPROM(EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN5, static_cast<uint8_t>(DEF_G_DISPLAY_INTERFACE_PIN5));
	writeEEPROM(EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN6, static_cast<uint8_t>(DEF_G_DISPLAY_INTERFACE_PIN6));
	writeEEPROM(EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN7, static_cast<uint8_t>(DEF_G_DISPLAY_INTERFACE_PIN7));
	writeEEPROM(EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN8, static_cast<uint8_t>(DEF_G_DISPLAY_INTERFACE_PIN8));
	writeEEPROM(EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN9, static_cast<uint8_t>(DEF_G_DISPLAY_INTERFACE_PIN9));
	writeEEPROM(EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN10, static_cast<uint8_t>(DEF_G_DISPLAY_INTERFACE_PIN10));
	writeEEPROM(EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN11, static_cast<uint8_t>(DEF_G_DISPLAY_INTERFACE_PIN11));
	writeEEPROM(EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN12, static_cast<uint8_t>(DEF_G_DISPLAY_INTERFACE_PIN12));
	writeEEPROM(EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN13, static_cast<uint8_t>(DEF_G_DISPLAY_INTERFACE_PIN13));
	writeEEPROM(EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN14, static_cast<uint8_t>(DEF_G_DISPLAY_INTERFACE_PIN14));
	writeEEPROM(EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN15, static_cast<uint8_t>(DEF_G_DISPLAY_INTERFACE_PIN15));
}

InfoDisplay info_display_;

