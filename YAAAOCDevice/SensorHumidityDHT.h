//DHT11/22 Humidity sensor class.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef SENSOR_HUMIDITY_DHT_H_
#define SENSOR_HUMIDITY_DHT_H_

#include <Arduino.h>

#include "SensorHumidity.h"

#include "YAAADHT.h"

class SensorHumidityDHT :
	public SensorHumidity
{
public:

	SensorHumidityDHT(YAAASensor::SensorSettings sensor_settings);

	~SensorHumidityDHT();

	/*
	initializes the humidity sensor with the info given in in the constructor call.
	@return: true on success, false otherwise. */
	bool begin();

	/*
	starts a measurement.
	@return true on success, false otherwise.*/
	bool measure();

	/*
	checks if a measurement has been completed. may perform work necessary to complete a mesurement. user is
	expected to call this function after calling measure() and call it until it returns true.
	@return true if a measurement has been completed. */
	bool hasValue();

	/*
	returns the last measured value.
	user is expected to call this function only after hasValue() has returned true.
	@return the barometric pressure in the given unit. */
	float getValue();

private:
	YAAADHT *sensor_;
};

#endif /* SENSOR_HUMIDITY_DHT11_H_ */
