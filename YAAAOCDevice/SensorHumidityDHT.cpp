//DHT11/22 Humidity sensor class.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "SensorHumidityDHT.h"

SensorHumidityDHT::SensorHumidityDHT(YAAASensor::SensorSettings sensor_settings) :
	SensorHumidity(sensor_settings),
	sensor_(NULL)
{
	sensor_ = new YAAADHT(sensor_settings.parameters_[0], sensor_settings.parameters_[1]);
}

SensorHumidityDHT::~SensorHumidityDHT()
{
	//no destructors are implemented for SimpleDHT and derived classes
	//if (sensor_)
	//	delete sensor_;

	//sensor_ = NULL;
}

bool SensorHumidityDHT::begin()
{
	return sensor_ && sensor_->begin();
}

bool SensorHumidityDHT::measure()
{
	return sensor_ && sensor_->measureHumidity();
}

bool SensorHumidityDHT::hasValue()
{
	return sensor_ && sensor_->hasHumidity();
}

float SensorHumidityDHT::getValue()
{
	if (!sensor_)
		return NAN;

	return sensor_->getHumidity();
}
