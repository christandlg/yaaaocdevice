//Analog IC (LM35, TMP36, ...) Temperature sensor class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "SensorTemperatureAnalogIC.h"

SensorTemperatureAnalogIC::SensorTemperatureAnalogIC(YAAASensor::SensorSettings sensor_settings) :
SensorTemperature(sensor_settings),
pin_(255),
zero_voltage_(DEF_SENSOR_TEMPERATURE_ANALOG_IC_ZERO_VOLTAGE), 
reference_voltage_(DEF_SENSOR_TEMPERATURE_ANALOG_IC_REFERENCE_VOLTAGE),
coefficient_(DEF_SENSOR_TEMPERATURE_ANALOG_IC_COEFFICIENT),
analog_voltage_(-1)
{
	pin_ = sensor_settings.parameters_[0];

	zero_voltage_ = ((sensor_settings.parameters_[2] << 8) | sensor_settings.parameters_[3]);

	reference_voltage_ = ((sensor_settings.parameters_[6] << 8) | sensor_settings.parameters_[7]);

	coefficient_ = ((sensor_settings.parameters_[8] << 8) | sensor_settings.parameters_[9]);

	setUnit(sensor_settings.unit_);
}

SensorTemperatureAnalogIC::~SensorTemperatureAnalogIC()
{
}

bool SensorTemperatureAnalogIC::begin()
{
	//check if the input pin is an analog pin. return false if it is not.
#ifdef ARDUINO_SAM_DUE
	if ((pin_ < A0) || (pin_ > A11))		//Arduino Due: 12 ADC inputs
#elif defined(ARDUINO_AVR_MEGA2560) //__AVR_ATmega2560__
	if ((pin_ < A0) || (pin_ > A15))		//Arduino Mega: 16 ADC inputs
#elif defined(ESP8266) //__ESP8266_ESP8266__
	if (pin_ != A0)							//ESP8266: 1 ADC input 
#elif defined(ESP32)	
	if (pin_ != A0 &&
		pin_ != A4 &&
		pin_ != A5 &&
		pin_ != A6 &&
		pin_ != A7 &&
		pin_ != A10 &&
		pin_ != A11 &&
		pin_ != A12 &&
		pin_ != A13 &&
		pin_ != A14 &&
		pin_ != A15 &&
		pin_ != A16 &&
		pin_ != A17 &&
		pin_ != A18 &&
		pin_ != A19)
#else
	if ((pin_ < A0) || (pin_ > A5))
#endif
		return false;

	return true;
}

bool SensorTemperatureAnalogIC::measure()
{
	analog_voltage_ = map(analogRead(pin_), 0, 1024, 0, reference_voltage_);
	updateMeasurementTime();

	return true;
}

bool SensorTemperatureAnalogIC::hasValue()
{
	return true;
}

float SensorTemperatureAnalogIC::getValue()
{
	//calculate temperature
	float temperature = static_cast<float>(analog_voltage_ - zero_voltage_) / static_cast<float>(coefficient_);

	//convert temperature and return.
	return(convertTemperature(
		temperature,
		SensorTemperature::UNIT_CELSIUS,
		temperature_unit_));
}
