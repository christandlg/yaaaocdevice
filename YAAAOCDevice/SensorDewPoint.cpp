//Dew Point sensor class.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "SensorDewPoint.h"

SensorDewPoint::SensorDewPoint(YAAASensor::SensorSettings sensor_settings) :
	YAAASensor(sensor_settings),
	temperature_unit_(DEF_SENSOR_DEW_POINT_UNIT)
{
	setUnit(sensor_settings.unit_);
}

SensorDewPoint::~SensorDewPoint()
{
	//nothing to do here...
}

uint8_t SensorDewPoint::getSensorType()
{
	return YAAASensor::SENSOR_TEMPERATURE;
}

uint8_t SensorDewPoint::getUnit()
{
	return temperature_unit_;
}

bool SensorDewPoint::setUnit(uint8_t unit)
{
	switch (unit)
	{
		case SensorDewPoint::UNIT_KELVIN:
		case SensorDewPoint::UNIT_CELSIUS:
		case SensorDewPoint::UNIT_FAHRENHEIT:
			temperature_unit_ = unit;
			return true;
		default:
			break;
	}

	return false;
}

float SensorDewPoint::convertTemperature(float temp, uint8_t unit_src, uint8_t unit_dst)
{
	if (unit_src == unit_dst)
		return temp;

	//convert temperature to kelvin
	switch (unit_src)
	{
		case SensorDewPoint::UNIT_CELSIUS:
			temp += 273.15f;
			break;
		case SensorDewPoint::UNIT_FAHRENHEIT:
			temp = ((temp + 459.67f) * 5.0f / 9.0f);
			break;
		default:
			break;
	}

	//convert temperature to requested unit
	switch (unit_dst)
	{
		case SensorDewPoint::UNIT_CELSIUS:
			temp -= 273.15f;
			break;
		case SensorDewPoint::UNIT_FAHRENHEIT:
			temp = temp * 9.0f / 5.0f - 459.67f;
			break;
		default:
			break;
	}

	return temp;
}

