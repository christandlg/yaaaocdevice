//EEPROMHandler class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "EEPROMHandler.h"

#ifdef ARDUINO_SAM_DUE
DueFlashStorage EEPROMHandler::dfs_;
#endif

EEPROMHandler::EEPROMHandler()
{
}

EEPROMHandler::~EEPROMHandler()
{
}

bool EEPROMHandler::begin()
{
#if defined(ESP8266) || defined(ESP32)
	EEPROM.begin(4096);
#endif
	return true;
}

uint8_t EEPROMHandler::getVariableType(uint16_t device, uint16_t variable)
{
	if (device == EEPROMHandler::device_type_::GENERAL)
	{
		switch (variable)
		{
		case EEPROMHandler::general_variable_::DISPLAY_TYPE:
		case EEPROMHandler::general_variable_::DISPLAY_INTERFACE_TYPE:
		case EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN0:
		case EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN1:
		case EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN2:
		case EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN3:
		case EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN4:
		case EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN5:
		case EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN6:
		case EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN7:
		case EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN8:
		case EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN9:
		case EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN10:
		case EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN11:
		case EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN12:
		case EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN13:
		case EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN14:
		case EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN15:
		case EEPROMHandler::general_variable_::ETHERNET_ENABLED:
		case EEPROMHandler::general_variable_::ETHERNET_DHCP:
		case EEPROMHandler::general_variable_::ONEWIRE_PIN:
		case EEPROMHandler::general_variable_::SD_CS_PIN:
		case EEPROMHandler::general_variable_::SD_LOG:
		case EEPROMHandler::general_variable_::I2C_PIN_SCL:
		case EEPROMHandler::general_variable_::I2C_PIN_SDA:
		case EEPROMHandler::general_variable_::WIFI_ENABLED:
		case EEPROMHandler::general_variable_::WIFI_DHCP:
		case EEPROMHandler::general_variable_::WIFI_CHANNEL:
#if defined(ESP8266) || defined(ESP32)
		case EEPROMHandler::general_variable_::OTA_MODE:
		case EEPROMHandler::general_variable_::OTA_ARDUINOOTA_PASSWORD_HASH:
#endif /* defined(ESP8266) || defined(ESP32) */
		case EEPROMHandler::general_variable_::MQTT_ENABLED:
			return EEPROMHandler::UINT8_T_;

		case EEPROMHandler::general_variable_::ETHERNET_PORT:
		case EEPROMHandler::general_variable_::WIFI_PORT:
#if defined(ESP8266) || defined(ESP32)
		case EEPROMHandler::general_variable_::OTA_ARDUINOOTA_PORT:
		case EEPROMHandler::general_variable_::OTA_WEBBROWSER_PORT:
		case EEPROMHandler::general_variable_::OTA_HTTPSERVER_PORT:
#endif /* defined(ESP8266) || defined(ESP32) */
		case EEPROMHandler::general_variable_::DISPLAY_RES_X:
		case EEPROMHandler::general_variable_::DISPLAY_RES_Y:
		case EEPROMHandler::general_variable_::MQTT_PORT:
			return EEPROMHandler::UINT16_T_;

		case EEPROMHandler::general_variable_::EEPROM_CONTROL:
			return EEPROMHandler::INT32_T_;

		case EEPROMHandler::general_variable_::SERIAL_BAUD_RATE:
#if defined(ESP8266) || defined(ESP32)
		case EEPROMHandler::general_variable_::OTA_HTTPSERVER_UPDATE_INTERVAL:
#endif /* defined(ESP8266) || defined(ESP32) */
		case EEPROMHandler::general_variable_::DISPLAY_UPDATE_INTERVAL:
			return EEPROMHandler::UINT32_T_;

		case EEPROMHandler::general_variable_::TIME_UTC_OFFSET:
			return EEPROMHandler::FLOAT_;

		case EEPROMHandler::general_variable_::ETHERNET_MAC:
		case EEPROMHandler::general_variable_::ETHERNET_IP:
		case EEPROMHandler::general_variable_::ETHERNET_DNS:
		case EEPROMHandler::general_variable_::ETHERNET_GATEWAY:
		case EEPROMHandler::general_variable_::ETHERNET_SUBNET:
		case EEPROMHandler::general_variable_::WIFI_MAC:
		case EEPROMHandler::general_variable_::WIFI_IP:
		case EEPROMHandler::general_variable_::WIFI_DNS:
		case EEPROMHandler::general_variable_::WIFI_GATEWAY:
		case EEPROMHandler::general_variable_::WIFI_SUBNET:
			return EEPROMHandler::ARRAY_;

		case EEPROMHandler::general_variable_::WIFI_SSID:
		case EEPROMHandler::general_variable_::WIFI_PASSWORD:
		case EEPROMHandler::general_variable_::WIFI_HOSTNAME:
#if defined(ESP8266) || defined(ESP32)
		case EEPROMHandler::general_variable_::OTA_HOSTNAME:
		case EEPROMHandler::general_variable_::OTA_ARDUINOOTA_PASSWORD:
		case EEPROMHandler::general_variable_::OTA_WEBBROWSER_PATH:
		case EEPROMHandler::general_variable_::OTA_WEBBROWSER_USERNAME:
		case EEPROMHandler::general_variable_::OTA_WEBBROWSER_PASSWORD:
		case EEPROMHandler::general_variable_::OTA_HTTPSERVER_ADDRESS:
		case EEPROMHandler::general_variable_::OTA_HTTPSERVER_PATH:
#endif /* defined(ESP8266) || defined(ESP32) */
		case EEPROMHandler::general_variable_::MQTT_SERVER:
		case EEPROMHandler::general_variable_::MQTT_CLIENT_ID:
		case EEPROMHandler::general_variable_::MQTT_USER:
		case EEPROMHandler::general_variable_::MQTT_PASSWORD:
		case EEPROMHandler::general_variable_::MQTT_TOPIC_PUBLISHED:
		case EEPROMHandler::general_variable_::MQTT_TOPIC_SUBSCRIBED:
			return EEPROMHandler::STRING_;
		}
	}

	//general telescope settings.
	if (device == EEPROMHandler::device_type_::OBSERVING_CONDITIONS)
	{
		switch (variable)
		{
		case EEPROMHandler::observing_conditions_variable_::LOG_CSV_FILE:
		case EEPROMHandler::observing_conditions_variable_::LOG_CSV_ENABLED:
		case EEPROMHandler::observing_conditions_variable_::LOG_CSV_DELIMITER:
			return EEPROMHandler::UINT8_T_;

		case EEPROMHandler::observing_conditions_variable_::LOG_CSV_INTERVAL:
		case EEPROMHandler::observing_conditions_variable_::AVERAGE_PERIOD:
			return EEPROMHandler::UINT32_T_;

		case EEPROMHandler::observing_conditions_variable_::SENSOR_CONFIGURATION:
			return EEPROMHandler::SENSOR_CONFIGURATION_;

		case EEPROMHandler::observing_conditions_variable_::HEATER_CONFIGURATION:
			return EEPROMHandler::HEATER_CONFIGURATION_;
		}
	}

	return EEPROMHandler::UNKNOWN_;
}

uint16_t EEPROMHandler::stringToVariable(uint16_t device, char* string)
{
	uint16_t return_value = -1;

	if (device == EEPROMHandler::device_type_::GENERAL)
	{
		if (strcmp_P(string, PSTR("EEPROM_CONTROL")) == 0)
			return_value = general_variable_::EEPROM_CONTROL;

		if (strcmp_P(string, PSTR("SERIAL_BAUD_RATE")) == 0)
			return_value = general_variable_::SERIAL_BAUD_RATE;

		if (strcmp_P(string, PSTR("DISPLAY_TYPE")) == 0)
			return_value = general_variable_::DISPLAY_TYPE;

		if (strcmp_P(string, PSTR("DISPLAY_RES_X")) == 0)
			return_value = general_variable_::DISPLAY_RES_X;

		if (strcmp_P(string, PSTR("DISPLAY_RES_Y")) == 0)
			return_value = general_variable_::DISPLAY_RES_Y;

		if (strcmp_P(string, PSTR("DISPLAY_UPDATE_INTERVAL")) == 0)
			return_value = general_variable_::DISPLAY_UPDATE_INTERVAL;

		if (strcmp_P(string, PSTR("DISPLAY_BRIGHTNESS")) == 0)
			return_value = general_variable_::DISPLAY_BRIGHTNESS;

		if (strcmp_P(string, PSTR("DISPLAY_CONTRAST")) == 0)
			return_value = general_variable_::DISPLAY_CONTRAST;

		if (strcmp_P(string, PSTR("DISPLAY_INTERFACE_TYPE")) == 0)
			return_value = general_variable_::DISPLAY_INTERFACE_TYPE;

		if (strcmp_P(string, PSTR("DISPLAY_INTERFACE_PIN0")) == 0)
			return_value = general_variable_::DISPLAY_INTERFACE_PIN0;

		if (strcmp_P(string, PSTR("DISPLAY_INTERFACE_PIN1")) == 0)
			return_value = general_variable_::DISPLAY_INTERFACE_PIN1;

		if (strcmp_P(string, PSTR("DISPLAY_INTERFACE_PIN2")) == 0)
			return_value = general_variable_::DISPLAY_INTERFACE_PIN2;

		if (strcmp_P(string, PSTR("DISPLAY_INTERFACE_PIN3")) == 0)
			return_value = general_variable_::DISPLAY_INTERFACE_PIN3;

		if (strcmp_P(string, PSTR("DISPLAY_INTERFACE_PIN4")) == 0)
			return_value = general_variable_::DISPLAY_INTERFACE_PIN4;

		if (strcmp_P(string, PSTR("DISPLAY_INTERFACE_PIN5")) == 0)
			return_value = general_variable_::DISPLAY_INTERFACE_PIN5;

		if (strcmp_P(string, PSTR("DISPLAY_INTERFACE_PIN6")) == 0)
			return_value = general_variable_::DISPLAY_INTERFACE_PIN6;

		if (strcmp_P(string, PSTR("DISPLAY_INTERFACE_PIN7")) == 0)
			return_value = general_variable_::DISPLAY_INTERFACE_PIN7;

		if (strcmp_P(string, PSTR("DISPLAY_INTERFACE_PIN8")) == 0)
			return_value = general_variable_::DISPLAY_INTERFACE_PIN8;

		if (strcmp_P(string, PSTR("DISPLAY_INTERFACE_PIN9")) == 0)
			return_value = general_variable_::DISPLAY_INTERFACE_PIN9;

		if (strcmp_P(string, PSTR("DISPLAY_INTERFACE_PIN10")) == 0)
			return_value = general_variable_::DISPLAY_INTERFACE_PIN10;

		if (strcmp_P(string, PSTR("DISPLAY_INTERFACE_PIN11")) == 0)
			return_value = general_variable_::DISPLAY_INTERFACE_PIN11;

		if (strcmp_P(string, PSTR("DISPLAY_INTERFACE_PIN12")) == 0)
			return_value = general_variable_::DISPLAY_INTERFACE_PIN12;

		if (strcmp_P(string, PSTR("DISPLAY_INTERFACE_PIN13")) == 0)
			return_value = general_variable_::DISPLAY_INTERFACE_PIN13;

		if (strcmp_P(string, PSTR("DISPLAY_INTERFACE_PIN14")) == 0)
			return_value = general_variable_::DISPLAY_INTERFACE_PIN14;

		if (strcmp_P(string, PSTR("DISPLAY_INTERFACE_PIN15")) == 0)
			return_value = general_variable_::DISPLAY_INTERFACE_PIN15;

		if (strcmp_P(string, PSTR("ETHERNET_ENABLED")) == 0)
			return_value = general_variable_::ETHERNET_ENABLED;

		if (strcmp_P(string, PSTR("ETHERNET_DHCP")) == 0)
			return_value = general_variable_::ETHERNET_DHCP;

		if (strcmp_P(string, PSTR("ETHERNET_MAC")) == 0)
			return_value = general_variable_::ETHERNET_MAC;

		if (strcmp_P(string, PSTR("ETHERNET_IP")) == 0)
			return_value = general_variable_::ETHERNET_IP;

		if (strcmp_P(string, PSTR("ETHERNET_DNS")) == 0)
			return_value = general_variable_::ETHERNET_DNS;

		if (strcmp_P(string, PSTR("ETHERNET_GATEWAY")) == 0)
			return_value = general_variable_::ETHERNET_GATEWAY;

		if (strcmp_P(string, PSTR("ETHERNET_SUBNET")) == 0)
			return_value = general_variable_::ETHERNET_SUBNET;

		if (strcmp_P(string, PSTR("ETHERNET_PORT")) == 0)
			return_value = general_variable_::ETHERNET_PORT;

		if (strcmp_P(string, PSTR("SD_CS_PIN")) == 0)
			return_value = general_variable_::SD_CS_PIN;

		if (strcmp_P(string, PSTR("SD_LOG")) == 0)
			return_value = general_variable_::SD_LOG;

		if (strcmp_P(string, PSTR("ONEWIRE_PIN")) == 0)
			return_value = general_variable_::ONEWIRE_PIN;

		if (strcmp_P(string, PSTR("I2C_PIN_SCL")) == 0)
			return_value = general_variable_::I2C_PIN_SCL;

		if (strcmp_P(string, PSTR("I2C_PIN_SDA")) == 0)
			return_value = general_variable_::I2C_PIN_SDA;

		if (strcmp_P(string, PSTR("TIME_UTC_OFFSET")) == 0)
			return_value = general_variable_::TIME_UTC_OFFSET;

		if (strcmp_P(string, PSTR("WIFI_ENABLED")) == 0)
			return_value = general_variable_::WIFI_ENABLED;

		if (strcmp_P(string, PSTR("WIFI_CHANNEL")) == 0)
			return_value = general_variable_::WIFI_CHANNEL;

		if (strcmp_P(string, PSTR("WIFI_DHCP")) == 0)
			return_value = general_variable_::WIFI_DHCP;

		if (strcmp_P(string, PSTR("WIFI_MAC")) == 0)
			return_value = general_variable_::WIFI_MAC;

		if (strcmp_P(string, PSTR("WIFI_IP")) == 0)
			return_value = general_variable_::WIFI_IP;

		if (strcmp_P(string, PSTR("WIFI_DNS")) == 0)
			return_value = general_variable_::WIFI_DNS;

		if (strcmp_P(string, PSTR("WIFI_GATEWAY")) == 0)
			return_value = general_variable_::WIFI_GATEWAY;

		if (strcmp_P(string, PSTR("WIFI_SUBNET")) == 0)
			return_value = general_variable_::WIFI_SUBNET;

		if (strcmp_P(string, PSTR("WIFI_PORT")) == 0)
			return_value = general_variable_::WIFI_PORT;

		if (strcmp_P(string, PSTR("WIFI_SSID")) == 0)
			return_value = general_variable_::WIFI_SSID;

		if (strcmp_P(string, PSTR("WIFI_PASSWORD")) == 0)
			return_value = general_variable_::WIFI_PASSWORD;

		if (strcmp_P(string, PSTR("WIFI_HOSTNAME")) == 0)
			return_value = general_variable_::WIFI_HOSTNAME;
#if defined(ESP8266) || defined(ESP32)
		if (strcmp_P(string, PSTR("OTA_MODE")) == 0)
			return_value = general_variable_::OTA_MODE;

		if (strcmp_P(string, PSTR("OTA_HOSTNAME")) == 0)
			return_value = general_variable_::OTA_HOSTNAME;

		if (strcmp_P(string, PSTR("OTA_ARDUINOOTA_PORT")) == 0)
			return_value = general_variable_::OTA_ARDUINOOTA_PORT;

		if (strcmp_P(string, PSTR("OTA_ARDUINOOTA_PASSWORD_HASH")) == 0)
			return_value = general_variable_::OTA_ARDUINOOTA_PASSWORD_HASH;

		if (strcmp_P(string, PSTR("OTA_ARDUINOOTA_PASSWORD")) == 0)
			return_value = general_variable_::OTA_ARDUINOOTA_PASSWORD;

		//if (strcmp_P(string, PSTR("OTA_WEBBROWSER_PORT")) == 0)
		//	return_value = general_variable_::OTA_WEBBROWSER_PORT;

		//if (strcmp_P(string, PSTR("OTA_WEBBROWSER_PATH")) == 0)
		//	return_value = general_variable_::OTA_WEBBROWSER_PATH;

		//if (strcmp_P(string, PSTR("OTA_WEBBROWSER_USERNAME")) == 0)
		//	return_value = general_variable_::OTA_WEBBROWSER_USERNAME;

		//if (strcmp_P(string, PSTR("OTA_WEBBROWSER_PASSWORD")) == 0)
		//	return_value = general_variable_::OTA_WEBBROWSER_PASSWORD;

		//if (strcmp_P(string, PSTR("OTA_HTTPSERVER_UPDATE_INTERVAL")) == 0)
		//	return_value = general_variable_::OTA_HTTPSERVER_UPDATE_INTERVAL;

		//if (strcmp_P(string, PSTR("OTA_HTTPSERVER_ADDRESS")) == 0)
		//	return_value = general_variable_::OTA_HTTPSERVER_ADDRESS;

		//if (strcmp_P(string, PSTR("OTA_HTTPSERVER_PATH")) == 0)
		//	return_value = general_variable_::OTA_HTTPSERVER_PATH;

		//if (strcmp_P(string, PSTR("OTA_HTTPSERVER_PORT")) == 0)
		//	return_value = general_variable_::OTA_HTTPSERVER_PORT;
#endif /* defined(ESP8266) || defined(ESP32) */

		if (strcmp_P(string, PSTR("MQTT_ENABLED")) == 0)
			return_value = general_variable_::MQTT_ENABLED;

		if (strcmp_P(string, PSTR("MQTT_SERVER")) == 0)
			return_value = general_variable_::MQTT_SERVER;
		
		if (strcmp_P(string, PSTR("MQTT_PORT")) == 0)
			return_value = general_variable_::MQTT_PORT;

		if (strcmp_P(string, PSTR("MQTT_CLIENT_ID")) == 0)
			return_value = general_variable_::MQTT_CLIENT_ID;

		if (strcmp_P(string, PSTR("MQTT_USER")) == 0)
			return_value = general_variable_::MQTT_USER;

		if (strcmp_P(string, PSTR("MQTT_PASSWORD")) == 0)
			return_value = general_variable_::MQTT_PASSWORD;

		if (strcmp_P(string, PSTR("MQTT_TOPIC_PUBLISHED")) == 0)
			return_value = general_variable_::MQTT_TOPIC_PUBLISHED;

		if (strcmp_P(string, PSTR("MQTT_TOPIC_SUBSCRIBED")) == 0)
			return_value = general_variable_::MQTT_TOPIC_SUBSCRIBED;
	}

	if (device == EEPROMHandler::device_type_::OBSERVING_CONDITIONS)
	{
		if (strcmp_P(string, PSTR("LOG_CSV_FILE")) == 0)
			return_value = observing_conditions_variable_::LOG_CSV_FILE;

		if (strcmp_P(string, PSTR("LOG_CSV_ENABLED")) == 0)
			return_value = observing_conditions_variable_::LOG_CSV_ENABLED;

		if (strcmp_P(string, PSTR("LOG_CSV_DELIMITER")) == 0)
			return_value = observing_conditions_variable_::LOG_CSV_DELIMITER;

		if (strcmp_P(string, PSTR("LOG_CSV_INTERVAL")) == 0)
			return_value = observing_conditions_variable_::LOG_CSV_INTERVAL;

		if (strcmp_P(string, PSTR("AVERAGE_PERIOD")) == 0)
			return_value = observing_conditions_variable_::AVERAGE_PERIOD;

		if (strcmp_P(string, PSTR("SENSOR_CONFIGURATION")) == 0)
			return_value = observing_conditions_variable_::SENSOR_CONFIGURATION;

		if (strcmp_P(string, PSTR("HEATER_CONFIGURATION")) == 0)
			return_value = observing_conditions_variable_::HEATER_CONFIGURATION;
	}

	return return_value;
}

uint8_t EEPROMHandler::getEEPROMMaxStringLength(uint16_t device, uint16_t variable)
{
	uint8_t length = 0;

	if (device == EEPROMHandler::device_type_::GENERAL)
	{
		switch (variable)
		{
		case EEPROMHandler::general_variable_::WIFI_SSID:
			length = 32;
		case EEPROMHandler::general_variable_::WIFI_PASSWORD:
		case EEPROMHandler::general_variable_::WIFI_HOSTNAME:
#if defined(ESP8266) || defined(ESP32)
		case EEPROMHandler::general_variable_::OTA_HOSTNAME:
		case EEPROMHandler::general_variable_::OTA_ARDUINOOTA_PASSWORD:
		case EEPROMHandler::general_variable_::OTA_WEBBROWSER_PATH:
		case EEPROMHandler::general_variable_::OTA_WEBBROWSER_USERNAME:
		case EEPROMHandler::general_variable_::OTA_WEBBROWSER_PASSWORD:
		case EEPROMHandler::general_variable_::OTA_HTTPSERVER_ADDRESS:
		case EEPROMHandler::general_variable_::OTA_HTTPSERVER_PORT:
#endif /* defined(ESP8266) || defined(ESP32) */
		case EEPROMHandler::general_variable_::MQTT_SERVER:
		case EEPROMHandler::general_variable_::MQTT_CLIENT_ID:
		case EEPROMHandler::general_variable_::MQTT_USER:
		case EEPROMHandler::general_variable_::MQTT_PASSWORD:
		case EEPROMHandler::general_variable_::MQTT_TOPIC_PUBLISHED:
		case EEPROMHandler::general_variable_::MQTT_TOPIC_SUBSCRIBED:
			length = 63;
		}
	}

	return length;
}

uint16_t EEPROMHandler::calculateOffset(uint16_t device, uint16_t variable, uint16_t index)
{
	//the value offset is different for each device and value read.
	uint16_t offset = 0;

	//calculate option- specific offsets.
	switch (device)
	{
	case device_type_::GENERAL:
	{

	}
	break;

	case device_type_::OBSERVING_CONDITIONS:
	{
		if (variable == observing_conditions_variable_::SENSOR_CONFIGURATION)
		{
			index = constrain(index, 0, 14);	//TODO hardcoded is bad

			offset = 20 * index; //TODO use YAAASensor::SensorSettings struct size ?
		}

		if (variable == observing_conditions_variable_::HEATER_CONFIGURATION)
		{
			index = constrain(index, 0, 2);		//TODO hardcoded is bad

			offset = 20 * index; //TODO use YAAAHeater::HeaterSettings struct size?
		}
	}
	break;
	}

	return offset;
}
