//YAAAOCDevice main file.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include <Arduino.h>

//OneWire library is needed for OneWire Temperature sensors.
#include <OneWire.h>

#include "EEPROMHandler.h"

#include "YAAATime.h"

#include "YAAALog.h"

#include "YAAASensor.h"

#include "OTAHandler.h"

#include "InfoDisplay.h"

#ifdef FEATURE_MQTT
#include "MQTTHandler.h"
#endif /* FEATURE_MQTT */

#ifdef FEATURE_AUTOREPORTER
#include "AutoReporter.h"
#endif /* FEATURE_AUTOREPORTER */

//Class needed for Communication between PC and Arduino.
#include "RemoteCommandHandler.h"
//------------------------------------------------

//ASCOM Devices
#include "ObservingConditions.h"

//------------------------------------------------
//Global Variables. 
OneWire *one_wire_;

//observing conditions
ObservingConditions observing_conditions_;

#ifdef FEATURE_MQTT
//MQTT handler 
MQTTHandler mqtt_handler_;
bool mqtt_enabled_ = false;
#endif /* FEATURE_MQTT */

#ifdef FEATURE_AUTOREPORTER
AutoReporter auto_reporter_;
#endif /* FEATURE_AUTOREPORTER */

//create logger
YAAALog log_("YAAA.log");
//------------------------------------------------

//timer ISRs
#if defined(ARDUINO_SAM_DUE)
//TODO add ISR for due
#elif defined(ARDUINO_AVR_MEGA2560)
//timer5 ISRs
ISR(TIMER5_COMPA_vect)
{
	//execute ISRs that need to be called regularly
	SensorRainRateTippingBucket::rotateISR();
	SensorWindSpeedImpulse::rotateISR();
}
#elif defined(ESP8266)
void timer0ISR()
{
	//execute ISRs that need to be called regularly
	SensorRainRateTippingBucket::rotateISR();
	SensorWindSpeedImpulse::rotateISR();

	timer0_write(ESP.getCycleCount() + 1 * static_cast<uint32_t>(ESP.getCpuFreqMHz()) * 1000000); //execute interrupt again in 1s
}
#elif defined(ESP32)
//ESP32 timer
hw_timer_t *timer = NULL;

void IRAM_ATTR onTimer()
{
	//execute ISRs that need to be called regularly
	SensorRainRateTippingBucket::rotateISR();
	SensorWindSpeedImpulse::rotateISR();
}
#endif

void writeAllDefaults()
{
	//this function initializes the Arduino's EEPROM with default values. 

	RemoteCommandHandler::writeDefaults();

	InfoDisplay::writeDefaults();

	ObservingConditions::writeDefaults();

#ifdef FEATURE_MQTT
	MQTTHandler::writeDefaults();

	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::MQTT_ENABLED,
		0,
		static_cast<uint8_t>(DEF_G_MQTT_ENABLED));
#endif /* FEATURE_MQTT */

#if defined(ESP8266) || defined(ESP32)
#ifdef FEATURE_OTA
	OTAHandler::writeDefaults();
#endif /* FEATURE_OTA */
#endif /* defined(ESP8266) || defined(ESP32) */

	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::SD_CS_PIN,
		0,
		static_cast<uint8_t>(DEF_G_SD_CS_PIN));

	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::SD_LOG,
		0,
		static_cast<uint8_t>(DEF_G_SD_LOG));

#if defined(ESP8266)
	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::I2C_PIN_SCL,
		0,
		D3);

	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::I2C_PIN_SDA,
		0,
		D2);
#elif defined(ESP32)
	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::I2C_PIN_SCL,
		0,
		SCL);

	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::I2C_PIN_SDA,
		0,
		SDA);
#endif

	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::ONEWIRE_PIN,
		0,
		static_cast<uint8_t>(DEF_G_ONEWIRE_PIN));

	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::TIME_UTC_OFFSET,
		0,
		DEF_G_TIME_UTC_OFFSET);

	//reset eeprom control number.
	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::EEPROM_CONTROL,
		0,
		DEF_G_EEPROM_CONTROL);
}

void loop() {
	// put your main code here, to run repeatedly:
	remote_command_handler_.run();

#ifdef FEATURE_MQTT
	if (mqtt_enabled_)
		mqtt_handler_.run();

	if (mqtt_enabled_ && (millis() % 1000 == 0))
	{
		Serial.print("MQTT status: "); Serial.println(mqtt_handler_.getState());
		if (mqtt_handler_.getState() == 0)
			mqtt_handler_.publish("test", "q123");

		delay(1);
	}
#endif /* 
#ifdef FEATURE_MQTT */

//display information on display.
	info_display_.run();

	observing_conditions_.run();

#ifdef FEATURE_AUTOREPORTER
	auto_reporter_.run();
#endif /* FEATURE_AUTOREPORTER */

#if defined(ESP8266) || defined(ESP32)
#ifdef FEATURE_OTA
	OTAHandler::run();
#endif /* FEATURE_OTA */
#endif /* defined(ESP8266) || defined(ESP32) */
}

void setup() {
	// put your setup code here, to run once:

	EEPROMHandler::begin();

	uint32_t eeprom_control_number = 0L;

	//check eeprom control number to determine if the EEPROM needs to be initialized with 
	//default values.
	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::EEPROM_CONTROL,
		0,
		eeprom_control_number);

	//write defaults to the EEPROM if necessary.
	if (eeprom_control_number != DEF_G_EEPROM_CONTROL)
		writeAllDefaults();

#if defined(ARDUINO_SAM_DUE)
	analogReadResolution(10);
#endif

	//I2C SCL and SDA pins are configurable on ESP8266 based boards.
#if defined(ESP8266) || defined(ESP32)
	uint8_t i2c_pin_scl = 255;
	uint8_t i2c_pin_sda = 255;

	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::I2C_PIN_SCL,
		0,
		i2c_pin_scl);

	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::I2C_PIN_SDA,
		0,
		i2c_pin_sda);
	Wire.begin(i2c_pin_sda, i2c_pin_scl);
#else
  //other Arduinos (Mega, Due) use fixed pins for SCL / SDA
	Wire.begin();
#endif

	YAAATime::begin();

	//read and set offset from UTC.
	float time_utc_offset = 0.0f;
	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::TIME_UTC_OFFSET,
		0,
		time_utc_offset);
	YAAATime::setUTCOffset(time_utc_offset);

	//read SD chip select pin and initialize log class
	uint8_t sd_chip_select = 255;
	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::SD_CS_PIN,
		0,
		sd_chip_select);
	YAAALog::initSD(sd_chip_select);

	log_.begin();

	//read log setting and set logging
	bool sd_log_enabled = false;
	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::SD_LOG,
		0,
		sd_log_enabled);
	log_.setLogMode(sd_log_enabled);

	remote_command_handler_.begin();

#if defined(ESP8266) || defined(ESP32)
#ifdef FETURE_OTA
	OTAHandler::begin();
#endif /* FEATURE_OTA */
#endif /* defined(ESP8266) || defined(ESP32) */

	//read onewire pin number
	uint8_t onewire_pin = 255;
	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::ONEWIRE_PIN,
		0,
		onewire_pin);
	one_wire_ = new OneWire(onewire_pin);


#ifdef FEATURE_MQTT
	//read which devices are available.
	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::MQTT_ENABLED,
		0,
		mqtt_enabled_);

	if (mqtt_enabled_)
		mqtt_handler_.begin();
#endif /* FEATURE_MQTT */

	//initialize ObservongConditions object and sensors.
	observing_conditions_.begin();

	//initialize display.
	info_display_.init();

#ifdef FEATURE_AUTOREPORTER
	auto_reporter_.setReportInterval(AutoReporter::INTERFACE_LORA, 30000);
	auto_reporter_.setReportInterval(AutoReporter::INTERFACE_MQTT, 20000);
#endif /* FEATURE_AUTOREPORTER */

	//Change the i2c clock to 400kHz.
	//increases maximum stepper motor steps / second when using Adafruit Motor Shield.
	//must be done *after* initializing the motor shields with Adafruit_MotorShield::begin().
	//https://learn.adafruit.com/adafruit-motor-shield-v2-for-arduino/faq
//#ifndef ARDUINO_SAM_DUE
	//TWBR = ((F_CPU / 400000l) - 16) / 2;
//#else
	//Wire.setClock(400000L);		//TODO MLX90614 cannot operate at 400kHz.

#ifdef ARDUINO_SAM_DUE
	//TODO timer interrupt setup for due
#elif defined ARDUINO_AVR_MEGA2560
	//set interrupt to trigger once every second
	TCCR5A = 0x00;								//clear control registers
	TCCR5B = 0x00;
	TCNT5 = 0;									//clear counter

	OCR5A = 15625;								//set compare match register

	TCCR5B |= (1 << WGM52);						//set to CTC mode
	TCCR5B |= ((1 << CS52) | (1 << CS50));		//set prescaler to 1024

	TIMSK5 |= (1 << OCIE5A);					//enable timer compare interrupt
#elif defined(ESP8266)
	//attach timer interrupt again.
	//attach timer interrupt for count rotation (see: https://hackaday.io/project/12818-not-grbl/log/47580-esp8266-timer0-and-isr)
	noInterrupts();

	timer0_isr_init();
	timer0_attachInterrupt(timer0ISR);
	timer0_write(ESP.getCycleCount() + 1 * static_cast<uint32_t>(ESP.getCpuFreqMHz()) * 1000000);	//execute interrupt the first time in 1s
	interrupts();
#elif defined(ESP32)
	//TODO add 
	timer = timerBegin(0, 1, true);
	timerAttachInterrupt(timer, &onTimer, true);
	timerAlarmWrite(timer, static_cast<uint32_t>(ESP.getCpuFreqMHz()) * 1000000, true);
	timerAlarmEnable(timer);
#endif
}
