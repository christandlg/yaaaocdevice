#include "YAAAHTU21D.h"



YAAAHTU21D::YAAAHTU21D(uint8_t i2c_address):
	i2c_address_(i2c_address),
	value_humidity_(0x0000),
	value_temperature_(0x0000)
{
	//nothing to do here...
}


YAAAHTU21D::~YAAAHTU21D()
{
}

bool YAAAHTU21D::begin(bool soft_reset)
{
	Wire.beginTransmission(i2c_address_);
	Wire.write(REGISTER_READ);
	if (Wire.endTransmission() != 0)
		return false;

	if (Wire.requestFrom(i2c_address_, static_cast<uint8_t>(1)) != 1)
		return false;

	if (soft_reset)
		softReset();

	return true;
}

bool YAAAHTU21D::measureHumidity()
{
	Wire.beginTransmission(i2c_address_);
	Wire.write(MEASURE_HUMIDITY);
	return (Wire.endTransmission() == 0);
}

bool YAAAHTU21D::measureTemperature()
{
	Wire.beginTransmission(i2c_address_);
	Wire.write(MEASURE_TEMPERATURE);
	return (Wire.endTransmission() == 0);
}

bool YAAAHTU21D::hasHumidity()
{
	if (Wire.requestFrom(i2c_address_, static_cast<uint8_t>(3)) != 3)
		return false;

	value_humidity_ = ((Wire.read() << 8) | Wire.read());

	//check if the read value is actually a humidity value (measurement type flag must be set)
	if ((value_humidity_ & 0x02) == 0)
		return false;

	//2 least significant bits encode measurement type and RFU, these must be set to 0
	value_humidity_ &= 0xFFFC;

	return true;
}

bool YAAAHTU21D::hasTemperature()
{
	if (Wire.requestFrom(i2c_address_, static_cast<uint8_t>(3)) != 3)
		return false;
	
	value_temperature_ = ((Wire.read() << 8) | Wire.read());

	//check if the read value is actually a temperature value (measurement type flag must be cleared)
	if ((value_temperature_ & 0x02) == 1)
		return false;

	//2 least significant bits encode measurement type and RFU, these must be set to 0
	value_temperature_ &= 0xFFFC;

	return true;
}

float YAAAHTU21D::getHumidity()
{
	return -6.0f + 125.0f / 65536.0f * static_cast<float>(value_humidity_);
}

float YAAAHTU21D::getTemperature()
{
	return -46.85f + 175.72f / 65536.0f * static_cast<float>(value_temperature_);
}

uint8_t YAAAHTU21D::getMeasurementResolution()
{
	//read user register from sensor. 
	Wire.beginTransmission(i2c_address_);
	Wire.write(REGISTER_READ);
	if (Wire.endTransmission() != 0)
		return false;

	if (Wire.requestFrom(i2c_address_, static_cast<uint8_t>(1)) != 1)
		return false;

	uint8_t user_register = Wire.read();

	return user_register & 0x81;
}

bool YAAAHTU21D::setMeasurementResolution(uint8_t resolution)
{
	//check if the given resolution is valid.
	switch (resolution)
	{
	case RESOLUTION_12BIT_14BIT:
	case RESOLUTION_08BIT_12BIT:
	case RESOLUTION_10BIT_13BIT:
	case RESOLUTION_11BIT_11BIT:
		break;
	default:
		return false;
	}

	uint8_t user_register = 0x00;

	if (!readUserRegister(user_register))
		return false;

	//clear highest and lowest bit
	user_register &= ~0x81;

	//set highest and lowest bit according to desired measurement resolution
	user_register |= resolution;

	return writeUserRegister(user_register);
}

bool YAAAHTU21D::getHeater()
{
	uint8_t user_register = 0x00;

	if (!readUserRegister(user_register))
		return false;

	return static_cast<bool>(user_register & BIT_HEATER);
}

bool YAAAHTU21D::setHeater(bool enable)
{
	uint8_t user_register = 0x00;

	if (!readUserRegister(user_register))
		return false;

	user_register &= ~BIT_HEATER;

	user_register |= (BIT_HEATER & enable);

	return writeUserRegister(user_register);
}

void YAAAHTU21D::softReset()
{
	Wire.beginTransmission(i2c_address_);
	Wire.write(RESET_SOFT);
	Wire.endTransmission();
}

bool YAAAHTU21D::readUserRegister(uint8_t &reg)
{
	//read user register from sensor. 
	Wire.beginTransmission(i2c_address_);
	Wire.write(REGISTER_READ);
	if (Wire.endTransmission() != 0)
		return false;

	if (Wire.requestFrom(i2c_address_, static_cast<uint8_t>(1)) != 1)
		return false;

	reg = Wire.read();

	return true;
}

bool YAAAHTU21D::writeUserRegister(uint8_t reg)
{
	//send new user register to sensor.
	Wire.beginTransmission(i2c_address_);
	Wire.write(REGISTER_WRITE);
	Wire.write(reg);

	if (Wire.endTransmission() != 0)
		return false;

	return true;
}
