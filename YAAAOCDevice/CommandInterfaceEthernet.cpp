//Command Interface (Ethernet) class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#if !defined(ESP8266) && !defined(ESP32)

#include "CommandInterfaceEthernet.h"

#ifdef FEATURE_ETHERNET

CommandInterfaceEthernet::CommandInterfaceEthernet() :
	server_(NULL),
	port_(DEF_G_ETHERNET_PORT),
	use_dhcp_(DEF_G_ETHERNET_DHCP)
{
	//nothing to do here...
}

CommandInterfaceEthernet::~CommandInterfaceEthernet()
{
	delete server_;
	server_ = NULL;
}

String CommandInterfaceEthernet::getConfig(uint8_t config)
{
	String return_value = "";

	switch (config)
	{
		case CONFIG_IP:
		case CONFIG_DNS:
		case CONFIG_GATEWAY:
		case CONFIG_SUBNET:
		{
			IPAddress ip;

			switch (config) {
				case CONFIG_IP:
					ip = Ethernet.localIP();
					break;
				case CONFIG_DNS:
					ip = Ethernet.dnsServerIP();
					break;
				case CONFIG_GATEWAY:
					ip = Ethernet.gatewayIP();
					break;
				case CONFIG_SUBNET:
					ip = Ethernet.subnetMask();
					break;
				default:
					ip = IPAddress(0, 0, 0, 0);
					break;
			}

			return_value += String(ip[0]);
			return_value += ".";
			return_value += String(ip[1]);
			return_value += ".";
			return_value += String(ip[2]);
			return_value += ".";
			return_value += String(ip[3]);
		}
		break;
		case CONFIG_PORT:
			return_value = String(port_);
			break;
		default:
			return_value = "";
			break;
	}

	return return_value;
}

bool CommandInterfaceEthernet::setConfig(uint8_t config, String value)
{
	return false;
}

void CommandInterfaceEthernet::writeDefaults()
{
	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::ETHERNET_ENABLED,
		0,
		static_cast<byte>(DEF_G_ETHERNET_ENABLED));

	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::ETHERNET_DHCP,
		0,
		static_cast<byte>(DEF_G_ETHERNET_DHCP));

	byte mac[] = DEF_G_ETHERNET_MAC;
	EEPROMHandler::writeArray(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::ETHERNET_MAC,
		0,
		mac,
		sizeof(mac));

	uint8_t ip[] = DEF_G_ETHERNET_IP;
	EEPROMHandler::writeArray(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::ETHERNET_IP,
		0,
		ip,
		sizeof(ip));

	uint8_t dns[] = DEF_G_ETHERNET_DNS;
	EEPROMHandler::writeArray(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::ETHERNET_DNS,
		0,
		dns,
		sizeof(dns));

	uint8_t gateway[] = DEF_G_ETHERNET_GATEWAY;
	EEPROMHandler::writeArray(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::ETHERNET_GATEWAY,
		0,
		gateway,
		sizeof(gateway));

	uint8_t subnet[] = DEF_G_ETHERNET_SUBNET;
	EEPROMHandler::writeArray(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::ETHERNET_SUBNET,
		0,
		subnet,
		sizeof(subnet));

	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::ETHERNET_PORT,
		0,
		static_cast<uint16_t>(DEF_G_ETHERNET_PORT));
}

int CommandInterfaceEthernet::available()
{
	//renew DHCP lease if necessary.
	if (use_dhcp_)
		Ethernet.maintain();
	
	EthernetClient client = server_->available();

	//wait for a new client to connect. if the client refers to the same socket as the
	//existing client, it is the existing client and this part of the code is skipped.
	//if a client is already connected, open and immediately close the connection.
	if (client && (client_ != client))
	{
		if (!client_.connected())
		{
			client_.stop();
			client_ = client;
		}
		else
		{
			client.stop();
		}
	}
	
	return client_.available();
}

int CommandInterfaceEthernet::read()
{
	return client_.read();
}

int CommandInterfaceEthernet::peek()
{
	return client_.peek();
}

size_t CommandInterfaceEthernet::write(uint8_t data)
{
	return client_.write(data);
}

size_t CommandInterfaceEthernet::write(const uint8_t * buffer, size_t size)
{
	return client_.write(buffer, size);
}

void CommandInterfaceEthernet::flush()
{
	client_.flush();
}

bool CommandInterfaceEthernet::beginInterface()
{
	//read settings from EEPROM.
//MAC address is always needed
	byte mac[6] = DEF_G_ETHERNET_MAC;

	EEPROMHandler::readArray(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::ETHERNET_MAC,
		0,
		mac,
		sizeof(mac));

	//read DHCP setting
	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::ETHERNET_DHCP,
		0,
		use_dhcp_);

	if (use_dhcp_ == true)
	{
		//This seems to block code execution until a DHCP lease is acquired.
		Ethernet.begin(mac);
	}
	else
	{
		//read IP, Gateway and Subnet from EEPROM
		uint8_t ip[4] = DEF_G_ETHERNET_IP;
		uint8_t gateway[4] = DEF_G_ETHERNET_GATEWAY;
		uint8_t dns[4] = DEF_G_ETHERNET_DNS;
		uint8_t subnet[4] = DEF_G_ETHERNET_SUBNET;

		EEPROMHandler::readArray(
			EEPROMHandler::device_type_::GENERAL,
			EEPROMHandler::general_variable_::ETHERNET_IP,
			0,
			ip,
			sizeof(ip));

		EEPROMHandler::readArray(
			EEPROMHandler::device_type_::GENERAL,
			EEPROMHandler::general_variable_::ETHERNET_DNS,
			0,
			dns,
			sizeof(dns));

		EEPROMHandler::readArray(
			EEPROMHandler::device_type_::GENERAL,
			EEPROMHandler::general_variable_::ETHERNET_GATEWAY,
			0,
			gateway,
			sizeof(gateway));

		EEPROMHandler::readArray(
			EEPROMHandler::device_type_::GENERAL,
			EEPROMHandler::general_variable_::ETHERNET_SUBNET,
			0,
			subnet,
			sizeof(subnet));

		Ethernet.begin(mac, ip, dns, gateway, subnet);
	}

	//read port setting
	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::ETHERNET_PORT,
		0,
		port_);

	server_ = new EthernetServer(port_);

	server_->begin();

	return true;
}

#endif /* FEATURE_ETHERNET */

#endif /* !defined(ESP8266) && !defined(ESP32) */
