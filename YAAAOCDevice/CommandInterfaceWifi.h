//Command Interface (WiFi) class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef COMMAND_INTERFACE_WIFI_H_
#define COMMAND_INTERFACE_WIFI_H_

#if defined(ESP8266) || defined(ESP32)

#include <Arduino.h>

#include "defaults.h"

#include "CommandInterface.h"

#include "EEPROMHandler.h"

#include "YAAAWifi.h"

class CommandInterfaceWifi :
	public CommandInterface
{
public:
	enum config_t : uint8_t
	{
		CONFIG_IP = 0,
		CONFIG_DNS = 1,
		CONFIG_GATEWAY = 2,
		CONFIG_SUBNET = 3,
		CONFIG_PORT = 4,
		CONFIG_RSSI = 5
	};

	static const uint32_t CONNECT_TIMEOUT = 10000;		//timeout for connection to wifi network.

	CommandInterfaceWifi();
	~CommandInterfaceWifi();

	/*
	@param config element identifier.
	@return config element as String. */
	String getConfig(uint8_t config);

	/*
	@param config element identifier
	@param value to set
	@return true on success, false otherwise. */
	bool setConfig(uint8_t config, String value);

	int available();
	int read();
	int peek();

	size_t write(uint8_t data);
	size_t write(const uint8_t *buffer, size_t size);

	void flush();

	/*
	writes default values for the Serial Interface to EEPROM. */
	static void writeDefaults();

private:
	bool beginInterface();

	WiFiServer server_;

	WiFiClient client_;

	uint16_t port_;
};

#endif /* defined(ESP8266) || defined(ESP32) */

#endif /*COMMAND_INTERFACE_WIFI_H_ */
