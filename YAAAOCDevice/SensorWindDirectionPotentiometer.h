//Wind Direction sensor (potentiometer based) class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef SENSOR_WIND_DIRECTION_POTENTIOMETER_H_
#define SENSOR_WIND_DIRECTION_POTENTIOMETER_H_

#include <Arduino.h>

#include "SensorWindDirection.h"

class SensorWindDirectionPotentiometer :
	public SensorWindDirection
{
public:
	SensorWindDirectionPotentiometer(YAAASensor::SensorSettings sensor_settings);

	~SensorWindDirectionPotentiometer();

	bool begin();

	bool measure();

	bool hasValue();

	float getValue();

private:
	uint8_t pin_;				//arduino input pin number

	uint16_t analog_min_;		//minimum analogRead value this potentiometer can produce
	uint16_t analog_max_;		//minimum analogRead value this potentiometer can produce

	float angle_offset_;		//offset from sensor's north to true north

	uint16_t analog_value_;
};

#endif /* SENSOR_WIND_DIRECTION_POTENTIOMETER_H_ */