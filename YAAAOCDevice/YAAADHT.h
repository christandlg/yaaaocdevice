#ifndef YAAADHT_H_
#define YAAADHT_H_

#include <Arduino.h>

#include <SimpleDHT.h>

class YAAADHT
{
public:
	enum dht_type_t : uint8_t
	{
		TYPE_DHT11 = 11,
		TYPE_DHT22 = 22
	};

	YAAADHT(uint8_t pin, uint8_t type);
	~YAAADHT();

	bool begin();

	bool measureHumidity();

	bool measureTemperature();

	bool hasHumidity();

	bool hasTemperature();

	float getHumidity();

	float getTemperature();

private:
	static const uint32_t READ_TIMEOUT_DHT11 = 1000L;
	static const uint32_t READ_TIMEOUT_DHT22 = 2000L;

	bool measure();

	SimpleDHT *sensor_;

	uint8_t pin_;

	uint8_t type_;

	float humidity_;
	float temperature_;

	uint32_t measure_interval_;
	uint32_t measure_last_;
};

#endif /* YAAADHT_H_ */