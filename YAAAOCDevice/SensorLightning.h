//lightning sensor class.

#ifndef SENSOR_LIGHTNING_H_
#define SENSOR_LIGHTNING_H_

#include <Arduino.h>

#include "YAAASensor.h"

class SensorLightning : public YAAASensor
{
public:
enum sensor_device_t : uint8_t
{
	DEVICE_ANALOG = 0,
	DEVICE_AS3935 = 1,		//sensor is an Austria Microsystems AS3935 Franklin lightning sensor
	DEVICE_UNKNOWN = 255
};

enum distance_unit_t : uint8_t
{
	UNIT_METERS = 0,
	UNIT_KILOMETERS = 1,
	UNIT_MILES = 2,
};

	SensorLightning(YAAASensor::SensorSettings sensor_settings);
	
	virtual ~SensorLightning();
	
	virtual bool begin() = 0;
	
	virtual bool measure() = 0;
	
	virtual bool hasValue() = 0;
	
	virtual float getValue() = 0;
	
	uint8_t getSensorType();
	
	uint8_t getUnit();
	
	bool setUnit(uint8_t unit);


protected:
	float convertUnit(float dist, uint8_t unit_src, uint8_t unit_dst);
	
	uint8_t unit_;
	
private:
	uint8_t sensor_type_;
};

#endif /* SENSOR_LIGHTNING_H_ */

