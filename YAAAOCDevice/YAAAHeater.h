//Heater class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef YAAAHEATER_H_
#define YAAAHEATER_H_

#include <Arduino.h>

class YAAAHeater
{
public:
	struct HeaterSettings
	{
		uint8_t device_;						//heater device as heater_device_t. set to DEVICE_DISABLED to disable (no instance is created in this case).
		
		byte parameters_[4];					//additional parameters for heater
	};

	enum heater_device_t : uint8_t
	{
		DEVICE_DISABLED = 0,			//heater is disabled.
		DEVICE_ARDUINO_PIN = 1,			//heater is controlled by an Arduino pin.
	};

	YAAAHeater();

	~YAAAHeater();

	virtual bool begin() = 0;

	/*
	@return: 0 if the heater is disabled, 1 - 255 if the heater is enabled. */
	virtual uint8_t getValue() = 0;

	/*
	enables or disables the heater. fails if automatic control is enabled or PWM control requested, but unsupported.
	@param value: 0 to disable, 1 - 255 to enable.
	@return true on success, false otherwise. */
	virtual bool setValue(uint8_t value) = 0;
};

#endif /* YAAAHEATER_H_ */