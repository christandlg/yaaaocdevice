//Wind Direction sensor (resistor network based) class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef SENSOR_WIND_DIRECTION_SWITCH_RESISTOR_H_
#define SENSOR_WIND_DIRECTION_SWITCH_RESISTOR_H_

#include <Arduino.h>

#include "SensorWindDirection.h"

class SensorWindDirectionSwitchResistor :
	public SensorWindDirection
{
public:
	SensorWindDirectionSwitchResistor(YAAASensor::SensorSettings sensor_settings);

	~SensorWindDirectionSwitchResistor();

	/*
	checks if user defined input pin is an analog input pin.
	@return true on success, false otherwise. */
	bool begin();

	/*
	performs a measurement. 
	@return: true on success, false otherwise. */
	bool measure();

	/*
	@return true if a measured value is available, false otherwise. */
	bool hasValue();

	/*
	@return wind direction reported by sensor in degrees.*/
	float getValue();

private:
	uint8_t pin_;				//Arduino's pin number the wind vane is connected to

	float angle_offset_;		//offset between true north and wind vane north. 

	uint16_t analog_value_;		//measured analog value
};

#endif /* SENSOR_WIND_DIRECTION_SWITCH_RESISTOR_H_ */