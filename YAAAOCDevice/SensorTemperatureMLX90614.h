//MLX90614 Temperature sensor class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef SENSOR_TEMPERATURE_MLX90614_H_
#define SENSOR_TEMPERATURE_MLX90614_H_

#include <Arduino.h>

#include "Wire.h"

#include "SensorTemperature.h"

class SensorTemperatureMLX90614 :
	public SensorTemperature
{
public:
	enum ram_address_t : uint8_t
	{
		RAM_RAW_IR_CH1 = 0x04,			//raw value of IR channel 1
		RAM_RAW_IR_CH2 = 0x05,			//raw value of IR channel 2
		RAM_TEMP_A = 0x06,				//ambient temperature
		RAM_TEMP_OBJ1 = 0x07,			//channel 1 linearized temperature
		RAM_TEMP_OBJ2 = 0x08			//channel 2 linearized temperature
	};

	SensorTemperatureMLX90614(YAAASensor::SensorSettings sensor_settings);
	~SensorTemperatureMLX90614();

	/*
	initializes the temperature sensor with the info given in in the constructor call.
	@return true on success (input pin is an analog input pin), false otherwise. */
	bool begin();

	/*
	starts a measurement.
	@return true on success, false otherwise.*/
	bool measure();

	/*
	checks if a measurement has been completed. may perform work necessary to complete a mesurement. user is
	expected to call this function after calling measure() repeatedly until it returns true.
	@return true if a measurement has been completed and returned a valid result, false otherwise. */
	bool hasValue();

	/*
	returns the last measured value.
	user is expected to call this function only after hasValue() has returned true.
	@return the barometric pressure in the given unit. */
	float getValue();

private:
	enum eeprom_address_t : uint8_t
	{
		EEPROM_T_O_MAX = 0x00,			//upper limit of object temperature range (for PWM output)
		EEPROM_T_O_MIN = 0x01,			//lower limit of object temperature range (for PWM output)
		EEPROM_PWMCTRL = 0x02,			//PWM data output configuration
		EEPROM_T_A_RANGE = 0x03,		//upper and lower limit of ambient temperature range (for PWM output)
		EEPROM_ECC = 0x04,				//emissivity setting ( dec2hex(round( 65535 x e)), e = 0.1 ... 1.0 )
		EEPROM_CONFIG_REG1 = 0x05,		//config register for analog and digital parts of the sensor. 
		EEPROM_SMBUS_ADDRESS = 0x0E,	//SMBus address (LSByte only)
		EEPROM_ID1 = 0x1C,
		EEPROM_ID2 = 0x1D,
		EEPROM_ID3 = 0x1E,
		EEPROM_ID4 = 0x1F,
	};

	enum opcode_t : uint8_t
	{
		OPCODE_RAM_ACCESS = 0x00,
		OPCODE_EEPROM_ACCESS = 0x20,
		OPCODE_READ_FLAGS = 0xF0,
		OPCODE_ENTER_SLEEP = 0xFF
	};

	enum flag_t : uint8_t
	{
		FLAG_EEBUSY = 0x80,		//EEPROM busy (previous write / erase still ongoing). active high.
		FLAG_EE_DEAD = 0x20,	//EEPROM double error has occurred. active high.
		FLAG_INIT_POR = 0x10,	//POR initialization still ongoing. active low.
	};

	/*
	calculates a temperature from the given value.
	@param value of ambient or object temperature.
	@return temperature in kelvin. */
	float calculateTemperature(uint16_t raw_value);

	/*
	checks if the error bit in a raw object temperature value has been set.
	@param raw_value raw object temperature value.
	@return true if the error bit is set. */
	inline bool checkErrorBit(uint16_t raw_value);

	uint8_t i2c_address_;		//factory default: 0x5A

	uint8_t zone_;

	uint16_t emissivity_;

	uint16_t value_;
};

#endif /* SENSOR_TEMPERATURE_MLX90614_H_ */