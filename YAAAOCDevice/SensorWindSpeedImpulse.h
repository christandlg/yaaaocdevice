//Wind Speed sensor (impulse based) class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef SENSOR_WIND_SPEED_IMPULSE_H_
#define SENSOR_WIND_SPEED_IMPULSE_H_

#include <Arduino.h>

#include "SensorWindSpeed.h"

class SensorWindSpeedImpulse :
	public SensorWindSpeed
{
public:
	SensorWindSpeedImpulse(YAAASensor::SensorSettings sensor_settings);
	~SensorWindSpeedImpulse();

	/*
	initializes the impulse wind speed sensor. 
	@return true on success, false otherwise. */
	bool begin();

	/*
	wind speed measurement is done automatically by interrupts, nothing to do here
	@return true */
	bool measure();

	/*
	wind speed measurement is done automatically by interrupts, nothing to do here
	@return true */
	bool hasValue();

	/*
	@return wind speed based on number of counted impulses in last count period. */
	float getValue();

	/*
	interrupt service routine (rotate) for impulse based wind speed sensor, calls function 
	rotate() if an instance has already been created. */
	static void rotateISR();

private:
	/*
	counts an impulse. implements software debouncing. */
	void impulse();

	/*
	copies value of counts_current_ to counts_prev_ and resets the count rotate timer. */
	void rotate();

	/*
	interrupt service routine (count) for the impulse based wind speed sensor. */
	static void isr();

	uint8_t pin_;									//input pin. 

	uint32_t count_period_;							//count period (measurement period) in ms.

	float wind_speed_at_1Hz_;						//wind speed in m/s if switch is triggered once per second.

	uint16_t software_debounce_delay_;				//minimum time between successive impulses, in us.
	
	volatile uint32_t impulse_last_;				//time (in us) of last counted impulse. used for software debouncing.

	volatile uint32_t counts_current_;				//counter for the current measurement interval.
	volatile uint32_t counts_last_;					//counter for the previous mesurement interval.

	volatile uint32_t count_start_last_;			//start time of last count period, in ms.

	volatile uint32_t count_period_last_;			//true value for last measurement interval, in ms.

	static SensorWindSpeedImpulse *instance_;		//pointer to SensorWindSpeedImpulse instance. only one instance can be created.
};

#endif /* SENSOR_WIND_SPEED_IMPULSE_H_ */