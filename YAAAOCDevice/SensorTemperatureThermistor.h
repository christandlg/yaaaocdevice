//Thermistor Temperature sensor class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef SENSOR_TEMPERATURE_THERMISTOR_H_
#define SENSOR_TEMPERATURE_THERMISTOR_H_

#include <math.h>

#include "SensorTemperature.h"

class SensorTemperatureThermistor :
	public SensorTemperature
{
public:
	SensorTemperatureThermistor(YAAASensor::SensorSettings sensor_settings);

	~SensorTemperatureThermistor();

	/*
	initializes the temperature sensor with the info given in in the constructor call.
	@return: see initialize(uint8_t *serial_number = NULL, uint8_t resolution = 0)*/
	bool begin();

	//@return: true if the measurement was successful, false otherwise 
	bool measure();

	/*
	@return true. */
	bool hasValue();

	//@return: the temperature in the requested temperature unit.* /
	float getValue();

private:
	uint8_t pin_;						//pin number of thermistor 

	float pullup_resistance_;			//resistance of (fixed) pullup resistor
	float reference_resistance_;		//resistance of thermistor at reference temperature
	float reference_temperature_;		//thermistor's reference temperature

	float B_;							//Steinhart-Hart parameter B of this thermistor

	uint16_t analog_value_;
};

#endif /* SENSOR_TEMPERATURE_THERMISTOR_H_ */

