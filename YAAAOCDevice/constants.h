//constants.h file
//this file contains constants definitions.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


#ifndef CONSTANTS_H_
#define CONSTANTS_H_

//#define EEPROM_WRITE_LOCK		//EEPROM write lock can be enabled at compile time by removing the comment

#define TEMP_SENSORS 3			//3 temperature sensors per device

//delimiters for use with strtok. '<' and '>' are only allowed at the beginning and the end of a message.
//additionally, ':' and '#' are only allowed in LX200 protocol transmissions.
#define COMMAND_DELIMITERS " <>,;:"
#define COMMAND_START "<"
#define COMMAND_END ">"
#define COMMAND_MAX_LENGTH 128

#define YAAAVERSION "0.0.1"
#define YAAADATETIME __DATE__ ", " __TIME__		//YAAADevice compile date / time

#endif /* CONSTANTS_H_ */
