//PWM-controlled heater class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "HeaterArduinoPin.h"

HeaterArduinoPin::HeaterArduinoPin(HeaterSettings heater_settings) :
	YAAAHeater(),
	pin_(heater_settings.parameters_[0]),
	inverted_(heater_settings.parameters_[1]), 
	control_value_(0)
{
	//nothing to do here...
}


HeaterArduinoPin::~HeaterArduinoPin()
{
	//nothing to do here...
}

bool HeaterArduinoPin::begin()
{
	pinMode(pin_, OUTPUT);

	//disable heater on startup
	digitalWrite(pin_, LOW ^ inverted_);

	return true;
}

uint8_t HeaterArduinoPin::getValue()
{
	return control_value_;
}

bool HeaterArduinoPin::setValue(uint8_t value)
{
	if (value == 0)
		digitalWrite(pin_, LOW ^ inverted_);
	else if (value == 255)
		digitalWrite(pin_, HIGH ^ inverted_);
	else
	{
		//todo add pin check, return if pwm is to be used on non-pwm capable pin
		if (false)
			return false;

		//if inverted_ is true, calculate 255 - value (invert bits)
#ifndef ESP32
		analogWrite(pin_, inverted_ ? ~value : value);
#else
		return false;
#endif
	}

	control_value_ = value;

	return true;
}
