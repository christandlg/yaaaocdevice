//Tipping Bucket type rain rate sensor class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "SensorRainRateTippingBucket.h"

SensorRainRateTippingBucket *SensorRainRateTippingBucket::instance_ = NULL;

SensorRainRateTippingBucket::SensorRainRateTippingBucket(YAAASensor::SensorSettings sensor_settings) :
	SensorRainRate(sensor_settings),
	pin_(255),
	software_debounce_delay_(DEF_SENSOR_RAIN_RATE_TIPPING_BUCKET_DEBOUNCE_DELAY),
	count_period_(DEF_SENSOR_RAIN_RATE_TIPPING_BUCKET_COUNT_PERIOD),
	mm_per_count_(DEF_SENSOR_RAIN_RATE_TIPPING_BUCKET_MM_PER_COUNT),
	impulse_last_(0L),
	counts_current_(0L),
	counts_last_(0L),
	count_start_last_(0L),
	count_period_last_(1L)
{
	pin_ = sensor_settings.parameters_[0];

	uint16_t um_per_count = (sensor_settings.parameters_[2] << 8 | sensor_settings.parameters_[3]);
	mm_per_count_ = static_cast<float>(um_per_count) / 1000.0f;

	count_period_ =
		(static_cast<uint32_t>(sensor_settings.parameters_[4]) << 24 |
		 static_cast<uint32_t>(sensor_settings.parameters_[5]) << 16 |
		 static_cast<uint32_t>(sensor_settings.parameters_[6]) << 8 |
		 static_cast<uint32_t>(sensor_settings.parameters_[7]));

	software_debounce_delay_ = (sensor_settings.parameters_[8] << 8 | sensor_settings.parameters_[9]);
}

SensorRainRateTippingBucket::~SensorRainRateTippingBucket()
{
}

bool SensorRainRateTippingBucket::begin()
{
	//check if an ISR can be attached to the input pin.
#ifdef ARDUINO_SAM_DUE
	if (pin_ > 53)								//Arduino Due: all digital pins can be used as interrupt sources
#elif defined(ARDUINO_AVR_MEGA2560) //__AVR_ATmega2560__
	if ((pin_ != 2) && (pin_ != 3) && (pin_ != 18) && (pin_ != 19) && (pin_ != 20) && (pin_ != 21))		//Arduino Mega: pins 2, 3, 18, 19, 20, 21 can be used as interrupt sources
#elif defined(ESP8266) //__ESP8266_ESP8266__
	if ((pin_ == D0) || (pin_ > 16))		//ESP8266: all pins except D0 (GPIO16) can be used as interrupt surouces.
#else
	if (true)
#endif
		return false;

	//do nothing if an instance has already been created.
	if (instance_)
		return false;

	instance_ = this;

	pinMode(pin_, INPUT);

	//attach reed switch interrupt
	attachInterrupt(digitalPinToInterrupt(pin_), SensorRainRateTippingBucket::isr, FALLING);

	return true;
}

bool SensorRainRateTippingBucket::measure()
{
	return true;
}

bool SensorRainRateTippingBucket::hasValue()
{
	return true;
}

float SensorRainRateTippingBucket::getValue()
{
	//calculate average impulses per hour based on number of impulses counted during last count interval
	float avg_impulses = static_cast<float>(counts_last_) / static_cast<float>(count_period_last_) * 3600000.0f;

	//return rain rate estimation based on the previous counting interval
	return convertRainRate(
		mm_per_count_ * avg_impulses,
		SensorRainRate::UNIT_MM_PER_HOUR,
		rain_rate_unit_);
}

void SensorRainRateTippingBucket::rotateISR()
{
	if (!instance_)
		return;

	noInterrupts();

	instance_->rotate();

	instance_->updateMeasurementTime();

	interrupts();
}

void SensorRainRateTippingBucket::impulse()
{
	//count a pulse only if the software debounce delay has passed.
	if (micros() - impulse_last_ > software_debounce_delay_)
	{
		counts_current_++;
		impulse_last_ = micros();
	}
}

void SensorRainRateTippingBucket::rotate()
{
	if (millis() - count_start_last_ > count_period_)
	{
		count_period_last_ = millis() - count_start_last_;

		counts_last_ = counts_current_;
		counts_current_ = 0;

		count_start_last_ = millis();
	}
}

void SensorRainRateTippingBucket::isr()
{
	noInterrupts();

	if (instance_)
		instance_->impulse();

	interrupts();
}
