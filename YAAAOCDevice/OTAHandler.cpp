//OTA class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#if defined(ESP8266) || defined(ESP32)

#include "OTAHandler.h"

#ifdef FEATURE_OTA

uint8_t OTAHandler::mode_ = DEF_G_OTA_MODE;

uint32_t OTAHandler::httpserver_update_last_ = 0;
uint32_t OTAHandler::httpserver_update_interval_ = DEF_G_OTA_HTTPSERVER_UPDATE_INTERVAL;

#if defined(ESP8266)
ESP8266WebServer *OTAHandler::httpServer = NULL;
ESP8266HTTPUpdateServer *OTAHandler::httpUpdater = NULL;
#endif /* defined(ESP8266) */

#if defined(ESP32)
//TODOto
#endif /* defined(ESP32) */

OTAHandler::OTAHandler()
{
	//nothing to do here...
}

OTAHandler::~OTAHandler()
{
}

bool OTAHandler::begin()
{
	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::OTA_MODE,
		0,
		mode_);

	if (mode_ & OTAHandler::MODE_ARDUINOOTA)
	{
		bool error = false;

		uint16_t port = DEF_G_OTA_ARDUINOOTA_PORT;
		String hostname = DEF_G_OTA_HOSTNAME;
		String password = DEF_G_OTA_ARDUINOOTA_PASSWORD;
		bool password_hash = DEF_G_OTA_ARDUINOOTA_PASSWORD_HASH;

		EEPROMHandler::readValue(
			EEPROMHandler::device_type_::GENERAL,
			EEPROMHandler::general_variable_::OTA_ARDUINOOTA_PORT,
			0,
			port
		);

		EEPROMHandler::readString(
			EEPROMHandler::device_type_::GENERAL,
			EEPROMHandler::general_variable_::OTA_HOSTNAME,
			0,
			hostname);

		EEPROMHandler::readValue(
			EEPROMHandler::device_type_::GENERAL,
			EEPROMHandler::general_variable_::OTA_ARDUINOOTA_PASSWORD_HASH,
			0,
			password_hash
		);

		EEPROMHandler::readString(
			EEPROMHandler::device_type_::GENERAL,
			EEPROMHandler::general_variable_::OTA_ARDUINOOTA_PASSWORD,
			0,
			password);

		//if initialization fails, clear bit
		if (error)
			mode_ &= ~OTAHandler::MODE_ARDUINOOTA;
		else
		{
			ArduinoOTA.setPort(port);
			ArduinoOTA.setHostname(hostname.c_str());

			if (password_hash)
				ArduinoOTA.setPasswordHash(password.c_str());
			else
				ArduinoOTA.setPassword(password.c_str());

			//function to handle OTA
			//the code below is essentially an adpted version of the ArduinoOTA example
			ArduinoOTA.onStart([]() {
				String type;
				if (ArduinoOTA.getCommand() == U_FLASH)
					type = "sketch";
				else // U_SPIFFS
					type = "filesystem";

				// NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
				log_.log("OTA start " + type);
			});
			ArduinoOTA.onEnd([]() {
				log_.log("OTA end");
			});
			ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {

			});
			ArduinoOTA.onError([](ota_error_t error) {
				Serial.printf("Error[%u]: ", error);
				if (error == OTA_AUTH_ERROR) log_.log("OTA Error: Auth Failed");
				else if (error == OTA_BEGIN_ERROR) log_.log("OTA Error: Begin Failed");
				else if (error == OTA_CONNECT_ERROR) log_.log("OTA Error: Connect Failed");
				else if (error == OTA_RECEIVE_ERROR) log_.log("OTA Error: Receive Failed");
				else if (error == OTA_END_ERROR) log_.log("OTA Error: End Failed");
			});

			ArduinoOTA.begin();
		}
	}

//	if (mode_ & OTAHandler::MODE_WEBBROWSER)
//	{
//		bool error = false;
//
//		uint16_t port = DEF_G_OTA_WEBBROWSER_PORT;
//		String hostname = DEF_G_OTA_HOSTNAME;
//		String path = DEF_G_OTA_WEBBROWSER_PATH;
//		String username = DEF_G_OTA_WEBBROWSER_USERNAME;
//		String password = DEF_G_OTA_WEBBROWSER_PASSWORD;
//
//		EEPROMHandler::readValue(
//			EEPROMHandler::device_type_::GENERAL,
//			EEPROMHandler::general_variable_::OTA_WEBBROWSER_PORT,
//			0,
//			port
//		);
//
//		//read HOSTNAME
		//EEPROMHandler::readString(
		//	EEPROMHandler::device_type_::GENERAL,
		//	EEPROMHandler::general_variable_::OTA_HOSTNAME,
		//	0,
		//	hostname);
//
//		//read PATH.
//		EEPROMHandler::readString(
//			EEPROMHandler::device_type_::GENERAL,
//			EEPROMHandler::general_variable_::OTA_WEBBROWSER_PATH,
//			0,
//			path);
//
//		//read USERNAME
//		EEPROMHandler::readString(
//			EEPROMHandler::device_type_::GENERAL,
//			EEPROMHandler::general_variable_::OTA_WEBBROWSER_USERNAME,
//			0,
//			username);
//
//		//read PASSWORD
//		EEPROMHandler::readString(
//			EEPROMHandler::device_type_::GENERAL,
//			EEPROMHandler::general_variable_::OTA_WEBBROWSER_PASSWORD,
//			0,
//			password);
//
//#if !defined(ESP8266)
//		error = true;
//#endif /* !defined(ESP8266) */
//
//
//		//if initialization fails, clear bit
//		if (error)
//			mode_ &= ~OTAHandler::MODE_WEBBROWSER;
//
//#if defined(ESP8266)
//		else
//		{
//			MDNS.begin(hostname.c_str());
//
//			httpUpdater->setup(httpServer, path.c_str(), username.c_str(), password.c_str());
//			httpServer->begin();
//
//			MDNS.addService("http", "tcp", 80);
//		}
//#endif /* defined(ESP8266) */
//	}
//
//	if (mode_ & OTAHandler::MODE_HTTPSERVER)
//	{
//		bool error = false;
//		//TODO
//
//		//if initialization fails, clear bit
//		if (error)
//			mode_ &= ~OTAHandler::MODE_HTTPSERVER;
//}

	return (mode_ == OTAHandler::MODE_DISABLED ? false : true);
}

void OTAHandler::run()
{
	if (mode_ == OTAHandler::MODE_DISABLED)
		return;

	if (mode_ & OTAHandler::MODE_ARDUINOOTA)
	{
		ArduinoOTA.handle();
	}

//	if (mode_ & OTAHandler::MODE_WEBBROWSER)
//	{
//#if defined(ESP8266)
//		httpServer->handleClient();
//#endif /* defined(ESP8266) */
//	}
//
//	if (mode_ & OTAHandler::MODE_HTTPSERVER)
//	{
//		String address = "";
//		String path = "";
//		uint16_t port = 8266;
//		String current_version = "";
//
//		//TODO get address, path, port and current version from EEPROM
//
//		//if initialization fails, clear bit
//		if (!address || !path || !current_version)
//		{
//			mode_ &= ~OTAHandler::MODE_HTTPSERVER;
//			return;
//		}
//		//
//		//t_httpUpdate_return ret = ESPhttpUpdate.update(address, port, path, current_version);
//
//
//		//TODO...
//	}
}

uint8_t OTAHandler::getOTAMode()
{
	return mode_;
}

bool OTAHandler::getOTAModeEnabled(uint8_t mode)
{
	return mode & mode_;
}

void OTAHandler::writeDefaults()
{
	EEPROMHandler::writeValue(EEPROMHandler::device_type_::GENERAL, EEPROMHandler::general_variable_::OTA_MODE, 0, static_cast<uint8_t>(1));

	EEPROMHandler::writeString(EEPROMHandler::device_type_::GENERAL, EEPROMHandler::general_variable_::OTA_HOSTNAME, 0, DEF_G_OTA_HOSTNAME);

	EEPROMHandler::writeValue(EEPROMHandler::device_type_::GENERAL, EEPROMHandler::general_variable_::OTA_ARDUINOOTA_PORT, 0, static_cast<uint16_t>(DEF_G_OTA_ARDUINOOTA_PORT));
	EEPROMHandler::writeValue(EEPROMHandler::device_type_::GENERAL, EEPROMHandler::general_variable_::OTA_ARDUINOOTA_PASSWORD_HASH, 0, static_cast<uint8_t>(DEF_G_OTA_ARDUINOOTA_PASSWORD_HASH));
	EEPROMHandler::writeString(EEPROMHandler::device_type_::GENERAL, EEPROMHandler::general_variable_::OTA_ARDUINOOTA_PASSWORD, 0, DEF_G_OTA_ARDUINOOTA_PASSWORD);

	//EEPROMHandler::writeValue(EEPROMHandler::device_type_::GENERAL, EEPROMHandler::general_variable_::OTA_WEBBROWSER_PORT, 0, static_cast<uint16_t>(DEF_G_OTA_WEBBROWSER_PORT));
	//EEPROMHandler::writeString(EEPROMHandler::device_type_::GENERAL, EEPROMHandler::general_variable_::OTA_WEBBROWSER_PATH, 0, DEF_G_OTA_WEBBROWSER_PATH);
	//EEPROMHandler::writeString(EEPROMHandler::device_type_::GENERAL, EEPROMHandler::general_variable_::OTA_WEBBROWSER_USERNAME, 0, DEF_G_OTA_WEBBROWSER_USERNAME);
	//EEPROMHandler::writeString(EEPROMHandler::device_type_::GENERAL, EEPROMHandler::general_variable_::OTA_WEBBROWSER_PASSWORD, 0, DEF_G_OTA_WEBBROWSER_PASSWORD);

	//EEPROMHandler::writeValue(EEPROMHandler::device_type_::GENERAL, EEPROMHandler::general_variable_::OTA_HTTPSERVER_UPDATE_INTERVAL, 0, static_cast<uint32_t>(DEF_G_OTA_HTTPSERVER_UPDATE_INTERVAL));
	//EEPROMHandler::writeString(EEPROMHandler::device_type_::GENERAL, EEPROMHandler::general_variable_::OTA_HTTPSERVER_ADDRESS, 0, DEF_G_OTA_HTTPSERVER_ADDRESS);
	//EEPROMHandler::writeString(EEPROMHandler::device_type_::GENERAL, EEPROMHandler::general_variable_::OTA_HTTPSERVER_PATH, 0, DEF_G_OTA_HTTPSERVER_PATH);
	//EEPROMHandler::writeValue(EEPROMHandler::device_type_::GENERAL, EEPROMHandler::general_variable_::OTA_HTTPSERVER_PORT, 0, static_cast<uint16_t>(DEF_G_OTA_HTTPSERVER_PORT));
}

#endif /* FEATURE_OTA */

#endif /* defined(ESP8266) || defined(ESP32) */