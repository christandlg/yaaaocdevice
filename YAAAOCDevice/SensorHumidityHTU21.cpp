//HTU21 Humidity sensor class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "SensorHumidityHTU21.h"

SensorHumidityHTU21::SensorHumidityHTU21(YAAASensor::SensorSettings sensor_settings, void *sensor) :
	SensorHumidity(sensor_settings),
	sensor_(NULL)
{
	if (sensor)
		sensor_ = static_cast<YAAAHTU21D*>(sensor);
	else
		sensor_ = new YAAAHTU21D(sensor_settings.parameters_[0]);
}

SensorHumidityHTU21::~SensorHumidityHTU21()
{
	if (sensor_)
		delete sensor_;

	sensor_ = NULL;
}

bool SensorHumidityHTU21::begin()
{
	return sensor_ && sensor_->begin();
}

bool SensorHumidityHTU21::measure()
{
	return sensor_ && sensor_->measureHumidity();
}

bool SensorHumidityHTU21::hasValue()
{
	return sensor_ && sensor_->hasHumidity();
}

float SensorHumidityHTU21::getValue()
{
	if (!sensor_)
		return NAN;

	return sensor_->getHumidity();
}

void * SensorHumidityHTU21::getSensor()
{
	return sensor_;
}

bool SensorHumidityHTU21::setMeasurementResolution(uint8_t resolution)
{
	return sensor_ && sensor_->setMeasurementResolution(resolution);
}
