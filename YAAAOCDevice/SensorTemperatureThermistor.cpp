//Thermistor Temperature sensor class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "SensorTemperatureThermistor.h"

SensorTemperatureThermistor::SensorTemperatureThermistor(YAAASensor::SensorSettings sensor_settings) :
	SensorTemperature(sensor_settings),
	pin_(255),
	pullup_resistance_(DEF_SENSOR_TEMPERATURE_THERMISTOR_PULLUP),
	reference_resistance_(DEF_SENSOR_TEMPERATURE_THERMISTOR_REFERENCE_RES),
	reference_temperature_(DEF_SENSOR_TEMPERATURE_THERMISTOR_REFERENCE_TEMP),
	B_(DEF_SENSOR_TEMPERATURE_THERMISTOR_PARAMETER_B),
	analog_value_(0)
{
	pin_ = sensor_settings.parameters_[0];

	//read pullup resistance. pullup resistance is saved as an unsigned integer, in 100s of Ohms.
	uint16_t pullup_resistance = ((sensor_settings.parameters_[2] << 8) | sensor_settings.parameters_[3]);
	pullup_resistance_ = static_cast<float>(pullup_resistance) * 100.0f;

	//read reference resistance. reference resistance is saved as an unsigned integer, in 100s of Ohms.
	uint16_t reference_resistance = ((sensor_settings.parameters_[4] << 8) | sensor_settings.parameters_[5]);
	reference_resistance_ = static_cast<float>(reference_resistance) * 100.0f;

	//read reference temperature. reference temperature is saved as a signed integer, in 1 / 10s of degrees of the set temperature unit.
	uint16_t reference_temperature = ((sensor_settings.parameters_[6] << 8) | sensor_settings.parameters_[7]);
	reference_temperature_ = static_cast<float>(reference_temperature) / 10.0f;

	//read B value. B value is saved as a signed integer.
	int16_t B = ((sensor_settings.parameters_[8] << 8) | sensor_settings.parameters_[9]);
	B_ = static_cast<float>(B);

	setUnit(sensor_settings.unit_);
}

SensorTemperatureThermistor::~SensorTemperatureThermistor()
{
}

bool SensorTemperatureThermistor::begin()
{
	//check if the input pin is an analog pin. return false if it is not.
#ifdef ARDUINO_SAM_DUE
	if ((pin_ < A0) || (pin_ > A11))		//Arduino Due: 12 ADC inputs
#elif defined ARDUINO_AVR_MEGA2560 //__AVR_ATmega2560__
	if ((pin_ < A0) || (pin_ > A15))		//Arduino Mega: 16 ADC inputs
#elif defined ESP8266 //__ESP8266_ESP8266__
	if (pin_ != A0)							//ESP8266: 1 ADC input 
#elif defined ESP32	
	if (pin_ != A0 &&
		pin_ != A4 &&
		pin_ != A5 &&
		pin_ != A6 &&
		pin_ != A7 &&
		pin_ != A10 &&
		pin_ != A11 &&
		pin_ != A12 &&
		pin_ != A13 &&
		pin_ != A14 &&
		pin_ != A15 &&
		pin_ != A16 &&
		pin_ != A17 &&
		pin_ != A18 &&
		pin_ != A19)
#else
	if ((pin_ < A0) || (pin_ > A5))
#endif
		return false;

	return true;
}

bool SensorTemperatureThermistor::measure()
{
	analog_value_ = analogRead(pin_);

	updateMeasurementTime();

	return true;
}

bool SensorTemperatureThermistor::hasValue()
{
	return true;
}

float SensorTemperatureThermistor::getValue()
{
	//calculate thermistor resistance from analog value;
	float resistance = pullup_resistance_ / (1023.0f / static_cast<float>(analog_value_) - 1.0f);

	//calcualte temperature in reference tempterature units
	float temperature = 1.0f / (1.0f / reference_temperature_ + 1.0f / B_ * log(resistance / reference_resistance_));

	//convert temperature and return.
	return(convertTemperature(
		temperature,
		SensorTemperature::UNIT_KELVIN,
		temperature_unit_));
}
