//Sensor class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "YAAASensor.h"

YAAASensor::YAAASensor(YAAASensor::SensorSettings sensor_settings) :
	measure_last_(0L)
{
	//nothing to do here...
}

YAAASensor::~YAAASensor()
{
	//nothing to do here...
}

uint32_t YAAASensor::getMeasurementAge()
{
	return millis() - measure_last_;
}

void * YAAASensor::getSensor()
{
	return nullptr;
}

void YAAASensor::updateMeasurementTime()
{
	measure_last_ = millis();
}

bool YAAASensor::hasEvent()
{
	return false;
}

String YAAASensor::handleEvent()
{
	return "";
}
