#include "YAAADHT.h"



YAAADHT::YAAADHT(uint8_t pin, uint8_t type) :
	sensor_(NULL),
	pin_(pin),
	type_(type),
	humidity_(NAN),
	temperature_(NAN),
	measure_interval_(0L),
	measure_last_(0L)
{

}


YAAADHT::~YAAADHT()
{
	//no destructors are implemented for SimpleDHT and derived classes
	//if (sensor_)
	//	delete sensor_;

	//sensor_ = NULL;
}

bool YAAADHT::begin()
{
	if ((type_ != YAAADHT::TYPE_DHT11) && (type_ != YAAADHT::TYPE_DHT22))
		return false;

	if (pin_ == 255)
		return false;

	switch (type_)
	{
	case YAAADHT::TYPE_DHT11:
		measure_interval_ = READ_TIMEOUT_DHT11;
		sensor_ = new SimpleDHT11(pin_);
		break;
	case YAAADHT::TYPE_DHT22:
		measure_interval_ = READ_TIMEOUT_DHT22;
		sensor_ = new SimpleDHT22(pin_);
		break;
	default:
		return false;
	}

	return true;
}

bool YAAADHT::measureHumidity()
{
	return measure();
}

bool YAAADHT::measureTemperature()
{
	return measure();
}

bool YAAADHT::hasHumidity()
{
	return !isnan(humidity_);
}

bool YAAADHT::hasTemperature()
{
	return !isnan(temperature_);
}

float YAAADHT::getHumidity()
{
	return humidity_;
}

float YAAADHT::getTemperature()
{
	return temperature_;
}

bool YAAADHT::measure()
{
	if (!sensor_)
		return false;

	//recent measurement, can use previous results.
	if (millis() - measure_last_ < measure_interval_)
		return true;

	float temperature, humidity;

	if (sensor_->read2(&temperature, &humidity, NULL) != 0)
		return false;

	humidity_ = humidity;
	temperature_ = temperature;

	measure_last_ = millis();

	return true;
}
