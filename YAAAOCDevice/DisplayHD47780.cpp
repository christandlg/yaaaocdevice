//HD47780 Display class for YAAADevice. 
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "DisplayHD47780.h"

DisplayHD47780::DisplayHD47780(uint16_t chars, uint16_t lines, uint8_t rs, uint8_t enable, uint8_t d0, uint8_t d1, uint8_t d2, uint8_t d3, uint8_t pin_brightness, uint8_t pin_contrast) :
num_chars_(chars), num_lines_(lines),
pin_brightness_(pin_brightness), pin_contrast_(pin_contrast),
val_brightness_(0), val_contrast_(0),
update_state_(UPDATE_DONE)
{
	//set supported features
	setFeature(Display::FEATURE_TEXT);
	setFeature(Display::FEATURE_CUSTOM_CHAR);

	if (pin_brightness_ != 255)
		setFeature(Display::FEATURE_BRIGHTNESS);

	if (pin_contrast_ != 255)
		setFeature(Display::FEATURE_CONTRAST);

	num_chars_ = constrain(num_chars_, 0, 20);
	num_lines_ = constrain(num_lines_, 0, 4);

	//arrays to store content of displays
	display_content_ = new byte[num_lines_ * num_chars_];
	display_content_previous_ = new byte[num_lines_ * num_chars_];

	//clear allocated memory
	memset(display_content_, 0, num_lines_ * num_chars_);
	memset(display_content_previous_, 0, num_lines_ * num_chars_);

	//initialize display
	display_ = new LiquidCrystal(rs, enable, d0, d1, d2, d3);

	display_->begin(num_chars_, num_lines_);

	//prepare a set of custom characters
	addCustomChar(0, locked_);
	addCustomChar(1, unlocked_);
	addCustomChar(2, locked_local_);
	addCustomChar(3, locked_remote_);
	addCustomChar(4, parked_);
	//addCustomChar(0, smiley);
	//addCustomChar(1, lock);
	//addCustomChar(2, lock1);
	//addCustomChar(3, telescope);
	//addCustomChar(4, telescope1);
	//addCustomChar(5, telescope2);
}

DisplayHD47780::~DisplayHD47780()
{
	if (display_content_ != NULL)
	{
		delete[] display_content_;
		delete[] display_content_previous_;
	}
}

void DisplayHD47780::update()
{
	switch (update_state_)
	{
		case UPDATE_RESET:
			updateReset();
			break;
		case UPDATE_CHECK:
			updateCheck();
			break;
		case UPDATE_CURSOR:
			//set cursor 
			updateCursor();
			break;
		case UPDATE_WRITE:
			//write single char to display
			updateCharacter();
			break;
		case UPDATE_NEXT:
			updateNext();
			break;
		case UPDATE_DONE:
			break;
	}
}

void DisplayHD47780::updateReset()
{
	curr_char_ = 0;
	curr_line_ = 0;

	update_state_ = UPDATE_CHECK;
}

void DisplayHD47780::updateCheck()
{
	//if no update is necessary skip to step UPDATE_NEXT
	if (display_content_[index(curr_char_, curr_line_)] !=
		display_content_previous_[index(curr_char_, curr_line_)])
		update_state_ = UPDATE_CURSOR;
	else
		update_state_ = UPDATE_NEXT;
}

void DisplayHD47780::updateCursor()
{
	display_->setCursor(curr_char_, curr_line_);

	update_state_ = DisplayHD47780::UPDATE_WRITE;
}

void DisplayHD47780::updateCharacter()
{
	display_->write(static_cast<byte>(display_content_[index(curr_char_, curr_line_)]));

	display_content_previous_[index(curr_char_, curr_line_)] = display_content_[index(curr_char_, curr_line_)];

	update_state_ = DisplayHD47780::UPDATE_NEXT;
}

void DisplayHD47780::updateNext()
{
	if (calculateNextPosition() == true)
		update_state_ = DisplayHD47780::UPDATE_CHECK;
	else
		update_state_ = DisplayHD47780::UPDATE_DONE;
}

uint8_t DisplayHD47780::getBrightness()
{
	//if (!checkFeature(Display::FEATURE_BRIGHTNESS))
	//	return 0;

	return val_brightness_;
}

uint8_t DisplayHD47780::getContrast()
{
	//if (!checkFeature(Display::FEATURE_CONTRAST))
	//	return 0;

	return val_contrast_;
}

bool DisplayHD47780::setBrightness(uint8_t value)
{
	//return false if brightness pin has not been set.
	if (!checkFeature(Display::FEATURE_BRIGHTNESS))
		return false;

	val_brightness_ = value;

#ifndef ESP32
	analogWrite(pin_brightness_, value);
#endif

	return true;
}

bool DisplayHD47780::setContrast(uint8_t value)
{
	//return false if brightness pin has not been set.
	if (!checkFeature(Display::FEATURE_CONTRAST))
		return false;

	val_contrast_ = value;

#ifndef ESP32
	analogWrite(pin_brightness_, value);
#endif

	return true;
}

bool DisplayHD47780::getResolution(uint16_t &x, uint16_t &y)
{
	x = num_chars_;
	y = num_lines_;

	return true;
}

void DisplayHD47780::printMessage(uint16_t pos, uint16_t line, String message)
{
	//cut string to maximum length
	message.remove(num_chars_ - pos);

	//workaround for issues with function min(a,b) on ESP8266 / ESP32.
#if defined (ESP8266) || defined(ESP32)
	uint16_t chars_to_write = _min(message.length(), num_chars_ - pos);
#else
	uint16_t chars_to_write = min(message.length(), num_chars_ - pos);
#endif

	for (uint16_t i = 0; i < chars_to_write; i++)
		addDisplayContent(pos + i, line, static_cast<byte>(message.c_str()[i]));
}

bool DisplayHD47780::addCustomChar(uint8_t index, uint8_t* character)
{
	if (index > 7)
		return false;

	//todo: check array length?

	//add character
	display_->createChar(index, character);

	return true;
}

bool DisplayHD47780::calculateNextPosition()
{
	curr_char_++;	//next position in the current line

	if (curr_char_ > num_chars_ - 1)
	{
		curr_line_++;		//next line
		curr_char_ = 0;	//1st character in line

		if (curr_line_ > num_lines_ - 1)
		{
			curr_line_ = 0;		//1st line
			return false;	//return false to indicate that the position has been reset
		}
	}

	return true;	//return true to indicate the next position has been calculated successfully
}

void DisplayHD47780::clear()
{
	for (uint16_t i = 0; i < num_chars_; i++)
		for (uint16_t j = 0; j < num_lines_; j++)
			addDisplayContent(i, j, ' ');
}

bool DisplayHD47780::printCustomChar(uint16_t pos, uint16_t line, byte index)
{
	//check if the feature is enabled
	if (!checkFeature(Display::FEATURE_CUSTOM_CHAR))
		return false;

	//check if a valid index was given
	if (index > 7)
		return false;

	return addDisplayContent(pos, line, index);
}

bool DisplayHD47780::addDisplayContent(uint16_t pos, uint16_t line, byte character)
{
	// return false if character position is invalid.
	if ((pos > num_chars_) || (line > num_lines_))
		return false;

	display_content_[index(pos, line)] = byte(character);

	//reset update state.
	update_state_ = DisplayHD47780::UPDATE_RESET;

	return true;
}

uint16_t DisplayHD47780::index(uint16_t pos, uint16_t line)
{
	return pos + line * num_chars_;
}

