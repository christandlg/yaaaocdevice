//RemoteCommandHandler class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "RemoteCommandHandler.h"

#include "EEPROMHandler.h"
#include "ObservingConditions.h"

#include "constants.h"

RemoteCommandHandler::RemoteCommandHandler() :
	device_(NULL),
	command_(NULL),
	value_(NULL),
	command_interface_current_(INTERFACE_SERIAL)
{
	for (uint8_t i = 0; i < NUM_COMMAND_INTERFACES; i++)
	{
		command_interfaces_[i] = NULL;
		input_buffers_[i] = "";
	}
}

RemoteCommandHandler::~RemoteCommandHandler()
{
	for (uint8_t i = 0; i < NUM_COMMAND_INTERFACES; i++)
	{
		delete command_interfaces_[i];
		input_buffers_[i] = "";		//not actually necessary
	}
}

void RemoteCommandHandler::begin()
{
	//Serial command interface - always present
	command_interfaces_[INTERFACE_SERIAL] = new CommandInterfaceSerial();
	command_interfaces_[INTERFACE_SERIAL]->begin();

	//ethernet command interface
#ifdef FEATURE_ETHERNET
#if !defined(ESP8266) && !defined(ESP32)
	bool ethernet_enabled = false;
	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::ETHERNET_ENABLED,
		0,
		ethernet_enabled);

	if (ethernet_enabled) {
		command_interfaces_[INTERFACE_ETHERNET] = new CommandInterfaceEthernet();
		command_interfaces_[INTERFACE_ETHERNET]->begin();
		input_buffers_[INTERFACE_ETHERNET] = "";
	}
#endif /* !defined(ESP8266) && !defined(ESP32) */
#endif /* FEATURE_ETHERNET */

	//WIFI command interface
#ifdef FEATURE_WIFI
#if defined(ESP8266) || defined(ESP32)
	bool wifi_enabled = false;
	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::WIFI_ENABLED,
		0,
		wifi_enabled);
	if (wifi_enabled)
	{
		YAAAWifi::begin();
		command_interfaces_[INTERFACE_WIFI] = new CommandInterfaceWifi();
		command_interfaces_[INTERFACE_WIFI]->begin();
		input_buffers_[INTERFACE_WIFI] = "";
	}
#endif /* defined(ESP8266) || defined(ESP32) */
#endif /* FEATURE_WIFI */

	//	//LoRa command interface
	//#ifdef FEATURE_LoRa
	//	bool lora_enabled = false;
	//
	//	EEPROMHandler::readValue(
	//		EEPROMHandler::device_type_::GENERAL,
	//		EEPROMHandler::general_variable_::LoRa_ENABLED,
	//		0,
	//		lora_enabled);
	//	if (lora_enabled)
	//	{
	//		command_interfaces_[INTERFACE_LoRa] = new CommandInterfaceLoRa();
	//		command_interfaces_[INTERFACE_LoRa]->begin();
	//		input_buffers_[INTERFACE_LoRa] = "";
	//	}
	//#endif /* FEATURE_LoRa */
}

void RemoteCommandHandler::run()
{
	switch (command_interface_current_)
	{
	case INTERFACE_SERIAL:
	case INTERFACE_ETHERNET:
	case INTERFACE_WIFI:
	case INTERFACE_LoRa:
		//fetch command from interface
		fetchCommand(command_interface_current_);

		//if command is complete: execute
		if (extractCommand(input_buffers_[command_interface_current_]))
			executeCommand();

		//next command interface on next iteration
		command_interface_current_++;

		break;
	default:
		//reset counter
		command_interface_current_ = INTERFACE_SERIAL;
		break;
	}
}

bool RemoteCommandHandler::isInterfaceEnabled(uint8_t interface)
{
	switch (interface)
	{
	case RemoteCommandHandler::INTERFACE_SERIAL:
	case RemoteCommandHandler::INTERFACE_ETHERNET:
	case RemoteCommandHandler::INTERFACE_WIFI:
		if (command_interfaces_[interface])
			return true;
	default:
		break;
	}

	return false;
}

//--------------------------------------------------------------------------------------------------
//private functions.

bool RemoteCommandHandler::extractCommand(String & in)
{
	//check if input string is empty
	if (in.length() == 0)
		return false;

	int16_t start = -1;
	int16_t end = -1;

	//check if input string contains a start of message character
	//check if the message protocol is YAAADEVICE
	start = in.indexOf(COMMAND_START);
	end = in.indexOf(COMMAND_END);

	//return false if no start of command character or end of command character has been received
	//or the end of command character has a smaller index than the start of command character (wtf?)
	if ((start < 0) || (end < 0) || (end < start))
		return false;

	//if the message contains an end of message character, copy the message 
	//to out. clear the in string from the beginning until the position of the 
	//end of message char (which should be empty anyway). 
	//end position character is not in the string retured by substring, so calling
	//substring with 'end + 1' is necessary
	String out = in.substring(start, end + 1);		//copy string to out
	in.remove(0, end + 1);					//clear in. 

	memcpy(input_, out.c_str(), out.length());

	return true;
}

void RemoteCommandHandler::fetchCommand(uint8_t interface)
{
	//do nothing if command interface is not initialized. 
	if (!isInterfaceEnabled(interface))
		return;

	//when a byte is in the interface's input buffer, read it into the local input buffer.
	//'<' marks the start of a new message. 
	//'>' marks the end of a message.
	//maximum message length is 128 bytes.
	while (command_interfaces_[interface]->available())
	{
		//read 1 byte from the interface
		char character = static_cast<char>(command_interfaces_[interface]->read());

		//reset input buffer if a start of command character was received. 
		if (strchr(COMMAND_START, character) != NULL)
			input_buffers_[interface] = "";		//reset input String

		input_buffers_[interface] += character;

		//exit loop if an end of command character was received. 
		if (strchr(COMMAND_END, character) != NULL)
			break;

		//reset input buffer if it is full.
		if (input_buffers_[interface].length() >= COMMAND_MAX_LENGTH)
			input_buffers_[interface] = "";
	}
}

void RemoteCommandHandler::executeCommand()
{
	//get pointer to target device identifier within input string.
	device_ = tokenize(input_);

	//get pointer to command identifier within input string.
	command_ = tokenize(NULL);

	switch (getDeviceType(device_))
	{
	case RemoteCommandHandler::GENERAL:
		generalCommand();
		break;
	case RemoteCommandHandler::OBSERVING_CONDITIONS:
		observingConditionsCommand();
		break;
	default:
		sendError(RemoteCommandHandler::ERROR_UNKNOWN_DEVICE);
		break;
	}
}

uint8_t RemoteCommandHandler::getCommandType()
{
	//tokenize input string and determine command type.
	value_ = tokenize(NULL);

	if (strcmp_P(value_, PSTR("GET")) == 0) return command_type_::GET;
	if (strcmp_P(value_, PSTR("SET")) == 0) return command_type_::SET;

	return UNKNOWN;
}

uint8_t RemoteCommandHandler::parseCommand(uint8_t device)
{
	//tokenize input string and determine command.
	//command_ = tokenize(NULL);

	if (device == RemoteCommandHandler::GENERAL)
	{
		if (command_ == NULL) return general_command_::NONE;

		if (strcmp_P(command_, PSTR("ONEWIRE_TEMP")) == 0) return general_command_::ONEWIRE_TEMP;

		if (strcmp_P(command_, PSTR("TIME")) == 0) return general_command_::TIME;

		if (strcmp_P(command_, PSTR("DATETIME")) == 0) return general_command_::DATETIME;

		if (strcmp_P(command_, PSTR("UTC_OFFSET")) == 0) return general_command_::UTC_OFFSET;

		if (strcmp_P(command_, PSTR("LOG_MODE")) == 0) return general_command_::LOG_MODE;

		if (strcmp_P(command_, PSTR("LOG_STATE")) == 0) return general_command_::LOG_STATE;

		if (strcmp_P(command_, PSTR("LOG_DUMP")) == 0) return general_command_::LOG_DUMP;

		if (strcmp_P(command_, PSTR("LOG_CLEAR")) == 0) return general_command_::LOG_CLEAR;

		if (strcmp_P(command_, PSTR("ETHERNET_IP")) == 0) return general_command_::ETHERNET_IP;

		if (strcmp_P(command_, PSTR("ETHERNET_GATEWAY")) == 0) return general_command_::ETHERNET_GATEWAY;

		if (strcmp_P(command_, PSTR("ETHERNET_DNS")) == 0) return general_command_::ETHERNET_DNS;

		if (strcmp_P(command_, PSTR("ETHERNET_SUBNET")) == 0) return general_command_::ETHERNET_SUBNET;

		if (strcmp_P(command_, PSTR("ETHERNET_SUBNET")) == 0) return general_command_::ETHERNET_PORT;

#if defined(ESP8266) || defined(ESP32)
		if (strcmp_P(command_, PSTR("WIFI_CONNECTED")) == 0) return general_command_::WIFI_CONNECTED;

		if (strcmp_P(command_, PSTR("WIFI_IP")) == 0) return general_command_::WIFI_IP;

		if (strcmp_P(command_, PSTR("WIFI_GATEWAY")) == 0) return general_command_::WIFI_GATEWAY;

		if (strcmp_P(command_, PSTR("WIFI_DNS")) == 0) return general_command_::WIFI_DNS;

		if (strcmp_P(command_, PSTR("WIFI_SUBNET")) == 0) return general_command_::WIFI_SUBNET;

		if (strcmp_P(command_, PSTR("WIFI_PORT")) == 0) return general_command_::WIFI_PORT;

		if (strcmp_P(command_, PSTR("WIFI_RSSI")) == 0) return general_command_::WIFI_RSSI;

		if (strcmp_P(command_, PSTR("OTA_ENABLED")) == 0) return general_command_::OTA_ENABLED;

		if (strcmp_P(command_, PSTR("OTA_MODE")) == 0) return general_command_::OTA_MODE;
#endif /* defined(ESP8266) || defined(ESP32) */

		if (strcmp_P(command_, PSTR("MQTT_ENABLED")) == 0) return general_command_::MQTT_ENABLED;

		if (strcmp_P(command_, PSTR("MQTT_STATE")) == 0) return general_command_::MQTT_STATE;

		if (strcmp_P(command_, PSTR("WRITE_DEFAULTS")) == 0) return general_command_::WRITE_DEFAULTS;

		if (strcmp_P(command_, PSTR("SRAM")) == 0) return general_command_::SRAM;

		if (strcmp_P(command_, PSTR("SETUP")) == 0) return general_command_::SETUP;

		if (strcmp_P(command_, PSTR("CONNECTED")) == 0) return general_command_::CONNECTED;
	}

	if (device == RemoteCommandHandler::OBSERVING_CONDITIONS)
	{
		if (command_ == NULL) return observing_conditions_command_::NONE;

		if (strcmp_P(command_, PSTR("AVERAGE_PERIOD")) == 0) return observing_conditions_command_::AVERAGE_PERIOD;

		if (strcmp_P(command_, PSTR("REFRESH")) == 0) return observing_conditions_command_::REFRESH;

		if (strcmp_P(command_, PSTR("MEASUREMENT_AGE")) == 0) return observing_conditions_command_::MEASUREMENT_AGE;

		if (strcmp_P(command_, PSTR("MEASURE")) == 0) return observing_conditions_command_::MEASURE;

		if (strcmp_P(command_, PSTR("VALUE")) == 0) return observing_conditions_command_::VALUE;

		if (strcmp_P(command_, PSTR("AVERAGE")) == 0) return observing_conditions_command_::AVERAGE;

		if (strcmp_P(command_, PSTR("SENSOR_ENABLED")) == 0) return observing_conditions_command_::SENSOR_ENABLED;

		if (strcmp_P(command_, PSTR("PERFORMS_AUTOMATIC")) == 0) return observing_conditions_command_::PERFORMS_AUTOMATIC;

		if (strcmp_P(command_, PSTR("PERFORMS_AVERAGING")) == 0) return observing_conditions_command_::PERFORMS_AVERAGING;

		if (strcmp_P(command_, PSTR("HEATER_ENABLED")) == 0) return observing_conditions_command_::HEATER_ENABLED;

		if (strcmp_P(command_, PSTR("HEATER")) == 0) return observing_conditions_command_::HEATER;

		if (strcmp_P(command_, PSTR("CONNECTED")) == 0) return observing_conditions_command_::CONNECTED;

		if (strcmp_P(command_, PSTR("WRITE_DEFAULTS")) == 0) return observing_conditions_command_::WRITE_DEFAULTS;

		if (strcmp_P(command_, PSTR("SETUP")) == 0) return observing_conditions_command_::SETUP;
	}

	return UNKNOWN;
}

char RemoteCommandHandler::getArrayDelimiter(uint16_t setup_device, uint16_t setup_option)
{
	switch (setup_device)
	{
	case EEPROMHandler::device_type_::GENERAL:
		switch (setup_option)
		{
		case EEPROMHandler::general_variable_::ETHERNET_IP:
		case EEPROMHandler::general_variable_::ETHERNET_DNS:
		case EEPROMHandler::general_variable_::ETHERNET_GATEWAY:
		case EEPROMHandler::general_variable_::ETHERNET_SUBNET:
		case EEPROMHandler::general_variable_::WIFI_IP:
		case EEPROMHandler::general_variable_::WIFI_DNS:
		case EEPROMHandler::general_variable_::WIFI_GATEWAY:
		case EEPROMHandler::general_variable_::WIFI_SUBNET:
			return '.';
		case EEPROMHandler::general_variable_::ETHERNET_MAC:
		case EEPROMHandler::general_variable_::WIFI_MAC:
			return ':';
		case EEPROMHandler::general_variable_::WIFI_SSID:
		case EEPROMHandler::general_variable_::WIFI_PASSWORD:
		case EEPROMHandler::general_variable_::WIFI_HOSTNAME:
			return '\0';
		}
		break;
	}

	return '.';	//return '.' if the setup option was unknown.
}

uint8_t RemoteCommandHandler::getArraySize(uint16_t setup_device, uint16_t setup_option)
{
	switch (setup_device)
	{
	case EEPROMHandler::device_type_::GENERAL:
		switch (setup_option)
		{
		case EEPROMHandler::general_variable_::ETHERNET_IP:
		case EEPROMHandler::general_variable_::ETHERNET_DNS:
		case EEPROMHandler::general_variable_::ETHERNET_GATEWAY:
		case EEPROMHandler::general_variable_::ETHERNET_SUBNET:
		case EEPROMHandler::general_variable_::WIFI_IP:
		case EEPROMHandler::general_variable_::WIFI_DNS:
		case EEPROMHandler::general_variable_::WIFI_GATEWAY:
		case EEPROMHandler::general_variable_::WIFI_SUBNET:
			return 4;
		case EEPROMHandler::general_variable_::ETHERNET_MAC:
		case EEPROMHandler::general_variable_::WIFI_MAC:
			return 6;
		case EEPROMHandler::general_variable_::WIFI_SSID:
			return 33;
		case EEPROMHandler::general_variable_::WIFI_PASSWORD:
		case EEPROMHandler::general_variable_::WIFI_HOSTNAME:
			return 64;
		}
		break;
	}

	return 0;	//return 0 if the setup option was unknown.
}

uint8_t RemoteCommandHandler::getDeviceType(char* chr)
{
	if (strcmp(chr, "G") == 0) return RemoteCommandHandler::GENERAL;

	if (strcmp(chr, "O") == 0) return RemoteCommandHandler::OBSERVING_CONDITIONS;

	return UNKNOWN;
}

char* RemoteCommandHandler::tokenize(char* chr)
{
	return strtok(chr, COMMAND_DELIMITERS);
}

void RemoteCommandHandler::sendError(uint16_t error_code)
{
	//responses from the arduino are expected in the following format / pattern:
	//"<D,X,0>"
	// || | ||
	// || | |end of message
	// || | return code.
	// || command identifier.
	// |device identifier.
	// beginning of message.

	String error_message = "ERROR_";
	error_message += String(error_code);

	sendResponse(error_message);
}

void RemoteCommandHandler::sendResponse(float return_value)
{
	sendResponse(device_, command_, String(return_value, 7));
}

template <class T> void RemoteCommandHandler::sendResponse(const T& return_value)
{
	sendResponse(device_, command_, String(return_value));
}

void RemoteCommandHandler::sendResponse(char* device, char* command, String return_value)
{
	//responses from the arduino are expected in the following format / pattern:
	//"<D,X,0>"
	// || | ||
	// || | |end of message
	// || | return code.
	// || command identifier.
	// |device identifier.
	// beginning of message.

	String response = "<";
	response += device;
	response += ",";
	response += command;
	response += ",";
	response += return_value;
	response += ">";

	command_interfaces_[command_interface_current_]->println(response);

	log_.log(">> " + response);
}

template <class T> void RemoteCommandHandler::sendRaw(const T& message)
{
	command_interfaces_[command_interface_current_]->print(message);
}

void RemoteCommandHandler::readOption(
	uint16_t setup_device,
	uint16_t setup_option,
	uint16_t index,
	uint8_t variable_type)
{
	//TODO replace with switch statement
	if (variable_type == EEPROMHandler::UINT8_T_)
	{
		uint8_t return_value;

		EEPROMHandler::readValue(
			setup_device,
			setup_option,
			index,
			return_value);
		sendResponse(return_value);
	}

	if (variable_type == EEPROMHandler::UINT16_T_)
	{
		uint16_t return_value;

		EEPROMHandler::readValue(
			setup_device,
			setup_option,
			index,
			return_value);
		sendResponse(return_value);
	}

	if (variable_type == EEPROMHandler::UINT32_T_)
	{
		uint32_t return_value;

		EEPROMHandler::readValue(
			setup_device,
			setup_option,
			index,
			return_value);
		sendResponse(return_value);
	}

	if (variable_type == EEPROMHandler::INT16_T_)
	{
		int16_t return_value;

		EEPROMHandler::readValue(
			setup_device,
			setup_option,
			index,
			return_value);
		sendResponse(return_value);
	}

	if (variable_type == EEPROMHandler::INT32_T_)
	{
		long return_value;

		EEPROMHandler::readValue(
			setup_device,
			setup_option,
			index,
			return_value);
		sendResponse(return_value);
	}

	if (variable_type == EEPROMHandler::FLOAT_)
	{
		float return_value;

		EEPROMHandler::readValue(
			setup_device,
			setup_option,
			index,
			return_value);
		sendResponse(return_value);
	}

	if (variable_type == EEPROMHandler::ARRAY_)
	{
		String return_value = "";

		//determine size of array to read
		uint8_t num_bytes = getArraySize(setup_device, setup_option);

		//use correct delimiter
		char delimiter = getArrayDelimiter(setup_device, setup_option);

		//allocate array and read variable from EEPROM
		byte *content = new byte[num_bytes];
		EEPROMHandler::readArray(
			setup_device,
			setup_option,
			index,
			content,
			num_bytes);

		//assemble string to send to pc
		for (uint8_t i = 0; i < num_bytes; i++)
		{
			switch (setup_option)
			{
			case EEPROMHandler::general_variable_::ETHERNET_MAC:
			case EEPROMHandler::general_variable_::WIFI_MAC:
				return_value += String(content[i], HEX);
				break;
				//case EEPROMHandler::general_variable_::WIFI_SSID:
				//case EEPROMHandler::general_variable_::WIFI_PASSWORD:
				//	return_value += content[i];
				//	break;
			default:
				return_value += String(content[i], DEC);
				break;
			}

			//if (setup_option == EEPROMHandler::general_variable_::ETHERNET_MAC)
			//{
			//	//return_value += "0x";
			//	return_value += String(content[i], HEX);
			//}
			//else
			//	return_value += String(content[i], DEC);

			if ((i < num_bytes - 1) && (delimiter != '\0'))
				return_value += delimiter;
		}

		delete content;

		sendResponse(return_value);
	}

	if (variable_type == EEPROMHandler::SENSOR_SETTINGS_)
	{
		String return_value = "";

		YAAASensor::SensorSettings sensor_settings =
		{
			DEF_SENSOR_TYPE,
			DEF_SENSOR_DEVICE,
			DEF_SENSOR_UNIT,
			DEF_SENSOR_PARAMETERS
		};

		EEPROMHandler::readValue(
			setup_device,
			setup_option,
			index,
			sensor_settings);

		return_value += sensor_settings.sensor_type_;
		return_value += " ";
		return_value += sensor_settings.sensor_device_;
		return_value += " ";
		return_value += sensor_settings.unit_;
		return_value += " ";

		//assemble string to send to pc
		for (uint8_t i = 0; i < sizeof(sensor_settings.parameters_); i++)	//SensorSettings::parameters MUST be static
		{
			return_value += String(sensor_settings.parameters_[i], HEX);

			if (i < 9)
				return_value += " ";
		}

		sendResponse(return_value);
	}
	//TODO either SENSOR_SETTINGS_ or SENSOR_CONFIGURATION_
	if (variable_type == EEPROMHandler::SENSOR_CONFIGURATION_)
	{
		String return_value = "";

		ObservingConditions::SensorConfiguration sensor_configuration =
		{
			{
				DEF_SENSOR_TYPE,
				DEF_SENSOR_DEVICE,
				DEF_SENSOR_UNIT,
				DEF_SENSOR_PARAMETERS
			},
			DEF_SENSOR_AUTOMATIC_MEAUREMENTS,
			DEF_SENSOR_AVERAGE_SAMPLES
		};

		EEPROMHandler::readValue(
			setup_device,
			setup_option,
			index,
			sensor_configuration);

		return_value += sensor_configuration.sensor_settings_.sensor_type_;
		return_value += " ";
		return_value += sensor_configuration.sensor_settings_.sensor_device_;
		return_value += " ";
		return_value += sensor_configuration.sensor_settings_.unit_;
		return_value += " ";

		//assemble string to send to pc
		for (uint8_t i = 0; i < sizeof(sensor_configuration.sensor_settings_.parameters_); i++)	//SensorSettings::parameters MUST be static
		{
			return_value += String(sensor_configuration.sensor_settings_.parameters_[i], HEX);

			return_value += " ";
		}

		return_value += sensor_configuration.automatic_measurement_interval_;
		return_value += " ";
		return_value += sensor_configuration.average_samples_;

		sendResponse(return_value);
	}

	if (variable_type == EEPROMHandler::HEATER_CONFIGURATION_)
	{
		String return_value = "";

		ObservingConditions::HeaterConfiguration heater_configuration =
		{
			{
				DEF_HEATER_DEVICE,
				DEF_HEATER_PARAMETERS
			},
			DEF_HEATER_AUTOMATIC_CONTROL_SOURCE,
			DEF_HEATER_AUTOMATIC_CONTROL_VALUE,
			DEF_HEATER_AUTOMATIC_CONTROL_INTERVAL,
			DEF_HEATER_DEW_POINT_THRESHOLD,
			DEF_HEATER_TEMPERATURE_THRESHOLD
		};

		EEPROMHandler::readValue(
			setup_device,
			setup_option,
			index,
			heater_configuration);

		return_value += heater_configuration.heater_settings_.device_;
		return_value += " ";

		for (uint8_t i = 0; i < sizeof(heater_configuration.heater_settings_.parameters_); i++)
		{
			return_value += String(heater_configuration.heater_settings_.parameters_[i], HEX);
			return_value += " ";
		}

		return_value += heater_configuration.automatic_control_source_;
		return_value += " ";
		return_value += heater_configuration.automatic_control_value_;
		return_value += " ";
		return_value += heater_configuration.automatic_update_interval_;
		return_value += " ";
		return_value += heater_configuration.dew_point_threshold_;
		return_value += " ";
		return_value += heater_configuration.temperature_threshold_;

		sendResponse(return_value);
	}

	if (variable_type == EEPROMHandler::STRING_)
	{
		String return_value = "";

		EEPROMHandler::readString(
			setup_device,
			setup_option,
			index,
			return_value);

		sendResponse(return_value.c_str());
	}

	if (variable_type == UNKNOWN) {
		sendError(RemoteCommandHandler::ERROR_UNKNOWN_OPTION);
	}
}

void RemoteCommandHandler::writeOption(
	uint16_t setup_device,
	uint16_t setup_option,
	uint16_t index,
	uint8_t variable_type)
{
	//char* string_value = tokenize(NULL);
	value_ = tokenize(NULL);

	//send an error message to the pc if the value to write could not be parsed.
	//empty strings are allowed. 
	if ((value_ == NULL) && (variable_type != EEPROMHandler::STRING_))
	{
		sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
		return;
	}

	if (variable_type == EEPROMHandler::UINT8_T_)
	{
		uint8_t setup_value = static_cast<uint8_t>(atoi(value_));

		EEPROMHandler::writeValue(
			setup_device,
			setup_option,
			index,
			setup_value);
	}

	if (variable_type == EEPROMHandler::UINT16_T_)
	{
		uint16_t setup_value = static_cast<uint16_t>(atoi(value_));

		EEPROMHandler::writeValue(
			setup_device,
			setup_option,
			index,
			setup_value);
	}

	if (variable_type == EEPROMHandler::INT16_T_)
	{
		int16_t setup_value = atoi(value_);

		EEPROMHandler::writeValue(
			setup_device,
			setup_option,
			index,
			setup_value);
	}

	if (variable_type == EEPROMHandler::INT32_T_)
	{
		long setup_value = strtol(value_, NULL, DEC);

		EEPROMHandler::writeValue(
			setup_device,
			setup_option,
			index,
			setup_value);
	}

	if (variable_type == EEPROMHandler::UINT32_T_)
	{
		uint32_t setup_value = strtoul(value_, NULL, DEC);

		EEPROMHandler::writeValue(
			setup_device,
			setup_option,
			index,
			setup_value);
	}

	if (variable_type == EEPROMHandler::FLOAT_)
	{
		float setup_value = static_cast<float>(atof(value_));

		EEPROMHandler::writeValue(
			setup_device,
			setup_option,
			index,
			setup_value);
	}

	if (variable_type == EEPROMHandler::ARRAY_)
	{
		//determine size of array to read
		uint8_t num_bytes = getArraySize(setup_device, setup_option);

		//allocate array 
		byte *content = new byte[num_bytes];

		//parse command to find bytes
		for (uint8_t i = 0; i < num_bytes; i++)
		{
			//check current substring
			if (value_ == NULL)
			{
				sendError(RemoteCommandHandler::ERROR_INVALID_PARAMETER);
				delete content;
				return;
			}

			//convert substring to byte.
			if (setup_option == EEPROMHandler::general_variable_::ETHERNET_MAC)
				content[i] = static_cast<byte>(strtoul(value_, NULL, HEX));
			else
				content[i] = static_cast<byte>(strtoul(value_, NULL, DEC));

			//read next substring
			value_ = tokenize(NULL);
		}

		//write array to EEPROM
		EEPROMHandler::writeArray(
			setup_device,
			setup_option,
			index,
			content,
			num_bytes);

		delete content;
	}

	if (variable_type == EEPROMHandler::SENSOR_SETTINGS_)
	{
		YAAASensor::SensorSettings sensor_settings =
		{
			DEF_SENSOR_TYPE,
			DEF_SENSOR_DEVICE,
			DEF_SENSOR_UNIT,
			DEF_SENSOR_PARAMETERS
		};

		//check current substring
		if (value_ == NULL)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_PARAMETER);
			return;
		}

		sensor_settings.sensor_type_ = static_cast<uint8_t>(strtoul(value_, NULL, DEC));

		//read next substring
		value_ = tokenize(NULL);

		//check current substring
		if (value_ == NULL)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_PARAMETER);
			return;
		}

		sensor_settings.sensor_device_ = static_cast<uint8_t>(strtoul(value_, NULL, DEC));

		//read next substring
		value_ = tokenize(NULL);

		//check current substring
		if (value_ == NULL)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_PARAMETER);
			return;
		}

		sensor_settings.unit_ = static_cast<uint8_t>(strtoul(value_, NULL, DEC));

		//parse sensor parameters array
		byte content[sizeof(sensor_settings.parameters_)];

		for (uint8_t i = 0; i < sizeof(content); i++)
		{
			//read next substring
			value_ = tokenize(NULL);

			//check current substring
			if (value_ == NULL)
			{
				sendError(RemoteCommandHandler::ERROR_INVALID_PARAMETER);
				return;
			}

			content[i] = static_cast<byte>(strtoul(value_, NULL, HEX));
		}

		memcpy(sensor_settings.parameters_, content, sizeof(sensor_settings.parameters_));

		//save struct
		EEPROMHandler::writeValue(
			setup_device,
			setup_option,
			index,
			sensor_settings);
	}

	if (variable_type == EEPROMHandler::SENSOR_CONFIGURATION_)
	{
		ObservingConditions::SensorConfiguration sensor_configuration =
		{
			{
				DEF_SENSOR_TYPE,
				DEF_SENSOR_DEVICE,
				DEF_SENSOR_UNIT,
				DEF_SENSOR_PARAMETERS
			},
			DEF_SENSOR_AUTOMATIC_MEAUREMENTS,
			DEF_SENSOR_AVERAGE_SAMPLES
		};

		//check current substring
		if (value_ == NULL)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_PARAMETER);
			return;
		}

		sensor_configuration.sensor_settings_.sensor_type_ = static_cast<uint8_t>(strtoul(value_, NULL, DEC));

		//read next substring
		value_ = tokenize(NULL);

		//check current substring
		if (value_ == NULL)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_PARAMETER);
			return;
		}

		sensor_configuration.sensor_settings_.sensor_device_ = static_cast<uint8_t>(strtoul(value_, NULL, DEC));

		//read next substring
		value_ = tokenize(NULL);

		//check current substring
		if (value_ == NULL)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_PARAMETER);
			return;
		}

		sensor_configuration.sensor_settings_.unit_ = static_cast<uint8_t>(strtoul(value_, NULL, DEC));

		byte content[sizeof(sensor_configuration.sensor_settings_.parameters_)];

		//parse sensor parameters array
		for (uint8_t i = 0; i < sizeof(content); i++)
		{
			//read next substring
			value_ = tokenize(NULL);

			//check current substring
			if (value_ == NULL)
			{
				sendError(RemoteCommandHandler::ERROR_INVALID_PARAMETER);
				return;
			}

			content[i] = static_cast<byte>(strtoul(value_, NULL, HEX));
		}

		memcpy(sensor_configuration.sensor_settings_.parameters_, content, sizeof(sensor_configuration.sensor_settings_.parameters_));

		//read next substring
		value_ = tokenize(NULL);

		//check current substring
		if (value_ == NULL)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_PARAMETER);
			return;
		}

		sensor_configuration.automatic_measurement_interval_ = strtoul(value_, NULL, DEC);

		//read next substring
		value_ = tokenize(NULL);

		//check current substring
		if (value_ == NULL)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_PARAMETER);
			return;
		}

		sensor_configuration.average_samples_ = static_cast<uint8_t>(strtoul(value_, NULL, DEC));

		//save struct
		EEPROMHandler::writeValue(
			setup_device,
			setup_option,
			index,
			sensor_configuration);
	}

	if (variable_type == EEPROMHandler::HEATER_CONFIGURATION_)
	{
		ObservingConditions::HeaterConfiguration heater_configuration =
		{
			{
				DEF_HEATER_DEVICE,
				DEF_HEATER_PARAMETERS
			},
			DEF_HEATER_AUTOMATIC_CONTROL_SOURCE,
			DEF_HEATER_AUTOMATIC_CONTROL_VALUE,
			DEF_HEATER_AUTOMATIC_CONTROL_INTERVAL,
			DEF_HEATER_DEW_POINT_THRESHOLD,
			DEF_HEATER_TEMPERATURE_THRESHOLD
		};

		//check current substring
		if (value_ == NULL)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_PARAMETER);
			return;
		}

		heater_configuration.heater_settings_.device_ = static_cast<uint8_t>(strtoul(value_, NULL, DEC));

		byte content[sizeof(heater_configuration.heater_settings_.parameters_)];

		//parse sensor parameters array
		for (uint8_t i = 0; i < sizeof(content); i++)
		{
			//read next substring
			value_ = tokenize(NULL);

			//check current substring
			if (value_ == NULL)
			{
				sendError(RemoteCommandHandler::ERROR_INVALID_PARAMETER);
				return;
			}

			content[i] = static_cast<byte>(strtoul(value_, NULL, HEX));
		}

		memcpy(heater_configuration.heater_settings_.parameters_, content, sizeof(heater_configuration.heater_settings_.parameters_));

		//read next substring
		value_ = tokenize(NULL);

		//check current substring
		if (value_ == NULL)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_PARAMETER);
			return;
		}

		heater_configuration.automatic_control_source_ = static_cast<uint8_t>(strtoul(value_, NULL, DEC));

		//read next substring
		value_ = tokenize(NULL);

		//check current substring
		if (value_ == NULL)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_PARAMETER);
			return;
		}

		heater_configuration.automatic_control_value_ = static_cast<uint8_t>(strtoul(value_, NULL, DEC));

		//read next substring
		value_ = tokenize(NULL);

		//check current substring
		if (value_ == NULL)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_PARAMETER);
			return;
		}

		heater_configuration.automatic_update_interval_ = strtoul(value_, NULL, DEC);

		//read next substring
		value_ = tokenize(NULL);

		//check current substring
		if (value_ == NULL)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_PARAMETER);
			return;
		}

		heater_configuration.dew_point_threshold_ = static_cast<float>(atof(value_));

		//read next substring
		value_ = tokenize(NULL);

		//check current substring
		if (value_ == NULL)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_PARAMETER);
			return;
		}

		heater_configuration.temperature_threshold_ = static_cast<float>(atof(value_));

		Serial.println(heater_configuration.heater_settings_.device_);
		for (uint8_t i = 0; i < sizeof(heater_configuration.heater_settings_.parameters_); i++)
		{
			Serial.print(heater_configuration.heater_settings_.parameters_[i]);
			Serial.print(" ");
		}
		Serial.println(heater_configuration.automatic_control_source_);
		Serial.println(heater_configuration.automatic_control_value_);
		Serial.println(heater_configuration.automatic_update_interval_);
		Serial.println(heater_configuration.dew_point_threshold_);
		Serial.println(heater_configuration.temperature_threshold_);

		//save struct
		EEPROMHandler::writeValue(
			setup_device,
			setup_option,
			index,
			heater_configuration);
	}

	if (variable_type == EEPROMHandler::STRING_)
	{
		//check if the device and option is valid for a string. 
		uint8_t max_length = EEPROMHandler::getEEPROMMaxStringLength(setup_device, setup_option);
		if (max_length == 0)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_PARAMETER);
			return;
		}

		String setup_value = (value_ == NULL ? "" : String(value_));

		//write array to EEPROM
		EEPROMHandler::writeString(
			setup_device,
			setup_option,
			index,
			setup_value);
	}

	//echo the written value to the PC.
	RemoteCommandHandler::readOption(setup_device, setup_option, index, variable_type);
}

//---------------------------------------------------------
//functions for the different devices
void RemoteCommandHandler::generalCommand()
{
	//parse telescope command.
	uint8_t command = parseCommand(RemoteCommandHandler::GENERAL);

	if (command == general_command_::NONE)
		return;	//no more commands in message.

	//send an error message when the command could not be parsed.
	if (command == UNKNOWN)
	{
		sendError(RemoteCommandHandler::ERROR_UNKNOWN_CMD);
		return;
	}

	//read command type.
	uint8_t command_type = getCommandType();

	if (command_type == UNKNOWN)
	{
		//send error message to pc.
		sendError(RemoteCommandHandler::ERROR_UNKNOWN_CMD_TYPE);
		return;
	}

	if (command == general_command_::ONEWIRE_TEMP)
	{
		if (command_type == command_type_::GET)
		{
			//parse temperature sensor number.
			char* string_number = tokenize(NULL);

			//when no sensor number has been given return the number of sensors
			if (string_number == NULL)
				sendResponse(SensorTemperatureOneWire::getNumSensors());

			else
			{
				uint8_t number = static_cast<uint8_t>(atoi(string_number));
				uint8_t addr[8];

				//read serial number and send it to the pc.
				if (SensorTemperatureOneWire::getSensorSerial(number, addr))
				{
					//convert array to string
					String address = "";

					for (uint8_t i = 0; i < 8; i++)
					{
						address += String(addr[i], HEX);

						if (i < 7)
							address += ' ';
					}

					//send serial number to pc
					sendResponse(address);
				}
				else
					sendResponse(false);
			}
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}

		return;
	}

	//TIME command received
	if (command == general_command_::TIME)
	{
		if (command_type == command_type_::GET)
		{
			sendResponse(YAAATime::getTime());
		}

		if (command_type == command_type_::SET)
		{
			time_t time = 0;

			char* string_time = tokenize(NULL);

			if (string_time == NULL)
			{
				sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
				return;
			}

			time = static_cast<time_t>(strtoul(string_time, NULL, DEC));

			YAAATime::setDateTime(time);

			sendResponse(YAAATime::getTime());
		}

		return;
	}

	//DATETIME command received
	if (command == general_command_::DATETIME)
	{
		if (command_type == command_type_::GET)
		{
			char *response = new char[20];
			YAAATime::timeToISO8601(response);
			sendResponse(response);
			delete response;
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}

		return;
	}

	//UTC_OFFSET command received
	if (command == general_command_::UTC_OFFSET)
	{
		if (command_type == command_type_::GET)
		{
			sendResponse(YAAATime::getUTCOffset());
		}

		if (command_type == command_type_::SET)
		{
			char* string_offset = tokenize(NULL);

			if (string_offset == NULL)
			{
				sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
				return;
			}

			sendResponse(YAAATime::setUTCOffset(atof(string_offset)));
		}

		return;
	}

	//LOG_MODE command received
	if (command == general_command_::LOG_MODE)
	{
		if (command_type == command_type_::GET)
		{
			sendResponse(log_.getLogMode());
		}

		if (command_type == command_type_::SET)
		{
			// parse requested log state.
			char* string_log = tokenize(NULL);

			if (string_log == NULL)
			{
				//send error message to pc.
				sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
				return;
			}

			bool log = static_cast<bool>(atoi(string_log));

			sendResponse(log_.setLogMode(log));
		}

		return;
	}

	//LOG_STATE command received
	if (command == general_command_::LOG_STATE)
	{
		if (command_type == command_type_::GET)
		{
			sendResponse(log_.getLogState());
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}

		return;
	}

	//LOG_DUMP command received
	if (command == general_command_::LOG_DUMP)
	{
		if (command_type == command_type_::GET)
		{
			//send false if the YAAALog class was not initialized.
			if (log_.getLogState() <= YAAALog::LOG_NOT_INITIALIZED)
			{
				sendResponse(false);
				return;
			}

			log_.startDump();

			while (log_.hasLine())
				sendRaw(log_.getLine());

			log_.stopDump();

			sendResponse(true);
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}

		return;
	}

	//LOG_CLEAR command received
	if (command == general_command_::LOG_CLEAR)
	{
		if (command_type == command_type_::GET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}

		if (command_type == command_type_::SET)
		{
			sendResponse(log_.clearLog());
		}

		return;
	}

	////SOFTWARE_VERSION command 
	//if (command == general_command_::SOFTWARE_VERSION)
	//{
	//	if (command_type == command_type_::GET)
	//	{
	//		sendResponse(YAAAVERSION);
	//	}
	//
	//	if (command_type == command_type_::SET)
	//	{
	//		sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
	//	}
	//
	//	return;
	//}
	//
	////SOFTWARE_DATETIME command 
	//if (command == general_command_::SOFTWARE_DATETIME)
	//{
	//	if (command_type == command_type_::GET)
	//	{
	//		sendResponse(YAAADATETIME);
	//	}
	//
	//	if (command_type == command_type_::SET)
	//	{
	//		sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
	//	}
	//
	//	return;
	//}
	//
	////DEVICE_NAME command 
	//if (command == general_command_::DEVICE_NAME)
	//{
	//	if (command_type == command_type_::GET)
	//	{
	//		sendResponse(DEVICE_NAME);
	//	}
	//
	//	if (command_type == command_type_::SET)
	//	{
	//		sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
	//	}
	//
	//	return;
	//}
	//
	////DEVICE_INFO command 
	//if (command == general_command_::DEVICE_INFO)
	//{
	//	if (command_type == command_type_::GET)
	//	{
	//		sendResponse(DEVICE_INFO);
	//	}
	//
	//	if (command_type == command_type_::SET)
	//	{
	//		sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
	//	}
	//
	//	return;
	//}

	if (command == general_command_::WRITE_DEFAULTS)
	{
		if (command_type == command_type_::GET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}

		if (command_type == command_type_::SET)
		{
			writeAllDefaults();
			sendResponse(true);
		}
		return;
	}

	//SETUP command received.
	if (command == general_command_::SETUP)
	{
		generalSetup(command_type);
		return;
	}

	if (command == general_command_::CONNECTED)
	{
		if (command_type == command_type_::GET)
		{
			sendResponse(true);
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}
		return;
	}

#if !defined(ESP8266) && !defined(ESP32)
	if (command == general_command_::ETHERNET_IP)
	{
		if (command_type == command_type_::GET)
		{
			if (!command_interfaces_[INTERFACE_ETHERNET])
				sendError(RemoteCommandHandler::ERROR_DEVICE_UNAVAILABLE);
			else
				sendResponse(command_interfaces_[INTERFACE_ETHERNET]->getConfig(CommandInterfaceEthernet::CONFIG_IP));
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}
		return;
	}

	if (command == general_command_::ETHERNET_DNS)
	{
		if (command_type == command_type_::GET)
		{
			if (!command_interfaces_[INTERFACE_ETHERNET])
				sendError(RemoteCommandHandler::ERROR_DEVICE_UNAVAILABLE);
			else
				sendResponse(command_interfaces_[INTERFACE_ETHERNET]->getConfig(CommandInterfaceEthernet::CONFIG_DNS));
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}
		return;
	}

	if (command == general_command_::ETHERNET_GATEWAY)
	{
		if (command_type == command_type_::GET)
		{
			if (!command_interfaces_[INTERFACE_ETHERNET])
				sendError(RemoteCommandHandler::ERROR_DEVICE_UNAVAILABLE);
			else
				sendResponse(command_interfaces_[INTERFACE_ETHERNET]->getConfig(CommandInterfaceEthernet::CONFIG_GATEWAY));
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}
		return;
	}

	if (command == general_command_::ETHERNET_SUBNET)
	{
		if (command_type == command_type_::GET)
		{
			if (!command_interfaces_[INTERFACE_ETHERNET])
				sendError(RemoteCommandHandler::ERROR_DEVICE_UNAVAILABLE);
			else
				sendResponse(command_interfaces_[INTERFACE_ETHERNET]->getConfig(CommandInterfaceEthernet::CONFIG_SUBNET));
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}
		return;
	}

	if (command == general_command_::ETHERNET_PORT)
	{
		if (command_type == command_type_::GET)
		{
			if (!command_interfaces_[INTERFACE_ETHERNET])
				sendError(RemoteCommandHandler::ERROR_DEVICE_UNAVAILABLE);
			else
				sendResponse(command_interfaces_[INTERFACE_ETHERNET]->getConfig(CommandInterfaceEthernet::CONFIG_PORT));
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}
		return;
	}
#endif /* !defined(ESP8266) && !defined(ESP32) */

#if defined(ESP8266) || defined(ESP32)
	if (command == general_command_::WIFI_CONNECTED)
	{
		if (command_type == command_type_::GET)
		{
			if (!command_interfaces_[INTERFACE_WIFI])
				sendError(RemoteCommandHandler::ERROR_DEVICE_UNAVAILABLE);
			else
				sendResponse(YAAAWifi::getConnected());
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}
		return;
	}

	if (command == general_command_::WIFI_IP)
	{
		if (command_type == command_type_::GET)
		{
			if (!command_interfaces_[INTERFACE_WIFI])
				sendError(RemoteCommandHandler::ERROR_DEVICE_UNAVAILABLE);
			else
				sendResponse(command_interfaces_[INTERFACE_WIFI]->getConfig(CommandInterfaceWifi::CONFIG_IP));
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}
		return;
	}

	if (command == general_command_::WIFI_DNS)
	{
		if (command_type == command_type_::GET)
		{
			if (!command_interfaces_[INTERFACE_WIFI])
				sendError(RemoteCommandHandler::ERROR_DEVICE_UNAVAILABLE);
			else
				sendResponse(command_interfaces_[INTERFACE_WIFI]->getConfig(CommandInterfaceWifi::CONFIG_DNS));
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}
		return;
	}

	if (command == general_command_::WIFI_GATEWAY)
	{
		if (command_type == command_type_::GET)
		{
			if (!command_interfaces_[INTERFACE_WIFI])
				sendError(RemoteCommandHandler::ERROR_DEVICE_UNAVAILABLE);
			else
				sendResponse(command_interfaces_[INTERFACE_WIFI]->getConfig(CommandInterfaceWifi::CONFIG_GATEWAY));
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}
		return;
	}

	if (command == general_command_::WIFI_SUBNET)
	{
		if (command_type == command_type_::GET)
		{
			if (!command_interfaces_[INTERFACE_WIFI])
				sendError(RemoteCommandHandler::ERROR_DEVICE_UNAVAILABLE);
			else
				sendResponse(command_interfaces_[INTERFACE_WIFI]->getConfig(CommandInterfaceWifi::CONFIG_SUBNET));
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}
		return;
	}

	if (command == general_command_::WIFI_PORT)
	{
		if (command_type == command_type_::GET)
		{
			if (!command_interfaces_[INTERFACE_WIFI])
				sendError(RemoteCommandHandler::ERROR_DEVICE_UNAVAILABLE);
			else
				sendResponse(command_interfaces_[INTERFACE_WIFI]->getConfig(CommandInterfaceWifi::CONFIG_PORT));
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}
		return;
	}

	if (command == general_command_::WIFI_RSSI)
	{
		if (command_type == command_type_::GET)
		{
			if (!command_interfaces_[INTERFACE_WIFI])
				sendError(RemoteCommandHandler::ERROR_DEVICE_UNAVAILABLE);
			else
				sendResponse(command_interfaces_[INTERFACE_WIFI]->getConfig(CommandInterfaceWifi::CONFIG_RSSI));
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}
		return;
	}

#ifdef FEATURE_OTA
	if (command == general_command_::OTA_ENABLED)
	{
		if (command_type == command_type_::GET)
		{
			sendResponse(OTAHandler::getOTAMode() != 0);
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}
		return;
	}

	if (command == general_command_::OTA_MODE)
	{
		if (command_type == command_type_::GET)
		{
			sendResponse(OTAHandler::getOTAMode());
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}
		return;
	}
#endif /* FEATURE_OTA */

	if (command == general_command_::MQTT_ENABLED)
	{
		if (command_type == command_type_::GET)
		{
			//sendResponse(OTAHandler::getOTAMode());
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}
		return;
	}

	if (command == general_command_::MQTT_STATE)
	{
		if (command_type == command_type_::GET)
		{
			//sendResponse(OTAHandler::getOTAMode());
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}
		return;
	}
#endif /* defined(ESP8266) || defined(ESP32) */
}

void RemoteCommandHandler::generalSetup(uint8_t command_type)
{
	uint16_t setup_device = EEPROMHandler::device_type_::GENERAL;
	uint16_t setup_option = 0;
	uint8_t index = 0;
	uint8_t variable_type = 0;

	//parse option to get or set:
	//setup_option = getOption(setup_device);
	char* string_option = tokenize(NULL);
	setup_option = EEPROMHandler::stringToVariable(setup_device, string_option);

	//send error response if the setup option was not recognized.
	if (setup_option == UNKNOWN_OPTION)
	{
		sendError(RemoteCommandHandler::ERROR_UNKNOWN_OPTION);
		return;
	}

	//get variable size of the variable to get or set:
	variable_type = EEPROMHandler::getVariableType(setup_device, setup_option);

	if (command_type == command_type_::GET)
	{
		readOption(setup_device, setup_option, index, variable_type);
	}

	if (command_type == command_type_::SET)
	{
		writeOption(setup_device, setup_option, index, variable_type);
	}
}

void RemoteCommandHandler::observingConditionsCommand()
{
	//parse telescope command.
	uint8_t command = parseCommand(RemoteCommandHandler::OBSERVING_CONDITIONS);

	if (command == observing_conditions_command_::NONE)		//no more commands in message.
		return;

	//send an error message when the command could not be parsed.
	if (command == UNKNOWN)
	{
		sendError(RemoteCommandHandler::ERROR_UNKNOWN_CMD);
		return;
	}

	//read command type.
	uint8_t command_type = getCommandType();

	if (command_type == UNKNOWN)
	{
		//send error message to pc.
		sendError(RemoteCommandHandler::ERROR_UNKNOWN_CMD_TYPE);
		return;
	}

	//CONNECTED GET command received
	if (command == observing_conditions_command_::CONNECTED)
	{
		if (command_type == command_type_::GET)
		{
			sendResponse(true);
		}
		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}

		return;
	}

	//CONNECTED SET command received
	if (command == observing_conditions_command_::CONNECTED)
	{
		if (command_type == command_type_::SET)
		{
			//send an error message to the pc when the command type is invalid for this command.
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}

		return;
	}

	if (command == observing_conditions_command_::SETUP)
	{
		observingConditionsSetup(command_type);
		return;
	}

	if (command == observing_conditions_command_::AVERAGE_PERIOD)
	{
		if (command_type == command_type_::GET)
		{
			sendResponse(observing_conditions_.getAveragePeriod());
		}

		if (command_type == command_type_::SET)
		{
			//parse period.
			char* string_period = tokenize(NULL);

			//send error messag to pc when string could not be tokenized.
			if (string_period == NULL)
			{
				sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
				return;
			}

			uint32_t period = strtol(string_period, NULL, DEC);

			observing_conditions_.setAveragePeriod(period);

			sendResponse(true);
		}

		return;
	}

	if (command == observing_conditions_command_::REFRESH)
	{
		if (command_type == command_type_::GET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}

		if (command_type == command_type_::SET)
		{
			sendResponse(observing_conditions_.forceRefresh());
		}

		return;
	}

	if (command == observing_conditions_command_::MEASUREMENT_AGE)
	{
		if (command_type == command_type_::GET)
		{
			uint8_t sensor = ObservingConditions::CONDITION_UNKNOWN;

			//parse alignment.
			char* string_sensor = tokenize(NULL);

			//parse sensor if sensor has been given
			if (string_sensor != NULL)
			{
				sensor = static_cast<uint8_t>(strtol(string_sensor, NULL, DEC));
			}

			sendResponse(observing_conditions_.getMeasurementAge(sensor));
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}

		return;
	}

	if (command == observing_conditions_command_::MEASURE)
	{
		if (command_type == command_type_::GET)
		{
			uint8_t sensor = ObservingConditions::CONDITION_UNKNOWN;

			//parse alignment.
			char* string_sensor = tokenize(NULL);

			//parse sensor if sensor has been given
			if (string_sensor == NULL)
			{
				sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
				return;
			}

			sensor = static_cast<uint8_t>(strtol(string_sensor, NULL, DEC));

			sendResponse(observing_conditions_.hasValue(sensor));
		}

		if (command_type == command_type_::SET)
		{
			uint8_t sensor = ObservingConditions::CONDITION_UNKNOWN;

			//parse alignment.
			char* string_sensor = tokenize(NULL);

			//parse sensor if sensor has been given
			if (string_sensor == NULL)
			{
				sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
				return;
			}

			sensor = static_cast<uint8_t>(strtol(string_sensor, NULL, DEC));

			sendResponse(observing_conditions_.measure(sensor));
		}

		return;
	}


	if (command == observing_conditions_command_::VALUE)
	{
		if (command_type == command_type_::GET)
		{
			uint8_t sensor = ObservingConditions::CONDITION_UNKNOWN;

			//parse alignment.
			char* string_sensor = tokenize(NULL);

			//parse sensor if sensor has been given
			if (string_sensor == NULL)
			{
				sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
				return;
			}

			sensor = static_cast<uint8_t>(strtol(string_sensor, NULL, DEC));

			sendResponse(observing_conditions_.getValue(sensor));
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}

		return;
	}

	if (command == observing_conditions_command_::AVERAGE)
	{
		if (command_type == command_type_::GET)
		{
			uint8_t sensor = ObservingConditions::CONDITION_UNKNOWN;

			//parse alignment.
			char* string_sensor = tokenize(NULL);

			//parse sensor if sensor has been given
			if (string_sensor == NULL)
			{
				sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
				return;
			}

			sensor = static_cast<uint8_t>(strtol(string_sensor, NULL, DEC));

			sendResponse(observing_conditions_.getAverage(sensor));
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}

		return;
	}

	if (command == observing_conditions_command_::SENSOR_ENABLED)
	{
		if (command_type == command_type_::GET)
		{
			uint8_t sensor = ObservingConditions::CONDITION_UNKNOWN;

			//parse alignment.
			char* string_sensor = tokenize(NULL);

			//parse sensor if sensor has been given
			if (string_sensor == NULL)
			{
				sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
				return;
			}

			sensor = static_cast<uint8_t>(strtol(string_sensor, NULL, DEC));

			sendResponse(observing_conditions_.isSensorEnabled(sensor));
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}

		return;
	}

	if (command == observing_conditions_command_::PERFORMS_AUTOMATIC)
	{
		if (command_type == command_type_::GET)
		{
			uint8_t sensor = ObservingConditions::CONDITION_UNKNOWN;

			//parse alignment.
			char* string_sensor = tokenize(NULL);

			//parse sensor if sensor has been given
			if (string_sensor == NULL)
			{
				sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
				return;
			}

			sensor = static_cast<uint8_t>(strtol(string_sensor, NULL, DEC));

			sendResponse(observing_conditions_.performsAutomaticMeasurements(sensor));
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}

		return;
	}

	if (command == observing_conditions_command_::PERFORMS_AVERAGING)
	{
		if (command_type == command_type_::GET)
		{
			uint8_t sensor = ObservingConditions::CONDITION_UNKNOWN;

			//parse alignment.
			char* string_sensor = tokenize(NULL);

			//parse sensor if sensor has been given
			if (string_sensor == NULL)
			{
				sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
				return;
			}

			sensor = static_cast<uint8_t>(strtol(string_sensor, NULL, DEC));

			sendResponse(observing_conditions_.performsAveraging(sensor));
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}

		return;
	}

	if (command == observing_conditions_command_::HEATER_ENABLED)
	{
		if (command_type == command_type_::GET)
		{
			uint8_t heater = 255;

			//parse alignment.
			char* string_heater = tokenize(NULL);

			//parse sensor if sensor has been given
			if (string_heater == NULL)
			{
				sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
				return;
			}

			heater = static_cast<uint8_t>(strtol(string_heater, NULL, DEC));

			sendResponse(observing_conditions_.isHeaterEnabled(heater));
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}

		return;
	}

	if (command == observing_conditions_command_::HEATER)
	{
		if (command_type == command_type_::GET)
		{
			uint8_t heater = 255;

			//parse alignment.
			char* string_heater = tokenize(NULL);

			//parse sensor if sensor has been given
			if (string_heater == NULL)
			{
				sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
				return;
			}

			heater = static_cast<uint8_t>(strtol(string_heater, NULL, DEC));

			sendResponse(observing_conditions_.getHeaterValue(heater));
		}

		if (command_type == command_type_::SET)
		{
			uint8_t heater = 255;
			uint8_t setting = 0;

			//parse alignment.
			char* string_heater = tokenize(NULL);
			char* string_setting = tokenize(NULL);

			//parse sensor if sensor has been given
			if ((string_heater == NULL) || (string_setting == NULL))
			{
				sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
				return;
			}

			heater = static_cast<uint8_t>(strtol(string_heater, NULL, DEC));
			setting = static_cast<uint8_t>(strtol(string_setting, NULL, DEC));

			sendResponse(observing_conditions_.setHeaterValue(heater, setting));
		}

		return;
	}

	if (command == observing_conditions_command_::WRITE_DEFAULTS)
	{
		if (command_type == command_type_::GET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}

		if (command_type == command_type_::SET)
		{
			ObservingConditions::writeDefaults();
			sendResponse(0);
		}

		return;
	}
}

void RemoteCommandHandler::observingConditionsSetup(uint8_t command_type)
{
	uint16_t setup_device = EEPROMHandler::device_type_::OBSERVING_CONDITIONS;
	uint16_t setup_option = 0;
	uint8_t index = 0;
	uint8_t variable_type = 0;

	char* string_option = tokenize(NULL);
	setup_option = EEPROMHandler::stringToVariable(setup_device, string_option);

	//if the option is not a (general) telescope option, check if it is a 
	//telescope axis option.
	if (setup_option == UNKNOWN_OPTION)
	{
		sendError(RemoteCommandHandler::ERROR_UNKNOWN_OPTION);
		return;
	}

	//get variable size of the variable to be get or set:
	//variable_type = getVariableType(setup_device, setup_option);
	variable_type = EEPROMHandler::getVariableType(setup_device, setup_option);

	if (variable_type == UNKNOWN)
		sendError(RemoteCommandHandler::RemoteCommandHandler::ERROR_INVALID_PARAMETER);

	//extract sensor number when reading or writing sensor or heater configuration.
	if ((variable_type == EEPROMHandler::SENSOR_CONFIGURATION_) ||
		(variable_type == EEPROMHandler::HEATER_CONFIGURATION_))
	{
		//parse index.
		char* string_index = tokenize(NULL);

		if (string_index == NULL)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_PARAMETER);
			return;
		}

		index = static_cast<uint8_t>(atoi(string_index));
	}

	if (command_type == command_type_::GET)
	{
		readOption(setup_device, setup_option, index, variable_type);
	}

	if (command_type == command_type_::SET)
	{
		writeOption(setup_device, setup_option, index, variable_type);
	}
}

void RemoteCommandHandler::writeDefaults()
{
	CommandInterfaceSerial::writeDefaults();

#if !defined(ESP8266) && !defined(ESP32)
	CommandInterfaceEthernet::writeDefaults();
#endif /* !defined(ESP8266) && !defined(ESP32) */

#if defined(ESP8266) || defined(ESP32)
	CommandInterfaceWifi::writeDefaults();
#endif /* defined(ESP8266) || defined(ESP32) */
}

RemoteCommandHandler remote_command_handler_;
