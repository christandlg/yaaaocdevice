//Wind Speed sensor class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "SensorWindSpeed.h"

SensorWindSpeed::SensorWindSpeed(YAAASensor::SensorSettings sensor_settings) :
	YAAASensor(sensor_settings),
	speed_unit_(DEF_SENSOR_WIND_SPEED_UNIT)
{
	setUnit(sensor_settings.unit_);
}


SensorWindSpeed::~SensorWindSpeed()
{
	//nothing to do here...
}

uint8_t SensorWindSpeed::getSensorType()
{
	return YAAASensor::SENSOR_WIND_SPEED;
}

uint8_t SensorWindSpeed::getUnit()
{
	return speed_unit_;
}

bool SensorWindSpeed::setUnit(uint8_t unit)
{
	switch (unit)
	{
		case SensorWindSpeed::SPEED_METERS_PER_SECOND:
		case SensorWindSpeed::SPEED_KM_PER_HOUR:
		case SensorWindSpeed::SPEED_FEET_PER_SECOND:
		case SensorWindSpeed::SPEED_MILES_PER_HOUR:
		case SensorWindSpeed::SPEED_KNOTS:
			speed_unit_ = unit;
			return true;
		default:
			break;
	}

	return false;
}

float SensorWindSpeed::convertSpeed(float speed, uint8_t unit_src, uint8_t unit_dst)
{
	if (unit_src == unit_dst)
		return speed;

	switch (unit_src)
	{
		case SensorWindSpeed::SPEED_KM_PER_HOUR:
			speed *= 1000.0f / 3600.0f;
			break;
		case SensorWindSpeed::SPEED_FEET_PER_SECOND:
			speed /= 0.3048f;
			break;
		case SensorWindSpeed::SPEED_MILES_PER_HOUR:
			speed *= 1609.344f / 3600.0f;
			break;
		case SensorWindSpeed::SPEED_KNOTS:
			speed *= 1852.0f / 3600.0f;
			break;
		default:
			break;
	}

	switch (unit_dst)
	{
		case SensorWindSpeed::SPEED_KM_PER_HOUR:
			speed *= 3600.0f / 1000.0f;
			break;
		case SensorWindSpeed::SPEED_FEET_PER_SECOND:
			speed *= 0.3048f;
			break;
		case SensorWindSpeed::SPEED_MILES_PER_HOUR:
			speed *= 3600.0f / 1609.344f;
			break;
		case SensorWindSpeed::SPEED_KNOTS:
			speed *= 3600.0f / 1852.0f;
			break;
		default:
			break;
	}

	return speed;
}
