//BME280 Humidity sensor class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "SensorHumidityBME280.h"

SensorHumidityBME280::SensorHumidityBME280(YAAASensor::SensorSettings sensor_settings, void *sensor):
	SensorHumidity(sensor_settings),
	sensor_(NULL)
{
	if (sensor)
		sensor_ = static_cast<BMx280MI*>(sensor);
	else
	{
		switch (sensor_settings.parameters_[0])
		{
		case SensorHumidityBME280::INTERFACE_I2C:
			sensor_ = new BMx280I2C(sensor_settings.parameters_[1]);
			break;
		case SensorHumidityBME280::INTERFACE_SPI:
			sensor_ = new BMx280SPI(sensor_settings.parameters_[1]);
			break;
		}
	}
}

SensorHumidityBME280::~SensorHumidityBME280()
{
	if (sensor_)
		delete sensor_;

	sensor_ = NULL;
}

bool SensorHumidityBME280::begin()
{
	if (!sensor_)
		return false;

	if (!sensor_->begin())
		return false;

	sensor_->writeOversamplingHumidity(BMx280MI::OSRS_H_x16);

	return true;
}

bool SensorHumidityBME280::measure()
{
	if (!sensor_)
		return false;

	return sensor_->measure();
}

bool SensorHumidityBME280::hasValue()
{
	if (!sensor_)
		return false;

	return sensor_->hasValue();
}

float SensorHumidityBME280::getValue()
{
	if (!sensor_)
		return NAN;

	return sensor_->getHumidity();
}
