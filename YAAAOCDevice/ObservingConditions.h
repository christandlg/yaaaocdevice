//ObservingConditions class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef OBSERVING_CONDITIONS_H_
#define OBSERVING_CONDITIONS_H_

#include <Arduino.h>

#include "EEPROMHandler.h"

#include "YAAALog.h"

#include "YAAASensor.h"

#include "SensorBrightness.h"
#include "SensorBrightnessBH1750.h"

#include "SensorDewPoint.h"

#include "SensorHumidity.h"
#include "SensorHumidityDHT.h"
#include "SensorHumidityHTU21.h"
#include "SensorHumidityBME280.h"

#include "SensorLightning.h"
#include "SensorLightningAS3935.h"

#include "SensorPressure.h"
#include "SensorPressureBMP180.h"
#include "SensorPressureBMx280.h"

#include "SensorRain.h"
#include "SensorRainFC37.h"

#include "SensorRainRate.h"
#include "SensorRainRateTippingBucket.h"

#include "SensorWindDirection.h"
#include "SensorWindDirectionPotentiometer.h"
#include "SensorWindDirectionSwitchResistor.h"

#include "SensorWindSpeed.h"
#include "SensorWindSpeedAnalogVoltage.h"
#include "SensorWindSpeedImpulse.h"

#include "SensorTemperature.h"
#include "SensorTemperatureDHT.h"
#include "SensorTemperatureOneWire.h"
#include "SensorTemperatureThermistor.h"
#include "SensorTemperatureAnalogIC.h"
#include "SensorTemperatureMLX90614.h"
#include "SensorTemperatureHTU21.h"
#include "SensorTemperatureBMP180.h"
#include "SensorTemperatureBMx280.h"

#include "YAAAHeater.h"
#include "HeaterArduinoPin.h"

extern YAAALog log_;

class ObservingConditions
{
public:	
	struct HeaterConfiguration
	{
		YAAAHeater::HeaterSettings heater_settings_;
		uint8_t automatic_control_source_;		//combination of flags from automatic_control_source_t. set to automatic_control_source_t::AUTOMATIC_DISABLED to disable.
		uint8_t automatic_control_value_;		//control value to use in automatic control. 0 = disabled, 255 = enabled (digital), 1-254 = enabled (PWM)
		uint32_t automatic_update_interval_;	//update interval in ms.
		float dew_point_threshold_;				//dew point threshold (relative to ambient temperature) for automatic control. 
		float temperature_threshold_;			//temperature threshold for automatic control. 
	};

	struct SensorConfiguration
	{
		YAAASensor::SensorSettings sensor_settings_;

		uint16_t automatic_measurement_interval_;		//automatic measurement interval, in seconds.
		uint8_t average_samples_;						//number of samples for averaging
	};

	//automatic control source for heater
	enum heater_automatic_control_source_t : uint8_t
	{
		AUTOMATIC_DISABLED = 0,			//automatic control is disabled.
		AUTOMATIC_RAIN = 1,				//heater is controlled by rain sensor (not rain rate sensor)
		AUTOMATIC_TEMPERATURE = 2,		//heater is controlled by temperature sensor 
		AUTOMATIC_DEW_POINT = 4,		//heater is controlled by dew point sensor / calculation
	};
	
	enum condition_t : uint8_t
	{
		CONDITION_CLOUD_COVER = 0,
		CONDITION_DEW_POINT = 1,
		CONDITION_HUMIDITY = 2,
		CONDITION_PRESSURE = 3,
		CONDITION_RAIN = 4,
		CONDITION_RAIN_RATE = 5,
		CONDITION_SKY_BRIGHTNESS = 6,
		CONDITION_SKY_QUALITY = 7,
		CONDITION_SKY_TEMPERATURE = 8,
		CONDITION_STAR_FWHM = 9,
		CONDITION_TEMPERATURE = 10,
		CONDITION_WIND_DIRECTION = 11,
		CONDITION_WIND_GUST = 12,
		CONDITION_WIND_SPEED = 13,
		CONDITION_LIGHTNING = 14,
		CONDITION_UNKNOWN = 255,
	};

	enum dew_point_calc_algorithm_t : uint8_t
	{
		ALGORITHM_SIMPLE = 0,		//simple estimation
		ALGORITHM_MAGNUS = 1,		//magnus estimation
		//ALGORITHM_NOAA = 2		//NOAA calculation
	};

	static const uint8_t NUM_CONDITIONS = 15;

	static const uint8_t NUM_HEATERS = 3;

	ObservingConditions();

	~ObservingConditions();

	/*
	initializes the ObservingConditions class. 
	@return true on success, false otherwise. */
	bool begin();

	/*
	worker function of ObservingConditions instance. Call this frequenty. */
	void run();

	/*
	forces the sensors to refresh. 
	@return true if measurements could be started on all enabled sensors, false otherwise. */
	bool forceRefresh();

	/*
	@param sensor as sensor_type_t
	@return true if averaging is enabled on this sensor, false otherwise or if the given sensor is
	invalid or not enabled. */
	bool performsAutomaticMeasurements(uint8_t sensor);

	/*
	@param sensor as sensor_type_t
	@return true if averaging is enabled on this sensor, false otherwise or if the given sensor is 
	invalid or not enabled. */
	bool performsAveraging(uint8_t sensor);

	/*
	@return average period, in ms. */
	uint32_t getAveragePeriod();

	/*
	@param desired average period for sensors (in ms).
	@return true on success, false otherwise. */
	void setAveragePeriod(uint32_t period);

	/*
	starts a measurement on the given sensor. 
	@param sensor: sensor as ObservingConditions::sensors_t 
	@return: true if measurement was started successfully, false otherwise. */
	bool measure(uint8_t sensor);

	/*
	checks if a measurement has been completed by the given sensor. 
	@param sensor: sensor as ObservingConditions::sensors_t 
	@return: true if measurement is completed, false otherwise.*/
	bool hasValue(uint8_t sensor);

	/*
	@param sensor: sensor as ObservingConditions::sensors_t 
	@return value in the unit read from EEPROM or NAN if it fails. */
	float getValue(uint8_t sensor);

	/*
	@param sensor as ObservingConditions::sensors_t 
	@return average value recorded by this sensor.returns - 999.0f if the sensor is 
	not available or averaging is disabled on this sensor. */
	float getAverage(uint8_t sensor);

	/*
	@param sensor as YAAASensor::sensor_type_t.	
	@return measurement age in ms or 0xFFFFFFFF if not available. */
	uint32_t getMeasurementAge(uint8_t sensor = ObservingConditions::CONDITION_UNKNOWN);

	/*
	checks if the given sensor exists.
	@param sensor as ObservingConditions::sensors_t 
	@return true if the sensor has been successfully created, false otherwise. */
	bool isSensorEnabled(uint8_t sensor);

	/*
	@return: true if the heater is enabled, false otherwise. */
	bool isHeaterEnabled(uint8_t heater);

	/*
	@param heater index (0 - NUM_HEATERS)
	@return current value heater is set to or 0 if the heater is not enabled. */
	uint8_t getHeaterValue(uint8_t heater);

	/*
	@param heater index (0 - NUM_HEATERS)
	@param value new heater value 
	@return true on success, false otherwise */
	bool setHeaterValue(uint8_t heater, uint8_t value);

	/*
	@return the log mode of the ObservingCondition's logger. returns false if the logger is not available. */
	bool getLogMode();

	/*
	@param desired log state
	@return true if setting the log state was successful, false otherwise. */
	bool setLogMode(bool enabled);

	/*
	@param condition
	@param short_name
	@return name as string */
	static String getConditionName(uint8_t condition, bool short_name = false);

	/*
	writes default values to the Arduino's EEPROM. */
	static void writeDefaults();

private:
	struct AutomaticMeasurementConfiguration
	{
		uint32_t am_start_interval_;			//time between consecutive starts of automatic measurements (in ms). setting this too low may lead to measurements being interrupted. set to 0 to disable automatic measurements. 
		uint32_t am_start_last_;				//time of last measurement start (in ms).
		uint32_t am_check_interval_;			//time between consecutive completion checks during automatic measurement (in ms). 
		uint32_t am_check_last_;				//time of last completion check during automatic measurement (in ms). 
		uint8_t am_state_;						//current state of automatic measurement as automatic_measurement_state_t.

		uint8_t average_counter_;				//counter for array indexing
		uint8_t average_samples_;				//number of samples to average per averaging interval. set to 0 to disable averaging.
		uint32_t average_collect_last_;
		float *average_array_;					//array containing values
	};

	enum automatic_measurement_state_t : uint8_t
	{
		STATE_IDLE,
		STATE_MEASUREMENT_RUNNING,
		STATE_MEASUREMENT_COMPLETE
	};

	/*
	initializes the given sensors AutomaticMeasurementConfiguration object. also initializes the averaging 
	array.
	@param sensor one of ObservingConditions::condition_t
	@param sensor_configuration: sensor configuration as ObservingConditions::SensorConfiguration
	@return: 
	false on failure (invalid sensor, sensor instance does not exist or AutomaticMeasurementConfiguration 
	object does already exist.	
	true on success or if automatic measurements are disabled for this sensor. */
	bool beginAutomaticMeasurements(uint8_t sensor, SensorConfiguration sensor_configuration);

	/*
	performs automatic measurements on sensors where it is enabled and, if enabled, updates the values usedd for averaging. 
	this function needs to be called periodically, as only one function on one sensor is executed in each execution. */
	void automaticMeasurements();
	
	/*
	@return true if updating the values for averaging is due. */
	inline bool isDueForAverage(uint8_t sensor);

	/*
	calculates the dew point.
	@param (optional) humidity: relative humidity. if not given or set to NAN, the average humidity or, if not available, 
	the last humidity measurement result is used.
	@param (optional) temperature: absolute (dry bulb) temperature, in degrees Celsius. if not given or set to NAN, the 
	average temperature or, if not available, the last humidity measurement result is used.
	@return calculated dew point or NAN if humidity or temperature are not available. */
	float calculateDewPoint(float humidity = NAN, float temperature = NAN);

	/*
	controls heaters. */
	void controlHeaters();

	/*
	periodically logs sensor data to a log file if logging is enabled. */
	void logSensorData();

	/*static const uint8_t NUM_CONDITIONS = 14;*/
	
	YAAASensor *sensors_[NUM_CONDITIONS];

	uint8_t sensor_counter_;				//sensor counter for automatic measurements

	static const uint32_t QUERIES_PER_AVERAGE_INTERVAL = 10;

	uint32_t average_period_;
	
	AutomaticMeasurementConfiguration *automatic_measurement_configuration_[NUM_CONDITIONS];
	
//-----------------------------------------------------------------------------------------------------
	YAAAHeater *heaters_[NUM_HEATERS];
	HeaterConfiguration heaters_config_[NUM_HEATERS];
	uint32_t heaters_last_update_[NUM_HEATERS];

	uint8_t heater_counter_;				//counter for automatic heater control

	YAAALog *log_sensors_;

	char log_delimiter_;

	uint32_t log_interval_;
	uint32_t log_last_;

	//parameters for dew point calculation / estimation
	const float A_ = 6.112f;
	const float B_ = 17.67f;
	const float C_ = 243.5f;
	const float D_ = 234.5f;
};

#endif /* OBSERVING_CONDITIONS_H_ */
