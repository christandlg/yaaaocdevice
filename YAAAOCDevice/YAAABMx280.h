#ifndef YAAABMx280_H_
#define YAAABMx280_H_

#include <Arduino.h>
//#include <Wire.h>

#include <BMx280MI.h>

class YAAABMx280
{
public:
	enum type_t : uint8_t
	{
		INTERFACE_I2C = 0,
		INTERFACE_SPI = 1
	};

	enum parameter_t : uint8_t
	{
		YAAABMx280_TEMPERATURE = 0,
		YAAABMx280_PRESSURE = 1,
		YAAABMx280_HUMIDITY = 2
	};

	YAAABMx280(uint8_t interface, uint8_t sensor_select);
	~YAAABMx280();

	/*
	@return true on success, false otherwise. */
	bool begin();

	/*
	starts a measurement.
	@return true on success, false otherwise. */
	bool measure();

	/*
	@return true if a measurement has been completed, false otherwise. */
	bool hasValue();

	/*
	@return last measured humidity, in %RH. */
	float getHumidity();

	/*
	@return last measured pressure, in Pa. */
	float getPressure();

	/*
	@return last measured temperature, in �C. */
	float getTemperature();

	/*
	@param parameter one of parameter_t
	@return the oversampling setting of the given parameter */
	uint8_t getOversampling(uint8_t condition);

	/*
	@param parameter one of parameter_t
	@param setting oversampling setting
	@return true on success, false otherwise */
	bool setOversampling(uint8_t condition, uint8_t setting);

	/*
	@return sensors power mode (standby / forced / normal) */
	bool getPowerMode();

	/*
	@param power mode
	@return true on success, false otherwise */
	bool setPowerMode(uint8_t mode);

	//@return standby time as standby_time_t. 
	uint8_t getStandbyTime();

	//sets the sensors standby time. only has an effect if measurements are done in 'normal' (automatic) mode. 
	//@param standby time as standby_time_t. 
	//@return true on success, false otherwise. 
	bool setStandbyTime(uint8_t standby_time);

private:
	BMx280MI *sensor_;
};

#endif /* YAAABMx280_H_ */