//YAAAWiFi class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef YAAAWIFI_H_
#define YAAAWIFI_H_

#if defined(ESP8266) || defined(ESP32)

#include <Arduino.h>

#ifdef ESP8266
#include <ESP8266WiFi.h>
#endif

#ifdef ESP32
#include <WiFi.h>
#endif

#include "defaults.h"

#include "EEPROMHandler.h"

class YAAAWifi
{
public:
	enum config_t : uint8_t
	{
		CONFIG_IP = 0,
		CONFIG_DNS = 1,
		CONFIG_GATEWAY = 2,
		CONFIG_SUBNET = 3,
		CONFIG_RSSI = 4
	};

	static const uint32_t CONNECT_TIMEOUT = 10000;		//timeout for connection to wifi network.

	/*
	initializes WiFi. reads settings from EEPROM and connects to network. */
	static bool begin();

	/*
	@return true if connected to a WiFi network, false otherwise. */
	static bool getConnected();

	/*
	@return true if WiFi is initialized (regardless of being connected), false otherwise. */
	static bool getInitialized();

	/*
	@return WiFi status, wraps WiFi.status() */
	static wl_status_t getStatus();

	/*
	@param config element identifier.
	@return config element as String. */
	static String getConfig(uint8_t config);

	/*
	@param config element identifier
	@param value to set
	@return true on success, false otherwise. */
	static bool setConfig(uint8_t config, String value);

	/*
	@return WiFiServer object. important: only use the returned object after WiFi has been initialized. */
	static WiFiServer getWiFiServer(uint32_t port);

	//static WiFiServerSecure getWiFiServerSecure(uint32_t port);

	/*
	writes default values for the Serial Interface to EEPROM. */
	static void writeDefaults();

private:
	YAAAWifi();		//prevents instantiation of class

	~YAAAWifi();
};

#endif /* defined(ESP8266) || defined(ESP32) */

#endif /* YAAAWIFI_H_ */