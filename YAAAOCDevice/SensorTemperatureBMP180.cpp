//BMP180 Temperature sensor class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "SensorTemperatureBMP180.h"

SensorTemperatureBMP180::SensorTemperatureBMP180(YAAASensor::SensorSettings sensor_settings, void *sensor) :
	SensorTemperature(sensor_settings),
	sensor_(NULL)
{
	if (sensor)
		sensor_ = static_cast<BMP180MI*>(sensor);
	else
		sensor_ = new BMP180I2C(sensor_settings.parameters_[0]);
}

SensorTemperatureBMP180::~SensorTemperatureBMP180()
{
	if (sensor_)
		delete sensor_;

	sensor_ = NULL;
}

bool SensorTemperatureBMP180::begin()
{
	if (!sensor_)
		return false;

	return sensor_->begin();
}

bool SensorTemperatureBMP180::measure()
{
	if (!sensor_)
		return false;

	return sensor_->measureTemperature();
}

bool SensorTemperatureBMP180::hasValue()
{
	if (!sensor_)
		return false;

	return sensor_->hasValue();
}

float SensorTemperatureBMP180::getValue()
{
	if (!sensor_)
		return NAN;

	return SensorTemperature::convertTemperature(
		sensor_->getTemperature(),
		SensorTemperature::UNIT_CELSIUS, 
		temperature_unit_);
}