﻿Imports System.Runtime.Serialization

<DataContract>
Public Class YAAASensor
    Implements ICloneable

    Enum sensor_type_t
        SENSOR_BRIGHTNESS = 10
        SENSOR_CLOUD_COVER = 20
        SENSOR_DEW_POINT = 40
        SENSOR_HUMIDITY = 70
        SENSOR_LIGHTNING = 120
        SENSOR_PRESSURE = 150
        SENSOR_RAIN = 170
        SENSOR_RAIN_RATE = 171
        SENSOR_SKY_QUALITY = 180
        SENSOR_STAR_FWHM = 181
        SENSOR_TEMPERATURE = 190
        SENSOR_WIND_DIRECTION = 220
        SENSOR_WIND_GUST = 221
        SENSOR_WIND_SPEED = 222
        SENSOR_UNKNOWN = 255
    End Enum

    Enum sensor_brightness_device_t
        'DEVICE_ANALOG = 0		'generic device that outputs an analog voltage that corresponds brightness.
        DEVICE_BH1750 = 1       'BH1750 brightness sensor
        DEVICE_UNKNOWN = 255
    End Enum

    Enum sensor_cloud_coverage_t
        DEVICE_UNKNOWN = 255
    End Enum

    Enum sensor_dew_point_t
        DEVICE_UNKNOWN = 255
    End Enum

    Enum sensor_humidity_device_t
        'DEVICE_ANALOG = 0       'generic device that outputs an analog voltage that corresponds to the barometric pressure.
        DEVICE_DHT = 1          'measurement device is a DHT11 Or DHT22
        DEVICE_HTU21 = 2        'measurement device is a HTU21
        DEVICE_BMx280 = 3       'temperature sensor is the sensor integrated in a BME280.
        DEVICE_UNKNOWN = 255
    End Enum

    Enum sensor_lightning_t
        DEVICE_ANALOG = 0
        DEVICE_AS3935 = 1       'austria micro systems AS3935 Franklin lightning sensor
        DEVICE_UNKNOWN = 255
    End Enum

    Enum sensor_pressure_device_t
        'DEVICE_ANALOG = 0		'generic device that outputs an analog voltage that corresponds to the barometric pressure.
        DEVICE_BMP180 = 1       'measurement device us a BMP180
        DEVICE_BMx280 = 2       'measurement device us a BMP280 or BME280
        DEVICE_UNKNOWN = 255
    End Enum

    Enum sensor_rain_device_t
        'DEVICE_ANALOG = 0		'generic device that outputs an analog voltage that corresponds rain.
        DEVICE_FC37 = 1         'rain detector is a FC37 or YL83 (Or similar)
        DEVICE_UNKNOWN = 255
    End Enum

    Enum sensor_rain_rate_device_t
        'DEVICE_ANALOG = 0               'generic device that outputs an analog voltage that corresponds rain.
        DEVICE_TIPPING_BUCKET = 1       'rain rate sensor is a tipping bucket detector that drives an interrupt. 
        DEVICE_UNKNOWN = 255
    End Enum

    Enum sensor_temperature_device_t
        DEVICE_ANALOG_IC = 0            'temperature sensor is a LM35 Or TMP36 temperature sensor (or similar)
        DEVICE_THERMISTOR = 1           'temperature sensor is based on a thermistor.
        DEVICE_ONEWIRE_DS18 = 11        'temperature sensor is a DS18x20 Or DS1820 one wire temperature sensor.
        'DEVICE_DHT = 20                 'temperature sensor is a DHT11 or DHT22 temperature and humidity sensor.
        DEVICE_HTU21 = 40              'temperature sensor is the sensor integrated in a HTU21 humidity sensor.
        DEVICE_BMP180 = 50              'temperature sensor is the sensor integrated in a BMP180.
        DEVICE_BMx280 = 51              'temperature sensor is the sensor integrated in a BMx280.
        DEVICE_MLX90614 = 60            'temperature sensor is a MLX90614 contactless infrared thermometer
        DEVICE_UNKNOWN = 255
    End Enum

    Enum sensor_sky_quality_t
        DEVICE_UNKNOWN = 255
    End Enum

    Enum sensor_star_fwhm_t
        DEVICE_UNKNOWN = 255
    End Enum

    Enum sensor_wind_direction_t
        DEVICE_POTENTIOMETER = 1        'device uses a potentiometer to generate an analog voltage that corresponds to wind direction
        DEVICE_SWITCH_RESISTOR = 2      ' device uses resistors and switches To generate an analog voltage that corresponds to wind direction
        DEVICE_UNKNOWN = 255
    End Enum

    Enum sensor_wind_gust_t
        DEVICE_UNKNOWN = 255
    End Enum

    Enum sensor_wind_speed_t
        DEVICE_ANALOG_VOLTAGE = 0       'device generates a voltage that corresponds to wind speed
        DEVICE_IMPULSE = 1              'device generates impulses whose interval corresponds to wind speed
        DEVICE_UNKNOWN = 255
    End Enum

    Enum unit_brightness_t
        UNIT_LUX = 0
        UNIT_NOX = 1        '1 nx = 1/1000Lx
        UNIT_PHOT = 2       '1 phot = 10000Lx
        UNIT_FC = 3         '1 foot-candle = 10.764lx
        'UNIT_UNKNOWN = 255
    End Enum

    Enum unit_cloud_cover_t
        UNIT_PERCENT = 0
        'UNIT_UNKNOWN = 255
    End Enum

    Enum unit_dew_point_t
        UNIT_CELSIUS = 0
        UNIT_KELVIN = 1
        UNIT_FAHRENHEIT = 2
        'UNIT_UNKNOWN = 255
    End Enum

    Enum unit_humidity_t
        UNIT_RELATIVE = 0       'current absolute relative to maximum absolute for this temperature
        UNIT_ABSOLUTE = 1       'g / m^3
        UNIT_SPECIFIC = 2       'ratio mass Of water vapor to total mass of measurement volume
        'UNIT_UNKNOWN = 255
    End Enum

    Enum unit_lightning_t
        UNIT_METERS = 0
        UNIT_KILOMETERS = 1
        UNIT_MILES = 2
    End Enum

    Enum unit_pressure_t
        UNIT_HECTOPASCAL = 0    'default
        UNIT_PASCAL = 1         'SI-Unit
        UNIT_BAR = 2            '10^5 Pa
        UNIT_PSI = 3            'pounds per square inch
        UNIT_ATM = 4            'standard atmosphere
        UNIT_TORR = 5           'mm of Hg
        UNIT_INHG = 6           'inches of Hg
        'UNIT_UNKNOWN = 255
    End Enum

    Enum unit_rain_t
        UNIT_BOOLEAN = 0
        'UNIT_UNKNOWN = 255
    End Enum

    Enum unit_rain_rate_t
        UNIT_MM_PER_HOUR = 0
        UNIT_IN_PER_HOUR = 1
        'UNIT_UNKNOWN = 255
    End Enum

    Enum unit_sky_quality_t
        UNIT_MAG_PER_SQ_ARCSEC = 0
        'UNIT_UNKNOWN = 255
    End Enum

    Enum unit_star_fwhm_t
        UNIT_ARCSEC = 0
        'UNIT_UNKNOWN = 255
    End Enum

    Enum unit_temperature_t
        UNIT_CELSIUS = 0
        UNIT_KELVIN = 1
        UNIT_FAHRENHEIT = 2
        'UNIT_UNKNOWN = 255
    End Enum

    Enum unit_wind_direction_t
        UNIT_DEGREES = 0
        'UNIT_UNKNOWN = 255
    End Enum

    Enum unit_wind_speed_t
        UNIT_METERS_PER_SECOND = 0
        UNIT_KM_PER_HOUR = 1
        UNIT_FEET_PER_SECOND = 2
        UNIT_MILES_PER_HOUR = 3
        UNIT_KNOTS = 4
        'UNIT_UNKNOWN = 255
    End Enum

    Public Structure sensorDefaultValues
        Public Shared ReadOnly sensor_type_default_ As YAAASensor.sensor_type_t = YAAASensor.sensor_type_t.SENSOR_UNKNOWN
        Public Shared ReadOnly sensor_device_default_ As Byte = 255
        Public Shared ReadOnly sensor_unit_default_ As Byte = 255
        Public Shared ReadOnly average_samples_default_ As Byte = 5
        Public Shared ReadOnly sensor_parameters_default_ As Byte() = {&HFF, &HFF, &HFF, &HFF, &HFF, &HFF, &HFF, &HFF, &HFF, &HFF}
    End Structure

    <DataContract>
    Public Structure sensorSettings
        Implements ICloneable

        <DataMember()>
        Public sensor_type_ As sensor_type_t
        <DataMember()>
        Public sensor_device_ As Byte
        <DataMember()>
        Public sensor_unit_ As Byte
        <DataMember()>
        Public sensor_parameters_ As Byte()

        Public Sub New(initialize As Boolean)
            If Not initialize Then
                Exit Sub
            End If

            sensor_type_ = sensorDefaultValues.sensor_type_default_
            sensor_device_ = sensorDefaultValues.sensor_device_default_
            sensor_unit_ = sensorDefaultValues.sensor_unit_default_
            sensor_parameters_ = sensorDefaultValues.sensor_parameters_default_
        End Sub

        Public Function Clone() As Object Implements ICloneable.Clone
            Dim return_value As sensorSettings = New sensorSettings(True)

            return_value.sensor_type_ = sensor_type_
            return_value.sensor_device_ = sensor_device_
            return_value.sensor_unit_ = sensor_unit_

            ReDim return_value.sensor_parameters_(sensor_parameters_.Length - 1)
            Array.Copy(sensor_parameters_, return_value.sensor_parameters_, sensor_parameters_.Length)

            Return return_value
        End Function
    End Structure

    <DataMember()>
    Public sensor_settings_ As sensorSettings

    Public Sub New(initialize As Boolean)
        sensor_settings_ = New sensorSettings(initialize)
    End Sub

    Public Sub New(settings As String)
        Dim separators() As String = {" ", ","}

        'split string
        Dim strArr() As String = settings.Split(separators, StringSplitOptions.RemoveEmptyEntries)

        If strArr.Length <> 13 Then
            Throw New Exception
        End If

        sensor_settings_.sensor_type_ = [Enum].Parse(GetType(YAAASensor.sensor_type_t), strArr(0))
        sensor_settings_.sensor_device_ = CByte(strArr(1))
        sensor_settings_.sensor_unit_ = CByte(strArr(2))

        ReDim sensor_settings_.sensor_parameters_(9)

        For i As Integer = 0 To 9
            sensor_settings_.sensor_parameters_(i) = CByte(Convert.ToInt32(strArr(3 + i), 16))
        Next

    End Sub

    Public Function Clone() As Object Implements ICloneable.Clone
        Dim return_value As YAAASensor = New YAAASensor(True)

        return_value.sensor_settings_ = sensor_settings_.Clone()

        Return return_value
    End Function

    'converts a temperatureSensorSettings struct to a string that can be sent to the Arduino.
    Public Function toArduinoString() As String
        Dim return_value As String = ""

        return_value &= Convert.ToInt32(sensor_settings_.sensor_type_)
        return_value &= " "
        return_value &= Convert.ToInt32(sensor_settings_.sensor_device_)
        return_value &= " "
        return_value &= Convert.ToInt32(sensor_settings_.sensor_unit_)

        For i As Integer = 0 To 9
            return_value &= " "
            return_value &= Hex(sensor_settings_.sensor_parameters_(i))
        Next

        Return return_value
    End Function

    Public Overloads Shared Operator =(ByVal s1 As YAAASensor, ByVal s2 As YAAASensor) As Boolean
        Dim parameters_equal As Boolean = True

        'compare parameters array
        For i As Byte = 0 To s1.sensor_settings_.sensor_parameters_.Length - 1
            If s1.sensor_settings_.sensor_parameters_(i) <> s2.sensor_settings_.sensor_parameters_(i) Then
                parameters_equal = False
            End If
        Next

        parameters_equal = parameters_equal AndAlso s1.sensor_settings_.sensor_type_ = s2.sensor_settings_.sensor_type_
        parameters_equal = parameters_equal AndAlso s1.sensor_settings_.sensor_device_ = s2.sensor_settings_.sensor_device_
        parameters_equal = parameters_equal AndAlso s1.sensor_settings_.sensor_unit_ = s2.sensor_settings_.sensor_unit_

        Return parameters_equal
    End Operator

    Public Overloads Shared Operator <>(ByVal s1 As YAAASensor, ByVal s2 As YAAASensor) As Boolean
        Return Not s1 = s2
    End Operator

    'checks if the information stored in the object is sufficient for the Arduino to decide if this sensor
    'is enabled or not.
    '@return true if the information is sufficient to enable the arduino to decide which sensor object is
    'represented by the object, false otherwise. 
    Public Function isSensorEnabled() As Boolean
        Dim sensor_type As sensor_type_t = sensor_type_t.SENSOR_UNKNOWN

        Dim sensor_device_type As System.Type = Nothing

        'check if the sensor type is defined in YAAASensor.sensor_type_t
        If Not [Enum].TryParse(Of YAAASensor.sensor_type_t)(sensor_settings_.sensor_type_, sensor_type) Then
            Return False
        End If

        'if the sensor type is undefined, return false
        If sensor_type = sensor_type_t.SENSOR_UNKNOWN Then
            Return False
        End If

        Select Case sensor_type
            Case YAAASensor.sensor_type_t.SENSOR_BRIGHTNESS
                sensor_device_type = GetType(YAAASensor.sensor_brightness_device_t)
            Case YAAASensor.sensor_type_t.SENSOR_CLOUD_COVER
                sensor_device_type = GetType(YAAASensor.sensor_cloud_coverage_t)
            Case YAAASensor.sensor_type_t.SENSOR_DEW_POINT
                sensor_device_type = GetType(YAAASensor.sensor_dew_point_t)
            Case YAAASensor.sensor_type_t.SENSOR_HUMIDITY
                sensor_device_type = GetType(YAAASensor.sensor_humidity_device_t)
            Case YAAASensor.sensor_type_t.SENSOR_PRESSURE
                sensor_device_type = GetType(YAAASensor.sensor_pressure_device_t)
            Case YAAASensor.sensor_type_t.SENSOR_RAIN
                sensor_device_type = GetType(YAAASensor.sensor_rain_device_t)
            Case YAAASensor.sensor_type_t.SENSOR_RAIN_RATE
                sensor_device_type = GetType(YAAASensor.sensor_rain_rate_device_t)
            Case YAAASensor.sensor_type_t.SENSOR_SKY_QUALITY
                sensor_device_type = GetType(YAAASensor.sensor_sky_quality_t)
            Case YAAASensor.sensor_type_t.SENSOR_STAR_FWHM
                sensor_device_type = GetType(YAAASensor.sensor_star_fwhm_t)
            Case YAAASensor.sensor_type_t.SENSOR_TEMPERATURE
                sensor_device_type = GetType(YAAASensor.sensor_temperature_device_t)
            Case YAAASensor.sensor_type_t.SENSOR_WIND_DIRECTION
                sensor_device_type = GetType(YAAASensor.sensor_wind_direction_t)
            Case YAAASensor.sensor_type_t.SENSOR_WIND_GUST
                sensor_device_type = GetType(YAAASensor.sensor_wind_gust_t)
            Case YAAASensor.sensor_type_t.SENSOR_WIND_SPEED
                sensor_device_type = GetType(YAAASensor.sensor_wind_speed_t)
            Case YAAASensor.sensor_type_t.SENSOR_LIGHTNING
                sensor_device_type = GetType(YAAASensor.sensor_lightning_t)
        End Select

        If Not [Enum].IsDefined(sensor_device_type, CInt(sensor_settings_.sensor_device_)) Then
            Return False
        End If

        If [Enum].Parse(sensor_device_type, CInt(sensor_settings_.sensor_device_)).ToString = "DEVICE_UNKNOWN" Then
            Return False
        End If

        Return True
    End Function
End Class
