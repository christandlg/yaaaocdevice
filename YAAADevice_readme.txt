Yet Another Arduino ASCOM Device Project

Goals of this Project:
- Create a single Arduino Sketch implementing different ASCOM devices (Dome, Filter Wheel, Focuser, Rotator, Telescope) as well as the ASCOM Observing Conditions Interface. 
- Create ASCOM drivers for these devices. 
- Implement drivers as local server drivers, allowing a single Arduino to control multiple devices at the same time.

Project State:
The project is in development. 
If you find a bug or would like to propose improvements, please do not hesitate to create an issue in the [issue tracker](https://bitbucket.org/christandlg/yaaadevice/issues).

Arduino Sketch:
- Telescope: 90% finished
- - everything implemented except Pulse Guiding and German Equatorial mount support. 
- - different step types for "fast" (slew, move axis) and "slow" (tracking) movements are supported.
- Filter Wheel: 95% implementation finished, needs refactoring and testing
- - supports both Wheel and Slider Type Filter Holders.
- Focuser: 95% implementation finished, needs refactoring and testing
- - Can operate in both absolute and relative mode.
- - Supports Home and Endstop Switches.
- - Supports Temperature Compensation, but currently uses only a linear model.
- Observing Conditions: 70% finished
- - implemented properties: humidity, pressure, rain / rain rate, sky brightness, sky temperature, temperature, wind direcion, wind speed
- Human Readable Command Protocol
- Temperature Sensors are supported for all Devices and can be read by the respective Drivers.
- (Partial) Meade Telescope / LX200 Protocol support is implemented and will be extended in the future.
- Support for Displays is in development - currently only HD47780 compatible Displays are supported and show Telescope Alignment, Filter Wheel Position and Focuser position / movement state (depending on Absolute/Relative mode setting).
- Communication is possible via Serial interface or TCP (using an Arduino Ethernet Shield)
- Commands and Responses can be logged to an SD card (using an ethernet shield, Data logging shield, ...). This is intended for debug purposes only as it impacts execution speed.

Driver:
- Telescope: 90% - implementation finished (exceptions: see above), needs refactoring and testing
- Filter Wheel: 95% - implementation finished, needs refactoring and testing
- Focuser: 95% - implementation finished, needs refactoring and testing
- Observing Conditions: 70% - all currently available observing conditions are implemented, needs refactoring and testing
- Drivers are not yet implemented as local server drivers, therefore only one device can be controlled at a time.

Documentation:
moderate - code is commented and documented in the source files, Telescope Driver Setup Dialog has Tooltips for most settings.
the wiki contains more information and quick start guides.


Required Hardware:
The Arduino Sketch is designed to run on an Arduino Mega 2560. 
Since 2016-05-07 the Sketch supports the Arduino Due, but is largely untested on the Due.
Since 2017-08-21 the Sketch supports the ESP8266 (NodeMcu) (dev branch only).
The Sketch may work on other Arduinos but has only been tested on a Mega 2560.

YAAADevice uses Stepper Motors. The following Stepper Motors / Drivers are supported: 

* Adafruit Motor Shield V2
* simple 4-wire Stepper Motors (28BYJ-48 and similar)
* A4988/DRV8825 Stepper Drivers (and similar)

Required Software:
Arduino IDE >= v1.8.0: https://www.arduino.cc/en/Main/Software
The latest ASCOM Platform Software: http://ascom-standards.org/Downloads/Index.htm
AccelStepper Library for Arduino: http://www.airspayce.com/mikem/arduino/AccelStepper/
DS1307RTC Library: https://www.pjrc.com/teensy/td_libs_DS1307RTC.html *
Time Library: https://www.pjrc.com/teensy/td_libs_Time.html *
OneWire Library: https://www.pjrc.com/teensy/td_libs_OneWire.html *
I2C device library collection for AVR/Arduino: https://github.com/jrowberg/i2cdevlib (dev branch only)
SimpleDHT library: https://github.com/winlinvip/SimpleDHT (dev branch only) *
The Repository's Arduino Sketch folder contains a modified version of the Adafruit Motor Shield V2 library.
Libraries marked with * can be installed via the Arduino IDE (Sketch > Include Library > Manage Libraries...)

Compiled drivers can be found in the Downloads section.