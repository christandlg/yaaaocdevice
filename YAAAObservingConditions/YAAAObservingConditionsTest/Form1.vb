﻿Public Class Form1

    Private driver As ASCOM.DriverAccess.ObservingConditions

    ''' <summary>
    ''' This event is where the driver is choosen. The device ID will be saved in the settings.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    Private Sub buttonChoose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonChoose.Click
        My.Settings.DriverId = ASCOM.DriverAccess.ObservingConditions.Choose(My.Settings.DriverId)
        SetUIState()
    End Sub

    ''' <summary>
    ''' Connects to the device to be tested.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    Private Sub buttonConnect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonConnect.Click
        If (IsConnected) Then
            driver.Connected = False
        Else
            driver = New ASCOM.DriverAccess.ObservingConditions(My.Settings.DriverId)
            Try
                driver.Connected = True
            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try
        End If
        SetUIState()
    End Sub

    Private Sub Form1_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        If IsConnected Then
            driver.Connected = False
        End If
        ' the settings are saved automatically when this application is closed.
    End Sub

    ''' <summary>
    ''' Sets the state of the UI depending on the device state
    ''' </summary>
    Private Sub SetUIState()
        buttonConnect.Enabled = Not String.IsNullOrEmpty(My.Settings.DriverId)
        buttonChoose.Enabled = Not IsConnected
        buttonConnect.Text = IIf(IsConnected, "Disconnect", "Connect")
    End Sub

    ''' <summary>
    ''' Gets a value indicating whether this instance is connected.
    ''' </summary>
    ''' <value>
    ''' 
    ''' <c>true</c> if this instance is connected; otherwise, <c>false</c>.
    ''' 
    ''' </value>
    Private ReadOnly Property IsConnected() As Boolean
        Get
            If Me.driver Is Nothing Then Return False
            Return driver.Connected
        End Get
    End Property

    ' TODO: Add additional UI and controls to test more of the driver being tested.

    Private Sub ButtonGetAveragePeriod_Click(sender As Object, e As EventArgs) Handles ButtonGetAveragePeriod.Click
        Try
            TextBoxAveragePeriod.Text = driver.AveragePeriod
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ButtonSetAveragePeriod_Click(sender As Object, e As EventArgs) Handles ButtonSetAveragePeriod.Click
        Try
            driver.AveragePeriod = TextBoxAveragePeriod.Text
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ButtonGetCloudCover_Click(sender As Object, e As EventArgs) Handles ButtonGetCloudCover.Click
        Try
            TextBoxCloudCover.Text = driver.CloudCover
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ButtonGetDewPoint_Click(sender As Object, e As EventArgs) Handles ButtonGetDewPoint.Click
        Try
            TextBoxDewPoint.Text = driver.DewPoint
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ButtonGetHumidity_Click(sender As Object, e As EventArgs) Handles ButtonGetHumidity.Click
        Try
            TextBoxHumidity.Text = driver.Humidity
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ButtonGetPressure_Click(sender As Object, e As EventArgs) Handles ButtonGetPressure.Click
        Try
            TextBoxPressure.Text = driver.Pressure
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ButtonGetRainRate_Click(sender As Object, e As EventArgs) Handles ButtonGetRainRate.Click
        Try
            TextBoxRainRate.Text = driver.RainRate
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ButtonGetSkyBrightness_Click(sender As Object, e As EventArgs) Handles ButtonGetSkyBrightness.Click
        Try
            TextBoxSkyBrightness.Text = driver.SkyBrightness
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ButtonGetSkyQuality_Click(sender As Object, e As EventArgs) Handles ButtonGetSkyQuality.Click
        Try
            TextBoxSkyQuality.Text = driver.SkyQuality
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ButtonGetSkyTemperature_Click(sender As Object, e As EventArgs) Handles ButtonGetSkyTemperature.Click
        Try
            TextBoxSkyTemperature.Text = driver.SkyTemperature
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ButtonGetStarFWHM_Click(sender As Object, e As EventArgs) Handles ButtonGetStarFWHM.Click
        Try
            TextBoxStarFWHM.Text = driver.StarFWHM
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ButtonGetTemperature_Click(sender As Object, e As EventArgs) Handles ButtonGetTemperature.Click
        Try
            TextBoxTemperature.Text = driver.Temperature
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ButtonGetWindDirection_Click(sender As Object, e As EventArgs) Handles ButtonGetWindDirection.Click
        Try
            TextBoxWindDirection.Text = driver.WindDirection
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ButtonGetWindGust_Click(sender As Object, e As EventArgs) Handles ButtonGetWindGust.Click
        Try
            TextBoxWindGust.Text = driver.WindGust
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ButtonGetWindSpeed_Click(sender As Object, e As EventArgs) Handles ButtonGetWindSpeed.Click
        Try
            TextBoxWindSpeed.Text = driver.WindSpeed
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ButtonRefresh_Click(sender As Object, e As EventArgs) Handles ButtonRefresh.Click
        Try
            driver.Refresh()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ButtonSensorDescription_Click(sender As Object, e As EventArgs) Handles ButtonSensorDescription.Click
        Dim propertyName As String = InputBox("property name")

        Try
            MsgBox(driver.SensorDescription(propertyName))
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ButtonTimeSinceLastUpdate_Click(sender As Object, e As EventArgs) Handles ButtonTimeSinceLastUpdate.Click
        Dim propertyName As String = InputBox("property name")
        Try
            MsgBox(driver.TimeSinceLastUpdate(propertyName))
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ButtonLoadSupportedActions_Click(sender As Object, e As EventArgs) Handles ButtonLoadSupportedActions.Click
        ListBoxSupportedActions.Items.Clear()

        Try
            For Each obj As Object In driver.SupportedActions
                ListBoxSupportedActions.Items.Add(obj)
            Next
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub ButtonExecuteSupportedAction_Click(sender As Object, e As EventArgs) Handles ButtonExecuteSupportedAction.Click
        'Dim opt As String = InputBox("option")
        Dim opt As String = ListBoxSupportedActions.SelectedItem.ToString
        'MsgBox(opt)
        MsgBox(driver.Action(ListBoxSupportedActions.SelectedItem.ToString(), opt))
    End Sub
End Class
