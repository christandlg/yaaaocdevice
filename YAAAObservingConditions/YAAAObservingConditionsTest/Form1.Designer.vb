﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.labelDriverId = New System.Windows.Forms.Label()
        Me.buttonConnect = New System.Windows.Forms.Button()
        Me.buttonChoose = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TextBoxCloudCover = New System.Windows.Forms.TextBox()
        Me.ButtonGetCloudCover = New System.Windows.Forms.Button()
        Me.ButtonGetDewPoint = New System.Windows.Forms.Button()
        Me.ButtonGetHumidity = New System.Windows.Forms.Button()
        Me.ButtonGetPressure = New System.Windows.Forms.Button()
        Me.ButtonGetRainRate = New System.Windows.Forms.Button()
        Me.ButtonGetSkyBrightness = New System.Windows.Forms.Button()
        Me.ButtonGetSkyQuality = New System.Windows.Forms.Button()
        Me.ButtonGetSkyTemperature = New System.Windows.Forms.Button()
        Me.ButtonGetStarFWHM = New System.Windows.Forms.Button()
        Me.ButtonGetTemperature = New System.Windows.Forms.Button()
        Me.ButtonGetWindDirection = New System.Windows.Forms.Button()
        Me.ButtonGetWindGust = New System.Windows.Forms.Button()
        Me.ButtonGetWindSpeed = New System.Windows.Forms.Button()
        Me.TextBoxDewPoint = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TextBoxHumidity = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TextBoxPressure = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TextBoxRainRate = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TextBoxSkyBrightness = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TextBoxSkyQuality = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TextBoxSkyTemperature = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.TextBoxStarFWHM = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.TextBoxTemperature = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.TextBoxWindDirection = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.TextBoxWindGust = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.TextBoxWindSpeed = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.ButtonGetAveragePeriod = New System.Windows.Forms.Button()
        Me.TextBoxAveragePeriod = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.ButtonSetAveragePeriod = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.ButtonTimeSinceLastUpdate = New System.Windows.Forms.Button()
        Me.ButtonSensorDescription = New System.Windows.Forms.Button()
        Me.ButtonRefresh = New System.Windows.Forms.Button()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.ListBoxSupportedActions = New System.Windows.Forms.ListBox()
        Me.ButtonLoadSupportedActions = New System.Windows.Forms.Button()
        Me.ButtonExecuteSupportedAction = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'labelDriverId
        '
        Me.labelDriverId.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.labelDriverId.DataBindings.Add(New System.Windows.Forms.Binding("Text", Global.ASCOM.YAAADevice.My.MySettings.Default, "DriverId", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.labelDriverId.Location = New System.Drawing.Point(12, 37)
        Me.labelDriverId.Name = "labelDriverId"
        Me.labelDriverId.Size = New System.Drawing.Size(291, 21)
        Me.labelDriverId.TabIndex = 5
        Me.labelDriverId.Text = Global.ASCOM.YAAADevice.My.MySettings.Default.DriverId
        Me.labelDriverId.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'buttonConnect
        '
        Me.buttonConnect.Location = New System.Drawing.Point(316, 36)
        Me.buttonConnect.Name = "buttonConnect"
        Me.buttonConnect.Size = New System.Drawing.Size(72, 23)
        Me.buttonConnect.TabIndex = 4
        Me.buttonConnect.Text = "Connect"
        Me.buttonConnect.UseVisualStyleBackColor = True
        '
        'buttonChoose
        '
        Me.buttonChoose.Location = New System.Drawing.Point(316, 7)
        Me.buttonChoose.Name = "buttonChoose"
        Me.buttonChoose.Size = New System.Drawing.Size(72, 23)
        Me.buttonChoose.TabIndex = 3
        Me.buttonChoose.Text = "Choose"
        Me.buttonChoose.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(7, 50)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(62, 13)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "CloudCover"
        '
        'TextBoxCloudCover
        '
        Me.TextBoxCloudCover.Location = New System.Drawing.Point(141, 47)
        Me.TextBoxCloudCover.Name = "TextBoxCloudCover"
        Me.TextBoxCloudCover.ReadOnly = True
        Me.TextBoxCloudCover.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxCloudCover.TabIndex = 7
        '
        'ButtonGetCloudCover
        '
        Me.ButtonGetCloudCover.Location = New System.Drawing.Point(247, 45)
        Me.ButtonGetCloudCover.Name = "ButtonGetCloudCover"
        Me.ButtonGetCloudCover.Size = New System.Drawing.Size(51, 23)
        Me.ButtonGetCloudCover.TabIndex = 8
        Me.ButtonGetCloudCover.Text = "Get"
        Me.ButtonGetCloudCover.UseVisualStyleBackColor = True
        '
        'ButtonGetDewPoint
        '
        Me.ButtonGetDewPoint.Location = New System.Drawing.Point(247, 74)
        Me.ButtonGetDewPoint.Name = "ButtonGetDewPoint"
        Me.ButtonGetDewPoint.Size = New System.Drawing.Size(51, 23)
        Me.ButtonGetDewPoint.TabIndex = 9
        Me.ButtonGetDewPoint.Text = "Get"
        Me.ButtonGetDewPoint.UseVisualStyleBackColor = True
        '
        'ButtonGetHumidity
        '
        Me.ButtonGetHumidity.Location = New System.Drawing.Point(247, 103)
        Me.ButtonGetHumidity.Name = "ButtonGetHumidity"
        Me.ButtonGetHumidity.Size = New System.Drawing.Size(51, 23)
        Me.ButtonGetHumidity.TabIndex = 10
        Me.ButtonGetHumidity.Text = "Get"
        Me.ButtonGetHumidity.UseVisualStyleBackColor = True
        '
        'ButtonGetPressure
        '
        Me.ButtonGetPressure.Location = New System.Drawing.Point(247, 132)
        Me.ButtonGetPressure.Name = "ButtonGetPressure"
        Me.ButtonGetPressure.Size = New System.Drawing.Size(51, 23)
        Me.ButtonGetPressure.TabIndex = 11
        Me.ButtonGetPressure.Text = "Get"
        Me.ButtonGetPressure.UseVisualStyleBackColor = True
        '
        'ButtonGetRainRate
        '
        Me.ButtonGetRainRate.Location = New System.Drawing.Point(247, 161)
        Me.ButtonGetRainRate.Name = "ButtonGetRainRate"
        Me.ButtonGetRainRate.Size = New System.Drawing.Size(51, 23)
        Me.ButtonGetRainRate.TabIndex = 12
        Me.ButtonGetRainRate.Text = "Get"
        Me.ButtonGetRainRate.UseVisualStyleBackColor = True
        '
        'ButtonGetSkyBrightness
        '
        Me.ButtonGetSkyBrightness.Location = New System.Drawing.Point(247, 190)
        Me.ButtonGetSkyBrightness.Name = "ButtonGetSkyBrightness"
        Me.ButtonGetSkyBrightness.Size = New System.Drawing.Size(51, 23)
        Me.ButtonGetSkyBrightness.TabIndex = 13
        Me.ButtonGetSkyBrightness.Text = "Get"
        Me.ButtonGetSkyBrightness.UseVisualStyleBackColor = True
        '
        'ButtonGetSkyQuality
        '
        Me.ButtonGetSkyQuality.Location = New System.Drawing.Point(247, 219)
        Me.ButtonGetSkyQuality.Name = "ButtonGetSkyQuality"
        Me.ButtonGetSkyQuality.Size = New System.Drawing.Size(51, 23)
        Me.ButtonGetSkyQuality.TabIndex = 14
        Me.ButtonGetSkyQuality.Text = "Get"
        Me.ButtonGetSkyQuality.UseVisualStyleBackColor = True
        '
        'ButtonGetSkyTemperature
        '
        Me.ButtonGetSkyTemperature.Location = New System.Drawing.Point(247, 248)
        Me.ButtonGetSkyTemperature.Name = "ButtonGetSkyTemperature"
        Me.ButtonGetSkyTemperature.Size = New System.Drawing.Size(51, 23)
        Me.ButtonGetSkyTemperature.TabIndex = 15
        Me.ButtonGetSkyTemperature.Text = "Get"
        Me.ButtonGetSkyTemperature.UseVisualStyleBackColor = True
        '
        'ButtonGetStarFWHM
        '
        Me.ButtonGetStarFWHM.Location = New System.Drawing.Point(247, 277)
        Me.ButtonGetStarFWHM.Name = "ButtonGetStarFWHM"
        Me.ButtonGetStarFWHM.Size = New System.Drawing.Size(51, 23)
        Me.ButtonGetStarFWHM.TabIndex = 16
        Me.ButtonGetStarFWHM.Text = "Get"
        Me.ButtonGetStarFWHM.UseVisualStyleBackColor = True
        '
        'ButtonGetTemperature
        '
        Me.ButtonGetTemperature.Location = New System.Drawing.Point(247, 306)
        Me.ButtonGetTemperature.Name = "ButtonGetTemperature"
        Me.ButtonGetTemperature.Size = New System.Drawing.Size(51, 23)
        Me.ButtonGetTemperature.TabIndex = 17
        Me.ButtonGetTemperature.Text = "Get"
        Me.ButtonGetTemperature.UseVisualStyleBackColor = True
        '
        'ButtonGetWindDirection
        '
        Me.ButtonGetWindDirection.Location = New System.Drawing.Point(247, 335)
        Me.ButtonGetWindDirection.Name = "ButtonGetWindDirection"
        Me.ButtonGetWindDirection.Size = New System.Drawing.Size(51, 23)
        Me.ButtonGetWindDirection.TabIndex = 18
        Me.ButtonGetWindDirection.Text = "Get"
        Me.ButtonGetWindDirection.UseVisualStyleBackColor = True
        '
        'ButtonGetWindGust
        '
        Me.ButtonGetWindGust.Location = New System.Drawing.Point(247, 364)
        Me.ButtonGetWindGust.Name = "ButtonGetWindGust"
        Me.ButtonGetWindGust.Size = New System.Drawing.Size(51, 23)
        Me.ButtonGetWindGust.TabIndex = 19
        Me.ButtonGetWindGust.Text = "Get"
        Me.ButtonGetWindGust.UseVisualStyleBackColor = True
        '
        'ButtonGetWindSpeed
        '
        Me.ButtonGetWindSpeed.Location = New System.Drawing.Point(247, 393)
        Me.ButtonGetWindSpeed.Name = "ButtonGetWindSpeed"
        Me.ButtonGetWindSpeed.Size = New System.Drawing.Size(51, 23)
        Me.ButtonGetWindSpeed.TabIndex = 20
        Me.ButtonGetWindSpeed.Text = "Get"
        Me.ButtonGetWindSpeed.UseVisualStyleBackColor = True
        '
        'TextBoxDewPoint
        '
        Me.TextBoxDewPoint.Location = New System.Drawing.Point(141, 76)
        Me.TextBoxDewPoint.Name = "TextBoxDewPoint"
        Me.TextBoxDewPoint.ReadOnly = True
        Me.TextBoxDewPoint.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxDewPoint.TabIndex = 23
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(7, 79)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(53, 13)
        Me.Label2.TabIndex = 22
        Me.Label2.Text = "DewPoint"
        '
        'TextBoxHumidity
        '
        Me.TextBoxHumidity.Location = New System.Drawing.Point(141, 105)
        Me.TextBoxHumidity.Name = "TextBoxHumidity"
        Me.TextBoxHumidity.ReadOnly = True
        Me.TextBoxHumidity.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxHumidity.TabIndex = 25
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(7, 108)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(47, 13)
        Me.Label3.TabIndex = 24
        Me.Label3.Text = "Humidity"
        '
        'TextBoxPressure
        '
        Me.TextBoxPressure.Location = New System.Drawing.Point(141, 134)
        Me.TextBoxPressure.Name = "TextBoxPressure"
        Me.TextBoxPressure.ReadOnly = True
        Me.TextBoxPressure.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxPressure.TabIndex = 27
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(7, 137)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(48, 13)
        Me.Label4.TabIndex = 26
        Me.Label4.Text = "Pressure"
        '
        'TextBoxRainRate
        '
        Me.TextBoxRainRate.Location = New System.Drawing.Point(141, 163)
        Me.TextBoxRainRate.Name = "TextBoxRainRate"
        Me.TextBoxRainRate.ReadOnly = True
        Me.TextBoxRainRate.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxRainRate.TabIndex = 29
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(7, 166)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(52, 13)
        Me.Label5.TabIndex = 28
        Me.Label5.Text = "RainRate"
        '
        'TextBoxSkyBrightness
        '
        Me.TextBoxSkyBrightness.Location = New System.Drawing.Point(141, 192)
        Me.TextBoxSkyBrightness.Name = "TextBoxSkyBrightness"
        Me.TextBoxSkyBrightness.ReadOnly = True
        Me.TextBoxSkyBrightness.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxSkyBrightness.TabIndex = 31
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(7, 195)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(74, 13)
        Me.Label6.TabIndex = 30
        Me.Label6.Text = "SkyBrightness"
        '
        'TextBoxSkyQuality
        '
        Me.TextBoxSkyQuality.Location = New System.Drawing.Point(141, 221)
        Me.TextBoxSkyQuality.Name = "TextBoxSkyQuality"
        Me.TextBoxSkyQuality.ReadOnly = True
        Me.TextBoxSkyQuality.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxSkyQuality.TabIndex = 33
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(7, 224)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(57, 13)
        Me.Label7.TabIndex = 32
        Me.Label7.Text = "SkyQuality"
        '
        'TextBoxSkyTemperature
        '
        Me.TextBoxSkyTemperature.Location = New System.Drawing.Point(141, 250)
        Me.TextBoxSkyTemperature.Name = "TextBoxSkyTemperature"
        Me.TextBoxSkyTemperature.ReadOnly = True
        Me.TextBoxSkyTemperature.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxSkyTemperature.TabIndex = 35
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(7, 253)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(85, 13)
        Me.Label8.TabIndex = 34
        Me.Label8.Text = "SkyTemperature"
        '
        'TextBoxStarFWHM
        '
        Me.TextBoxStarFWHM.Location = New System.Drawing.Point(141, 279)
        Me.TextBoxStarFWHM.Name = "TextBoxStarFWHM"
        Me.TextBoxStarFWHM.ReadOnly = True
        Me.TextBoxStarFWHM.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxStarFWHM.TabIndex = 37
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(7, 282)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(60, 13)
        Me.Label9.TabIndex = 36
        Me.Label9.Text = "StarFWHM"
        '
        'TextBoxTemperature
        '
        Me.TextBoxTemperature.Location = New System.Drawing.Point(141, 308)
        Me.TextBoxTemperature.Name = "TextBoxTemperature"
        Me.TextBoxTemperature.ReadOnly = True
        Me.TextBoxTemperature.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxTemperature.TabIndex = 39
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(7, 311)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(67, 13)
        Me.Label10.TabIndex = 38
        Me.Label10.Text = "Temperature"
        '
        'TextBoxWindDirection
        '
        Me.TextBoxWindDirection.Location = New System.Drawing.Point(141, 337)
        Me.TextBoxWindDirection.Name = "TextBoxWindDirection"
        Me.TextBoxWindDirection.ReadOnly = True
        Me.TextBoxWindDirection.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxWindDirection.TabIndex = 41
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(7, 340)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(74, 13)
        Me.Label11.TabIndex = 40
        Me.Label11.Text = "WindDirection"
        '
        'TextBoxWindGust
        '
        Me.TextBoxWindGust.Location = New System.Drawing.Point(141, 366)
        Me.TextBoxWindGust.Name = "TextBoxWindGust"
        Me.TextBoxWindGust.ReadOnly = True
        Me.TextBoxWindGust.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxWindGust.TabIndex = 43
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(7, 369)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(54, 13)
        Me.Label12.TabIndex = 42
        Me.Label12.Text = "WindGust"
        '
        'TextBoxWindSpeed
        '
        Me.TextBoxWindSpeed.Location = New System.Drawing.Point(141, 395)
        Me.TextBoxWindSpeed.Name = "TextBoxWindSpeed"
        Me.TextBoxWindSpeed.ReadOnly = True
        Me.TextBoxWindSpeed.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxWindSpeed.TabIndex = 45
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(7, 398)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(63, 13)
        Me.Label13.TabIndex = 44
        Me.Label13.Text = "WindSpeed"
        '
        'ButtonGetAveragePeriod
        '
        Me.ButtonGetAveragePeriod.Location = New System.Drawing.Point(247, 19)
        Me.ButtonGetAveragePeriod.Name = "ButtonGetAveragePeriod"
        Me.ButtonGetAveragePeriod.Size = New System.Drawing.Size(51, 23)
        Me.ButtonGetAveragePeriod.TabIndex = 50
        Me.ButtonGetAveragePeriod.Text = "Get"
        Me.ButtonGetAveragePeriod.UseVisualStyleBackColor = True
        '
        'TextBoxAveragePeriod
        '
        Me.TextBoxAveragePeriod.Location = New System.Drawing.Point(141, 21)
        Me.TextBoxAveragePeriod.Name = "TextBoxAveragePeriod"
        Me.TextBoxAveragePeriod.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxAveragePeriod.TabIndex = 49
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(7, 24)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(77, 13)
        Me.Label15.TabIndex = 48
        Me.Label15.Text = "AveragePeriod"
        '
        'ButtonSetAveragePeriod
        '
        Me.ButtonSetAveragePeriod.Location = New System.Drawing.Point(304, 19)
        Me.ButtonSetAveragePeriod.Name = "ButtonSetAveragePeriod"
        Me.ButtonSetAveragePeriod.Size = New System.Drawing.Size(51, 23)
        Me.ButtonSetAveragePeriod.TabIndex = 51
        Me.ButtonSetAveragePeriod.Text = "Set"
        Me.ButtonSetAveragePeriod.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.ButtonSetAveragePeriod)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.ButtonGetAveragePeriod)
        Me.GroupBox1.Controls.Add(Me.TextBoxCloudCover)
        Me.GroupBox1.Controls.Add(Me.TextBoxAveragePeriod)
        Me.GroupBox1.Controls.Add(Me.ButtonGetCloudCover)
        Me.GroupBox1.Controls.Add(Me.ButtonGetDewPoint)
        Me.GroupBox1.Controls.Add(Me.TextBoxWindSpeed)
        Me.GroupBox1.Controls.Add(Me.ButtonGetHumidity)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.ButtonGetPressure)
        Me.GroupBox1.Controls.Add(Me.TextBoxWindGust)
        Me.GroupBox1.Controls.Add(Me.ButtonGetRainRate)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.ButtonGetSkyBrightness)
        Me.GroupBox1.Controls.Add(Me.TextBoxWindDirection)
        Me.GroupBox1.Controls.Add(Me.ButtonGetSkyQuality)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.ButtonGetSkyTemperature)
        Me.GroupBox1.Controls.Add(Me.TextBoxTemperature)
        Me.GroupBox1.Controls.Add(Me.ButtonGetStarFWHM)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.ButtonGetTemperature)
        Me.GroupBox1.Controls.Add(Me.TextBoxStarFWHM)
        Me.GroupBox1.Controls.Add(Me.ButtonGetWindDirection)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.ButtonGetWindGust)
        Me.GroupBox1.Controls.Add(Me.TextBoxSkyTemperature)
        Me.GroupBox1.Controls.Add(Me.ButtonGetWindSpeed)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.TextBoxSkyQuality)
        Me.GroupBox1.Controls.Add(Me.TextBoxDewPoint)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.TextBoxSkyBrightness)
        Me.GroupBox1.Controls.Add(Me.TextBoxHumidity)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.TextBoxRainRate)
        Me.GroupBox1.Controls.Add(Me.TextBoxPressure)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 65)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(376, 425)
        Me.GroupBox1.TabIndex = 52
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Properties"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.ButtonTimeSinceLastUpdate)
        Me.GroupBox2.Controls.Add(Me.ButtonSensorDescription)
        Me.GroupBox2.Controls.Add(Me.ButtonRefresh)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 496)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(376, 59)
        Me.GroupBox2.TabIndex = 53
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Methods"
        '
        'ButtonTimeSinceLastUpdate
        '
        Me.ButtonTimeSinceLastUpdate.Location = New System.Drawing.Point(198, 19)
        Me.ButtonTimeSinceLastUpdate.Name = "ButtonTimeSinceLastUpdate"
        Me.ButtonTimeSinceLastUpdate.Size = New System.Drawing.Size(157, 22)
        Me.ButtonTimeSinceLastUpdate.TabIndex = 2
        Me.ButtonTimeSinceLastUpdate.Text = "TimeSinceLastUpdate"
        Me.ButtonTimeSinceLastUpdate.UseVisualStyleBackColor = True
        '
        'ButtonSensorDescription
        '
        Me.ButtonSensorDescription.Location = New System.Drawing.Point(80, 19)
        Me.ButtonSensorDescription.Name = "ButtonSensorDescription"
        Me.ButtonSensorDescription.Size = New System.Drawing.Size(112, 22)
        Me.ButtonSensorDescription.TabIndex = 1
        Me.ButtonSensorDescription.Text = "SensorDescription"
        Me.ButtonSensorDescription.UseVisualStyleBackColor = True
        '
        'ButtonRefresh
        '
        Me.ButtonRefresh.Location = New System.Drawing.Point(16, 19)
        Me.ButtonRefresh.Name = "ButtonRefresh"
        Me.ButtonRefresh.Size = New System.Drawing.Size(58, 22)
        Me.ButtonRefresh.TabIndex = 0
        Me.ButtonRefresh.Text = "Refresh"
        Me.ButtonRefresh.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.ButtonExecuteSupportedAction)
        Me.GroupBox3.Controls.Add(Me.ButtonLoadSupportedActions)
        Me.GroupBox3.Controls.Add(Me.ListBoxSupportedActions)
        Me.GroupBox3.Location = New System.Drawing.Point(12, 561)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(376, 100)
        Me.GroupBox3.TabIndex = 54
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Supported Actions"
        '
        'ListBoxSupportedActions
        '
        Me.ListBoxSupportedActions.FormattingEnabled = True
        Me.ListBoxSupportedActions.Location = New System.Drawing.Point(6, 19)
        Me.ListBoxSupportedActions.Name = "ListBoxSupportedActions"
        Me.ListBoxSupportedActions.Size = New System.Drawing.Size(292, 69)
        Me.ListBoxSupportedActions.TabIndex = 0
        '
        'ButtonLoadSupportedActions
        '
        Me.ButtonLoadSupportedActions.Location = New System.Drawing.Point(304, 19)
        Me.ButtonLoadSupportedActions.Name = "ButtonLoadSupportedActions"
        Me.ButtonLoadSupportedActions.Size = New System.Drawing.Size(66, 23)
        Me.ButtonLoadSupportedActions.TabIndex = 1
        Me.ButtonLoadSupportedActions.Text = "load"
        Me.ButtonLoadSupportedActions.UseVisualStyleBackColor = True
        '
        'ButtonExecuteSupportedAction
        '
        Me.ButtonExecuteSupportedAction.Location = New System.Drawing.Point(304, 48)
        Me.ButtonExecuteSupportedAction.Name = "ButtonExecuteSupportedAction"
        Me.ButtonExecuteSupportedAction.Size = New System.Drawing.Size(66, 23)
        Me.ButtonExecuteSupportedAction.TabIndex = 2
        Me.ButtonExecuteSupportedAction.Text = "execute"
        Me.ButtonExecuteSupportedAction.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(400, 680)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.labelDriverId)
        Me.Controls.Add(Me.buttonConnect)
        Me.Controls.Add(Me.buttonChoose)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents labelDriverId As System.Windows.Forms.Label
    Private WithEvents buttonConnect As System.Windows.Forms.Button
    Private WithEvents buttonChoose As System.Windows.Forms.Button
    Friend WithEvents Label1 As Label
    Friend WithEvents TextBoxCloudCover As TextBox
    Friend WithEvents ButtonGetCloudCover As Button
    Friend WithEvents ButtonGetDewPoint As Button
    Friend WithEvents ButtonGetHumidity As Button
    Friend WithEvents ButtonGetPressure As Button
    Friend WithEvents ButtonGetRainRate As Button
    Friend WithEvents ButtonGetSkyBrightness As Button
    Friend WithEvents ButtonGetSkyQuality As Button
    Friend WithEvents ButtonGetSkyTemperature As Button
    Friend WithEvents ButtonGetStarFWHM As Button
    Friend WithEvents ButtonGetTemperature As Button
    Friend WithEvents ButtonGetWindDirection As Button
    Friend WithEvents ButtonGetWindGust As Button
    Friend WithEvents ButtonGetWindSpeed As Button
    Friend WithEvents TextBoxDewPoint As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents TextBoxHumidity As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents TextBoxPressure As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents TextBoxRainRate As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents TextBoxSkyBrightness As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents TextBoxSkyQuality As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents TextBoxSkyTemperature As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents TextBoxStarFWHM As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents TextBoxTemperature As TextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents TextBoxWindDirection As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents TextBoxWindGust As TextBox
    Friend WithEvents Label12 As Label
    Friend WithEvents TextBoxWindSpeed As TextBox
    Friend WithEvents Label13 As Label
    Friend WithEvents ButtonGetAveragePeriod As Button
    Friend WithEvents TextBoxAveragePeriod As TextBox
    Friend WithEvents Label15 As Label
    Friend WithEvents ButtonSetAveragePeriod As Button
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents ButtonTimeSinceLastUpdate As Button
    Friend WithEvents ButtonSensorDescription As Button
    Friend WithEvents ButtonRefresh As Button
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents ButtonExecuteSupportedAction As Button
    Friend WithEvents ButtonLoadSupportedActions As Button
    Friend WithEvents ListBoxSupportedActions As ListBox
End Class
