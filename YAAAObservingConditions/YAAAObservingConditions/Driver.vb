'tabs=4
' --------------------------------------------------------------------------------
' TODO fill in this information for your driver, then remove this line!
'
' ASCOM ObservingConditions driver for YAAADevice
'
' Description:	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam 
'				nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam 
'				erat, sed diam voluptua. At vero eos et accusam et justo duo 
'				dolores et ea rebum. Stet clita kasd gubergren, no sea takimata 
'				sanctus est Lorem ipsum dolor sit amet.
'
' Implements:	ASCOM ObservingConditions interface version: 1.0
' Author:		(XXX) Your N. Here <your@email.here>
'
' Edit Log:
'
' Date			Who	Vers	Description
' -----------	---	-----	-------------------------------------------------------
' dd-mmm-yyyy	XXX	1.0.0	Initial edit, from ObservingConditions template
' ---------------------------------------------------------------------------------
'
'
' Your driver's ID is ASCOM.YAAADevice.ObservingConditions
'
' The Guid attribute sets the CLSID for ASCOM.DeviceName.ObservingConditions
' The ClassInterface/None addribute prevents an empty interface called
' _ObservingConditions from being created and used as the [default] interface
'

' This definition is used to select code that's only applicable for one device type
#Const Device = "ObservingConditions"

Imports ASCOM
Imports ASCOM.Astrometry
Imports ASCOM.Astrometry.AstroUtils
Imports ASCOM.DeviceInterface
Imports ASCOM.Utilities

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Globalization
Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Runtime.Serialization
Imports System.Text
Imports System.Xml

<Guid("bc6f23d8-946c-4138-a3f5-f160755214e6")>
<ClassInterface(ClassInterfaceType.None)>
Public Class ObservingConditions

    ' The Guid attribute sets the CLSID for ASCOM.YAAADevice.ObservingConditions
    ' The ClassInterface/None addribute prevents an empty interface called
    ' _YAAADevice from being created and used as the [default] interface

    ' TODO Replace the not implemented exceptions with code to implement the function or
    ' throw the appropriate ASCOM exception.
    '
    Implements IObservingConditions

    '
    ' Driver ID and descriptive string that shows in the Chooser
    '
    Friend Shared driverID As String = "ASCOM.YAAADevice.ObservingConditions"
    Private Shared driverDescription As String = "YAAADevice ObservingConditions"

    Enum connection_type_t
        SERIAL = 0
        NETWORK = 1
    End Enum

    Enum serial_error_code_t
        ERROR_NONE = 0      'not necessary?
        ERROR_UNKNOWN_DEVICE = 1
        ERROR_UNKNOWN_CMD = 2
        ERROR_UNKNOWN_CMD_TYPE = 4
        ERROR_INVALID_CMD_TYPE = 8
        ERROR_INVALID_PARAMETER = 16
        ERROR_MISSING_PARAMETER = 32
        ERROR_UNKNOWN_OPTION = 64
        ERROR_DEVICE_UNAVAILABLE = 128
        ERROR_DEVICE_LOCKED = 256       'command not accepted because device has been locked for local control.
    End Enum

    Enum condition_t
        CONDITION_CLOUD_COVER = 0
        CONDITION_DEW_POINT = 1
        CONDITION_HUMIDITY = 2
        CONDITION_PRESSURE = 3
        CONDITION_RAIN = 4
        CONDITION_RAIN_RATE = 5
        CONDITION_SKY_BRIGHTNESS = 6
        CONDITION_SKY_QUALITY = 7
        CONDITION_SKY_TEMPERATURE = 8
        CONDITION_STAR_FWHM = 9
        CONDITION_TEMPERATURE = 10
        CONDITION_WIND_DIRECTION = 11
        CONDITION_WIND_GUST = 12
        CONDITION_WIND_SPEED = 13
        CONDITION_LIGHTNING = 14
        CONDITION_UNKNOWN = 255
    End Enum

#Region "Constants used for profile persistence"
    'Friend Shared comPortProfileName As String = "COM Port" 'Constants used for Profile persistence
    'Friend Shared traceStateProfileName As String = "Trace Level"

    Private Structure observingConditionsProfileNames
        Public Shared traceStateProfileName As String = "Trace Level"

        Public Shared connectionTypeProfileName As String = "Connection Type"

        Public Shared comPortProfileName As String = "COM Port"
        Public Shared baudRateProfileName As String = "Baud Rate"

        Public Shared ipAddressProfileName As String = "IP Address"
        Public Shared portProfileName As String = "Port"

        Public Shared logSupportProfileName As String = "Log Support"
        Public Shared logEnabledProfileName As String = "Log Enabled"
        Public Shared logFieldDelimiterProfileName As String = "Log Field Delimiter"
        Public Shared logIntervalProfileName As String = "Log Interval"

        Public Shared averagePeriodProfileName As String = "Average Period"

        Public Shared sensorTypeProfileName As String = "Sensor Type"
        Public Shared sensorDeviceProfileName As String = "Sensor Device"
        Public Shared sensorUnitProfileName As String = "Sensor Unit"
        Public Shared sensorAveragingSamplesProfileName As String = "Averaging Samples"
        Public Shared sensorParametersProfileName As String = "Parameters"
    End Structure

    Public Shared settingsStoreDirectory = YAAASharedClassLibrary.YAAASETTINGS_DIRECTORY & "/YAAAObservingConditions"

    Friend Shared NUM_SENSORS As Integer = 15       'number of sensors

    Private SET_CONNECTED_DELAY As Integer = 5000

    Private TIMEOUT_SERIAL As Integer = 5000
    Private TIMEOUT_TCP As Integer = 5000

    Private TIMEOUT_MEASURE As UInteger = 2000      '2000 ms timeout for starting measurements.
    Private TIMEOUT_HASVALUE As UInteger = 5000     '5000 ms timeout for completing measurements.
#End Region

#Region "Default values"

    Private Structure observingConditionsDefaultValues
        Public Shared traceStateDefault As Boolean = True

        Public Shared connectionTypeDefault As connection_type_t = connection_type_t.SERIAL

        Public Shared comPortDefault As String = "COM1"
        Public Shared baudRateDefault As Integer = 9600

        Public Shared ipAddressDefault As String = "192.168.0.103"
        Public Shared portDefault As Integer = 23

        Public Shared logSupportDefault = False
        Public Shared logEnabledDefault = False
        Public Shared logFieldDelimiterDefault = Chr(44)
        Public Shared logIntervalDefault = 10000

        Public Shared automaticMeasurementInterval = 0
        Public Shared averageSamplesDefault = 5
        Public Shared averagePeriodDefault As UInt32 = 600000
    End Structure

#End Region

#Region "Variables to hold the currrent device configuration"

    <DataContract>
    Friend Structure observingConditionsDriverSettings
        Implements ICloneable

        <DataMember()>
        Public Shared trace_state_ As Boolean

        <DataMember()>
        Public connection_type_ As connection_type_t

        <DataMember()>
        Public com_port_ As String
        <DataMember()>
        Public baud_rate_ As Integer

        <DataMember()>
        Public ip_address_ As String
        <DataMember()>
        Public port_ As Integer

        <DataMember()>
        Public observing_conditions_settings_ As observingConditionsSettings

        Public Sub New(Optional initialize As Boolean = True)
            trace_state_ = observingConditionsDefaultValues.traceStateDefault

            connection_type_ = observingConditionsDefaultValues.connectionTypeDefault

            com_port_ = observingConditionsDefaultValues.comPortDefault
            baud_rate_ = observingConditionsDefaultValues.baudRateDefault

            ip_address_ = observingConditionsDefaultValues.ipAddressDefault
            port_ = observingConditionsDefaultValues.portDefault

            observing_conditions_settings_ = New observingConditionsSettings(True)
        End Sub

        Public Function Clone() As Object Implements ICloneable.Clone
            Dim return_value As observingConditionsDriverSettings = New observingConditionsDriverSettings(True)

            return_value.connection_type_ = connection_type_
            return_value.com_port_ = com_port_
            return_value.baud_rate_ = baud_rate_
            return_value.ip_address_ = ip_address_
            return_value.port_ = port_

            return_value.observing_conditions_settings_ = observing_conditions_settings_.Clone()

            Return return_value
        End Function
    End Structure

    <DataContract>
    Friend Structure observingConditionsSettings
        Implements ICloneable

        <DataMember>
        Public log_csv_file_ As Boolean         'true to initialize a log file.
        <DataMember>
        Public log_csv_enabled_ As Boolean      'default log mode (enabled / disabled)
        <DataMember>
        Public log_csv_delimiter_ As Char       'cell delimiter for .csv files
        <DataMember>
        Public log_csv_interval_ As UInt32      'interval between successive logs to the csv file, in ms


        <DataMember()>
        Public average_period_ As UInt32        'average period in ms

        <DataMember()>
        Public sensors_ As SensorConfiguration()

        Public Sub New(Optional initialize As Boolean = True)
            If Not initialize Then
                Exit Sub
            End If

            log_csv_file_ = observingConditionsDefaultValues.logSupportDefault
            log_csv_enabled_ = observingConditionsDefaultValues.logEnabledDefault
            log_csv_delimiter_ = observingConditionsDefaultValues.logFieldDelimiterDefault
            log_csv_interval_ = observingConditionsDefaultValues.logIntervalDefault

            average_period_ = observingConditionsDefaultValues.averagePeriodDefault

            ReDim sensors_(NUM_SENSORS - 1)

            For i As Integer = 0 To sensors_.Length - 1
                sensors_(i).sensor_ = New YAAASharedClassLibrary.YAAASensor(True)
            Next
        End Sub

        Public Function Clone() As Object Implements ICloneable.Clone
            Dim return_value As observingConditionsSettings = New observingConditionsSettings(True)

            return_value.log_csv_file_ = log_csv_file_
            return_value.log_csv_enabled_ = log_csv_enabled_
            return_value.log_csv_delimiter_ = log_csv_delimiter_
            return_value.log_csv_interval_ = log_csv_interval_

            return_value.average_period_ = average_period_

            ReDim return_value.sensors_(sensors_.Length - 1)

            Array.Copy(sensors_, return_value.sensors_, sensors_.Length)

            Return return_value
        End Function
    End Structure

    <DataContract>
    Public Structure SensorConfiguration
        Implements ICloneable

        <DataMember()>
        Public sensor_ As YAAASharedClassLibrary.YAAASensor

        <DataMember()>
        Public automatic_measurement_interval_ As UInt16
        <DataMember()>
        Public average_samples_ As Byte

        Public Function Clone() As Object Implements ICloneable.Clone
            Dim return_value As SensorConfiguration = New SensorConfiguration

            return_value.sensor_ = sensor_.Clone()
            return_value.automatic_measurement_interval_ = automatic_measurement_interval_
            return_value.average_samples_ = average_samples_

            Return return_value
        End Function

        Public Sub New(Optional initialize As Boolean = True)
            If Not initialize Then
                Exit Sub
            End If

            sensor_ = New YAAASharedClassLibrary.YAAASensor(True)

            automatic_measurement_interval_ = observingConditionsDefaultValues.automaticMeasurementInterval
            average_samples_ = observingConditionsDefaultValues.averageSamplesDefault
        End Sub

        Public Sub New(settings As String)
            Dim separators() As String = {" ", ","}

            'split string
            Dim strArr() As String = settings.Split(separators, StringSplitOptions.RemoveEmptyEntries)

            '15 text blocks are expected
            If strArr.Length <> 15 Then
                Throw New Exception
            End If

            'extract string describing YAAASensor settings from string
            Dim sensorSettings As String = ""
            For i As Integer = 0 To 12
                sensorSettings += strArr(i)
                sensorSettings += " "
            Next

            sensor_ = New YAAASharedClassLibrary.YAAASensor(sensorSettings)

            automatic_measurement_interval_ = CUInt(strArr(13))
            average_samples_ = CByte(strArr(14))
        End Sub

        'converts a temperatureSensorSettings struct to a string that can be sent to the Arduino.
        Public Function toArduinoString() As String
            Dim return_value As String = ""

            return_value &= sensor_.toArduinoString()
            return_value &= " "

            return_value &= Convert.ToInt32(automatic_measurement_interval_)
            return_value &= " "
            return_value &= Convert.ToInt32(average_samples_)

            Return return_value
        End Function

        Public Overloads Shared Operator =(ByVal s1 As SensorConfiguration, ByVal s2 As SensorConfiguration) As Boolean
            Dim parameters_equal As Boolean = True

            parameters_equal = parameters_equal AndAlso s1.sensor_ = s2.sensor_
            parameters_equal = parameters_equal AndAlso s1.automatic_measurement_interval_ = s2.automatic_measurement_interval_
            parameters_equal = parameters_equal AndAlso s1.average_samples_ = s2.average_samples_

            Return parameters_equal
        End Operator

        Public Overloads Shared Operator <>(ByVal s1 As SensorConfiguration, ByVal s2 As SensorConfiguration) As Boolean
            Return Not s1 = s2
        End Operator
    End Structure

    Private observing_conditions_driver_settings_ As observingConditionsDriverSettings
    Private observing_conditions_driver_settings_EEPROM_ As observingConditionsDriverSettings

    Friend EEPROM_force_write_ As Boolean = False

#End Region

    'Friend Shared comPort As String ' Variables to hold the currrent device configuration
    'Friend Shared traceState As Boolean

    Private utilities As Util ' Private variable to hold an ASCOM Utilities object
    Private astroUtilities As AstroUtils ' Private variable to hold an AstroUtils object to provide the Range method
    Private TL As TraceLogger ' Private variable to hold the trace logger object (creates a diagnostic log file with information that you specify)

    Friend serial_connection_ As ASCOM.Utilities.Serial

    Friend tcp_connection_ As Net.Sockets.TcpClient

    '
    ' Constructor - Must be public for COM registration!
    '
    Public Sub New()
        TL = New TraceLogger("", "YAAAObservingConditions")

        TL.Enabled = False

        observing_conditions_driver_settings_ = New observingConditionsDriverSettings(True)
        observing_conditions_driver_settings_EEPROM_ = New observingConditionsDriverSettings(True)

        ReadProfile() ' Read device configuration from the ASCOM Profile store

        TL.Enabled = observingConditionsDriverSettings.trace_state_

        TL.LogMessage("ObservingConditions", "Starting initialisation")

        utilities = New Util() ' Initialise util object
        astroUtilities = New AstroUtils 'Initialise new astro utiliites object

        'TODO: Implement your additional construction here
        serial_connection_ = New ASCOM.Utilities.Serial
        serial_connection_.DTREnable = False

        TL.LogMessage("ObservingConditions", "Completed initialisation")
    End Sub

    '
    ' PUBLIC COM INTERFACE IObservingConditions IMPLEMENTATION
    '

#Region "Common properties and methods"
    ''' <summary>
    ''' Displays the Setup Dialog form.
    ''' If the user clicks the OK button to dismiss the form, then
    ''' the new settings are saved, otherwise the old values are reloaded.
    ''' THIS IS THE ONLY PLACE WHERE SHOWING USER INTERFACE IS ALLOWED!
    ''' </summary>
    Public Sub SetupDialog() Implements IObservingConditions.SetupDialog
        ' consider only showing the setup dialog if not connected
        ' or call a different dialog if connected
        If IsConnected Then
            System.Windows.Forms.MessageBox.Show("Already connected, just press OK")
        End If

        Using F As SetupDialogForm = New SetupDialogForm(Me)
            Dim result As System.Windows.Forms.DialogResult = F.ShowDialog()
            If result = DialogResult.OK Then
                WriteProfile() ' Persist device configuration values to the ASCOM Profile store
            End If
        End Using
    End Sub

    Public ReadOnly Property SupportedActions() As ArrayList Implements IObservingConditions.SupportedActions
        Get
            TL.LogMessage("SupportedActions Get", "Returning empty arraylist")
            Dim actions = New ArrayList()

            actions.Add("Get_Lightning_Distance")       'returns distance to last registered lightning

            Return actions
        End Get
    End Property

    Public Function Action(ByVal ActionName As String, ByVal ActionParameters As String) As String Implements IObservingConditions.Action
        Select Case (ActionName)
            Case "Get_Lightning_Distance"
                Return LightningDistance()
        End Select

        Throw New ActionNotImplementedException("Action " & ActionName & " is not supported by this driver")
    End Function

    Public Sub CommandBlind(ByVal Command As String, Optional ByVal Raw As Boolean = False) Implements IObservingConditions.CommandBlind
        CheckConnected("CommandBlind")
        ' Call CommandString and return as soon as it finishes
        Me.CommandString(Command, Raw)
        ' or
        Throw New MethodNotImplementedException("CommandBlind")
    End Sub

    Public Function CommandBool(ByVal Command As String, Optional ByVal Raw As Boolean = False) As Boolean _
        Implements IObservingConditions.CommandBool
        CheckConnected("CommandBool")
        Dim ret As String = CommandString(Command, Raw)
        ' TODO decode the return string and return true or false
        ' or
        Throw New MethodNotImplementedException("CommandBool")
    End Function

    Public Function CommandString(ByVal Command As String, Optional ByVal Raw As Boolean = False) As String _
        Implements IObservingConditions.CommandString
        CheckConnected("CommandString")
        ' it's a good idea to put all the low level communication with the device here,
        ' then all communication calls this function
        ' you need something to ensure that only one command is in progress at a time
        Throw New MethodNotImplementedException("CommandString")
    End Function

    Public Property Connected() As Boolean Implements IObservingConditions.Connected
        Get
            Dim cn As Boolean = IsConnected
            TL.LogMessage("Connected Get", cn.ToString())
            Return cn
        End Get

        Set(value As Boolean)
            TL.LogMessage("Connected Set", value.ToString())

            If value Then
                'connect to hardware
                IsConnected = True

                'todo: allow Arduino to send wakeup message before attempting to contact Arduino?

                Dim response As String

                Try
                    response = sendCommand("CONNECTED,GET")
                Catch Exc As System.TimeoutException
                    TL.LogMessage("Connected Set", "Error: Timeout, check connection")
                    Throw New DriverException(Exc.Message, Exc)
                Catch Exc As ASCOM.Utilities.Exceptions.SerialPortInUseException
                    TL.LogMessage("Connected Set", "Error: unable to acquire the serial port")
                    Throw New DriverException(Exc.Message, Exc)
                Catch Exc As ASCOM.NotConnectedException
                    TL.LogMessage("Connected Set", "Error: serial port is not connected")
                    Throw New DriverException(Exc.Message, Exc)
                Catch Exc As Exception
                    TL.LogMessage("Connected Set", "Error: unknown error, check inner exception")   'TODO timeout exceptions are acutally caught here
                    Throw New DriverException(Exc.Message, Exc)
                End Try

                response = response.Replace("CONNECTED,", "").Trim

                If Not IsNumeric(response) Then
                    TL.LogMessage("Connected Set", "Error: could not check connection state because response contained non-numeric elements: " & response)
                    Throw New DriverException("could not check connection state because response contained non-numeric elements: " & response)
                End If

                If CInt(response).Equals(0) Then
                    TL.LogMessage("Connected Set", "Error: Arduino responding, but Observing Conditions interface is not enabled")

                    'disconnect from hardware
                    IsConnected = False

                    Throw New ASCOM.DriverException("Connected Set Error: Arduino responding, but Observing Conditions interface is not enabled")
                ElseIf CInt(response).Equals(1) Then
                    TL.LogMessage("Connected Set", "Successfully connected")
                Else
                    TL.LogMessage("Connected Set", "Error: unknown Response: " & response)
                    Throw New ASCOM.DriverException("Connected Set Error: unknown Response: " & response)
                End If
            Else
                IsConnected = False
            End If
        End Set
    End Property

    Public ReadOnly Property Description As String Implements IObservingConditions.Description
        Get
            ' this pattern seems to be needed to allow a public property to return a private field
            Dim d As String = driverDescription
            TL.LogMessage("Description Get", d)
            Return d
        End Get
    End Property

    Public ReadOnly Property DriverInfo As String Implements IObservingConditions.DriverInfo
        Get
            Dim m_version As Version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version
            ' TODO customise this driver description
            Dim s_driverInfo As String = "Information about the driver itself. Version: " + m_version.Major.ToString() + "." + m_version.Minor.ToString()
            TL.LogMessage("DriverInfo Get", s_driverInfo)
            Return s_driverInfo
        End Get
    End Property

    Public ReadOnly Property DriverVersion() As String Implements IObservingConditions.DriverVersion
        Get
            ' Get our own assembly and report its version number
            TL.LogMessage("DriverVersion Get", Reflection.Assembly.GetExecutingAssembly.GetName.Version.ToString(2))
            Return Reflection.Assembly.GetExecutingAssembly.GetName.Version.ToString(2)
        End Get
    End Property

    Public ReadOnly Property InterfaceVersion() As Short Implements IObservingConditions.InterfaceVersion
        Get
            TL.LogMessage("InterfaceVersion Get", "1")
            Return 1
        End Get
    End Property

    Public ReadOnly Property Name As String Implements IObservingConditions.Name
        Get
            Dim s_name As String = "YAAADevice ObservingConditions Driver"
            TL.LogMessage("Name Get", s_name)
            Return s_name
        End Get
    End Property

    Public Sub Dispose() Implements IObservingConditions.Dispose
        ' Clean up the tracelogger and util objects
        TL.Enabled = False
        TL.Dispose()
        TL = Nothing
        utilities.Dispose()
        utilities = Nothing
        astroUtilities.Dispose()
        astroUtilities = Nothing
    End Sub

#End Region

#Region "IObservingConditions Implementation"

    Public Property AveragePeriod() As Double Implements IObservingConditions.AveragePeriod
        Get
            CheckConnected("AveragePeriod()")

            Dim return_value As Double = 0.0

            Dim response As String = ""
            response = sendCommand("AVERAGE_PERIOD,GET")
            response = response.Replace("AVERAGE_PERIOD,", "").Trim

            return_value = Val(response) / 3600000.0        'convert ms to hours

            TL.LogMessage("AveragePeriod", return_value)

            Return return_value
        End Get
        Set(value As Double)
            CheckConnected("AveragePeriod()")

            If value < 0 Then
                TL.LogMessage("AveragePeriod()", "invalid value: " & value)
                Throw New ASCOM.InvalidValueException("invalid value: " & value)
            End If

            Dim response As String = ""

            response = sendCommand("AVERAGE_PERIOD,SET," & CUInt(value * 3600000.0))    'convert hours to ms
            response = response.Replace("AVERAGE_PERIOD,", "").Trim

            If Not CBool(response) Then
                TL.LogMessage("AveragePeriod", "invalid value: " & value)
                Throw New ASCOM.InvalidValueException("invalid value: " & value)
            End If
        End Set
    End Property

    Public ReadOnly Property CloudCover() As Double Implements IObservingConditions.CloudCover
        Get
            CheckConnected("Cloudcover")

            Dim return_value As Double = Val(getValueOf(condition_t.CONDITION_CLOUD_COVER))

            TL.LogMessage("CloudCover", return_value)

            Return return_value
        End Get
    End Property

    Public ReadOnly Property DewPoint() As Double Implements IObservingConditions.DewPoint
        Get
            CheckConnected("DewPoint")



            Dim return_value As Double = 0.0

            If checkSensorEnabled(condition_t.CONDITION_DEW_POINT) Then
                'if the dew point property is enabled, the humidity property must be implemented as well. 
                If Not checkSensorEnabled(condition_t.CONDITION_HUMIDITY) AndAlso Not checkSensorEnabled(condition_t.CONDITION_TEMPERATURE) Then
                    TL.LogMessage("Dew Point", "Dew Point Sensor is enabled, but no Humidity or Temperature Sensor is enabled. A Humidity or Temperature Sensor must be enabled as well to satisfy ASCOM specifications.")
                    Throw New ASCOM.PropertyNotImplementedException("DewPoint")
                End If

                return_value = Val(getValueOf(condition_t.CONDITION_DEW_POINT))   'read the dew point if a dew point sensor is enabled.
            ElseIf checkSensorEnabled(condition_t.CONDITION_HUMIDITY) AndAlso checkSensorEnabled(condition_t.CONDITION_TEMPERATURE) Then
                return_value = utilities.Humidity2DewPoint(Humidity, Temperature)   'if no dew point sensor is enabled, read temperature and humidity and calculate dew point
            Else
                TL.LogMessage("Dew Point", "No sensor(s) enabled to measure Dew Point. Either a Dew Point Sensor or both a Humidity and a Temperature Sensor are required.")
                Throw New ASCOM.PropertyNotImplementedException("DewPoint")
            End If

            TL.LogMessage("DewPoint", return_value)

            Return return_value
        End Get
    End Property

    Public ReadOnly Property Humidity() As Double Implements IObservingConditions.Humidity
        Get
            CheckConnected("Humidity")

            Dim return_value As Double = 0.0

            If checkSensorEnabled(condition_t.CONDITION_HUMIDITY) Then
                'if the dew point property is enabled, the humidity property must be implemented as well. 
                If Not checkSensorEnabled(condition_t.CONDITION_DEW_POINT) AndAlso Not checkSensorEnabled(condition_t.CONDITION_TEMPERATURE) Then
                    TL.LogMessage("Dew Point", "Humidity Sensor is enabled, but no Dew Point or Temperature Sensor is enabled. A Dew Poitn or Temperature Sensor must be enabled as well to satisfy ASCOM specifications.")
                    Throw New ASCOM.PropertyNotImplementedException("DewPoint")
                End If

                return_value = Val(getValueOf(condition_t.CONDITION_HUMIDITY))    'read the humidity if a humidity sensor is enabled.
            ElseIf checkSensorEnabled(condition_t.CONDITION_DEW_POINT) AndAlso checkSensorEnabled(condition_t.CONDITION_TEMPERATURE) Then
                return_value = utilities.DewPoint2Humidity(DewPoint, Temperature)   'if no humidity sensor is enabled, read temperature and dew point and calculate humidity
            Else
                TL.LogMessage("Humidity", "No sensor(s) enabled to measure Humidity. Either a Humidity Sensor or both a Dew Point and a Temperature Sensor are required.")
                Throw New ASCOM.PropertyNotImplementedException("Humidity")
            End If

            TL.LogMessage("Humidity", return_value)

            Return return_value
        End Get
    End Property

    Public ReadOnly Property Pressure() As Double Implements IObservingConditions.Pressure
        Get
            CheckConnected("Pressure")

            Dim return_value As Double = Val(getValueOf(condition_t.CONDITION_PRESSURE))

            TL.LogMessage("Pressure", return_value)

            Return return_value
        End Get
    End Property

    Public ReadOnly Property RainRate() As Double Implements IObservingConditions.RainRate
        Get
            CheckConnected("RainRate")

            Dim return_value As Double = 0.0

            If checkSensorEnabled(condition_t.CONDITION_RAIN_RATE) Then

                return_value = Val(getValueOf(condition_t.CONDITION_RAIN_RATE))
            Else
                return_value = Val(getValueOf(condition_t.CONDITION_RAIN))
            End If

            TL.LogMessage("RainRate", return_value)

            Return return_value
        End Get
    End Property

    Public ReadOnly Property SkyBrightness() As Double Implements IObservingConditions.SkyBrightness
        Get
            CheckConnected("SkyBrightness")

            Dim return_value As Double = Val(getValueOf(condition_t.CONDITION_SKY_BRIGHTNESS))

            TL.LogMessage("SkyBrightness", return_value)

            Return return_value
        End Get
    End Property

    Public ReadOnly Property SkyQuality() As Double Implements IObservingConditions.SkyQuality
        Get
            CheckConnected("SkyQuality")

            Dim return_value As Double = Val(getValueOf(condition_t.CONDITION_SKY_QUALITY))

            TL.LogMessage("SkyQuality", return_value)

            Return return_value
        End Get
    End Property

    Public ReadOnly Property SkyTemperature() As Double Implements IObservingConditions.SkyTemperature
        Get
            CheckConnected("SkyTemperature")

            Dim return_value As Double = Val(getValueOf(condition_t.CONDITION_SKY_TEMPERATURE))

            TL.LogMessage("SkyTemperature", return_value)

            Return return_value
        End Get
    End Property

    Public ReadOnly Property StarFWHM() As Double Implements IObservingConditions.StarFWHM
        Get
            CheckConnected("StarFWHM")

            Dim return_value As Double = Val(getValueOf(condition_t.CONDITION_STAR_FWHM))

            TL.LogMessage("StarFWHM", return_value)

            Return return_value
        End Get
    End Property

    Public ReadOnly Property Temperature() As Double Implements IObservingConditions.Temperature
        Get
            CheckConnected("Temperature")

            Dim return_value As Double = Val(getValueOf(condition_t.CONDITION_TEMPERATURE))

            TL.LogMessage("Temperature", return_value)

            Return return_value
        End Get
    End Property

    Public ReadOnly Property WindDirection() As Double Implements IObservingConditions.WindDirection
        Get
            CheckConnected("WindDirection")

            Dim return_value As Double = Val(getValueOf(condition_t.CONDITION_WIND_DIRECTION))

            TL.LogMessage("WindDirection", return_value)

            'discussion on this requirement is ongoing nad therefore is currently disabled.
            ''as per ASCOM specifications, if the wind speed is 0.0m/s the wind direction must be reported as 0.0 degrees.
            'If checkSensorEnabled(condition_t.CONDITION_WIND_SPEED) AndAlso WindSpeed = 0.0 Then
            '    TL.LogMessage("WindDirection", "Wind Speed is 0.0, reporting Wind Direction as 0.0 degrees as per ASCOM specifications.")
            '    return_value = 0.0
            'End If

            Return return_value
        End Get
    End Property

    Public ReadOnly Property WindGust() As Double Implements IObservingConditions.WindGust
        Get
            CheckConnected("WindGust")

            Dim return_value As Double = Val(getValueOf(condition_t.CONDITION_WIND_GUST))

            TL.LogMessage("WindGust", return_value)

            Return return_value
        End Get
    End Property

    Public ReadOnly Property WindSpeed() As Double Implements IObservingConditions.WindSpeed
        Get
            CheckConnected("WindSpeed")

            Dim return_value As Double = Val(getValueOf(condition_t.CONDITION_WIND_SPEED))

            TL.LogMessage("WindSpeed", return_value)

            Return return_value
        End Get
    End Property

    Public Function TimeSinceLastUpdate(PropertyName As String) As Double Implements IObservingConditions.TimeSinceLastUpdate
        TL.LogMessage("TimeSinceLastUpdate()", PropertyName)

        Dim return_value As Double = 0.0
        Dim condition As condition_t = Nothing

        Select Case PropertyName.Trim.ToLowerInvariant
            Case "cloudcover"
                condition = condition_t.CONDITION_CLOUD_COVER
            Case "dewpoint"
                condition = condition_t.CONDITION_DEW_POINT
            Case "humidity"
                condition = condition_t.CONDITION_HUMIDITY
            Case "pressure"
                condition = condition_t.CONDITION_PRESSURE
            Case "rain"
                condition = condition_t.CONDITION_RAIN
            Case "rainrate"
                condition = condition_t.CONDITION_RAIN_RATE
            Case "skybrightness"
                condition = condition_t.CONDITION_SKY_BRIGHTNESS
            Case "skyquality"
                condition = condition_t.CONDITION_SKY_QUALITY
            Case "skytemperature"
                condition = condition_t.CONDITION_SKY_TEMPERATURE
            Case "starfwhm"
                condition = condition_t.CONDITION_STAR_FWHM
            Case "temperature"
                condition = condition_t.CONDITION_TEMPERATURE
            Case "winddirection"
                condition = condition_t.CONDITION_WIND_DIRECTION
            Case "windgust"
                condition = condition_t.CONDITION_WIND_GUST
            Case "windspeed"
                condition = condition_t.CONDITION_WIND_SPEED
            Case ""
                condition = condition_t.CONDITION_UNKNOWN
            Case Else
                Throw New ASCOM.InvalidValueException()
        End Select

        'dew point property needs special attention.
        If condition = condition_t.CONDITION_DEW_POINT Then
            If checkSensorEnabled(condition_t.CONDITION_DEW_POINT) Then
                If Not (checkSensorEnabled(condition_t.CONDITION_HUMIDITY) OrElse checkSensorEnabled(condition_t.CONDITION_TEMPERATURE)) Then
                    Throw New ASCOM.MethodNotImplementedException()
                End If
            Else
                If Not (checkSensorEnabled(condition_t.CONDITION_HUMIDITY) AndAlso checkSensorEnabled(condition_t.CONDITION_TEMPERATURE)) Then
                    Throw New ASCOM.MethodNotImplementedException()
                End If
            End If
        End If

        'humidity property needs special attention.
        If condition = condition_t.CONDITION_HUMIDITY Then
            If checkSensorEnabled(condition_t.CONDITION_HUMIDITY) Then
                If Not (checkSensorEnabled(condition_t.CONDITION_DEW_POINT) OrElse checkSensorEnabled(condition_t.CONDITION_TEMPERATURE)) Then
                    Throw New ASCOM.MethodNotImplementedException()
                End If
            Else
                If Not (checkSensorEnabled(condition_t.CONDITION_DEW_POINT) AndAlso checkSensorEnabled(condition_t.CONDITION_TEMPERATURE)) Then
                    Throw New ASCOM.MethodNotImplementedException()
                End If
            End If
        End If

        'rain rate property needs special attention.
        If condition = condition_t.CONDITION_RAIN_RATE Then
            If Not checkSensorEnabled(condition_t.CONDITION_RAIN_RATE) AndAlso Not checkSensorEnabled(condition_t.CONDITION_RAIN) Then
                Throw New ASCOM.MethodNotImplementedException()
            End If
        End If

        If condition = condition_t.CONDITION_DEW_POINT And Not checkSensorEnabled(condition_t.CONDITION_DEW_POINT) Then
            'read both times since last update, return the smaller one
            return_value = Math.Min(Val(getMeasurementAge(condition_t.CONDITION_DEW_POINT)), Val(getMeasurementAge(condition_t.CONDITION_TEMPERATURE))) / 1000.0

        ElseIf condition = condition_t.CONDITION_HUMIDITY And Not checkSensorEnabled(condition_t.CONDITION_HUMIDITY) Then
            'read both times since last update, return the smaller one
            return_value = Math.Min(Val(getMeasurementAge(condition_t.CONDITION_DEW_POINT)), Val(getMeasurementAge(condition_t.CONDITION_TEMPERATURE))) / 1000.0

        ElseIf condition = condition_t.CONDITION_RAIN_RATE And Not checkSensorEnabled(condition_t.CONDITION_RAIN_RATE) Then
            return_value = Val(getMeasurementAge(condition_t.CONDITION_RAIN)) / 1000.0
        Else
            If condition <> condition_t.CONDITION_UNKNOWN And Not checkSensorEnabled(condition) Then
                Throw New ASCOM.MethodNotImplementedException()
            End If

            return_value = Val(getMeasurementAge(condition)) / 1000.0
        End If

        TL.LogMessage("TimeSinceLastUpdate", "condition: " & condition.ToString & ": " & return_value & "s")

        Return return_value
    End Function

    Public Function SensorDescription(PropertyName As String) As String Implements IObservingConditions.SensorDescription
        TL.LogMessage("SensorDescription()", PropertyName)

        Dim condition As condition_t = condition_t.CONDITION_UNKNOWN

        Select Case PropertyName.Trim.ToLowerInvariant
            Case "averageperiod"
                Return "Average period in hours, immediate values are only available"
            Case "cloudcover"
                condition = condition_t.CONDITION_CLOUD_COVER
            Case "dewpoint"
                condition = condition_t.CONDITION_DEW_POINT
            Case "humidity"
                condition = condition_t.CONDITION_HUMIDITY
            Case "pressure"
                condition = condition_t.CONDITION_PRESSURE
            Case "rain"
                condition = condition_t.CONDITION_RAIN
            Case "rainrate"
                condition = condition_t.CONDITION_RAIN_RATE
            Case "skybrightness"
                condition = condition_t.CONDITION_SKY_BRIGHTNESS
            Case "skyquality"
                condition = condition_t.CONDITION_SKY_QUALITY
            Case "skytemperature"
                condition = condition_t.CONDITION_SKY_TEMPERATURE
            Case "starfwhm"
                condition = condition_t.CONDITION_STAR_FWHM
            Case "temperature"
                condition = condition_t.CONDITION_TEMPERATURE
            Case "winddirection"
                condition = condition_t.CONDITION_WIND_DIRECTION
            Case "windgust"
                condition = condition_t.CONDITION_WIND_GUST
            Case "windspeed"
                condition = condition_t.CONDITION_WIND_SPEED
        End Select
        If condition = condition_t.CONDITION_UNKNOWN Then
            Throw New ASCOM.InvalidValueException("name \'" & PropertyName & "\' is an invalid property name")
        End If

        If condition = condition_t.CONDITION_DEW_POINT Then
            With observing_conditions_driver_settings_.observing_conditions_settings_
                If .sensors_(condition_t.CONDITION_DEW_POINT).sensor_.isSensorEnabled And Not (.sensors_(condition_t.CONDITION_HUMIDITY).sensor_.isSensorEnabled Or .sensors_(condition_t.CONDITION_TEMPERATURE).sensor_.isSensorEnabled) Then
                    Throw New ASCOM.MethodNotImplementedException()
                End If
            End With
        End If

        If condition = condition_t.CONDITION_HUMIDITY Then
            With observing_conditions_driver_settings_.observing_conditions_settings_
                If .sensors_(condition_t.CONDITION_HUMIDITY).sensor_.isSensorEnabled And Not (.sensors_(condition_t.CONDITION_DEW_POINT).sensor_.isSensorEnabled Or .sensors_(condition_t.CONDITION_TEMPERATURE).sensor_.isSensorEnabled) Then
                    Throw New ASCOM.MethodNotImplementedException()
                End If
            End With
        End If


        Dim return_value As String = ""

        With observing_conditions_driver_settings_.observing_conditions_settings_.sensors_(condition).sensor_
            'dew point and humidity sensors need "special attention"
            If condition = condition_t.CONDITION_DEW_POINT Then

                If .isSensorEnabled() Then
                    return_value &= "Sensor type: "
                    return_value &= [Enum].Parse(GetType(YAAASharedClassLibrary.YAAASensor.sensor_type_t), .sensor_settings_.sensor_type_).ToString & ", "

                    return_value &= "Sensor device: "
                    return_value &= [Enum].Parse(GetType(YAAASharedClassLibrary.YAAASensor.sensor_dew_point_t), .sensor_settings_.sensor_device_).ToString & ", "

                    return_value &= "Sensor unit: "

                    If [Enum].IsDefined(GetType(YAAASharedClassLibrary.YAAASensor.unit_dew_point_t), CInt(.sensor_settings_.sensor_unit_)) Then
                        return_value &= [Enum].Parse(GetType(YAAASharedClassLibrary.YAAASensor.unit_dew_point_t), .sensor_settings_.sensor_unit_).ToString
                    Else
                        return_value &= "unknown unit" & .sensor_settings_.sensor_unit_
                    End If
                ElseIf observing_conditions_driver_settings_.observing_conditions_settings_.sensors_(condition_t.CONDITION_HUMIDITY).sensor_.isSensorEnabled AndAlso observing_conditions_driver_settings_.observing_conditions_settings_.sensors_(condition_t.CONDITION_TEMPERATURE).sensor_.isSensorEnabled Then
                    return_value &= "dew point is calculated using humidity and temperature sensor data."
                Else
                    Throw New ASCOM.MethodNotImplementedException()
                End If

            ElseIf condition = condition_t.CONDITION_HUMIDITY Then
                If .isSensorEnabled() Then
                    return_value &= "Sensor type: "
                    return_value &= [Enum].Parse(GetType(YAAASharedClassLibrary.YAAASensor.sensor_type_t), .sensor_settings_.sensor_type_).ToString & ", "

                    return_value &= "Sensor device: "
                    return_value &= [Enum].Parse(GetType(YAAASharedClassLibrary.YAAASensor.sensor_humidity_device_t), .sensor_settings_.sensor_device_).ToString & ", "

                    return_value &= "Sensor unit: "

                    If [Enum].IsDefined(GetType(YAAASharedClassLibrary.YAAASensor.unit_humidity_t), CInt(.sensor_settings_.sensor_unit_)) Then
                        return_value &= [Enum].Parse(GetType(YAAASharedClassLibrary.YAAASensor.unit_humidity_t), .sensor_settings_.sensor_unit_).ToString
                    Else
                        return_value &= "unknown unit" & .sensor_settings_.sensor_unit_
                    End If
                ElseIf observing_conditions_driver_settings_.observing_conditions_settings_.sensors_(condition_t.CONDITION_DEW_POINT).sensor_.isSensorEnabled AndAlso observing_conditions_driver_settings_.observing_conditions_settings_.sensors_(condition_t.CONDITION_TEMPERATURE).sensor_.isSensorEnabled Then
                    return_value &= "humdity is calculated using dew point and temperature sensor data."
                Else
                    Throw New ASCOM.MethodNotImplementedException()
                End If
            ElseIf condition = condition_t.CONDITION_RAIN_RATE And Not .isSensorEnabled Then
                return_value = SensorDescription("rain")
            Else
                'print information on sensors
                If .isSensorEnabled Then
                    Dim sensor_type As YAAASharedClassLibrary.YAAASensor.sensor_type_t

                    Dim sensor_device_type As System.Type = Nothing
                    Dim sensor_unit_type As System.Type = Nothing

                    sensor_type = [Enum].Parse(GetType(YAAASharedClassLibrary.YAAASensor.sensor_type_t), .sensor_settings_.sensor_type_)

                    Select Case sensor_type
                        Case YAAASharedClassLibrary.YAAASensor.sensor_type_t.SENSOR_BRIGHTNESS
                            sensor_device_type = GetType(YAAASharedClassLibrary.YAAASensor.sensor_brightness_device_t)
                            sensor_unit_type = GetType(YAAASharedClassLibrary.YAAASensor.unit_brightness_t)
                        Case YAAASharedClassLibrary.YAAASensor.sensor_type_t.SENSOR_CLOUD_COVER
                            sensor_device_type = GetType(YAAASharedClassLibrary.YAAASensor.sensor_cloud_coverage_t)
                            sensor_unit_type = GetType(YAAASharedClassLibrary.YAAASensor.unit_cloud_cover_t)
                        Case YAAASharedClassLibrary.YAAASensor.sensor_type_t.SENSOR_DEW_POINT
                            sensor_device_type = GetType(YAAASharedClassLibrary.YAAASensor.sensor_dew_point_t)
                            sensor_unit_type = GetType(YAAASharedClassLibrary.YAAASensor.unit_dew_point_t)
                        Case YAAASharedClassLibrary.YAAASensor.sensor_type_t.SENSOR_HUMIDITY
                            sensor_device_type = GetType(YAAASharedClassLibrary.YAAASensor.sensor_humidity_device_t)
                            sensor_unit_type = GetType(YAAASharedClassLibrary.YAAASensor.unit_humidity_t)
                        Case YAAASharedClassLibrary.YAAASensor.sensor_type_t.SENSOR_PRESSURE
                            sensor_device_type = GetType(YAAASharedClassLibrary.YAAASensor.sensor_pressure_device_t)
                            sensor_unit_type = GetType(YAAASharedClassLibrary.YAAASensor.unit_pressure_t)
                        Case YAAASharedClassLibrary.YAAASensor.sensor_type_t.SENSOR_RAIN
                            sensor_device_type = GetType(YAAASharedClassLibrary.YAAASensor.sensor_rain_device_t)
                            sensor_unit_type = GetType(YAAASharedClassLibrary.YAAASensor.unit_rain_t)
                        Case YAAASharedClassLibrary.YAAASensor.sensor_type_t.SENSOR_RAIN_RATE
                            sensor_device_type = GetType(YAAASharedClassLibrary.YAAASensor.sensor_rain_rate_device_t)
                            sensor_unit_type = GetType(YAAASharedClassLibrary.YAAASensor.unit_rain_rate_t)
                        Case YAAASharedClassLibrary.YAAASensor.sensor_type_t.SENSOR_SKY_QUALITY
                            sensor_device_type = GetType(YAAASharedClassLibrary.YAAASensor.sensor_sky_quality_t)
                            sensor_unit_type = GetType(YAAASharedClassLibrary.YAAASensor.unit_sky_quality_t)
                        Case YAAASharedClassLibrary.YAAASensor.sensor_type_t.SENSOR_STAR_FWHM
                            sensor_device_type = GetType(YAAASharedClassLibrary.YAAASensor.sensor_star_fwhm_t)
                            sensor_unit_type = GetType(YAAASharedClassLibrary.YAAASensor.unit_star_fwhm_t)
                        Case YAAASharedClassLibrary.YAAASensor.sensor_type_t.SENSOR_TEMPERATURE
                            sensor_device_type = GetType(YAAASharedClassLibrary.YAAASensor.sensor_temperature_device_t)
                            sensor_unit_type = GetType(YAAASharedClassLibrary.YAAASensor.unit_temperature_t)
                        Case YAAASharedClassLibrary.YAAASensor.sensor_type_t.SENSOR_WIND_DIRECTION
                            sensor_device_type = GetType(YAAASharedClassLibrary.YAAASensor.sensor_wind_direction_t)
                            sensor_unit_type = GetType(YAAASharedClassLibrary.YAAASensor.unit_wind_direction_t)
                        Case YAAASharedClassLibrary.YAAASensor.sensor_type_t.SENSOR_WIND_GUST
                            sensor_device_type = GetType(YAAASharedClassLibrary.YAAASensor.sensor_wind_gust_t)
                            sensor_unit_type = GetType(YAAASharedClassLibrary.YAAASensor.unit_wind_speed_t)
                        Case YAAASharedClassLibrary.YAAASensor.sensor_type_t.SENSOR_WIND_SPEED
                            sensor_device_type = GetType(YAAASharedClassLibrary.YAAASensor.sensor_wind_speed_t)
                            sensor_unit_type = GetType(YAAASharedClassLibrary.YAAASensor.unit_wind_speed_t)
                    End Select

                    return_value &= "Sensor type: "
                    return_value &= [Enum].Parse(GetType(YAAASharedClassLibrary.YAAASensor.sensor_type_t), .sensor_settings_.sensor_type_).ToString & ", "

                    return_value &= "Sensor device: "
                    return_value &= [Enum].Parse(sensor_device_type, .sensor_settings_.sensor_device_).ToString & ", "

                    return_value &= "Sensor unit: "
                    return_value &= [Enum].Parse(sensor_unit_type, .sensor_settings_.sensor_unit_).ToString & ", "
                Else
                    Throw New ASCOM.MethodNotImplementedException()
                End If
            End If

            return_value &= "Average Samples: "
            'return_value &= .sensor_settings_.average_samples_
        End With

        Return return_value
    End Function

    Public Sub Refresh() Implements IObservingConditions.Refresh
        TL.LogMessage("Refresh", "Not implemented")
        Throw New ASCOM.MethodNotImplementedException("Refresh")
    End Sub
#End Region

#Region "Private properties and methods"
    ' here are some useful properties and methods that can be used as required
    ' to help with

#Region "ASCOM Registration"

    Private Shared Sub RegUnregASCOM(ByVal bRegister As Boolean)

        Using P As New Profile() With {.DeviceType = "ObservingConditions"}
            If bRegister Then
                P.Register(driverID, driverDescription)
            Else
                P.Unregister(driverID)
            End If
        End Using

    End Sub

    <ComRegisterFunction()>
    Public Shared Sub RegisterASCOM(ByVal T As Type)

        RegUnregASCOM(True)

    End Sub

    <ComUnregisterFunction()>
    Public Shared Sub UnregisterASCOM(ByVal T As Type)

        RegUnregASCOM(False)

    End Sub

#End Region

    ''' 
    '''
    '''
    Private ReadOnly Property LightningDistance() As Double
        Get
            CheckConnected("LightningDistance")

            Dim return_value As Double = Val(getValueOf(condition_t.CONDITION_LIGHTNING))

            TL.LogMessage("LightningDistance", return_value)

            Return return_value
        End Get
    End Property

    ''' <summary>
    ''' Returns true if there is a valid connection to the driver hardware
    ''' </summary>
    Private Property IsConnected As Boolean
        Get
            Dim connected As Boolean = False

            'check that the driver hardware connection exists and is connected to the hardware
            Select Case observing_conditions_driver_settings_.connection_type_
                Case connection_type_t.SERIAL
                    If Not serial_connection_ Is Nothing Then
                        connected = serial_connection_.Connected
                    Else
                        connected = False
                    End If
                Case connection_type_t.NETWORK
                    If Not tcp_connection_ Is Nothing Then
                        connected = tcp_connection_.Connected
                    Else
                        connected = False
                    End If
                Case Else
                    Throw New ASCOM.DriverException("unknown connection state " & observing_conditions_driver_settings_.connection_type_.ToString)
            End Select

            TL.LogMessage("IsConnected Get", connected.ToString() & If(connected, " (connection type: " & observing_conditions_driver_settings_.connection_type_.ToString & ")", ""))

            Return Connected
        End Get
        Set(value As Boolean)
            TL.LogMessage("IsConnected Set", value.ToString())

            If value = IsConnected Then
                TL.LogMessage("IsConnected Set", "already " & If(value, "connected", "disconnected"))
                Return
            End If

            If value Then
                Select Case observing_conditions_driver_settings_.connection_type_
                    Case connection_type_t.SERIAL
                        TL.LogMessage("IsConnected Set", "Connecting to port " & observing_conditions_driver_settings_.com_port_ & "@" & observing_conditions_driver_settings_.baud_rate_)

                        'connect to the device:
                        serial_connection_.PortName = observing_conditions_driver_settings_.com_port_   'use port number read earlier
                        serial_connection_.Speed = observing_conditions_driver_settings_.baud_rate_     'use baud rate read earlier

                        'open serial connection and catch exception if the com port is not available.
                        Try
                            serial_connection_.Connected = True 'enable serial communication.
                            serial_connection_.ClearBuffers()
                        Catch ex As ASCOM.Utilities.Exceptions.InvalidValueException
                            Dim err_msg As String = "Error: COM port does not exist."
                            TL.LogMessage("IsConnected Set", err_msg)
                            Throw New ASCOM.DriverException(err_msg, ex)
                        Catch ex As Exception       'TODO catch specific exception types and handle them 
                            Dim err_msg As String = "error: could not open serial connection."
                            TL.LogMessage("IsConnected Set", err_msg)
                            Throw New ASCOM.DriverException(err_msg, ex)
                        End Try

                        serial_connection_.ReceiveTimeoutMs = TIMEOUT_SERIAL
                    Case connection_type_t.NETWORK
                        TL.LogMessage("IsConnected Set", "Connecting to " + observing_conditions_driver_settings_.ip_address_ + ": " & observing_conditions_driver_settings_.port_)

                        Try
                            tcp_connection_ = New Net.Sockets.TcpClient

                            tcp_connection_.Connect(observing_conditions_driver_settings_.ip_address_, observing_conditions_driver_settings_.port_)
                        Catch ex As ArgumentNullException
                            Dim err_msg As String = "error: the host name is null."
                            TL.LogMessage("IsConnected Set", err_msg)
                            Throw New ASCOM.DriverException(err_msg, ex)
                        Catch ex As ArgumentOutOfRangeException
                            Dim err_msg As String = "error: the port number is outside the valid range of " & System.Net.IPEndPoint.MinPort & " to " & System.Net.IPEndPoint.MaxPort & "."
                            TL.LogMessage("IsConnected Set", err_msg)
                            Throw New ASCOM.DriverException(err_msg, ex)
                        Catch ex As System.Net.Sockets.SocketException
                            Dim err_msg As String = "error: an error occured when accessing the socket."
                            TL.LogMessage("IsConnected Set", err_msg)
                            Throw New ASCOM.DriverException(err_msg, ex)
                        Catch ex As ObjectDisposedException
                            Dim err_msg As String = "error: the TCP client is closed."
                            TL.LogMessage("IsConnected Set", err_msg)
                            Throw New ASCOM.DriverException(err_msg, ex)
                        Catch ex As Exception
                            Dim err_msg As String = "error: could not open TCP connection."
                            TL.LogMessage("IsConnected Set", err_msg)
                            Throw New ASCOM.DriverException(err_msg, ex)
                        End Try

                        tcp_connection_.SendTimeout = TIMEOUT_TCP
                        tcp_connection_.ReceiveTimeout = TIMEOUT_TCP
                End Select

                'Arduino is reset when opening a Serial Connection. 
                'bootup time might increase in the future. 
                'Wait some time for it to boot up...
                System.Threading.Thread.Sleep(SET_CONNECTED_DELAY)
            Else
                Select Case observing_conditions_driver_settings_.connection_type_
                    Case connection_type_t.SERIAL
                        'disconnected from the device.
                        Try
                            serial_connection_.Connected = False
                        Catch Exc As System.TimeoutException
                            TL.LogMessage("IsConnected Set", "error: could not close serial connection")
                            Throw New DriverException(Exc.Message, Exc)
                        End Try

                        TL.LogMessage("IsConnected Set", "Disconnected from port " + observing_conditions_driver_settings_.com_port_)
                    Case connection_type_t.NETWORK
                        Try
                            tcp_connection_.Close()
                        Catch Exc As System.TimeoutException
                            TL.LogMessage("IsConnected Set", "error: could not close TCP connection")
                            Throw New DriverException(Exc.Message, Exc)
                        End Try

                        TL.LogMessage("IsConnected Set", "Disconnecting from " & observing_conditions_driver_settings_.ip_address_ & ":" & observing_conditions_driver_settings_.port_)
                End Select
            End If
        End Set
    End Property

    ''' <summary>
    ''' Use this function to throw an exception if we aren't connected to the hardware
    ''' </summary>
    ''' <param name="message"></param>
    Private Sub CheckConnected(ByVal message As String)
        TL.LogMessage("CheckConnected", IsConnected)

        If Not IsConnected Then
            Throw New NotConnectedException(message)
        End If
    End Sub

    ''' <summary>
    ''' Read the device configuration from the ASCOM Profile store
    ''' </summary>
    Friend Sub ReadProfile()
        TL.LogMessage("ReadProfile", "Start")

        Using driverProfile As New Profile()
            Dim buffer As String = ""

            driverProfile.DeviceType = "ObservingConditions"

            observingConditionsDriverSettings.trace_state_ = Convert.ToBoolean(driverProfile.GetValue(driverID, observingConditionsProfileNames.traceStateProfileName, String.Empty, observingConditionsDefaultValues.traceStateDefault))

            buffer = driverProfile.GetValue(driverID, observingConditionsProfileNames.connectionTypeProfileName, String.Empty, observingConditionsDefaultValues.connectionTypeDefault)
            observing_conditions_driver_settings_.connection_type_ = [Enum].Parse(GetType(connection_type_t), buffer)

            observing_conditions_driver_settings_.ip_address_ = driverProfile.GetValue(driverID, observingConditionsProfileNames.ipAddressProfileName, String.Empty, observingConditionsDefaultValues.ipAddressDefault)
            observing_conditions_driver_settings_.port_ = driverProfile.GetValue(driverID, observingConditionsProfileNames.portProfileName, String.Empty, observingConditionsDefaultValues.portDefault)

            observing_conditions_driver_settings_.com_port_ = driverProfile.GetValue(driverID, observingConditionsProfileNames.comPortProfileName, String.Empty, observingConditionsDefaultValues.comPortDefault)
            observing_conditions_driver_settings_.baud_rate_ = driverProfile.GetValue(driverID, observingConditionsProfileNames.baudRateProfileName, String.Empty, observingConditionsDefaultValues.baudRateDefault)

            With observing_conditions_driver_settings_.observing_conditions_settings_
                .log_csv_file_ = driverProfile.GetValue(driverID, observingConditionsProfileNames.logSupportProfileName, String.Empty, observingConditionsDefaultValues.logSupportDefault)
                .log_csv_enabled_ = driverProfile.GetValue(driverID, observingConditionsProfileNames.logEnabledProfileName, String.Empty, observingConditionsDefaultValues.logEnabledDefault)
                .log_csv_delimiter_ = driverProfile.GetValue(driverID, observingConditionsProfileNames.logFieldDelimiterProfileName, String.Empty, observingConditionsDefaultValues.logFieldDelimiterDefault)
                .log_csv_interval_ = driverProfile.GetValue(driverID, observingConditionsProfileNames.logIntervalProfileName, String.Empty, observingConditionsDefaultValues.logIntervalDefault)

                .average_period_ = driverProfile.GetValue(driverID, observingConditionsProfileNames.averagePeriodProfileName, String.Empty, observingConditionsDefaultValues.averagePeriodDefault)
            End With

            For i As Integer = 0 To NUM_SENSORS - 1
                With observing_conditions_driver_settings_.observing_conditions_settings_.sensors_(i).sensor_.sensor_settings_
                    Try
                        buffer = driverProfile.GetValue(driverID, observingConditionsProfileNames.sensorTypeProfileName, [Enum].Parse(GetType(condition_t), i).ToString, "SENSOR_UNKNOWN")
                        .sensor_type_ = [Enum].Parse(GetType(YAAASharedClassLibrary.YAAASensor.sensor_type_t), buffer)
                        '.average_samples_ = driverProfile.GetValue(driverID, observingConditionsProfileNames.sensorAveragingSamplesProfileName, [Enum].Parse(GetType(condition_t), i).ToString, 5)

                        .sensor_device_ = driverProfile.GetValue(driverID, observingConditionsProfileNames.sensorDeviceProfileName, [Enum].Parse(GetType(condition_t), i).ToString, 255)
                        .sensor_unit_ = driverProfile.GetValue(driverID, observingConditionsProfileNames.sensorUnitProfileName, [Enum].Parse(GetType(condition_t), i).ToString, 255)

                        buffer = driverProfile.GetValue(driverID, observingConditionsProfileNames.sensorParametersProfileName, [Enum].Parse(GetType(condition_t), i).ToString, "0xFF 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF")
                        .sensor_parameters_ = YAAASharedClassLibrary.hexToByteArray(buffer)
                    Catch exc As Exception
                        TL.LogMessage("ReadProfile", "error reading Sensor Data of condition " & [Enum].Parse(GetType(condition_t), i).ToString & ": " & exc.ToString)
                    End Try
                End With
            Next
        End Using

        TL.LogMessage("ReadProfile", "End")
    End Sub

    ''' <summary>
    ''' Write the device configuration to the  ASCOM  Profile store
    ''' </summary>
    Friend Sub WriteProfile()
        TL.LogMessage("WriteProfile", "Start")

        Using driverProfile As New Profile()
            driverProfile.DeviceType = "ObservingConditions"
            driverProfile.WriteValue(driverID, observingConditionsProfileNames.traceStateProfileName, observingConditionsDriverSettings.trace_state_.ToString())
            driverProfile.WriteValue(driverID, observingConditionsProfileNames.connectionTypeProfileName, observing_conditions_driver_settings_.connection_type_.ToString)

            driverProfile.WriteValue(driverID, observingConditionsProfileNames.ipAddressProfileName, observing_conditions_driver_settings_.ip_address_)
            driverProfile.WriteValue(driverID, observingConditionsProfileNames.portProfileName, observing_conditions_driver_settings_.port_)

            driverProfile.WriteValue(driverID, observingConditionsProfileNames.comPortProfileName, observing_conditions_driver_settings_.com_port_)
            driverProfile.WriteValue(driverID, observingConditionsProfileNames.baudRateProfileName, observing_conditions_driver_settings_.baud_rate_)

            With observing_conditions_driver_settings_.observing_conditions_settings_
                driverProfile.WriteValue(driverID, observingConditionsProfileNames.logSupportProfileName, .log_csv_file_.ToString())
                driverProfile.WriteValue(driverID, observingConditionsProfileNames.logEnabledProfileName, .log_csv_enabled_.ToString())
                driverProfile.WriteValue(driverID, observingConditionsProfileNames.logFieldDelimiterProfileName, .log_csv_delimiter_.ToString())
                driverProfile.WriteValue(driverID, observingConditionsProfileNames.logIntervalProfileName, .log_csv_interval_.ToString())

                driverProfile.WriteValue(driverID, observingConditionsProfileNames.averagePeriodProfileName, .average_period_.ToString())
            End With

            For i As Integer = 0 To NUM_SENSORS - 1
                With observing_conditions_driver_settings_.observing_conditions_settings_.sensors_(i).sensor_.sensor_settings_
                    driverProfile.WriteValue(driverID, observingConditionsProfileNames.sensorTypeProfileName, .sensor_type_.ToString(), [Enum].Parse(GetType(condition_t), i).ToString)
                    driverProfile.WriteValue(driverID, observingConditionsProfileNames.sensorDeviceProfileName, .sensor_device_, [Enum].Parse(GetType(condition_t), i).ToString)
                    driverProfile.WriteValue(driverID, observingConditionsProfileNames.sensorUnitProfileName, .sensor_unit_, [Enum].Parse(GetType(condition_t), i).ToString)
                    'driverProfile.WriteValue(driverID, observingConditionsProfileNames.sensorAveragingSamplesProfileName, .average_samples_, [Enum].Parse(GetType(condition_t), i).ToString)

                    Dim parameters_string As String = ""
                    For j As Integer = 0 To .sensor_parameters_.Length - 1
                        parameters_string += Hex(.sensor_parameters_(j))
                        parameters_string += " "
                    Next

                    driverProfile.WriteValue(driverID, observingConditionsProfileNames.sensorParametersProfileName, parameters_string, [Enum].Parse(GetType(condition_t), i).ToString)
                End With
            Next
        End Using
        TL.LogMessage("WriteProfile", "End")
    End Sub

#End Region

    'encapsulates and sends a command to the Arduino.
    '@param command command to send.
    '@return response from Arduino.
    Private Function sendCommand(command As String) As String
        Dim message As String = ""
        Dim response As String = ""

        'wrap command string.
        message = "<"               'beginning of the message
        message &= "O,"             'device identifier
        message &= command          'command
        message &= ">"              'end of message

        Select Case observing_conditions_driver_settings_.connection_type_
            Case connection_type_t.SERIAL
                'log sent command
                TL.LogMessage("SERIAL TRANSMIT", message)

                'transmit message to arduino
                serial_connection_.Transmit(message)

                response = serial_connection_.ReceiveTerminated(">")

                'log response from Arduino
                TL.LogMessage("SERIAL RECEIVE", response)

            Case connection_type_t.NETWORK
                'log sent command
                TL.LogMessage("TCP TRANSMIT", message)

                'copy message to buffer and send buffer
                Dim sendBytes As [Byte]() = Encoding.ASCII.GetBytes(message)
                tcp_connection_.GetStream.Write(sendBytes, 0, sendBytes.Length)

                'receive response into receive buffer
                Dim recLength As Integer = 0    'number of bytes received
                Dim recBytes(tcp_connection_.ReceiveBufferSize) As Byte
                response = ""

                'read data until an end-of-message character has been received ('>' or '#')
                While Not (response.Contains(">") OrElse response.Contains("#"))
                    recLength = tcp_connection_.GetStream.Read(recBytes, 0, CInt(tcp_connection_.ReceiveBufferSize))
                    response &= Encoding.ASCII.GetString(recBytes, 0, recLength)
                End While

                'log response from Arduino
                TL.LogMessage("TCP RECEIVE", response)

                'read \cr\lf sent by arduino (the arduino sends these in a seperate packet).
                If tcp_connection_.GetStream.DataAvailable Then
                    tcp_connection_.GetStream.Read(recBytes, 0, CInt(tcp_connection_.ReceiveBufferSize))
                End If

        End Select

        response = response.Trim

        response = response.Replace("<O,", "")
        response = response.Replace(">", "")

        'check the response for serial communication errors. throws an exception if a communication error occurred.
        checkMessageError(response)

        Return response
    End Function

    'checks a response for a serial communication error. throws an exception if an error occurred.
    '@param response response from Arduino.
    Private Sub checkMessageError(response As String)
        Dim error_pattern As String = "ERROR_\d+$"
        Dim match As RegularExpressions.Match = RegularExpressions.Regex.Match(response, error_pattern)

        'do nothing if no error message has been received.
        If Not match.Success Then Exit Sub

        'parse the error code when an error message has been received.
        Dim error_code As serial_error_code_t = serial_error_code_t.ERROR_NONE
        Dim error_code_known As Boolean = False

        Dim error_message As String = match.ToString
        TL.LogMessage("checkMessageError", error_message)

        error_code_known = [Enum].TryParse(error_message.Replace("ERROR_", ""), error_code)

        If Not error_code_known Then
            TL.LogMessage("checkMessageError", "unknown error: " & error_message)
            Throw New ASCOM.DriverException("checkMessageError: unknown error: " & error_message)
        Else
            TL.LogMessage("checkMessageError", error_code.ToString)

            If error_code = serial_error_code_t.ERROR_NONE Then
                Exit Sub
            Else
                Throw New ASCOM.DriverException("Serial Communication Error: " & error_code.ToString)
            End If
        End If
    End Sub

#Region "additional friend functions"

    'function to return a telescopeDriverSettings struct containing the current driver settings.
    '@param eeprom true to get EEPROM settings, false to get normal settings
    Friend Function getDriverSettings(Optional eeprom As Boolean = False) As observingConditionsDriverSettings
        Dim return_value As observingConditionsDriverSettings

        If eeprom = True Then
            return_value = observing_conditions_driver_settings_EEPROM_.Clone
        Else
            return_value = observing_conditions_driver_settings_.Clone
        End If

        Return return_value
    End Function

    'overwrites the driver's telescope settings object with the provided object.
    Friend Sub setDriverSettings(settings As observingConditionsDriverSettings, Optional eeprom As Boolean = False)
        If eeprom = True Then
            observing_conditions_driver_settings_EEPROM_ = settings.Clone
        Else
            observing_conditions_driver_settings_ = settings.Clone
        End If
    End Sub


#Region "Reading / writing settings fron / to .xml files"

    'opens a file and attempts to read a observingConditionsDriverSettings structure from it. 
    '@param file_name full path and file name to file
    '@return driver settings stored in file as observingConditionsDriverSettings
    Friend Function xmlToDriverSettings(file_name As String) As observingConditionsDriverSettings
        Dim return_value As observingConditionsDriverSettings = New observingConditionsDriverSettings(True)

        Dim fs As New FileStream(file_name, FileMode.Open, FileAccess.Read)
        Dim reader As XmlDictionaryReader = XmlDictionaryReader.CreateTextReader(fs, New XmlDictionaryReaderQuotas())
        Dim serializer = New DataContractSerializer(GetType(observingConditionsDriverSettings))

        return_value = CType(serializer.ReadObject(reader, True), observingConditionsDriverSettings)
        reader.Close()
        fs.Close()

        Return return_value
    End Function

    'serializes a observingConditionsDriverSettings struct and saves it to a file.
    '@param file_name full path and file name of file
    '@param settings focuser driver settings as observingConditionsDriverSettings
    Friend Sub xmlFromDriverSettings(file_name As String, settings As observingConditionsDriverSettings)
        Dim serializer As New DataContractSerializer(GetType(observingConditionsDriverSettings))
        Dim writer As New FileStream(file_name, FileMode.Create)

        serializer.WriteObject(writer, settings)
        writer.Close()
    End Sub

#End Region

#End Region

#Region "Reading and Writing from / to the Ardunio's EEPROM"

    Friend Sub ReadEEPROM()
        CheckConnected("ReadEEPROM")

        TL.LogMessage("ReadEEPROM", "Start")

        observing_conditions_driver_settings_EEPROM_ = observing_conditions_driver_settings_.Clone()

        readSetting(observing_conditions_driver_settings_EEPROM_.observing_conditions_settings_.log_csv_file_, "LOG_CSV_FILE")
        readSetting(observing_conditions_driver_settings_EEPROM_.observing_conditions_settings_.log_csv_enabled_, "LOG_CSV_ENABLED")
        readSetting(observing_conditions_driver_settings_EEPROM_.observing_conditions_settings_.log_csv_delimiter_, "LOG_CSV_DELIMITER")
        readSetting(observing_conditions_driver_settings_EEPROM_.observing_conditions_settings_.log_csv_interval_, "LOG_CSV_INTERVAL")

        readSetting(observing_conditions_driver_settings_EEPROM_.observing_conditions_settings_.average_period_, "AVERAGE_PERIOD")

        For i As Integer = 0 To NUM_SENSORS - 1
            readSetting(observing_conditions_driver_settings_EEPROM_.observing_conditions_settings_.sensors_(i), "SENSOR_CONFIGURATION," & i)
        Next

        TL.LogMessage("ReadEEPROM", "End")
    End Sub

    Friend Sub WriteEEPROM()
        CheckConnected("WriteEEPROM")

        TL.LogMessage("WriteEEPROM", "Start")

        WriteProfile()

        With observing_conditions_driver_settings_.observing_conditions_settings_
            writeSetting(.log_csv_file_, observing_conditions_driver_settings_EEPROM_.observing_conditions_settings_.log_csv_file_, "LOG_CSV_FILE")
            writeSetting(.log_csv_enabled_, observing_conditions_driver_settings_EEPROM_.observing_conditions_settings_.log_csv_enabled_, "LOG_CSV_ENABLED")
            writeSetting(.log_csv_delimiter_, observing_conditions_driver_settings_EEPROM_.observing_conditions_settings_.log_csv_delimiter_, "LOG_CSV_DELIMITER")
            writeSetting(.log_csv_interval_, observing_conditions_driver_settings_EEPROM_.observing_conditions_settings_.log_csv_interval_, "LOG_CSV_INTERVAL")

            writeSetting(.average_period_, observing_conditions_driver_settings_EEPROM_.observing_conditions_settings_.average_period_, "AVERAGE_PERIOD")

            For i As Integer = 0 To NUM_SENSORS - 1
                writeSetting(.sensors_(i), observing_conditions_driver_settings_EEPROM_.observing_conditions_settings_.sensors_(i), "SENSOR_CONFIGURATION," & i)
            Next
        End With

        TL.LogMessage("WriteEEPROM", "End")
    End Sub

#Region "reading settings from arduino"

    Private Sub readSetting(ByRef value As Boolean, setting As String)
        value = Not (readSettings(setting).Equals("0"))
    End Sub

    Private Sub readSetting(ByRef value As Byte, setting As String)
        value = CByte(readSettings(setting))
    End Sub

    Private Sub readSetting(ByRef value As Char, setting As String)
        value = Chr(readSettings(setting))
    End Sub

    Private Sub readSetting(ByRef value As Short, setting As String)
        value = CShort(readSettings(setting))
    End Sub

    Private Sub readSetting(ByRef value As UShort, setting As String)
        value = CUShort(readSettings(setting))
    End Sub

    Private Sub readSetting(ByRef value As Single, setting As String)
        value = Val(readSettings(setting))
    End Sub

    Private Sub readSetting(ByRef value As Double, setting As String)
        value = Val(readSettings(setting))
    End Sub

    Private Sub readSetting(ByRef value As Integer, setting As String)
        value = CInt(readSettings(setting))
    End Sub

    Private Sub readSetting(ByRef value As UInteger, setting As String)
        value = CUInt(readSettings(setting))
    End Sub

    Private Sub readSetting(ByRef value As SensorConfiguration, setting As String)
        Dim str_buffer As String = readSettings(setting)
        value = New SensorConfiguration(str_buffer)
    End Sub

    Private Sub readSetting(ByRef value As YAAASharedClassLibrary.YAAASensor, setting As String)
        Dim str_buffer As String = readSettings(setting)
        value = New YAAASharedClassLibrary.YAAASensor(str_buffer)
    End Sub

    Private Function readSettings(setting As String)
        Dim message As String
        Dim response As String

        message = "SETUP,"          'setup command 
        message &= "GET,"           'read command
        message &= setting          'setting to read

        response = sendCommand(message)
        response = response.Replace("SETUP,", "").Trim      'trim the rest of the message.

        Return response
    End Function

#End Region

#Region "writing settings to arduino"

    Private Sub writeSetting(value As Boolean, ByRef eeprom_value As Boolean, setting As String)
        If EEPROM_force_write_ Or value <> eeprom_value Then
            writeSettings(Convert.ToByte(value), setting)
            eeprom_value = value
        End If
    End Sub

    Private Sub writeSetting(value As Byte, ByRef eeprom_value As Byte, setting As String)
        If EEPROM_force_write_ Or value <> eeprom_value Then
            writeSettings(Convert.ToByte(value), setting)
            eeprom_value = value
        End If
    End Sub

    Private Sub writeSetting(value As Char, ByRef eeprom_value As Char, setting As String)
        If EEPROM_force_write_ Or value <> eeprom_value Then
            writeSettings(Convert.ToByte(value), setting)
            eeprom_value = value
        End If
    End Sub

    Private Sub writeSetting(value As Short, ByRef eeprom_value As Short, setting As String)
        If EEPROM_force_write_ Or value <> eeprom_value Then
            writeSettings(value.ToString(), setting)
            eeprom_value = value
        End If
    End Sub

    Private Sub writeSetting(value As UShort, ByRef eeprom_value As UShort, setting As String)
        If EEPROM_force_write_ Or value <> eeprom_value Then
            writeSettings(value.ToString(), setting)
            eeprom_value = value
        End If
    End Sub

    Private Sub writeSetting(value As Single, ByRef eeprom_value As Single, setting As String)
        If EEPROM_force_write_ Or value <> eeprom_value Then
            writeSettings(value.ToString("F7", CultureInfo.InvariantCulture), setting)
            eeprom_value = value
        End If
    End Sub

    Private Sub writeSetting(value As Double, ByRef eeprom_value As Double, setting As String)
        If EEPROM_force_write_ Or value <> eeprom_value Then
            writeSettings(value.ToString("F7", CultureInfo.InvariantCulture), setting)
            eeprom_value = value
        End If
    End Sub

    Private Sub writeSetting(value As Integer, ByRef eeprom_value As Integer, setting As String)
        If EEPROM_force_write_ Or value <> eeprom_value Then
            writeSettings(value.ToString(), setting)
            eeprom_value = value
        End If
    End Sub

    Private Sub writeSetting(value As UInteger, ByRef eeprom_value As UInteger, setting As String)
        If EEPROM_force_write_ Or value <> eeprom_value Then
            writeSettings(value.ToString(), setting)
            eeprom_value = value
        End If
    End Sub

    Private Sub writeSetting(value As SensorConfiguration, ByRef eeprom_value As SensorConfiguration, setting As String)
        If EEPROM_force_write_ Or value <> eeprom_value Then
            writeSettings(value.toArduinoString(), setting)
            eeprom_value = value.Clone
        End If
    End Sub

    Private Sub writeSetting(value As YAAASharedClassLibrary.YAAASensor, ByRef eeprom_value As YAAASharedClassLibrary.YAAASensor, setting As String)

        If EEPROM_force_write_ Or value <> eeprom_value Then
            writeSettings(value.toArduinoString(), setting)
            eeprom_value = value.Clone
        End If
    End Sub

    Private Function writeSettings(value As String, setting As String)
        Dim message As String
        Dim response As String

        message = "SETUP,"              'setup command 
        message &= "SET,"               'read command
        message &= setting & ","        'setting to write
        message &= value.ToString()     'value to set

        response = sendCommand(message)
        response = response.Replace("SETUP,", "").Trim      'trim the rest of the message.

        Return response
    End Function

#End Region

#End Region


#Region "additional private functions"

    Private Function checkSensorEnabled(condition As condition_t) As Boolean
        Dim response As String = ""

        response = sendCommand("SENSOR_ENABLED,GET," & condition)
        response = response.Replace("SENSOR_ENABLED,", "").Trim

        Return CBool(response)
    End Function

    Private Function checkPerformsAveraging(condition As condition_t) As Boolean
        Dim response As String = ""

        response = sendCommand("PERFORMS_AVERAGING,GET," & condition)
        response = response.Replace("PERFORMS_AVERAGING,", "").Trim

        Return CBool(response)
    End Function

    Private Function getAverage(condition As condition_t) As String
        Dim response As String = ""
        response = sendCommand("AVERAGE,GET," & condition)
        response = response.Replace("AVERAGE,", "").Trim
        Return response
    End Function

    'sends a MEASURE command to the arduino and returns the result.
    Private Function measure(condition As condition_t) As String
        Dim response As String = ""
        response = sendCommand("MEASURE,SET," & condition)
        response = response.Replace("MEASURE,", "").Trim
        Return response
    End Function

    'sends a 
    Private Function hasValue(condition As condition_t) As String
        Dim response As String = ""
        response = sendCommand("MEASURE,GET," & condition)
        response = response.Replace("MEASURE,", "").Trim
        Return response
    End Function

    Private Function getValue(condition As condition_t) As String
        Dim response As String = ""
        response = sendCommand("VALUE,GET," & condition)
        response = response.Replace("VALUE,", "").Trim
        Return response
    End Function

    '
    Private Function getValueOf(condition As condition_t) As String
        If Not checkSensorEnabled(condition) Then
            TL.LogMessage("getValueOf()", condition.ToString() & ": no sensor enabled for this condition")
            Throw New ASCOM.PropertyNotImplementedException(condition.ToString() & ": no sensor enabled for this condition")
        End If

        Dim return_value As String = ""

        If checkPerformsAveraging(condition) Then
            return_value = getAverage(condition)
        Else
            Dim timer As New Stopwatch
            timer.Start()

            'start measurement
            While Not CBool(measure(condition))
                'throw an exception if a measurement could not be started within the measurement timeout
                If timer.ElapsedMilliseconds > TIMEOUT_MEASURE Then
                    Throw New ASCOM.DriverException(condition.ToString() & ": starting measurement failed")
                End If

                System.Threading.Thread.Sleep(100)
            End While

            'restart timer
            timer.Restart()

            'wait until measurement is complete
            Do
                'throw an exception if the measurement does not complete within the hasvalue timeout
                If timer.ElapsedMilliseconds > TIMEOUT_HASVALUE Then
                    Throw New ASCOM.DriverException(condition.ToString() & ": measurement did not complete within " & TIMEOUT_HASVALUE & "ms")
                End If

                System.Threading.Thread.Sleep(100)
            Loop While Not CBool(hasValue(condition))

            timer.Stop()

            'get value
            return_value = getValue(condition)
        End If

        TL.LogMessage("getValueOf()", condition.ToString() & ": " & return_value)

        Return return_value
    End Function

    Private Function getMeasurementAge(condition As condition_t) As String
        Dim response As String = ""
        response = sendCommand("MEASUREMENT_AGE,GET," & condition)
        response = response.Replace("MEASUREMENT_AGE,", "").Trim
        Return response
    End Function

    'analyzes error code and uses tracelogger to log error. 
    '@param command name of the calling function
    '@param errorCode error code returned by Arduino.
    '@param e enum to check error code against.
    '@return true if an error was logged, false otherwise.
    Private Function logErrorCode(command As String, errorCode As Integer, ByVal e As Type) As Boolean
        Dim return_value = False
        Dim eValues As Array = System.Enum.GetValues(e)

        For Each value In eValues
            If errorCode And value Then
                return_value = True
                TL.LogMessage(command, "Error code " & value & " - " & value.ToString())
            End If
        Next

        Return return_value
    End Function

#End Region

End Class
