<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SetupDialogForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.OK_Button = New System.Windows.Forms.Button()
        Me.Cancel_Button = New System.Windows.Forms.Button()
        Me.chkTrace = New System.Windows.Forms.CheckBox()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPageGeneral = New System.Windows.Forms.TabPage()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.TextBoxLogInterval = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ComboBoxLogDelimiter = New System.Windows.Forms.ComboBox()
        Me.CheckBoxLogModeDefault = New System.Windows.Forms.CheckBox()
        Me.CheckBoxLogSupport = New System.Windows.Forms.CheckBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.TextBoxDefAveragePeriod = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox23 = New System.Windows.Forms.GroupBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.RadioButtonNetwork = New System.Windows.Forms.RadioButton()
        Me.RadioButtonSerial = New System.Windows.Forms.RadioButton()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.GroupBox37 = New System.Windows.Forms.GroupBox()
        Me.TextBoxPort = New System.Windows.Forms.TextBox()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.TextBoxIPAddress = New System.Windows.Forms.TextBox()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.GroupBox36 = New System.Windows.Forms.GroupBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.ComboBoxBaudRate = New System.Windows.Forms.ComboBox()
        Me.ComboBoxComPorts = New System.Windows.Forms.ComboBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.TabPageSensors = New System.Windows.Forms.TabPage()
        Me.FlowLayoutPanelObservingConditions = New System.Windows.Forms.FlowLayoutPanel()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.ButtonReadEEPROM = New System.Windows.Forms.Button()
        Me.ButtonWriteEEPROM = New System.Windows.Forms.Button()
        Me.CheckBoxEEPROMForceWrite = New System.Windows.Forms.CheckBox()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ToolStripSplitButton1 = New System.Windows.Forms.ToolStripSplitButton()
        Me.ToolStripStatusLabel2 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.TableLayoutPanel4 = New System.Windows.Forms.TableLayoutPanel()
        Me.ButtonExportSettings = New System.Windows.Forms.Button()
        Me.ButtonImportSettings = New System.Windows.Forms.Button()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.TableLayoutPanel1.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPageGeneral.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox23.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.GroupBox37.SuspendLayout()
        Me.GroupBox36.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPageSensors.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.TableLayoutPanel4.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.OK_Button, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Cancel_Button, 1, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(387, 488)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(146, 29)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'OK_Button
        '
        Me.OK_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.OK_Button.Location = New System.Drawing.Point(3, 3)
        Me.OK_Button.Name = "OK_Button"
        Me.OK_Button.Size = New System.Drawing.Size(67, 23)
        Me.OK_Button.TabIndex = 0
        Me.OK_Button.Text = "OK"
        '
        'Cancel_Button
        '
        Me.Cancel_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Cancel_Button.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Cancel_Button.Location = New System.Drawing.Point(76, 3)
        Me.Cancel_Button.Name = "Cancel_Button"
        Me.Cancel_Button.Size = New System.Drawing.Size(67, 23)
        Me.Cancel_Button.TabIndex = 1
        Me.Cancel_Button.Text = "Cancel"
        '
        'chkTrace
        '
        Me.chkTrace.AutoSize = True
        Me.chkTrace.Location = New System.Drawing.Point(394, 18)
        Me.chkTrace.Name = "chkTrace"
        Me.chkTrace.Size = New System.Drawing.Size(69, 17)
        Me.chkTrace.TabIndex = 8
        Me.chkTrace.Text = "Trace on"
        Me.chkTrace.UseVisualStyleBackColor = True
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPageGeneral)
        Me.TabControl1.Controls.Add(Me.TabPageSensors)
        Me.TabControl1.Location = New System.Drawing.Point(12, 12)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(531, 428)
        Me.TabControl1.TabIndex = 10
        '
        'TabPageGeneral
        '
        Me.TabPageGeneral.Controls.Add(Me.GroupBox2)
        Me.TabPageGeneral.Controls.Add(Me.GroupBox1)
        Me.TabPageGeneral.Controls.Add(Me.GroupBox23)
        Me.TabPageGeneral.Controls.Add(Me.chkTrace)
        Me.TabPageGeneral.Controls.Add(Me.PictureBox1)
        Me.TabPageGeneral.Location = New System.Drawing.Point(4, 22)
        Me.TabPageGeneral.Name = "TabPageGeneral"
        Me.TabPageGeneral.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPageGeneral.Size = New System.Drawing.Size(523, 402)
        Me.TabPageGeneral.TabIndex = 0
        Me.TabPageGeneral.Text = "General"
        Me.TabPageGeneral.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.TextBoxLogInterval)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.ComboBoxLogDelimiter)
        Me.GroupBox2.Controls.Add(Me.CheckBoxLogModeDefault)
        Me.GroupBox2.Controls.Add(Me.CheckBoxLogSupport)
        Me.GroupBox2.Location = New System.Drawing.Point(6, 164)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(382, 70)
        Me.GroupBox2.TabIndex = 92
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Observing Condition Log Settings"
        '
        'TextBoxLogInterval
        '
        Me.TextBoxLogInterval.Location = New System.Drawing.Point(296, 44)
        Me.TextBoxLogInterval.Name = "TextBoxLogInterval"
        Me.TextBoxLogInterval.Size = New System.Drawing.Size(74, 20)
        Me.TextBoxLogInterval.TabIndex = 95
        Me.ToolTip1.SetToolTip(Me.TextBoxLogInterval, "Interval between consecutive logs to the .csv file")
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(200, 47)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(85, 13)
        Me.Label6.TabIndex = 94
        Me.Label6.Text = "Log Interval (ms)"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(200, 20)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(90, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = ".csv field delimiter"
        '
        'ComboBoxLogDelimiter
        '
        Me.ComboBoxLogDelimiter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxLogDelimiter.FormattingEnabled = True
        Me.ComboBoxLogDelimiter.Items.AddRange(New Object() {"comma", "semicolon", "colon", "space", "tab"})
        Me.ComboBoxLogDelimiter.Location = New System.Drawing.Point(296, 17)
        Me.ComboBoxLogDelimiter.Name = "ComboBoxLogDelimiter"
        Me.ComboBoxLogDelimiter.Size = New System.Drawing.Size(74, 21)
        Me.ComboBoxLogDelimiter.TabIndex = 2
        Me.ToolTip1.SetToolTip(Me.ComboBoxLogDelimiter, ".csv file field delimiter. default: ',' (comma)")
        '
        'CheckBoxLogModeDefault
        '
        Me.CheckBoxLogModeDefault.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxLogModeDefault.Location = New System.Drawing.Point(6, 46)
        Me.CheckBoxLogModeDefault.Name = "CheckBoxLogModeDefault"
        Me.CheckBoxLogModeDefault.Size = New System.Drawing.Size(188, 17)
        Me.CheckBoxLogModeDefault.TabIndex = 1
        Me.CheckBoxLogModeDefault.Text = "Logging enabled at startup"
        Me.ToolTip1.SetToolTip(Me.CheckBoxLogModeDefault, "If true, logging sensor values is enabled after Arduino reset.")
        Me.CheckBoxLogModeDefault.UseVisualStyleBackColor = True
        '
        'CheckBoxLogSupport
        '
        Me.CheckBoxLogSupport.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxLogSupport.Location = New System.Drawing.Point(6, 19)
        Me.CheckBoxLogSupport.Name = "CheckBoxLogSupport"
        Me.CheckBoxLogSupport.Size = New System.Drawing.Size(188, 17)
        Me.CheckBoxLogSupport.TabIndex = 0
        Me.CheckBoxLogSupport.Text = "Log Support"
        Me.ToolTip1.SetToolTip(Me.CheckBoxLogSupport, "Set to true to support logging sensor values to a SD card.")
        Me.CheckBoxLogSupport.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.TextBoxDefAveragePeriod)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(6, 240)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(382, 45)
        Me.GroupBox1.TabIndex = 91
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Other"
        '
        'TextBoxDefAveragePeriod
        '
        Me.TextBoxDefAveragePeriod.Location = New System.Drawing.Point(281, 15)
        Me.TextBoxDefAveragePeriod.Name = "TextBoxDefAveragePeriod"
        Me.TextBoxDefAveragePeriod.Size = New System.Drawing.Size(89, 20)
        Me.TextBoxDefAveragePeriod.TabIndex = 93
        Me.ToolTip1.SetToolTip(Me.TextBoxDefAveragePeriod, "Default average period. ")
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 18)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(132, 13)
        Me.Label1.TabIndex = 92
        Me.Label1.Text = "Default Average Period (h)"
        '
        'GroupBox23
        '
        Me.GroupBox23.Controls.Add(Me.Panel1)
        Me.GroupBox23.Controls.Add(Me.Label44)
        Me.GroupBox23.Controls.Add(Me.GroupBox37)
        Me.GroupBox23.Controls.Add(Me.GroupBox36)
        Me.GroupBox23.Location = New System.Drawing.Point(6, 6)
        Me.GroupBox23.Name = "GroupBox23"
        Me.GroupBox23.Size = New System.Drawing.Size(382, 152)
        Me.GroupBox23.TabIndex = 90
        Me.GroupBox23.TabStop = False
        Me.GroupBox23.Text = "Communication"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.RadioButtonNetwork)
        Me.Panel1.Controls.Add(Me.RadioButtonSerial)
        Me.Panel1.Location = New System.Drawing.Point(96, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(280, 35)
        Me.Panel1.TabIndex = 90
        '
        'RadioButtonNetwork
        '
        Me.RadioButtonNetwork.AutoSize = True
        Me.RadioButtonNetwork.Location = New System.Drawing.Point(103, 9)
        Me.RadioButtonNetwork.Name = "RadioButtonNetwork"
        Me.RadioButtonNetwork.Size = New System.Drawing.Size(65, 17)
        Me.RadioButtonNetwork.TabIndex = 90
        Me.RadioButtonNetwork.TabStop = True
        Me.RadioButtonNetwork.Text = "Network"
        Me.RadioButtonNetwork.UseVisualStyleBackColor = True
        '
        'RadioButtonSerial
        '
        Me.RadioButtonSerial.AutoSize = True
        Me.RadioButtonSerial.Location = New System.Drawing.Point(14, 9)
        Me.RadioButtonSerial.Name = "RadioButtonSerial"
        Me.RadioButtonSerial.Size = New System.Drawing.Size(51, 17)
        Me.RadioButtonSerial.TabIndex = 90
        Me.RadioButtonSerial.TabStop = True
        Me.RadioButtonSerial.Text = "Serial"
        Me.RadioButtonSerial.UseVisualStyleBackColor = True
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Location = New System.Drawing.Point(12, 23)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(78, 13)
        Me.Label44.TabIndex = 90
        Me.Label44.Text = "Connect using:"
        '
        'GroupBox37
        '
        Me.GroupBox37.Controls.Add(Me.TextBoxPort)
        Me.GroupBox37.Controls.Add(Me.Label60)
        Me.GroupBox37.Controls.Add(Me.TextBoxIPAddress)
        Me.GroupBox37.Controls.Add(Me.Label58)
        Me.GroupBox37.Location = New System.Drawing.Point(6, 101)
        Me.GroupBox37.Name = "GroupBox37"
        Me.GroupBox37.Size = New System.Drawing.Size(370, 46)
        Me.GroupBox37.TabIndex = 91
        Me.GroupBox37.TabStop = False
        Me.GroupBox37.Text = "Network"
        '
        'TextBoxPort
        '
        Me.TextBoxPort.Location = New System.Drawing.Point(275, 19)
        Me.TextBoxPort.Name = "TextBoxPort"
        Me.TextBoxPort.Size = New System.Drawing.Size(89, 20)
        Me.TextBoxPort.TabIndex = 91
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.Location = New System.Drawing.Point(194, 22)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(26, 13)
        Me.Label60.TabIndex = 81
        Me.Label60.Text = "Port"
        '
        'TextBoxIPAddress
        '
        Me.TextBoxIPAddress.Location = New System.Drawing.Point(99, 19)
        Me.TextBoxIPAddress.Name = "TextBoxIPAddress"
        Me.TextBoxIPAddress.Size = New System.Drawing.Size(89, 20)
        Me.TextBoxIPAddress.TabIndex = 90
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.Location = New System.Drawing.Point(6, 22)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(17, 13)
        Me.Label58.TabIndex = 80
        Me.Label58.Text = "IP"
        '
        'GroupBox36
        '
        Me.GroupBox36.Controls.Add(Me.Label4)
        Me.GroupBox36.Controls.Add(Me.Label3)
        Me.GroupBox36.Controls.Add(Me.ComboBoxBaudRate)
        Me.GroupBox36.Controls.Add(Me.ComboBoxComPorts)
        Me.GroupBox36.Location = New System.Drawing.Point(6, 53)
        Me.GroupBox36.Name = "GroupBox36"
        Me.GroupBox36.Size = New System.Drawing.Size(370, 42)
        Me.GroupBox36.TabIndex = 90
        Me.GroupBox36.TabStop = False
        Me.GroupBox36.Text = "Serial"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(6, 16)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(50, 13)
        Me.Label4.TabIndex = 78
        Me.Label4.Text = "Com Port"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label3.Location = New System.Drawing.Point(194, 16)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(58, 13)
        Me.Label3.TabIndex = 79
        Me.Label3.Text = "Baud Rate"
        '
        'ComboBoxBaudRate
        '
        Me.ComboBoxBaudRate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxBaudRate.FormattingEnabled = True
        Me.ComboBoxBaudRate.Items.AddRange(New Object() {"300", "600", "1200", "2400", "4800", "9600", "14400", "19200", "28800", "38400", "57600", "115200"})
        Me.ComboBoxBaudRate.Location = New System.Drawing.Point(275, 13)
        Me.ComboBoxBaudRate.Name = "ComboBoxBaudRate"
        Me.ComboBoxBaudRate.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ComboBoxBaudRate.Size = New System.Drawing.Size(89, 21)
        Me.ComboBoxBaudRate.TabIndex = 2
        '
        'ComboBoxComPorts
        '
        Me.ComboBoxComPorts.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxComPorts.FormattingEnabled = True
        Me.ComboBoxComPorts.Location = New System.Drawing.Point(99, 13)
        Me.ComboBoxComPorts.Name = "ComboBoxComPorts"
        Me.ComboBoxComPorts.Size = New System.Drawing.Size(89, 21)
        Me.ComboBoxComPorts.TabIndex = 0
        '
        'PictureBox1
        '
        Me.PictureBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PictureBox1.Image = Global.ASCOM.YAAADevice.My.Resources.Resources.ASCOM
        Me.PictureBox1.Location = New System.Drawing.Point(469, 6)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(48, 56)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox1.TabIndex = 2
        Me.PictureBox1.TabStop = False
        '
        'TabPageSensors
        '
        Me.TabPageSensors.Controls.Add(Me.FlowLayoutPanelObservingConditions)
        Me.TabPageSensors.Location = New System.Drawing.Point(4, 22)
        Me.TabPageSensors.Name = "TabPageSensors"
        Me.TabPageSensors.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPageSensors.Size = New System.Drawing.Size(523, 402)
        Me.TabPageSensors.TabIndex = 1
        Me.TabPageSensors.Text = "Sensors"
        Me.TabPageSensors.UseVisualStyleBackColor = True
        '
        'FlowLayoutPanelObservingConditions
        '
        Me.FlowLayoutPanelObservingConditions.AutoScroll = True
        Me.FlowLayoutPanelObservingConditions.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanelObservingConditions.Location = New System.Drawing.Point(6, 6)
        Me.FlowLayoutPanelObservingConditions.Name = "FlowLayoutPanelObservingConditions"
        Me.FlowLayoutPanelObservingConditions.Size = New System.Drawing.Size(511, 393)
        Me.FlowLayoutPanelObservingConditions.TabIndex = 11
        Me.FlowLayoutPanelObservingConditions.WrapContents = False
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel3.ColumnCount = 1
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.TableLayoutPanel2, 0, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBoxEEPROMForceWrite, 0, 0)
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(12, 444)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 2
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 36.9863!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 63.0137!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(141, 73)
        Me.TableLayoutPanel3.TabIndex = 92
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel2.ColumnCount = 2
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.ButtonReadEEPROM, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.ButtonWriteEEPROM, 1, 0)
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(3, 30)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 1
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(135, 40)
        Me.TableLayoutPanel2.TabIndex = 89
        '
        'ButtonReadEEPROM
        '
        Me.ButtonReadEEPROM.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ButtonReadEEPROM.Location = New System.Drawing.Point(3, 3)
        Me.ButtonReadEEPROM.Name = "ButtonReadEEPROM"
        Me.ButtonReadEEPROM.Size = New System.Drawing.Size(61, 34)
        Me.ButtonReadEEPROM.TabIndex = 83
        Me.ButtonReadEEPROM.Text = "Read EEPROM"
        Me.ToolTip1.SetToolTip(Me.ButtonReadEEPROM, "Read values from Arduino.")
        '
        'ButtonWriteEEPROM
        '
        Me.ButtonWriteEEPROM.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ButtonWriteEEPROM.Cursor = System.Windows.Forms.Cursors.Default
        Me.ButtonWriteEEPROM.Location = New System.Drawing.Point(70, 3)
        Me.ButtonWriteEEPROM.Name = "ButtonWriteEEPROM"
        Me.ButtonWriteEEPROM.Size = New System.Drawing.Size(62, 34)
        Me.ButtonWriteEEPROM.TabIndex = 84
        Me.ButtonWriteEEPROM.Text = "Write EEPROM"
        Me.ToolTip1.SetToolTip(Me.ButtonWriteEEPROM, "Save values to ASCOM profile store and write values to the Arduino's EEPROM.")
        '
        'CheckBoxEEPROMForceWrite
        '
        Me.CheckBoxEEPROMForceWrite.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.CheckBoxEEPROMForceWrite.AutoSize = True
        Me.CheckBoxEEPROMForceWrite.Location = New System.Drawing.Point(3, 7)
        Me.CheckBoxEEPROMForceWrite.Name = "CheckBoxEEPROMForceWrite"
        Me.CheckBoxEEPROMForceWrite.Size = New System.Drawing.Size(127, 17)
        Me.CheckBoxEEPROMForceWrite.TabIndex = 86
        Me.CheckBoxEEPROMForceWrite.Text = "Force EEPROM write"
        Me.ToolTip1.SetToolTip(Me.CheckBoxEEPROMForceWrite, "When activated, 'Write EEPROM' sends all values to the Arduino instead of only ch" &
        "anged ones.")
        Me.CheckBoxEEPROMForceWrite.UseVisualStyleBackColor = True
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripSplitButton1, Me.ToolStripStatusLabel2, Me.ToolStripStatusLabel1})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 520)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(545, 22)
        Me.StatusStrip1.TabIndex = 93
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ToolStripSplitButton1
        '
        Me.ToolStripSplitButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripSplitButton1.Name = "ToolStripSplitButton1"
        Me.ToolStripSplitButton1.Size = New System.Drawing.Size(135, 20)
        Me.ToolStripSplitButton1.Text = "ToolStripSplitButton1"
        '
        'ToolStripStatusLabel2
        '
        Me.ToolStripStatusLabel2.Name = "ToolStripStatusLabel2"
        Me.ToolStripStatusLabel2.Size = New System.Drawing.Size(120, 17)
        Me.ToolStripStatusLabel2.Text = "ToolStripStatusLabel2"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(17, 17)
        Me.ToolStripStatusLabel1.Text = """"""
        '
        'TableLayoutPanel4
        '
        Me.TableLayoutPanel4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel4.ColumnCount = 2
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.Controls.Add(Me.ButtonExportSettings, 0, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.ButtonImportSettings, 1, 0)
        Me.TableLayoutPanel4.Location = New System.Drawing.Point(387, 453)
        Me.TableLayoutPanel4.Name = "TableLayoutPanel4"
        Me.TableLayoutPanel4.RowCount = 1
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.Size = New System.Drawing.Size(146, 29)
        Me.TableLayoutPanel4.TabIndex = 94
        '
        'ButtonExportSettings
        '
        Me.ButtonExportSettings.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.ButtonExportSettings.Location = New System.Drawing.Point(3, 3)
        Me.ButtonExportSettings.Name = "ButtonExportSettings"
        Me.ButtonExportSettings.Size = New System.Drawing.Size(67, 23)
        Me.ButtonExportSettings.TabIndex = 0
        Me.ButtonExportSettings.Text = "Export"
        Me.ToolTip1.SetToolTip(Me.ButtonExportSettings, "Export current settings to a .xml file")
        '
        'ButtonImportSettings
        '
        Me.ButtonImportSettings.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.ButtonImportSettings.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.ButtonImportSettings.Location = New System.Drawing.Point(76, 3)
        Me.ButtonImportSettings.Name = "ButtonImportSettings"
        Me.ButtonImportSettings.Size = New System.Drawing.Size(67, 23)
        Me.ButtonImportSettings.TabIndex = 1
        Me.ButtonImportSettings.Text = "Import"
        Me.ToolTip1.SetToolTip(Me.ButtonImportSettings, "Import settings from a .xml file")
        '
        'ToolTip1
        '
        Me.ToolTip1.AutoPopDelay = 50000
        Me.ToolTip1.InitialDelay = 500
        Me.ToolTip1.ReshowDelay = 100
        '
        'SetupDialogForm
        '
        Me.AcceptButton = Me.OK_Button
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.Cancel_Button
        Me.ClientSize = New System.Drawing.Size(545, 542)
        Me.Controls.Add(Me.TableLayoutPanel4)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.TableLayoutPanel3)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "SetupDialogForm"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "YAAAObservingConditions Setup"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TabControl1.ResumeLayout(False)
        Me.TabPageGeneral.ResumeLayout(False)
        Me.TabPageGeneral.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox23.ResumeLayout(False)
        Me.GroupBox23.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.GroupBox37.ResumeLayout(False)
        Me.GroupBox37.PerformLayout()
        Me.GroupBox36.ResumeLayout(False)
        Me.GroupBox36.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPageSensors.ResumeLayout(False)
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.TableLayoutPanel3.PerformLayout()
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.TableLayoutPanel4.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents OK_Button As System.Windows.Forms.Button
    Friend WithEvents Cancel_Button As System.Windows.Forms.Button
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents chkTrace As System.Windows.Forms.CheckBox
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPageGeneral As TabPage
    Friend WithEvents TabPageSensors As TabPage
    Friend WithEvents FlowLayoutPanelObservingConditions As FlowLayoutPanel
    Friend WithEvents TableLayoutPanel3 As TableLayoutPanel
    Friend WithEvents TableLayoutPanel2 As TableLayoutPanel
    Friend WithEvents ButtonReadEEPROM As Button
    Friend WithEvents ButtonWriteEEPROM As Button
    Friend WithEvents CheckBoxEEPROMForceWrite As CheckBox
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents TableLayoutPanel4 As TableLayoutPanel
    Friend WithEvents ButtonExportSettings As Button
    Friend WithEvents ButtonImportSettings As Button
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents TextBoxDefAveragePeriod As TextBox
    Private WithEvents Label1 As Label
    Friend WithEvents GroupBox23 As GroupBox
    Friend WithEvents Panel1 As Panel
    Friend WithEvents RadioButtonNetwork As RadioButton
    Friend WithEvents RadioButtonSerial As RadioButton
    Friend WithEvents Label44 As Label
    Friend WithEvents GroupBox37 As GroupBox
    Friend WithEvents TextBoxPort As TextBox
    Private WithEvents Label60 As Label
    Friend WithEvents TextBoxIPAddress As TextBox
    Private WithEvents Label58 As Label
    Friend WithEvents GroupBox36 As GroupBox
    Private WithEvents Label4 As Label
    Private WithEvents Label3 As Label
    Friend WithEvents ComboBoxBaudRate As ComboBox
    Friend WithEvents ComboBoxComPorts As ComboBox
    Friend WithEvents ToolStripStatusLabel1 As ToolStripStatusLabel
    Friend WithEvents ToolStripSplitButton1 As ToolStripSplitButton
    Friend WithEvents ToolStripStatusLabel2 As ToolStripStatusLabel
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents CheckBoxLogSupport As CheckBox
    Friend WithEvents ComboBoxLogDelimiter As ComboBox
    Friend WithEvents TextBoxLogInterval As TextBox
    Private WithEvents Label6 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents CheckBoxLogModeDefault As CheckBox
    Friend WithEvents ToolTip1 As ToolTip
End Class
