﻿Public Class UserControlObservingConditionSensor

    Private condition_ As ObservingConditions.condition_t

    'Private sensor_ As YAAASharedClassLibrary.YAAASensor = Nothing
    Private sensor_configuration_ As ObservingConditions.SensorConfiguration = Nothing

    Private control_ As YAAASensorUserControlLibrary.IUserControlSensor = Nothing

    Private sensor_device_ As Byte
    Private sensor_device_type_ As System.Type = Nothing

    Private sensor_unit_type_ As System.Type = Nothing


    'Public Sub New(condition As ObservingConditions.condition_t, sensor As YAAASharedClassLibrary.YAAASensor)
    Public Sub New(condition As ObservingConditions.condition_t, sensor_configuration As ObservingConditions.SensorConfiguration)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        condition_ = condition

        sensor_configuration_ = sensor_configuration.Clone()

        Select Case condition_
            Case ObservingConditions.condition_t.CONDITION_CLOUD_COVER
                sensor_device_type_ = GetType(YAAASharedClassLibrary.YAAASensor.sensor_cloud_coverage_t)
                sensor_unit_type_ = GetType(YAAASharedClassLibrary.YAAASensor.unit_cloud_cover_t)
                GroupBox1.Text = "Cloud Cover"
                If sensor_configuration_.sensor_.sensor_settings_.sensor_type_ = YAAASharedClassLibrary.YAAASensor.sensor_type_t.SENSOR_CLOUD_COVER Then
                    CheckBoxConditionEnabled.Checked = True
                Else
                    CheckBoxConditionEnabled.Checked = False
                End If
            Case ObservingConditions.condition_t.CONDITION_DEW_POINT
                GroupBox1.Text = "Dew Point"
                sensor_device_type_ = GetType(YAAASharedClassLibrary.YAAASensor.sensor_dew_point_t)
                sensor_unit_type_ = GetType(YAAASharedClassLibrary.YAAASensor.unit_dew_point_t)
                If sensor_configuration_.sensor_.sensor_settings_.sensor_type_ = YAAASharedClassLibrary.YAAASensor.sensor_type_t.SENSOR_DEW_POINT Then
                    CheckBoxConditionEnabled.Checked = True
                Else
                    CheckBoxConditionEnabled.Checked = False
                End If
            Case ObservingConditions.condition_t.CONDITION_HUMIDITY
                GroupBox1.Text = "Humidity"
                sensor_device_type_ = GetType(YAAASharedClassLibrary.YAAASensor.sensor_humidity_device_t)
                sensor_unit_type_ = GetType(YAAASharedClassLibrary.YAAASensor.unit_humidity_t)
                If sensor_configuration_.sensor_.sensor_settings_.sensor_type_ = YAAASharedClassLibrary.YAAASensor.sensor_type_t.SENSOR_HUMIDITY Then
                    CheckBoxConditionEnabled.Checked = True
                Else
                    CheckBoxConditionEnabled.Checked = False
                End If
            Case ObservingConditions.condition_t.CONDITION_PRESSURE
                GroupBox1.Text = "Pressure"
                sensor_device_type_ = GetType(YAAASharedClassLibrary.YAAASensor.sensor_pressure_device_t)
                sensor_unit_type_ = GetType(YAAASharedClassLibrary.YAAASensor.unit_pressure_t)
                If sensor_configuration_.sensor_.sensor_settings_.sensor_type_ = YAAASharedClassLibrary.YAAASensor.sensor_type_t.SENSOR_PRESSURE Then
                    CheckBoxConditionEnabled.Checked = True
                Else
                    CheckBoxConditionEnabled.Checked = False
                End If
            Case ObservingConditions.condition_t.CONDITION_RAIN
                GroupBox1.Text = "Rain"
                sensor_device_type_ = GetType(YAAASharedClassLibrary.YAAASensor.sensor_rain_device_t)
                sensor_unit_type_ = GetType(YAAASharedClassLibrary.YAAASensor.unit_rain_t)
                If sensor_configuration_.sensor_.sensor_settings_.sensor_type_ = YAAASharedClassLibrary.YAAASensor.sensor_type_t.SENSOR_RAIN Then
                    CheckBoxConditionEnabled.Checked = True
                Else
                    CheckBoxConditionEnabled.Checked = False
                End If
            Case ObservingConditions.condition_t.CONDITION_RAIN_RATE
                GroupBox1.Text = "Rain Rate"
                sensor_device_type_ = GetType(YAAASharedClassLibrary.YAAASensor.sensor_rain_rate_device_t)
                sensor_unit_type_ = GetType(YAAASharedClassLibrary.YAAASensor.unit_rain_rate_t)
                If sensor_configuration_.sensor_.sensor_settings_.sensor_type_ = YAAASharedClassLibrary.YAAASensor.sensor_type_t.SENSOR_RAIN_RATE Then
                    CheckBoxConditionEnabled.Checked = True
                Else
                    CheckBoxConditionEnabled.Checked = False
                End If
            Case ObservingConditions.condition_t.CONDITION_SKY_BRIGHTNESS
                GroupBox1.Text = "Sky Brightness"
                sensor_device_type_ = GetType(YAAASharedClassLibrary.YAAASensor.sensor_brightness_device_t)
                sensor_unit_type_ = GetType(YAAASharedClassLibrary.YAAASensor.unit_brightness_t)
                If sensor_configuration_.sensor_.sensor_settings_.sensor_type_ = YAAASharedClassLibrary.YAAASensor.sensor_type_t.SENSOR_BRIGHTNESS Then
                    CheckBoxConditionEnabled.Checked = True
                Else
                    CheckBoxConditionEnabled.Checked = False
                End If
            Case ObservingConditions.condition_t.CONDITION_SKY_QUALITY
                GroupBox1.Text = "Sky Quality"
                sensor_device_type_ = GetType(YAAASharedClassLibrary.YAAASensor.sensor_sky_quality_t)
                sensor_unit_type_ = GetType(YAAASharedClassLibrary.YAAASensor.unit_sky_quality_t)
                If sensor_configuration_.sensor_.sensor_settings_.sensor_type_ = YAAASharedClassLibrary.YAAASensor.sensor_type_t.SENSOR_SKY_QUALITY Then
                    CheckBoxConditionEnabled.Checked = True
                Else
                    CheckBoxConditionEnabled.Checked = False
                End If
            Case ObservingConditions.condition_t.CONDITION_SKY_TEMPERATURE
                GroupBox1.Text = "Sky Temperature"
                sensor_device_type_ = GetType(YAAASharedClassLibrary.YAAASensor.sensor_temperature_device_t)
                sensor_unit_type_ = GetType(YAAASharedClassLibrary.YAAASensor.unit_temperature_t)
                If sensor_configuration_.sensor_.sensor_settings_.sensor_type_ = YAAASharedClassLibrary.YAAASensor.sensor_type_t.SENSOR_TEMPERATURE Then
                    CheckBoxConditionEnabled.Checked = True
                Else
                    CheckBoxConditionEnabled.Checked = False
                End If
            Case ObservingConditions.condition_t.CONDITION_STAR_FWHM
                GroupBox1.Text = "Star FWHM"
                sensor_device_type_ = GetType(YAAASharedClassLibrary.YAAASensor.sensor_star_fwhm_t)
                sensor_unit_type_ = GetType(YAAASharedClassLibrary.YAAASensor.unit_star_fwhm_t)
                If sensor_configuration_.sensor_.sensor_settings_.sensor_type_ = YAAASharedClassLibrary.YAAASensor.sensor_type_t.SENSOR_STAR_FWHM Then
                    CheckBoxConditionEnabled.Checked = True
                Else
                    CheckBoxConditionEnabled.Checked = False
                End If
            Case ObservingConditions.condition_t.CONDITION_TEMPERATURE
                GroupBox1.Text = "Temperature"
                sensor_device_type_ = GetType(YAAASharedClassLibrary.YAAASensor.sensor_temperature_device_t)
                sensor_unit_type_ = GetType(YAAASharedClassLibrary.YAAASensor.unit_temperature_t)
                If sensor_configuration_.sensor_.sensor_settings_.sensor_type_ = YAAASharedClassLibrary.YAAASensor.sensor_type_t.SENSOR_TEMPERATURE Then
                    CheckBoxConditionEnabled.Checked = True
                Else
                    CheckBoxConditionEnabled.Checked = False
                End If
            Case ObservingConditions.condition_t.CONDITION_WIND_DIRECTION
                GroupBox1.Text = "Wind Direction"
                sensor_device_type_ = GetType(YAAASharedClassLibrary.YAAASensor.sensor_wind_direction_t)
                sensor_unit_type_ = GetType(YAAASharedClassLibrary.YAAASensor.unit_wind_direction_t)
                If sensor_configuration_.sensor_.sensor_settings_.sensor_type_ = YAAASharedClassLibrary.YAAASensor.sensor_type_t.SENSOR_WIND_DIRECTION Then
                    CheckBoxConditionEnabled.Checked = True
                Else
                    CheckBoxConditionEnabled.Checked = False
                End If
            Case ObservingConditions.condition_t.CONDITION_WIND_GUST
                GroupBox1.Text = "Wind Gust"
                sensor_device_type_ = GetType(YAAASharedClassLibrary.YAAASensor.sensor_wind_gust_t)
                sensor_unit_type_ = GetType(YAAASharedClassLibrary.YAAASensor.unit_wind_speed_t)
                If sensor_configuration_.sensor_.sensor_settings_.sensor_type_ = YAAASharedClassLibrary.YAAASensor.sensor_type_t.SENSOR_WIND_GUST Then
                    CheckBoxConditionEnabled.Checked = True
                Else
                    CheckBoxConditionEnabled.Checked = False
                End If
            Case ObservingConditions.condition_t.CONDITION_WIND_SPEED
                GroupBox1.Text = "Wind Speed"
                sensor_device_type_ = GetType(YAAASharedClassLibrary.YAAASensor.sensor_wind_speed_t)
                sensor_unit_type_ = GetType(YAAASharedClassLibrary.YAAASensor.unit_wind_speed_t)
                If sensor_configuration_.sensor_.sensor_settings_.sensor_type_ = YAAASharedClassLibrary.YAAASensor.sensor_type_t.SENSOR_WIND_SPEED Then
                    CheckBoxConditionEnabled.Checked = True
                Else
                    CheckBoxConditionEnabled.Checked = False
                End If
            Case ObservingConditions.condition_t.CONDITION_LIGHTNING
                GroupBox1.Text = "Lightning"
                sensor_device_type_ = GetType(YAAASharedClassLibrary.YAAASensor.sensor_lightning_t)
                sensor_unit_type_ = GetType(YAAASharedClassLibrary.YAAASensor.unit_lightning_t)
                If sensor_configuration_.sensor_.sensor_settings_.sensor_type_ = YAAASharedClassLibrary.YAAASensor.sensor_type_t.SENSOR_LIGHTNING Then
                    CheckBoxConditionEnabled.Checked = True
                Else
                    CheckBoxConditionEnabled.Checked = False
                End If
            Case Else
                GroupBox1.Text = "Unknown Observing Condition: " & condition_.ToString
        End Select

        'if a valid sensor type was selected, add devices for this type to the sensor device drop down list
        If sensor_device_type_ Is Nothing Then
            ComboBoxSensorDevice.Items.Add("DEVICE_UNKNOWN")
        Else
            ComboBoxSensorDevice.Items.AddRange([Enum].GetNames(sensor_device_type_))

            If [Enum].IsDefined(sensor_device_type_, CInt(sensor_configuration_.sensor_.sensor_settings_.sensor_device_)) Then
                ComboBoxSensorDevice.SelectedItem = [Enum].Parse(sensor_device_type_, sensor_configuration_.sensor_.sensor_settings_.sensor_device_).ToString
            End If
        End If

        'if a valid sensor unit was selected, add devices for this type to the sensor device drop down list
        If sensor_unit_type_ Is Nothing Then
            ComboBoxSensorUnit.Items.Add("UNIT_UNKNOWN")
        Else
            ComboBoxSensorUnit.Items.AddRange([Enum].GetNames(sensor_unit_type_))

            If [Enum].IsDefined(sensor_unit_type_, CInt(sensor_configuration_.sensor_.sensor_settings_.sensor_unit_)) Then
                ComboBoxSensorUnit.SelectedItem = [Enum].Parse(sensor_unit_type_, sensor_configuration_.sensor_.sensor_settings_.sensor_unit_).ToString
            Else
                ComboBoxSensorUnit.SelectedIndex = 0
            End If
        End If

        'if no valid sensor was selected or the sensor type stored in sensor_.sensor_settings_.sensor_device_ does not correspond to
        'any sensor devices known for this sensor type, select the first item of the drop down list.
        If ComboBoxSensorDevice.SelectedIndex < 0 Then
            ComboBoxSensorDevice.SelectedItem = "DEVICE_UNKNOWN"
        End If

        If ComboBoxSensorUnit.SelectedIndex < 0 Then
            ComboBoxSensorUnit.SelectedItem = "UNIT_UNKNOWN"
        End If

        TextBoxAverageSamples.Text = sensor_configuration_.average_samples_
        TextBoxAMI.Text = sensor_configuration_.automatic_measurement_interval_
    End Sub

    '@return a ObservingCondition.SensorConfiguration object containing the sensor configuration for this observing condition sensor.
    Friend Function getSensor(ByRef sensor_configuration As ObservingConditions.SensorConfiguration) As Boolean
        Dim valid As Boolean = True

        'get sensor settings from user control if it exists
        If Not control_ Is Nothing Then
                valid = valid And control_.getSensor(sensor_configuration_.sensor_)
            End If

            'update sensor device and unit
            If Not sensor_device_type_ Is Nothing Then
                sensor_configuration_.sensor_.sensor_settings_.sensor_device_ = [Enum].Parse(sensor_device_type_, ComboBoxSensorDevice.SelectedItem.ToString)
            End If

            If Not sensor_unit_type_ Is Nothing Then
                sensor_configuration_.sensor_.sensor_settings_.sensor_unit_ = [Enum].Parse(sensor_unit_type_, ComboBoxSensorUnit.SelectedItem.ToString)
            End If

        'if the enabled checkbox is checked, set the sensor type according to the observing condition
        'weird, I know, but I don't have the time right now to update all to a sensible solution
        If CheckBoxConditionEnabled.Checked Then
            Select Case condition_
                Case ObservingConditions.condition_t.CONDITION_CLOUD_COVER
                    sensor_configuration_.sensor_.sensor_settings_.sensor_type_ = YAAASharedClassLibrary.YAAASensor.sensor_type_t.SENSOR_CLOUD_COVER
                Case ObservingConditions.condition_t.CONDITION_DEW_POINT
                    sensor_configuration_.sensor_.sensor_settings_.sensor_type_ = YAAASharedClassLibrary.YAAASensor.sensor_type_t.SENSOR_DEW_POINT
                Case ObservingConditions.condition_t.CONDITION_HUMIDITY
                    sensor_configuration_.sensor_.sensor_settings_.sensor_type_ = YAAASharedClassLibrary.YAAASensor.sensor_type_t.SENSOR_HUMIDITY
                Case ObservingConditions.condition_t.CONDITION_PRESSURE
                    sensor_configuration_.sensor_.sensor_settings_.sensor_type_ = YAAASharedClassLibrary.YAAASensor.sensor_type_t.SENSOR_PRESSURE
                Case ObservingConditions.condition_t.CONDITION_RAIN
                    sensor_configuration_.sensor_.sensor_settings_.sensor_type_ = YAAASharedClassLibrary.YAAASensor.sensor_type_t.SENSOR_RAIN
                Case ObservingConditions.condition_t.CONDITION_RAIN_RATE
                    sensor_configuration_.sensor_.sensor_settings_.sensor_type_ = YAAASharedClassLibrary.YAAASensor.sensor_type_t.SENSOR_RAIN_RATE
                Case ObservingConditions.condition_t.CONDITION_SKY_BRIGHTNESS
                    sensor_configuration_.sensor_.sensor_settings_.sensor_type_ = YAAASharedClassLibrary.YAAASensor.sensor_type_t.SENSOR_BRIGHTNESS
                Case ObservingConditions.condition_t.CONDITION_SKY_QUALITY
                    sensor_configuration_.sensor_.sensor_settings_.sensor_type_ = YAAASharedClassLibrary.YAAASensor.sensor_type_t.SENSOR_SKY_QUALITY
                Case ObservingConditions.condition_t.CONDITION_SKY_TEMPERATURE
                    sensor_configuration_.sensor_.sensor_settings_.sensor_type_ = YAAASharedClassLibrary.YAAASensor.sensor_type_t.SENSOR_TEMPERATURE
                Case ObservingConditions.condition_t.CONDITION_STAR_FWHM
                    sensor_configuration_.sensor_.sensor_settings_.sensor_type_ = YAAASharedClassLibrary.YAAASensor.sensor_type_t.SENSOR_STAR_FWHM
                Case ObservingConditions.condition_t.CONDITION_TEMPERATURE
                    sensor_configuration_.sensor_.sensor_settings_.sensor_type_ = YAAASharedClassLibrary.YAAASensor.sensor_type_t.SENSOR_TEMPERATURE
                Case ObservingConditions.condition_t.CONDITION_WIND_DIRECTION
                    sensor_configuration_.sensor_.sensor_settings_.sensor_type_ = YAAASharedClassLibrary.YAAASensor.sensor_type_t.SENSOR_WIND_DIRECTION
                Case ObservingConditions.condition_t.CONDITION_WIND_GUST
                    sensor_configuration_.sensor_.sensor_settings_.sensor_type_ = YAAASharedClassLibrary.YAAASensor.sensor_type_t.SENSOR_WIND_GUST
                Case ObservingConditions.condition_t.CONDITION_WIND_SPEED
                    sensor_configuration_.sensor_.sensor_settings_.sensor_type_ = YAAASharedClassLibrary.YAAASensor.sensor_type_t.SENSOR_WIND_SPEED
                Case ObservingConditions.condition_t.CONDITION_LIGHTNING
                    sensor_configuration_.sensor_.sensor_settings_.sensor_type_ = YAAASharedClassLibrary.YAAASensor.sensor_type_t.SENSOR_LIGHTNING
                Case Else
                    'do nothing
            End Select
        Else
            'setting to SENSOR_UNKNOWN disables the sensor for this observing condition
            sensor_configuration_.sensor_.sensor_settings_.sensor_type_ = YAAASharedClassLibrary.YAAASensor.sensor_type_t.SENSOR_UNKNOWN
        End If

        valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxAMI, sensor_configuration_.automatic_measurement_interval_)
        valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxAverageSamples, sensor_configuration_.average_samples_)

        sensor_configuration = sensor_configuration_.Clone()

        Return valid
    End Function

    Private Sub UpdateUserControl()
        FlowLayoutPanel1.Controls.Clear()

        control_ = Nothing

        With sensor_configuration_.sensor_
            Select Case condition_
                Case ObservingConditions.condition_t.CONDITION_DEW_POINT
                Case ObservingConditions.condition_t.CONDITION_HUMIDITY
                    Select Case .sensor_settings_.sensor_device_
                        Case YAAASharedClassLibrary.YAAASensor.sensor_humidity_device_t.DEVICE_DHT
                            control_ = New YAAASensorUserControlLibrary.UserControlSensorDHT()
                        Case YAAASharedClassLibrary.YAAASensor.sensor_humidity_device_t.DEVICE_HTU21
                            control_ = New YAAASensorUserControlLibrary.UserControlSensorHTU21()
                        Case YAAASharedClassLibrary.YAAASensor.sensor_humidity_device_t.DEVICE_BMx280
                            control_ = New YAAASensorUserControlLibrary.UserControlSensorBMx280()
                    End Select
                Case ObservingConditions.condition_t.CONDITION_PRESSURE
                    Select Case .sensor_settings_.sensor_device_
                        Case YAAASharedClassLibrary.YAAASensor.sensor_pressure_device_t.DEVICE_BMP180
                            control_ = New YAAASensorUserControlLibrary.UserControlSensorBMP180()
                        Case YAAASharedClassLibrary.YAAASensor.sensor_pressure_device_t.DEVICE_BMx280
                            control_ = New YAAASensorUserControlLibrary.UserControlSensorBMx280()
                    End Select
                Case ObservingConditions.condition_t.CONDITION_RAIN
                    Select Case .sensor_settings_.sensor_device_
                        Case YAAASharedClassLibrary.YAAASensor.sensor_rain_device_t.DEVICE_FC37
                            control_ = New YAAASensorUserControlLibrary.UserControlSensorRainFC37()
                    End Select
                Case ObservingConditions.condition_t.CONDITION_RAIN_RATE
                    Select Case .sensor_settings_.sensor_device_
                        Case YAAASharedClassLibrary.YAAASensor.sensor_rain_rate_device_t.DEVICE_TIPPING_BUCKET
                            control_ = New YAAASensorUserControlLibrary.UserControlSensorRainRateTippingBucket()
                    End Select
                Case ObservingConditions.condition_t.CONDITION_SKY_BRIGHTNESS
                    Select Case .sensor_settings_.sensor_device_
                        Case YAAASharedClassLibrary.YAAASensor.sensor_brightness_device_t.DEVICE_BH1750
                            control_ = New YAAASensorUserControlLibrary.UserControlSensorBrightnessBH1750()
                    End Select
                Case ObservingConditions.condition_t.CONDITION_SKY_QUALITY
                Case ObservingConditions.condition_t.CONDITION_SKY_TEMPERATURE
                    Select Case .sensor_settings_.sensor_device_
                        Case YAAASharedClassLibrary.YAAASensor.sensor_temperature_device_t.DEVICE_ANALOG_IC
                            control_ = New YAAASensorUserControlLibrary.UserControlSensorTemperatureAnalogIC()
                        Case YAAASharedClassLibrary.YAAASensor.sensor_temperature_device_t.DEVICE_THERMISTOR
                            control_ = New YAAASensorUserControlLibrary.UserControlSensorTemperatureThermistor()
                        Case YAAASharedClassLibrary.YAAASensor.sensor_temperature_device_t.DEVICE_ONEWIRE_DS18
                            control_ = New YAAASensorUserControlLibrary.UserControlSensorTemperatureOneWireDS18()
                        Case YAAASharedClassLibrary.YAAASensor.sensor_temperature_device_t.DEVICE_MLX90614
                            control_ = New YAAASensorUserControlLibrary.UserControlSensorTemperatureMLX90614()
                        Case YAAASharedClassLibrary.YAAASensor.sensor_temperature_device_t.DEVICE_BMP180
                            control_ = New YAAASensorUserControlLibrary.UserControlSensorBMP180()
                        Case YAAASharedClassLibrary.YAAASensor.sensor_temperature_device_t.DEVICE_BMx280
                            control_ = New YAAASensorUserControlLibrary.UserControlSensorBMx280()
                        Case YAAASharedClassLibrary.YAAASensor.sensor_temperature_device_t.DEVICE_HTU21
                            control_ = New YAAASensorUserControlLibrary.UserControlSensorHTU21()
                    End Select
                Case ObservingConditions.condition_t.CONDITION_TEMPERATURE
                    Select Case .sensor_settings_.sensor_device_
                        Case YAAASharedClassLibrary.YAAASensor.sensor_temperature_device_t.DEVICE_ANALOG_IC
                            control_ = New YAAASensorUserControlLibrary.UserControlSensorTemperatureAnalogIC()
                        Case YAAASharedClassLibrary.YAAASensor.sensor_temperature_device_t.DEVICE_THERMISTOR
                            control_ = New YAAASensorUserControlLibrary.UserControlSensorTemperatureThermistor()
                        Case YAAASharedClassLibrary.YAAASensor.sensor_temperature_device_t.DEVICE_ONEWIRE_DS18
                            control_ = New YAAASensorUserControlLibrary.UserControlSensorTemperatureOneWireDS18()
                        Case YAAASharedClassLibrary.YAAASensor.sensor_temperature_device_t.DEVICE_MLX90614
                            control_ = New YAAASensorUserControlLibrary.UserControlSensorTemperatureMLX90614()
                        Case YAAASharedClassLibrary.YAAASensor.sensor_temperature_device_t.DEVICE_BMP180
                            control_ = New YAAASensorUserControlLibrary.UserControlSensorBMP180()
                        Case YAAASharedClassLibrary.YAAASensor.sensor_temperature_device_t.DEVICE_BMx280
                            control_ = New YAAASensorUserControlLibrary.UserControlSensorBMx280()
                        Case YAAASharedClassLibrary.YAAASensor.sensor_temperature_device_t.DEVICE_HTU21
                            control_ = New YAAASensorUserControlLibrary.UserControlSensorHTU21()
                    End Select
                Case ObservingConditions.condition_t.CONDITION_STAR_FWHM
                Case ObservingConditions.condition_t.CONDITION_WIND_DIRECTION
                    Select Case .sensor_settings_.sensor_device_
                        Case YAAASharedClassLibrary.YAAASensor.sensor_wind_direction_t.DEVICE_POTENTIOMETER
                            control_ = New YAAASensorUserControlLibrary.UserControlSensorWindDirectionPotentiometer()
                        Case YAAASharedClassLibrary.YAAASensor.sensor_wind_direction_t.DEVICE_SWITCH_RESISTOR
                            control_ = New YAAASensorUserControlLibrary.UserControlSensorWindDirectionSwitchResistor()
                    End Select
                Case ObservingConditions.condition_t.CONDITION_WIND_GUST
                Case ObservingConditions.condition_t.CONDITION_WIND_SPEED
                    Select Case .sensor_settings_.sensor_device_
                        Case YAAASharedClassLibrary.YAAASensor.sensor_wind_speed_t.DEVICE_ANALOG_VOLTAGE
                            control_ = New YAAASensorUserControlLibrary.UserControlSensorWindSpeedAnalogVoltage()
                        Case YAAASharedClassLibrary.YAAASensor.sensor_wind_speed_t.DEVICE_IMPULSE
                            control_ = New YAAASensorUserControlLibrary.UserControlSensorWindSpeedImpulse()
                    End Select
                Case ObservingConditions.condition_t.CONDITION_LIGHTNING
                    Select Case .sensor_settings_.sensor_device_
                        Case YAAASharedClassLibrary.YAAASensor.sensor_wind_speed_t.DEVICE_IMPULSE
                            control_ = New YAAASensorUserControlLibrary.UserControlSensorLightningAS3935()
                    End Select
            End Select

        End With

        If Not control_ Is Nothing Then
            control_.setSensor(sensor_configuration_.sensor_)
            FlowLayoutPanel1.Controls.Add(control_)
        End If

    End Sub

    Private Sub ComboBoxSensorDevice_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBoxSensorDevice.SelectedIndexChanged
        SensorDeviceChanged()
    End Sub

    Private Sub SensorDeviceChanged()
        sensor_configuration_.sensor_.sensor_settings_.sensor_device_ = 255

        With sensor_configuration_.sensor_.sensor_settings_
            Select Case condition_
                Case ObservingConditions.condition_t.CONDITION_CLOUD_COVER
                    If Not [Enum].TryParse(Of YAAASharedClassLibrary.YAAASensor.sensor_cloud_coverage_t)(ComboBoxSensorDevice.SelectedItem.ToString, .sensor_device_) Then
                        .sensor_device_ = YAAASharedClassLibrary.YAAASensor.sensor_brightness_device_t.DEVICE_UNKNOWN
                    End If
                Case ObservingConditions.condition_t.CONDITION_DEW_POINT
                    If Not [Enum].TryParse(Of YAAASharedClassLibrary.YAAASensor.sensor_dew_point_t)(ComboBoxSensorDevice.SelectedItem.ToString, .sensor_device_) Then
                        .sensor_device_ = YAAASharedClassLibrary.YAAASensor.sensor_brightness_device_t.DEVICE_UNKNOWN
                    End If
                Case ObservingConditions.condition_t.CONDITION_HUMIDITY
                    If Not [Enum].TryParse(Of YAAASharedClassLibrary.YAAASensor.sensor_humidity_device_t)(ComboBoxSensorDevice.SelectedItem.ToString, .sensor_device_) Then
                        .sensor_device_ = YAAASharedClassLibrary.YAAASensor.sensor_brightness_device_t.DEVICE_UNKNOWN
                    End If
                Case ObservingConditions.condition_t.CONDITION_PRESSURE
                    If Not [Enum].TryParse(Of YAAASharedClassLibrary.YAAASensor.sensor_pressure_device_t)(ComboBoxSensorDevice.SelectedItem.ToString, .sensor_device_) Then
                        .sensor_device_ = YAAASharedClassLibrary.YAAASensor.sensor_brightness_device_t.DEVICE_UNKNOWN
                    End If
                Case ObservingConditions.condition_t.CONDITION_RAIN
                    If Not [Enum].TryParse(Of YAAASharedClassLibrary.YAAASensor.sensor_rain_device_t)(ComboBoxSensorDevice.SelectedItem.ToString, .sensor_device_) Then
                        .sensor_device_ = YAAASharedClassLibrary.YAAASensor.sensor_brightness_device_t.DEVICE_UNKNOWN
                    End If
                Case ObservingConditions.condition_t.CONDITION_RAIN_RATE
                    If Not [Enum].TryParse(Of YAAASharedClassLibrary.YAAASensor.sensor_rain_rate_device_t)(ComboBoxSensorDevice.SelectedItem.ToString, .sensor_device_) Then
                        .sensor_device_ = YAAASharedClassLibrary.YAAASensor.sensor_brightness_device_t.DEVICE_UNKNOWN
                    End If
                Case ObservingConditions.condition_t.CONDITION_SKY_BRIGHTNESS
                    If Not [Enum].TryParse(Of YAAASharedClassLibrary.YAAASensor.sensor_brightness_device_t)(ComboBoxSensorDevice.SelectedItem.ToString, .sensor_device_) Then
                        .sensor_device_ = YAAASharedClassLibrary.YAAASensor.sensor_brightness_device_t.DEVICE_UNKNOWN
                    End If
                Case ObservingConditions.condition_t.CONDITION_SKY_QUALITY
                    If Not [Enum].TryParse(Of YAAASharedClassLibrary.YAAASensor.sensor_sky_quality_t)(ComboBoxSensorDevice.SelectedItem.ToString, .sensor_device_) Then
                        .sensor_device_ = YAAASharedClassLibrary.YAAASensor.sensor_brightness_device_t.DEVICE_UNKNOWN
                    End If
                Case ObservingConditions.condition_t.CONDITION_SKY_TEMPERATURE
                    If Not [Enum].TryParse(Of YAAASharedClassLibrary.YAAASensor.sensor_temperature_device_t)(ComboBoxSensorDevice.SelectedItem.ToString, .sensor_device_) Then
                        .sensor_device_ = YAAASharedClassLibrary.YAAASensor.sensor_brightness_device_t.DEVICE_UNKNOWN
                    End If
                Case ObservingConditions.condition_t.CONDITION_STAR_FWHM
                    If Not [Enum].TryParse(Of YAAASharedClassLibrary.YAAASensor.sensor_star_fwhm_t)(ComboBoxSensorDevice.SelectedItem.ToString, .sensor_device_) Then
                        .sensor_device_ = YAAASharedClassLibrary.YAAASensor.sensor_brightness_device_t.DEVICE_UNKNOWN
                    End If
                Case ObservingConditions.condition_t.CONDITION_TEMPERATURE
                    If Not [Enum].TryParse(Of YAAASharedClassLibrary.YAAASensor.sensor_temperature_device_t)(ComboBoxSensorDevice.SelectedItem.ToString, .sensor_device_) Then
                        .sensor_device_ = YAAASharedClassLibrary.YAAASensor.sensor_brightness_device_t.DEVICE_UNKNOWN
                    End If
                Case ObservingConditions.condition_t.CONDITION_WIND_DIRECTION
                    If Not [Enum].TryParse(Of YAAASharedClassLibrary.YAAASensor.sensor_wind_direction_t)(ComboBoxSensorDevice.SelectedItem.ToString, .sensor_device_) Then
                        .sensor_device_ = YAAASharedClassLibrary.YAAASensor.sensor_brightness_device_t.DEVICE_UNKNOWN
                    End If
                Case ObservingConditions.condition_t.CONDITION_WIND_GUST
                    If Not [Enum].TryParse(Of YAAASharedClassLibrary.YAAASensor.sensor_wind_gust_t)(ComboBoxSensorDevice.SelectedItem.ToString, .sensor_device_) Then
                        .sensor_device_ = YAAASharedClassLibrary.YAAASensor.sensor_brightness_device_t.DEVICE_UNKNOWN
                    End If
                Case ObservingConditions.condition_t.CONDITION_WIND_SPEED
                    If Not [Enum].TryParse(Of YAAASharedClassLibrary.YAAASensor.sensor_wind_speed_t)(ComboBoxSensorDevice.SelectedItem.ToString, .sensor_device_) Then
                        .sensor_device_ = YAAASharedClassLibrary.YAAASensor.sensor_brightness_device_t.DEVICE_UNKNOWN
                    End If
                Case ObservingConditions.condition_t.CONDITION_LIGHTNING
                    If Not [Enum].TryParse(Of YAAASharedClassLibrary.YAAASensor.sensor_lightning_t)(ComboBoxSensorDevice.SelectedItem.ToString, .sensor_device_) Then
                        .sensor_device_ = YAAASharedClassLibrary.YAAASensor.sensor_brightness_device_t.DEVICE_UNKNOWN
                    End If
            End Select
        End With

        UpdateUserControl()
    End Sub
End Class
