Imports System.Windows.Forms
Imports System.Runtime.InteropServices
Imports ASCOM.Utilities
Imports ASCOM.YAAADevice

Imports System.ComponentModel

<ComVisible(False)>
Public Class SetupDialogForm

    Private observing_conditions_ As ObservingConditions

    'Private user_controls_() As UserControlSensor
    Private user_controls_() As UserControlObservingConditionSensor

    Public Sub New(o As ObservingConditions)
        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        observing_conditions_ = o

        'load available com ports into com port combobox
        ComboBoxComPorts.Items.AddRange(observing_conditions_.serial_connection_.AvailableCOMPorts)

        updateStatusStrip()
    End Sub

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click ' OK button event handler
        ' Persist new values of user settings to the ASCOM profile
        If Not writeProfileSettings() Then
            Exit Sub
        End If

        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click 'Cancel button event handler
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub ShowAscomWebPage(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox1.DoubleClick, PictureBox1.Click
        ' Click on ASCOM logo event handler
        Try
            System.Diagnostics.Process.Start("http://ascom-standards.org/")
        Catch noBrowser As System.ComponentModel.Win32Exception
            If noBrowser.ErrorCode = -2147467259 Then
                MessageBox.Show(noBrowser.Message)
            End If
        Catch other As System.Exception
            MessageBox.Show(other.Message)
        End Try
    End Sub

    Private Sub SetupDialogForm_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load ' Form load event handler
        ' Retrieve current values of user settings from the ASCOM Profile
        'InitUI()
        ReadProfileSettings()
    End Sub

    'Private Sub InitUI()
    '    chkTrace.Checked = ObservingConditions.traceState
    '    ' set the list of com ports to those that are currently available
    '    ComboBoxComPorts.Items.Clear()
    '    ComboBoxComPorts.Items.AddRange(System.IO.Ports.SerialPort.GetPortNames())       ' use System.IO because it's static
    '    ' select the current port if possible
    '    If ComboBoxComPorts.Items.Contains(ObservingConditions.comPort) Then
    '        ComboBoxComPorts.SelectedItem = ObservingConditions.comPort
    '    End If
    'End Sub

    Private Sub ReadProfileSettings()
        'ComboBoxComPorts.Items.Clear()
        'ComboBoxComPorts.Items.AddRange(System.IO.Ports.SerialPort.GetPortNames())       ' use System.IO because it's static
        '' select the current port if possible
        'If ComboBoxComPorts.Items.Contains(observing_conditions_..comPort) Then
        '    ComboBoxComPorts.SelectedItem = ObservingConditions.comPort
        'End If


        ' Retrieve current values of user settings from the driver
        setMaskToSettings(observing_conditions_.getDriverSettings())
    End Sub

    Private Function writeProfileSettings() As Boolean
        Dim success As Boolean = True

        ToolStripStatusLabel1.Text = "Writing Profile Settings"
        Application.DoEvents()

        Try
            'set driver settings to user settings in setup dialog input mask
            observing_conditions_.setDriverSettings(getSettingsFromMask())

            'save settings to ASCOM profile store
            observing_conditions_.WriteProfile()
        Catch ex As Exception
            'when an error occurred during reading the settings from the input fields, display an
            'error message and do not close the window.
            MsgBox("Invalid values detected, some settings have not been saved. Check all input fields with orange background and try again.", MsgBoxStyle.Critical)
            success = False
        End Try

        updateStatusStrip()

        Return success
    End Function

    Private Sub ButtonExportSettings_Click(sender As Object, e As EventArgs) Handles ButtonExportSettings.Click
        Dim SaveFileDialog1 As New SaveFileDialog
        SaveFileDialog1.AddExtension = True
        SaveFileDialog1.OverwritePrompt = True
        SaveFileDialog1.DefaultExt = "xml"
        SaveFileDialog1.Filter = YAAASharedClassLibrary.YAAASETTINGS_FILE_DIALOG_FILTER
        SaveFileDialog1.InitialDirectory = ObservingConditions.settingsStoreDirectory
        SaveFileDialog1.FileName = YAAASharedClassLibrary.YAAASETTINGS_FILE_DEFAULT

        If SaveFileDialog1.ShowDialog() = DialogResult.OK Then
            If SaveFileDialog1.FileName <> "" Then
                Try
                    observing_conditions_.xmlFromDriverSettings(SaveFileDialog1.FileName, getSettingsFromMask())
                Catch ex As Exception
                    MsgBox(ex.ToString, MsgBoxStyle.Critical, "error")
                End Try
            End If
        End If
    End Sub

    Private Sub ButtonImportSettings_Click(sender As Object, e As EventArgs) Handles ButtonImportSettings.Click
        Dim OpenFileDialog1 = New OpenFileDialog
        OpenFileDialog1.Filter = YAAASharedClassLibrary.YAAASETTINGS_FILE_DIALOG_FILTER
        OpenFileDialog1.InitialDirectory = ObservingConditions.settingsStoreDirectory
        OpenFileDialog1.RestoreDirectory = True

        If OpenFileDialog1.ShowDialog() = DialogResult.OK Then
            If OpenFileDialog1.FileName <> "" Then
                Dim settings As ObservingConditions.observingConditionsDriverSettings = New ObservingConditions.observingConditionsDriverSettings(True)

                Try
                    settings = observing_conditions_.xmlToDriverSettings(OpenFileDialog1.FileName)
                    setMaskToSettings(settings)
                Catch ex As Exception
                    MsgBox(ex.ToString, MsgBoxStyle.Critical, "error")
                End Try
            End If
        End If
    End Sub

    Private Sub ButtonReadEEPROM_Click(sender As Object, e As EventArgs) Handles ButtonReadEEPROM.Click
        Dim current_settings As ObservingConditions.observingConditionsDriverSettings = observing_conditions_.getDriverSettings()

        Try
            connect(True)

            If observing_conditions_.Connected Then
                ToolStripStatusLabel1.Text = "Reading from EEPROM..."
                Application.DoEvents()

                observing_conditions_.ReadEEPROM()

                setMaskToSettings(observing_conditions_.getDriverSettings(True))
            End If
        Catch ex As DriverException     'ASCOM.DriverAccess exceptions are returned by Connected Property
            Dim err_msg As String = ""
            err_msg &= "Connecting to YAAADevice failed. " & Environment.NewLine
            err_msg &= ex.Message & Environment.NewLine
            If Not ex.InnerException Is Nothing Then    'add inner exception message if available
                err_msg &= ex.InnerException.Message & Environment.NewLine
            End If

            MessageBox.Show(err_msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Catch ex As Exception           'catch any other exceptions just in case
            Dim err_msg As String = ""
            err_msg &= "Error occurred: " & Environment.NewLine
            err_msg &= ex.Message & Environment.NewLine
            If Not ex.InnerException Is Nothing Then    'add inner exception message if available
                err_msg &= ex.InnerException.Message & Environment.NewLine
            End If

            MessageBox.Show(err_msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

        updateStatusStrip()
    End Sub

    Private Sub ButtonWriteEEPROM_Click(sender As Object, e As EventArgs) Handles ButtonWriteEEPROM.Click
        'save values in ASCOM profile.
        If Not writeProfileSettings() Then
            Exit Sub
        End If

        observing_conditions_.EEPROM_force_write_ = CheckBoxEEPROMForceWrite.Checked

        Try
            connect(True)

            'if the YAAADevice is connected, try to write the EEPROM contents
            If observing_conditions_.Connected Then
                ToolStripStatusLabel1.Text = "Writing to EEPROM..."
                Application.DoEvents()

                observing_conditions_.WriteEEPROM()
            End If
        Catch ex As DriverException     'ASCOM.DriverAccess exceptions are returned by Connected Property
            Dim err_msg As String = ""
            err_msg &= "Connecting to YAAADevice failed. " & Environment.NewLine
            err_msg &= ex.Message & Environment.NewLine
            If Not ex.InnerException Is Nothing Then    'add inner exception message if available
                err_msg &= ex.InnerException.Message & Environment.NewLine
            End If

            MessageBox.Show(err_msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Catch ex As Exception           'other exceptions are returned by function ReadEEPROM()
            Dim err_msg As String = ""
            err_msg &= "Unknown error occurred: " & Environment.NewLine
            err_msg &= ex.Message & Environment.NewLine
            If Not ex.InnerException Is Nothing Then    'add inner exception message if available
                err_msg &= ex.InnerException.Message & Environment.NewLine
            End If

            MessageBox.Show(err_msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

        'reset Force EEPROM Write to default.
        observing_conditions_.EEPROM_force_write_ = False
        CheckBoxEEPROMForceWrite.Checked = False

        updateStatusStrip()
    End Sub

    Private Function getSettingsFromMask() As ObservingConditions.observingConditionsDriverSettings
        Dim valid As Boolean = True
        Dim return_value As ObservingConditions.observingConditionsDriverSettings = New ObservingConditions.observingConditionsDriverSettings(True)

        ObservingConditions.observingConditionsDriverSettings.trace_state_ = chkTrace.Checked

        'only store com port when a com port is selected.
        If Not ComboBoxComPorts.SelectedItem Is Nothing Then
            return_value.com_port_ = ComboBoxComPorts.SelectedItem.ToString               'com port the arduino is connected to.
        End If

        return_value.baud_rate_ = CInt(ComboBoxBaudRate.SelectedItem.ToString)            'baud rate.

        valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxIPAddress, return_value.ip_address_)
        valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxPort, return_value.port_)

        If RadioButtonSerial.Checked Then
            return_value.connection_type_ = ObservingConditions.connection_type_t.SERIAL
        End If
        If RadioButtonNetwork.Checked Then
            return_value.connection_type_ = ObservingConditions.connection_type_t.NETWORK
        End If

        With return_value.observing_conditions_settings_
            .log_csv_file_ = CheckBoxLogSupport.Checked
            .log_csv_enabled_ = CheckBoxLogModeDefault.Checked
            .log_csv_delimiter_ = getCSVDelimiterChar(ComboBoxLogDelimiter.SelectedItem.ToString)

            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxLogInterval, .log_csv_interval_)

            Dim average_period As Double = 0.0
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxDefAveragePeriod, average_period)        'read average period in hours

            'convert to ms
            .average_period_ = Convert.ToUInt32(average_period * 3600000.0)

            For i As Integer = 0 To .sensors_.Length - 1
                valid = valid And user_controls_(i).getSensor(.sensors_(i))
            Next
        End With

        If Not valid Then
            Throw New ASCOM.InvalidValueException()
        End If

        Return return_value
    End Function

    Private Sub setMaskToSettings(settings As ObservingConditions.observingConditionsDriverSettings)
        chkTrace.Checked = ObservingConditions.observingConditionsDriverSettings.trace_state_

        With settings
            Select Case .connection_type_
                Case ObservingConditions.connection_type_t.SERIAL
                    RadioButtonSerial.Checked = True
                Case ObservingConditions.connection_type_t.NETWORK
                    RadioButtonNetwork.Checked = True
            End Select

            If Not ComboBoxComPorts.Items.Contains(.com_port_) Then
                ComboBoxComPorts.Items.Add(.com_port_)
            End If

            ComboBoxComPorts.SelectedItem = .com_port_.ToString
            ComboBoxBaudRate.SelectedItem = .baud_rate_.ToString

            TextBoxIPAddress.Text = .ip_address_
            TextBoxPort.Text = .port_.ToString
        End With

        With settings.observing_conditions_settings_
            CheckBoxLogSupport.Checked = .log_csv_file_
            CheckBoxLogModeDefault.Checked = .log_csv_enabled_
            ComboBoxLogDelimiter.SelectedItem = getCSVDelimiterString(.log_csv_delimiter_)
            TextBoxLogInterval.Text = .log_csv_interval_

            TextBoxDefAveragePeriod.Text = CDbl(.average_period_) / 3600000.0
        End With

        'add observing conditions controls to flow layout panel
        addObservingConditionControls(settings)
    End Sub

    Private Sub connect(value As Boolean)
        Dim current_settings As ObservingConditions.observingConditionsDriverSettings = observing_conditions_.getDriverSettings()

        current_settings.com_port_ = ComboBoxComPorts.SelectedItem.ToString
        current_settings.baud_rate_ = ComboBoxBaudRate.SelectedItem.ToString

        current_settings.ip_address_ = TextBoxIPAddress.Text
        current_settings.port_ = TextBoxPort.Text

        If RadioButtonSerial.Checked Then
            current_settings.connection_type_ = ObservingConditions.connection_type_t.SERIAL
        End If
        If RadioButtonNetwork.Checked Then
            current_settings.connection_type_ = ObservingConditions.connection_type_t.NETWORK
        End If

        observing_conditions_.setDriverSettings(current_settings)

        Dim destination As String = ""

        If value Then
            Dim message As String = "Attempting to connect to: "
            If current_settings.connection_type_ = ObservingConditions.connection_type_t.SERIAL Then
                destination = current_settings.com_port_.ToString() & " | "
                destination &= current_settings.baud_rate_.ToString() & "b/s"
            Else
                destination = current_settings.ip_address_ & ":" & current_settings.port_
            End If

            ToolStripSplitButton1.Text = message & destination
            Application.DoEvents()

            'connect to the arduino and read values from the EEPROM.
            observing_conditions_.Connected = True

        Else
            ToolStripSplitButton1.Text = "Disconnecting..."
            Application.DoEvents()

            observing_conditions_.Connected = False
        End If

        If observing_conditions_.Connected Then
            ToolStripSplitButton1.Text = "Connected to: " & destination
        Else
            ToolStripSplitButton1.Text = "Disconnected"
        End If

        Application.DoEvents()
    End Sub

    Private Sub addObservingConditionControls(settings As ObservingConditions.observingConditionsDriverSettings)
        With FlowLayoutPanelObservingConditions
            '.AutoScroll = False
            .FlowDirection = FlowDirection.TopDown
            '.VerticalScroll.Enabled = True
            '.VerticalScroll.Visible = True
            .WrapContents = False
        End With

        If Not user_controls_ Is Nothing Then
            For Each user_control In user_controls_
                user_control.Dispose()
            Next
        End If

        ReDim user_controls_(14)        '15 observing conditions

        For i As Integer = 0 To user_controls_.Length - 1
            user_controls_(i) = New UserControlObservingConditionSensor([Enum].Parse(GetType(ObservingConditions.condition_t), i), settings.observing_conditions_settings_.sensors_(i)) 'getUserControl([Enum].Parse(GetType(ObservingConditions.condition_t), i), settings.observing_conditions_settings_.sensors_(i))

            FlowLayoutPanelObservingConditions.Controls.Add(user_controls_(i))
        Next
    End Sub

    Private Sub updateStatusStrip()
        Dim current_settings As ObservingConditions.observingConditionsDriverSettings = observing_conditions_.getDriverSettings()

        Dim destination As String = ""

        If observing_conditions_.Connected Then
            Dim message As String = "Connected to: "
            If current_settings.connection_type_ = ObservingConditions.connection_type_t.SERIAL Then
                destination = current_settings.com_port_.ToString() & " | "
                destination &= current_settings.baud_rate_.ToString() & "b/s"
            Else
                destination = current_settings.ip_address_ & ":" & current_settings.port_
            End If

            ToolStripSplitButton1.Text = message & destination
            ToolStripSplitButton1.DropDownItems.Clear()
            ToolStripSplitButton1.DropDownItems.Add("Disconnect")
        Else
            ToolStripSplitButton1.Text = "Disconnected"
            ToolStripSplitButton1.DropDownItems.Clear()
            ToolStripSplitButton1.DropDownItems.Add("Connect")
        End If

        ToolStripStatusLabel2.Text = ""
        ToolStripStatusLabel2.Spring = True

        ToolStripStatusLabel1.Alignment = ToolStripItemAlignment.Right
        ToolStripStatusLabel1.Text = "Ready"
    End Sub

    Private Function getCSVDelimiterChar(delimiter As String) As Char
        Select Case delimiter
            Case "comma"
                Return Chr(44)
            Case "colon"
                Return Chr(58)
            Case "semicolon"
                Return Chr(59)
            Case "tab"
                Return Chr(9)
            Case "space"
                Return Chr(32)
        End Select

        Return Chr(44)
    End Function

    Private Function getCSVDelimiterString(delimiter As Char) As String
        Select Case delimiter
            Case Chr(44)
                Return "comma"
            Case Chr(58)
                Return "colon"
            Case Chr(59)
                Return "semicolon"
            Case Chr(9)
                Return "tab"
            Case Chr(32)
                Return "space"
        End Select

        Return "comma"
    End Function

    Private Sub SetupDialogForm_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        connect(False)
    End Sub

    Private Sub ToolStripSplitButton1_DropDownItemClicked(sender As Object, e As ToolStripItemClickedEventArgs) Handles ToolStripSplitButton1.DropDownItemClicked
        Try
            connect(Not observing_conditions_.Connected)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

        updateStatusStrip()
    End Sub

End Class
