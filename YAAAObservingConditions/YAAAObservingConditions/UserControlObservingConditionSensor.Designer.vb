﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UserControlObservingConditionSensor
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.TextBoxAMI = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.ComboBoxSensorUnit = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.ComboBoxSensorDevice = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TextBoxAverageSamples = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.CheckBoxConditionEnabled = New System.Windows.Forms.CheckBox()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.CheckBoxConditionEnabled)
        Me.GroupBox1.Controls.Add(Me.TextBoxAMI)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.ComboBoxSensorUnit)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.ComboBoxSensorDevice)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.TextBoxAverageSamples)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.FlowLayoutPanel1)
        Me.GroupBox1.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(485, 171)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "GroupBox1"
        '
        'TextBoxAMI
        '
        Me.TextBoxAMI.Location = New System.Drawing.Point(131, 75)
        Me.TextBoxAMI.Name = "TextBoxAMI"
        Me.TextBoxAMI.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxAMI.TabIndex = 12
        Me.ToolTip1.SetToolTip(Me.TextBoxAMI, "Automatic measurement interval, in seconds. Should be smaller than (average perio" &
        "d) / (average samples). " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Set to 0 to disable automatic measurements and averagi" &
        "ng for this sensor.")
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(6, 78)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(153, 13)
        Me.Label3.TabIndex = 11
        Me.Label3.Text = "Automatic Mesurement Interval"
        '
        'ComboBoxSensorUnit
        '
        Me.ComboBoxSensorUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxSensorUnit.DropDownWidth = 200
        Me.ComboBoxSensorUnit.FormattingEnabled = True
        Me.ComboBoxSensorUnit.Location = New System.Drawing.Point(131, 46)
        Me.ComboBoxSensorUnit.Name = "ComboBoxSensorUnit"
        Me.ComboBoxSensorUnit.Size = New System.Drawing.Size(100, 21)
        Me.ComboBoxSensorUnit.TabIndex = 10
        Me.ToolTip1.SetToolTip(Me.ComboBoxSensorUnit, "Selectable Items depend on Sensor Type. ")
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(6, 49)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(62, 13)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Sensor Unit"
        '
        'ComboBoxSensorDevice
        '
        Me.ComboBoxSensorDevice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxSensorDevice.DropDownWidth = 200
        Me.ComboBoxSensorDevice.FormattingEnabled = True
        Me.ComboBoxSensorDevice.Location = New System.Drawing.Point(379, 19)
        Me.ComboBoxSensorDevice.Name = "ComboBoxSensorDevice"
        Me.ComboBoxSensorDevice.Size = New System.Drawing.Size(100, 21)
        Me.ComboBoxSensorDevice.TabIndex = 8
        Me.ToolTip1.SetToolTip(Me.ComboBoxSensorDevice, "Sensor Device." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Selectable items depend on Sensor Type. Select SENSOR_UNKOWN to d" &
        "isable sensor. ")
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(251, 22)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(77, 13)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Sensor Device"
        '
        'TextBoxAverageSamples
        '
        Me.TextBoxAverageSamples.Location = New System.Drawing.Point(379, 47)
        Me.TextBoxAverageSamples.Name = "TextBoxAverageSamples"
        Me.TextBoxAverageSamples.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxAverageSamples.TabIndex = 4
        Me.ToolTip1.SetToolTip(Me.TextBoxAverageSamples, "Number of samples recorded per average interval. Set to 0 to disable averaging fo" &
        "r this sensor")
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(254, 50)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(90, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Average Samples"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(6, 101)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(473, 65)
        Me.FlowLayoutPanel1.TabIndex = 0
        '
        'ToolTip1
        '
        Me.ToolTip1.AutoPopDelay = 50000
        Me.ToolTip1.InitialDelay = 500
        Me.ToolTip1.ReshowDelay = 100
        '
        'CheckBoxConditionEnabled
        '
        Me.CheckBoxConditionEnabled.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxConditionEnabled.Location = New System.Drawing.Point(9, 17)
        Me.CheckBoxConditionEnabled.Name = "CheckBoxConditionEnabled"
        Me.CheckBoxConditionEnabled.Size = New System.Drawing.Size(222, 24)
        Me.CheckBoxConditionEnabled.TabIndex = 13
        Me.CheckBoxConditionEnabled.Text = "Enabled"
        Me.CheckBoxConditionEnabled.UseVisualStyleBackColor = True
        '
        'UserControlObservingConditionSensor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "UserControlObservingConditionSensor"
        Me.Size = New System.Drawing.Size(488, 174)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents TextBoxAverageSamples As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents ComboBoxSensorDevice As ComboBox
    Friend WithEvents Label4 As Label
    Friend WithEvents ComboBoxSensorUnit As ComboBox
    Friend WithEvents Label5 As Label
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents TextBoxAMI As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents CheckBoxConditionEnabled As CheckBox
End Class
